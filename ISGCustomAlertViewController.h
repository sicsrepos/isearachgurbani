//
//  ISGCustomAlertViewController.h
//  iSearchGujral
//
//  Created by XLR8 on 23/10/17.
//  Copyright © 2017 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISGTheme.h"
#import "ISGAppDelegate.h"
#import <MessageUI/MessageUI.h>
#import "BookMarkPages.h"
@interface ISGCustomAlertViewController : UIViewController<MFMailComposeViewControllerDelegate,UIDocumentInteractionControllerDelegate>
{
    NSString *strShabd,*strId,*strPage,*sggsData,*script;

}
@property(nonatomic,retain) UIDocumentInteractionController *documentationInteractionController;

@property (nonatomic,strong) NSString *bani,*key,*rootClass;
@property (nonatomic,strong) NSMutableArray *arrayData,*arrayBaani;
@property (nonatomic) int arrayIndex;
@property (nonatomic,strong) NSString *strFavour;

@property (nonatomic, retain) NSData * cellImage;
@property (nonatomic, retain) NSString * index;
@property (nonatomic, retain) NSString * pageNo;
@property (nonatomic, retain) NSString * scriptID;
@property (nonatomic, retain) NSString * query;
@property (nonatomic, retain) NSString * heading;
@property (nonatomic, retain) UIImage * imageSMS;
@property (nonatomic, retain) NSData *imageData;

@end
