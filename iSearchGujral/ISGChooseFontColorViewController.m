//
//  ISGChooseFontColorViewController.m
//  iSearchGujral
//
//  Created by SICS on 10/12/14.
//  Copyright (c) 2014 sics. All rights reserved.
//

#import "ISGChooseFontColorViewController.h"
#import "ISGTheme.h"
#import "RSBrightnessSlider.h"
#import "RSOpacitySlider.h"
#import "RSSaturationSlider.h"
#import "RSHueSlider.h"


@interface ISGChooseFontColorViewController ()<UIGestureRecognizerDelegate>
{
    UILabel * labelPreview;
    ISGTheme *theme;
    CGFloat r, g, b, a;
    CGFloat height, width, gape;
}


@end

@implementation ISGChooseFontColorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if ([UIScreen mainScreen].bounds.size.height == 480)
    {
        width = [[UIScreen mainScreen]bounds].size.width*2.2/4;
        height = 12;
        gape = 12;
    }
    else
    {
        width = [[UIScreen mainScreen]bounds].size.width*2.2/4;
        height = width/9;
        gape = 20;
    }
    
    labelPreview = [[UILabel alloc] initWithFrame:CGRectMake(0, height+gape+5, [[UIScreen mainScreen] bounds].size.width, 40)];
    if ([UIScreen mainScreen].bounds.size.height > 568)
        labelPreview.center = CGPointMake([[UIScreen mainScreen] bounds].size.width/2, height+2*gape);
    else
        labelPreview.center = CGPointMake([[UIScreen mainScreen] bounds].size.width/2, height+gape+5);
    labelPreview.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:labelPreview];
    
    if ([_language isEqualToString:@"Gurmukhi"])
    {
        labelPreview.text=@"] jpu ]";
        [labelPreview setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:2*gape]];
        labelPreview.textColor = theme.gurmukhiColor;
    }
    else if ([_language isEqualToString:@"Roman"])
    {
        labelPreview.text=@"|| Jap ||";
        [labelPreview setFont:[UIFont boldSystemFontOfSize:gape+5]];
        labelPreview.textColor = theme.romanColor;
    }
    else if ([_language isEqualToString:@"Hindi"])
    {
        labelPreview.text=@"] jpu ]";
        [labelPreview setFont:[UIFont fontWithName:@"Gurbanihindi" size:2*gape]];
        labelPreview.textColor = theme.hindiColor;
    }
    else if ([_language isEqualToString:@"English"])
    {
        labelPreview.text=@"Chant And Meditate:";
        [labelPreview setFont:[UIFont boldSystemFontOfSize:gape+5]];
        labelPreview.textColor = theme.englishColor;
    }
    else if ([_language isEqualToString:@"Teeka"])
    {
        labelPreview.text=@"(bwxI dw nwm hY)";
        [labelPreview setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:2*gape]];
        labelPreview.textColor = theme.teekaColor;
    }
    else
    {
        labelPreview.text=@"jpu (m: 1) gurU gRMQ swihb - AMg 1 pN 3";
        [labelPreview setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:gape]];
        labelPreview.textColor = theme.attributeColor;
    }
    
    [self setcolorpickers];

}

- (void)setcolorpickers
{
    _colorPicker = [[RSColorPickerView alloc] initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width/4, CGRectGetMaxY(labelPreview.frame)+gape, [[UIScreen mainScreen]bounds].size.width/2, [[UIScreen mainScreen]bounds].size.width/2)];
    [_colorPicker setDelegate:self];
    _colorPicker.cropToCircle = YES;
    [self.view addSubview:_colorPicker];
    
    _brightnessSlider = [[RSBrightnessSlider alloc] initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width*3.8/4-width, CGRectGetMaxY(_colorPicker.frame)+gape, width, height)];
    [_brightnessSlider setColorPicker:_colorPicker];
    [self.view addSubview:_brightnessSlider];
    
    UILabel * brightness = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_brightnessSlider.frame)-width+60, _brightnessSlider.frame.origin.y, width-60, height)];
    brightness.text = @"Brightness";
    brightness.font = [UIFont systemFontOfSize:16.0f];
    brightness.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:brightness];
    
    _opacitySlider = [[RSOpacitySlider alloc] initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width*3.8/4-width, CGRectGetMaxY(_brightnessSlider.frame)+gape, width, height)];
    [_opacitySlider setColorPicker:_colorPicker];
    [self.view addSubview:_opacitySlider];
    
    UILabel * opacity = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_brightnessSlider.frame)-width+60, _opacitySlider.frame.origin.y, width-60, height)];
    opacity.text = @"Opacity";
    opacity.font = [UIFont systemFontOfSize:16.0f];
    opacity.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:opacity];
    
    _saturationSlider = [[RSSaturationSlider alloc] initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width*3.8/4-width, CGRectGetMaxY(_opacitySlider.frame)+gape, width, height)];
    [_saturationSlider setColorPicker:_colorPicker];
    [self.view addSubview:_saturationSlider];
    
    UILabel * saturation = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_brightnessSlider.frame)-width+60, _saturationSlider.frame.origin.y, width-60, height)];
    saturation.text = @"Saturation";
    saturation.font = [UIFont systemFontOfSize:16.0f];
    saturation.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:saturation];
    
    _hueSlider = [[RSHueSlider alloc] initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width*3.8/4-width, CGRectGetMaxY(_saturationSlider.frame)+gape, width, height)];
    [_hueSlider setColorPicker:_colorPicker];
    [self.view addSubview:_hueSlider];
    
    UILabel * hue = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_brightnessSlider.frame)-width+60, _hueSlider.frame.origin.y, width-60, height)];
    hue.text = @"Hue";
    hue.font = [UIFont systemFontOfSize:16.0f];
    hue.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:hue];
    
    UIButton * buttonSave = [[UIButton alloc] initWithFrame:CGRectMake(self.view.center.x - 50, CGRectGetMaxY(hue.frame) + gape, width/2, height+10)];
    buttonSave.center = CGPointMake([[UIScreen mainScreen]bounds].size.width/2, CGRectGetMaxY(_hueSlider.frame)+40);
    [buttonSave setTitle:@"Save" forState:UIControlStateNormal];
    [buttonSave setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonSave setBackgroundImage:[UIImage imageNamed:@"simpleButton.png"] forState:UIControlStateNormal];
    [buttonSave addTarget:self action:@selector(actionSave:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonSave];

}

#pragma mark - RSColorPickerView delegate methods

- (void)colorPickerDidChangeSelection:(RSColorPickerView *)cp {
    
    [[cp selectionColor] getRed:&r green:&g blue:&b alpha:&a];
    
    _brightnessSlider.value = [cp brightness];
    _opacitySlider.value = [cp opacity];
    _saturationSlider.value = [cp saturation];

    if (r != 1 || g != 1 || b!=1 ||a != 1)
    labelPreview.textColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
    //[self setFontColors];
    
}

- (void) actionSave:(UIButton *)sender
{
    [self setFontColors];
}

- (void)setFontColors
{
    if (r != 1 || g != 1 || b!=1 ||a != 1)
    {
        if ([_language isEqualToString:@"Gurmukhi"])
        {
            theme.gurmukhiColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
        }
        else if ([_language isEqualToString:@"Roman"])
        {
            theme.romanColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
        }
        else if ([_language isEqualToString:@"Hindi"])
        {
            theme.hindiColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
        }
        else if ([_language isEqualToString:@"English"])
        {
            theme.englishColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
        }
        else if ([_language isEqualToString:@"Teeka"])
        {
            theme.teekaColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
        }
        else
        {
            theme.attributeColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
        }
        _hueSlider.backgroundColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
        _saturationSlider.backgroundColor = [UIColor colorWithRed:r green:g blue:b alpha:a];

    }
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:theme] forKey:@"theme"];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"refresh" object:Nil];
    
    [self.view setNeedsDisplay];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
