//
//  BookMarkFolders.m
//  iSearchGujral
//
//  Created by SICS on 09/01/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "BookMarkFolders.h"
#import "BookMarkPages.h"
#import "ISGAppDelegate.h"

#define entity @"BookMarkFolders"

@implementation BookMarkFolders

@dynamic folderName;
@dynamic index;
@dynamic relationshipToPages;

+(void) addBookMarkFolderWithName:(NSString *)folderName index:(int)index
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    BookMarkFolders * bookMarkFolders = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:managedObjectContext];
    
    bookMarkFolders.folderName = folderName;
    bookMarkFolders.index = [NSString stringWithFormat:@"%d",index];
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
}

+(void) deleteBookMarkFolderWithName:(NSString *)folderName
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"folderName == %@",folderName];
    [fetchRequest setPredicate:predicate];
    
    NSArray * array = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
   
   [array enumerateObjectsUsingBlock:^(BookMarkFolders * folders, NSUInteger idx, BOOL *stop)
     {
         
         [managedObjectContext deleteObject:folders];
    }];
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
    
}

+(NSArray *) fetchAllBookMarkFolders
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    
    NSArray * array = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return array;
}

+(void) deleteAllBookMarkFolders
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    
    NSArray * array = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [array enumerateObjectsUsingBlock:^(BookMarkFolders * folders, NSUInteger idx, BOOL *stop)
     {
         [managedObjectContext deleteObject:folders];
    }];
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
}

+(void) updateBookMarkIndex:(NSArray *)arrayIndex andFolders:(NSArray *)arrayFolders 
{
    NSError * error = nil;
//  [self deleteAllBookMarkFolders];
    NSArray * array = [self fetchAllBookMarkFolders];
    
    for (int i=0; i<array.count-1; i++)
    {
        NSLog(@"%d", i);
        BookMarkFolders * bookMarkFolders;
        bookMarkFolders.folderName = arrayFolders[i];
        bookMarkFolders.index = arrayIndex[i];
    }
    
     
    
    

  /*test  [array enumerateObjectsUsingBlock:^(BookMarkFolders * bookMarkFolders, NSUInteger idx, BOOL *stop) {
        bookMarkFolders.folderName = arrayFolders[idx];
        bookMarkFolders.index = arrayIndex[idx];
    }];*/
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
//    
//    
//    for (int i =0; i<arrayIndex.count; i++)
//    {
//        BookMarkFolders * bookMarkFolders = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:managedObjectContext];
//        bookMarkFolders.folderName = arrayFolders[i];
//        bookMarkFolders.index = arrayIndex[i];
//    }
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
}

@end
