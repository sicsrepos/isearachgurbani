//
//  ISGSubChaptersViewController.h
//  iSearchGujral
//
//  Created by SICS on 11/12/14.
//  Copyright (c) 2014 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"

@interface ISGSubChaptersViewController : UIViewController
{
    IBOutlet UITableView * tableSubChapters;
    IBOutlet UIImageView *imageBg;
    NSString *stringBackgroud;
    int fontSize;
    NSString *fontStyle;
    FMDatabase *database;
    NSMutableArray *arrayItems;
}

@property (nonatomic) int from;
@property (nonatomic) int to;
@property (nonatomic, strong) NSString * titleName;

@end
