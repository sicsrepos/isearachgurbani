//
//  ISGFontColorViewController.m
//  iSearchGujral
//
//  Created by SICS on 10/12/14.
//  Copyright (c) 2014 sics. All rights reserved.
//

#import "ISGFontColorViewController.h"
#import "ISGChooseFontColorViewController.h"

@interface ISGFontColorViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSArray * arrayLanguages;
    IBOutlet UITableView * tableViewFontColor;
}

@end

@implementation ISGFontColorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    arrayLanguages = @[@"Gurmukhi",@"Roman",@"Hindi",@"English",@"Teeka",@"Attributes"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableView DataSource and Delegate
-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayLanguages count];
}
-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    float fontSize = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 26 : 16;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text=[arrayLanguages objectAtIndex:indexPath.row];
    cell.textLabel.font=[UIFont systemFontOfSize:fontSize];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor clearColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 60;
    }
    else
        return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    
    ISGChooseFontColorViewController * chooseColor  = [storyboard instantiateViewControllerWithIdentifier:@"ISGChooseFontColorViewController"];
    //ISGChooseFontColorViewController * chooseColor = [[ISGChooseFontColorViewController alloc]initWithNibName:@"ISGChooseFontColorViewController" bundle:Nil];
    chooseColor.language = arrayLanguages[indexPath.row];
    [self.navigationController pushViewController:chooseColor animated:YES];
}


@end
