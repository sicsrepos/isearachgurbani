//
//  ISGExploreViewController.m
//  iSearchGujral
//
//  Created by SICS on 25/11/14.
//  Copyright (c) 2014 sics. All rights reserved.
//

#import "ISGExploreViewController.h"
#import "ISGTheme.h"
#import "ISGListExploreViewController.h"
#import "ISGReadSGGSViewController.h"
#import "ISGShabdIndexViewController.h"

@interface ISGExploreViewController ()<UIGestureRecognizerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSString * message;
    CGFloat screenHeight;
    CGFloat screenWidth;
    
}

@end

@implementation ISGExploreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [tableExplore setBackgroundColor:[UIColor clearColor]];
    
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self setTheme];
    self.title=@"Explore";
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    screenHeight = screenSize.height;
    screenWidth = screenSize.width;
    NSLog(@"screen height %f",screenHeight);
   /* if ((screenHeight > 850) && (screenHeight < 1000)) {
        tableExplore.frame = CGRectMake(40, 60 ,240,450);
        
    }
    
    if(screenHeight==1024.000000)
    {
        tableExplore.frame = CGRectMake(40, 44, screenWidth-80,screenHeight-500);
    }
    else if(screenHeight==736)
    {
        tableExplore.frame = CGRectMake(40, 55, screenWidth-80,450);
        
    }
    else if(screenHeight==568)
    {
        tableExplore.frame = CGRectMake(40, 55, screenWidth-80,550);
        
    }
    else if(screenHeight==667)
    {
        tableExplore.frame = CGRectMake(40, 65, screenWidth-80, screenHeight-80);
    }
    if(screenHeight==1366.000)
    {
    
        tableExplore.frame = CGRectMake(40, 50, screenWidth-80,screenHeight-750);
    }
    tableExplore.center = self.view.center;*/

    exploreArray=@[@"SGGS Chapter index",@"SGGS Ang by Ang",@"SGGS Shabad index",@"Bhai Gurdas Vaar index",@"Sri Dasam Granth Index",@"Sri Dasam Granth Page by Page",@"Kabit by Kabit",@"Bhai Nand Lal Baani"];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"refresh" object:Nil];
    
  
    
//    UILongPressGestureRecognizer * longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureAction:)];
//    [longPressGesture setMinimumPressDuration:1.f];
//    [tableExplore addGestureRecognizer:longPressGesture];
    // Do any additional setup after loading the view from its nib.
}


-(void)refresh:(NSNotification *)notification
{
    [self setTheme];
    [tableExplore reloadData];
}

-(void)setTheme
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    ISGTheme *theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    stringBackgroud=theme.background;
    fontSize=[theme.fontSize intValue];
    if(fontSize==0)
    {
        fontSize=13;
    }
    if(stringBackgroud.length<=7)
    {
        stringBackgroud=@"LightPattern";
    }
    if (!theme.backgroundImage)
    {
        [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
    }
    else
        imageBg.image = theme.backgroundImage;
    
}
-(void)SetDefaultValues
{
    stringBackgroud=@"LightPattern";
    fontSize=12;
    //fontStyle=@"gurbaniwebthick";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
}

- (void) longPressGestureAction:(UILongPressGestureRecognizer *)longPressGesture
{
    if(longPressGesture.state != UIGestureRecognizerStateEnded)
    {
        UITableViewCell * cell = (UITableViewCell*)longPressGesture.view;
        [cell becomeFirstResponder];
        UIMenuController *menu = [UIMenuController sharedMenuController];
        [menu setTargetRect:cell.textLabel.frame inView:cell];
        message = cell.textLabel.text;
        UIMenuItem *item1 = [[UIMenuItem alloc]initWithTitle:@"Copy" action:@selector(copyMessage)];
        //UIMenuItem *item2 = [[UIMenuItem alloc]initWithTitle:@"Custom Option" action:@selector(customOptionSelected)];
        //[menu setMenuItems:@[item1, item2]];
        [menu setMenuItems:@[item1]];
        [menu setMenuVisible:YES animated:YES];
    }
}


- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    BOOL retValue = NO;
    retValue = [super canPerformAction:action withSender:sender];
    return retValue;
}


-(void)copyMessage
{
    UIPasteboard *clipBoard = [UIPasteboard generalPasteboard];
    [clipBoard setString:message];
}
-(void)customOptionSelected
{
    NSLog(@"Custom Menu Selected");
}



-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [exploreArray count];
}
-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(screenHeight==1024.000000)
    {
        cell.textLabel.font=[UIFont boldSystemFontOfSize:30];
    }
    else
        cell.textLabel.font=[UIFont boldSystemFontOfSize:fontSize];
    if([stringBackgroud isEqualToString:@"LightPattern"]||[stringBackgroud isEqualToString:@"WhitePattern"])
    {
        [cell.textLabel setTextColor:[UIColor blackColor]];
    }
    else
    {
        [cell.textLabel setTextColor:[UIColor whiteColor]];
    }
    
    
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.numberOfLines=1;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.text=[exploreArray objectAtIndex:indexPath.row];
    cell.textLabel.userInteractionEnabled = YES;
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UILongPressGestureRecognizer * longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureAction:)];
    [longPressGesture setMinimumPressDuration:1.0f];
    [cell addGestureRecognizer:longPressGesture];
    
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(screenHeight==1024.000000)
    {
        return 80;
    }
    else
        return 40;
}

-(BOOL)canBecomeFirstResponder
{
    return YES;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGListExploreViewController * listVC  = [storyboard instantiateViewControllerWithIdentifier:@"ISGListExploreViewController"];
                //ISGListExploreViewController * listVC =[[ISGListExploreViewController alloc]initWithNibName:@"ISGListExploreViewController" bundle:Nil];
                listVC.titleName = exploreArray[indexPath.row];
                [self.navigationController pushViewController:listVC animated:YES];
            }
            break;
            
        case 1:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGReadSGGSViewController * readSGGSVC  = [storyboard instantiateViewControllerWithIdentifier:@"ISGReadSGGSViewController"];
                //ISGReadSGGSViewController * readSGGSVC =[[ISGReadSGGSViewController alloc]initWithNibName:@"ISGReadSGGSViewController" bundle:Nil];
                readSGGSVC.stringCategory = exploreArray[indexPath.row];
                readSGGSVC.titleName = @"Sri Guru Granth Sahib";
                [self.navigationController pushViewController:readSGGSVC animated:YES];
            }
            break;
            
        case 2:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGShabdIndexViewController * shabdVC  = [storyboard instantiateViewControllerWithIdentifier:@"ISGShabdIndexViewController"];
                //ISGShabdIndexViewController * shabdVC = [[ISGShabdIndexViewController alloc] initWithNibName:@"ISGShabdIndexViewController" bundle:nil];
                shabdVC.titleName = exploreArray[indexPath.row];
                [self.navigationController pushViewController:shabdVC animated:YES];
            }
            break;
            
        case 3:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGListExploreViewController * listVC  = [storyboard instantiateViewControllerWithIdentifier:@"ISGListExploreViewController"];
                //ISGListExploreViewController * listVC =[[ISGListExploreViewController alloc]initWithNibName:@"ISGListExploreViewController" bundle:Nil];
                listVC.titleName = exploreArray[indexPath.row];
                [self.navigationController pushViewController:listVC animated:YES];
            }
            break;
            
        case 4:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGListExploreViewController * listVC  = [storyboard instantiateViewControllerWithIdentifier:@"ISGListExploreViewController"];
                //ISGListExploreViewController * listVC =[[ISGListExploreViewController alloc]initWithNibName:@"ISGListExploreViewController" bundle:Nil];
                listVC.titleName = exploreArray[indexPath.row];
                [self.navigationController pushViewController:listVC animated:YES];
            }
            break;
            
        case 5:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGReadSGGSViewController * readSGGSVC  = [storyboard instantiateViewControllerWithIdentifier:@"ISGReadSGGSViewController"];
                //ISGReadSGGSViewController * readSGGSVC =[[ISGReadSGGSViewController alloc]initWithNibName:@"ISGReadSGGSViewController" bundle:Nil];
                readSGGSVC.stringCategory = exploreArray[indexPath.row];
                [self.navigationController pushViewController:readSGGSVC animated:YES];
            }
            break;
            
        case 6:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGReadSGGSViewController * readSGGSVC  = [storyboard instantiateViewControllerWithIdentifier:@"ISGReadSGGSViewController"];
               // ISGReadSGGSViewController * readSGGSVC =[[ISGReadSGGSViewController alloc]initWithNibName:@"ISGReadSGGSViewController" bundle:Nil];
                readSGGSVC.stringCategory = exploreArray[indexPath.row];
                [self.navigationController pushViewController:readSGGSVC animated:YES];
            }
            break;
            
        case 7:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGListExploreViewController * listVC  = [storyboard instantiateViewControllerWithIdentifier:@"ISGListExploreViewController"];
                //ISGListExploreViewController * listVC =[[ISGListExploreViewController alloc]initWithNibName:@"ISGListExploreViewController" bundle:Nil];
                listVC.titleName = exploreArray[indexPath.row];
                [self.navigationController pushViewController:listVC animated:YES];
            }
            break;
            
            
        default:
            break;
    }
    
}








@end
