//
//  SearchHistory.m
//  iSearchGujral
//
//  Created by SICS on 17/08/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "SearchHistory.h"
#import "ISGAppDelegate.h"
#define entity @"SearchHistory"


@implementation SearchHistory

@dynamic searchLanguage;
@dynamic searchQuery;
@dynamic searchText;
@dynamic searchCategory;
@dynamic searchType;
@dynamic searchQueryWildcard;

+(void) addSearchHistoryForText:(NSString *)text Language:(NSString *)language Query:(NSString *)query Category:(NSString *)category Type:(NSString *)type andWildCardQuery:(NSString *)queryWildcard
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    SearchHistory * search = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:managedObjectContext];
    
    search.searchText = text;
    search.searchLanguage = language;
    search.searchQuery = query;
    search.searchCategory = category;
    search.searchType = type;
    search.searchQueryWildcard = queryWildcard;
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
}

+(void) deleteAllSearchHistory
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    NSArray * array = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [array enumerateObjectsUsingBlock:^(SearchHistory * obj, NSUInteger idx, BOOL *stop)
     {
         [managedObjectContext deleteObject:obj];
     }];
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
}

+(NSArray *)fetchAllSearchHistory
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    NSArray * array = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return array;
}

+(void) updateSearchHistory:(NSArray *)arrayHistory
{
    NSError * error = nil;
    [self deleteAllSearchHistory];
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    
    for (int i =0; i<arrayHistory.count; i++)
    {
        SearchHistory * search = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:managedObjectContext];
        NSDictionary * dict = (NSDictionary *)arrayHistory[i];
        search.searchText = [dict objectForKey:@"text"];
        search.searchLanguage = [dict objectForKey:@"laguage"];
        search.searchQuery = [dict objectForKey:@"query"];
        search.searchCategory = [dict objectForKey:@"category"];
        search.searchType = [dict objectForKey:@"type"];
        search.searchQueryWildcard = [dict objectForKey:@"queryWildcard"];
    }
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
}

@end
