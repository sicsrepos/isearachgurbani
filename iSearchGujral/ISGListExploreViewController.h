//
//  ISGListExploreViewController.h
//  iSearchGujral
//
//  Created by SICS on 11/12/14.
//  Copyright (c) 2014 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"

@interface ISGListExploreViewController : UIViewController
{
    IBOutlet UIImageView *imageBg;
    NSString *stringBackgroud;
    int fontSize;
    NSString *fontStyle;
    FMDatabase *database;
    NSMutableArray *arrayItems;
    NSMutableArray * arraySubChapterItems;
}
@property (nonatomic, strong) NSString * titleName;
@property (nonatomic, strong) NSString * alphabet;

@end
