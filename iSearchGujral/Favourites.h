//
//  Favourites.h
//  iSearchGujral
//
//  Created by mac on 8/5/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Favourites : NSManagedObject

@property (nonatomic, retain) NSString * ang;
@property (nonatomic, retain) NSString * scrpt;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSData * baani;

@end
