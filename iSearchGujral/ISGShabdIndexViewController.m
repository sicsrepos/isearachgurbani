//
//  ISGShabdIndexViewController.m
//  iSearchGujral
//
//  Created by SICS on 25/02/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "ISGShabdIndexViewController.h"
#import "ISGTheme.h"
#import "ISGListExploreViewController.h"
#import "ISGSubChaptersViewController.h"

@interface ISGShabdIndexViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIImageView * imageBg;
    IBOutlet UITableView * tableShabd;
    NSArray * arrayShabd;
    NSString *stringBackgroud;
    int fontSize;
    NSString *fontStyle;
    NSMutableArray *arrayItems;
    CGFloat screenHeight;
}

@end

@implementation ISGShabdIndexViewController
@synthesize titleName;

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayShabd = @[@"SGGS Shabd Index Gurmukhi",@"SGGS Shabd Index Roman",@"SGGS Shabd Index Devanagri(Hindi)",@"SGGS Shabd Index Alphabetical"];
    
    [tableShabd setBackgroundColor:[UIColor clearColor]];
    
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self setTheme];
    self.title=titleName;
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    screenHeight = screenSize.height;
    NSLog(@"screen height %f",screenHeight);
  /*  if ((screenHeight > 850) && (screenHeight < 1000)) {
        tableShabd.frame = CGRectMake(60, 60 ,240,450);
    }
    if(screenHeight==1024.000000)
    {
        tableShabd.frame = CGRectMake(44, 44, 240,screenHeight-500);
    }
    
    
//    if(screenHeight==1024.000000)
//    {
//         tableShabd.frame = CGRectMake(40, 120, UIScreen.mainScreen.bounds.size.width-80,screenHeight-450);
//    }
    else if(screenHeight==568.000000)
    {
         tableShabd.frame = CGRectMake(20, 100, UIScreen.mainScreen.bounds.size.width,screenHeight);
    }
    
    else if(screenHeight==480.000000)
    {
         tableShabd.frame = CGRectMake(20, 100, 290,screenHeight-80);
    }
    else if(screenHeight == 736)
    {
         tableShabd.frame = CGRectMake(0, 120, screenSize.width, screenHeight-150);
    }
    else if(screenHeight == 667)
    {
         tableShabd.frame = CGRectMake(40, 120, screenSize.width, screenHeight-100);
    }
    else
    {
        tableShabd.frame = CGRectMake(44, 50, 240,screenHeight-750);
    }
    
    tableShabd.center = self.view.center;
*/
    
    
    arrayItems =[[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"refresh" object:Nil];
    // Do any additional setup after loading the view from its nib.
}

-(void)refresh:(NSNotification *)notification
{
    [self setTheme];
    [tableShabd reloadData];
}

-(void)setTheme
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    ISGTheme *theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    stringBackgroud=theme.background;
    fontSize=[theme.fontSize intValue];
    if(fontSize==0)
    {
        fontSize=13;
    }
    if(stringBackgroud.length<=7)
    {
        stringBackgroud=@"LightPattern";
    }
    [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
}
-(void)SetDefaultValues
{
    stringBackgroud=@"LightPattern";
    fontSize=12;
    //fontStyle=@"gurbaniwebthick";
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    BOOL retValue = NO;
    retValue = [super canPerformAction:action withSender:sender];
    return retValue;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayShabd.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(screenHeight==1024.000000)
    {
        cell.textLabel.font=[UIFont boldSystemFontOfSize:30];
    }
    else
        cell.textLabel.font=[UIFont boldSystemFontOfSize:fontSize];
    
    if([stringBackgroud isEqualToString:@"LightPattern"]||[stringBackgroud isEqualToString:@"WhitePattern"])
    {
        [cell.textLabel setTextColor:[UIColor blackColor]];
    }
    else
    {
        [cell.textLabel setTextColor:[UIColor whiteColor]];
    }
    
    
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.numberOfLines=1;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.text= arrayShabd[indexPath.row];
    cell.textLabel.userInteractionEnabled = YES;
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == arrayShabd.count-1)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGSubChaptersViewController * subVC  = [storyboard instantiateViewControllerWithIdentifier:@"ISGSubChaptersViewController"];
        //ISGSubChaptersViewController * subVC = [[ISGSubChaptersViewController alloc] initWithNibName:@"ISGSubChaptersViewController" bundle:nil];
        [self.navigationController pushViewController:subVC animated:YES];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGListExploreViewController * listVC  = [storyboard instantiateViewControllerWithIdentifier:@"ISGListExploreViewController"];
       // ISGListExploreViewController * listVC =[[ISGListExploreViewController alloc]initWithNibName:@"ISGListExploreViewController" bundle:Nil];
        listVC.titleName = arrayShabd[indexPath.row];
        [self.navigationController pushViewController:listVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(screenHeight==1024.000000)
    {
        return 80;
    }
    else
        return 40;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
