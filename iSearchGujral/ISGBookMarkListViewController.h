//
//  ISGBookMarkListViewController.h
//  iSearchGujral
//
//  Created by SICS on 12/01/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "ATSDragToReorderTableViewController.h"
#import "BookMarkFolders.h"

@interface ISGBookMarkListViewController : ATSDragToReorderTableViewController
//IBOutlet UITableView *tableFavourites;


@property (nonatomic, strong) BookMarkFolders * folderName;
@property (nonatomic, strong) NSString * titleName;
@end
