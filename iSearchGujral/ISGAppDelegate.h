//
//  ISGAppDelegate.h
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"
#import "SSZipArchive.h"
#import <FacebookSDK/FacebookSDK.h>
// last commit vks
@interface ISGAppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate,SSZipArchiveDelegate>
{
    NSString *databaseName,*databasePath;
}

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) FMDatabase *database;
//@property (strong,nonatomic) FBSession *session;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
