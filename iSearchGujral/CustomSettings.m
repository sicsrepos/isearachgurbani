//
//  CustomSettings.m
//  iSearchGujral
//
//  Created by SICS on 26/08/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "CustomSettings.h"
#import "ISGAppDelegate.h"
#define entity @"CustomSettings"


@implementation CustomSettings

@dynamic name;
@dynamic setting;

+(void) addCustomSettingName:(NSString *)settingName andSettings:(NSData *)settings
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    CustomSettings * setting = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:managedObjectContext];
    
    setting.name = settingName;
    setting.setting = settings;
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
}

+(NSArray *) fetchAllCustomSettings
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    NSArray * array = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return array;
}

+(void) deleteCustomSettingWithSettings:(NSData *)settings
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"setting == %@",settings];
    [fetchRequest setPredicate:predicate];
    
    NSArray * array = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [array enumerateObjectsUsingBlock:^(CustomSettings * setting, NSUInteger idx, BOOL *stop)
     {
         [managedObjectContext deleteObject:setting];
     }];
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
}

@end
