//
//  ISGExploreViewController.h
//  iSearchGujral
//
//  Created by SICS on 25/11/14.
//  Copyright (c) 2014 sics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISGExploreViewController : UIViewController
{
    IBOutlet UIImageView *imageBg;
   
    NSArray *exploreArray;
    NSString *stringBackgroud;
    IBOutlet UITableView *tableExplore;
    int fontSize;
    NSString *fontStyle;
}

@end
