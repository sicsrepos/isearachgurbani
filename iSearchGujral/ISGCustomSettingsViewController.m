//
//  ISGCustomSettingsViewController.m
//  iSearchGujral
//
//  Created by SICS on 26/08/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "ISGCustomSettingsViewController.h"
#import "CustomSettings.h"

@interface ISGCustomSettingsViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView * tableViewCustomSettings;
    NSMutableArray * arrayCustomSettings;
    int selectedIndex;
}

@end

@implementation ISGCustomSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    tableViewCustomSettings.backgroundColor = [UIColor clearColor];
    selectedIndex = -1;
    [self getCustomSettings];
    tableViewCustomSettings.allowsMultipleSelectionDuringEditing = NO;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getCustomSettings
{
    arrayCustomSettings = [[NSMutableArray alloc] initWithArray:[CustomSettings fetchAllCustomSettings]];
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    for (int i = 0; i < arrayCustomSettings.count; i++)
    {
        CustomSettings * setting = (CustomSettings *)arrayCustomSettings[i];
        if ([setting.setting isEqualToData:data])
        {
            selectedIndex = i;
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayCustomSettings.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [cell.textLabel setTextColor:[UIColor blackColor]];
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.numberOfLines=1;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CustomSettings * setting = (CustomSettings *)arrayCustomSettings[indexPath.row];
    cell.textLabel.text=[NSString stringWithFormat:@"%@ %d",setting.name, indexPath.row+1];
    cell.textLabel.userInteractionEnabled = YES;
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (indexPath.row == selectedIndex)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomSettings * setting = (CustomSettings *)arrayCustomSettings[indexPath.row];
    [[NSUserDefaults standardUserDefaults] setObject:setting.setting forKey:@"theme"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"refresh" object:Nil];
    selectedIndex = indexPath.row;
    [tableView reloadData];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        CustomSettings * setting = (CustomSettings *)arrayCustomSettings[indexPath.row];
        [CustomSettings deleteCustomSettingWithSettings:setting.setting];
        [arrayCustomSettings removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        [tableView reloadData];
    }
}

@end
