//
//  BookMarkFolders.h
//  iSearchGujral
//
//  Created by SICS on 09/01/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BookMarkPages;

@interface BookMarkFolders : NSManagedObject

@property (nonatomic, retain) NSString * folderName;
@property (nonatomic, retain) NSString * index;
@property (nonatomic, retain) NSSet *relationshipToPages;

+(void) addBookMarkFolderWithName:(NSString *)folderName index:(int)index;
+(void) deleteBookMarkFolderWithName:(NSString *)folderName;
+(NSArray *) fetchAllBookMarkFolders;
+(void) deleteAllBookMarkFolders;
+(void) updateBookMarkIndex:(NSArray *)arrayIndex andFolders:(NSArray *)arrayFolders;

@end

@interface BookMarkFolders (CoreDataGeneratedAccessors)

- (void)addRelationshipToPagesObject:(BookMarkPages *)value;
- (void)removeRelationshipToPagesObject:(BookMarkPages *)value;
- (void)addRelationshipToPages:(NSSet *)values;
- (void)removeRelationshipToPages:(NSSet *)values;

@end
