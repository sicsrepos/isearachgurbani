//
//  CustomSettings.h
//  iSearchGujral
//
//  Created by SICS on 26/08/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CustomSettings : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSData * setting;

+(void) addCustomSettingName:(NSString *)settingName andSettings:(NSData *)settings;
+(NSArray *) fetchAllCustomSettings;
+(void) deleteCustomSettingWithSettings:(NSData *)settings;

@end
