//
//  ISGBaniDetailViewController.h
//  iSearchGujral
//
//  Created by mac on 4/22/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISGTheme.h"
#import "SVProgressHUD.h"
#import "Favourites.h"
#import "ISGAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
@interface ISGBaniDetailViewController : UIViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate>
{
    NSMutableArray *cells;
    NSMutableArray *heightsCell;
    
    UIImage *attachmentImage;
    NSMutableArray *arrayItems;

    NSMutableArray *arrayLanguages;
    IBOutlet UIScrollView *scroll;
    IBOutlet UITableView *table;
    IBOutlet UIButton *buttonNext;
    IBOutlet UIButton *buttonPrev;
    IBOutlet UITableView *tableBaani;
    IBOutlet UIImageView *imageBg;
    IBOutlet UIView *popView;
    NSString *stringBackgroud,*message;
    int fontSize,page;
    int shabd,dataID;
    NSString *fontStyle;
    NSString *language1;
    NSString *language2,*language3,*language4,*language5,*language6,*language7;
    NSString *strShabd,*strId,*strPage,*sggsData,*script;
    UILongPressGestureRecognizer *lpHandler;
    int size,gurmukhiSize,romanSize,hindiSize,englishSize,teekaSize,attributeSize;
    UIColor *gurmukhiColor,*romanColor,*hindiColor,*englishColor,*teekaColor,*attributeColor;
    UITapGestureRecognizer * tapGesture;
}
@property (strong, nonatomic) IBOutlet UISwitch *tapOutsideSwitch;
@property (nonatomic,strong) NSString *bani,*key,*rootClass;
@property (nonatomic,strong) NSString *strFavour;
@property (nonatomic,strong) NSMutableArray *arrayData,*arrayBaani;
@property (nonatomic) int arrayIndex;
-(IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender;
-(IBAction)swipeRight:(UISwipeGestureRecognizer *)sender;
-(IBAction)btnNext:(id)sender;
-(IBAction)btnPrev:(id)sender;
-(IBAction)btnProceed:(id)sender;
@end

