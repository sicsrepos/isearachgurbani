//
//  ISGPreferenceViewController.h
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISGLanguageViewController.h"
#import "ISGFontViewController.h"
#import "ISGGurumukhiFontViewController.h"
#import "ISGBackgroundViewController.h"
#import "ISGHelpViewController.h"
@interface ISGPreferenceViewController : UIViewController<UITableViewDelegate,UIAlertViewDelegate>
{
    NSArray *arrayList;
}
@end
