//
//  ISGBaniListViewController.h
//  iSearchGujral
//
//  Created by mac on 4/22/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISGBaniDetailViewController.h"
#import "ISGTheme.h"
@interface ISGBaniListViewController : UIViewController<UITableViewDelegate>
{
     NSMutableArray *arrayItems;
    IBOutlet UIImageView *imageBg;
    NSString *stringBackgroud;
    IBOutlet UITableView *tableBaani;
    int fontSize;
    NSString *fontStyle;
}
@property (nonatomic,retain) NSString *bani;
@property (nonatomic,retain) NSString *key;
@property (nonatomic,retain) NSMutableArray *arrayItems;
@end
