//
//  ISGCell.m
//  iSearchGujral
//
//  Created by mac on 6/1/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGCell.h"

@implementation ISGCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}


- (id)initWithQuote:(UILabel *)label
{
    self = [super initWithFrame:CGRectMake(0, 0, 320, 0)];
    if (self) {
        int labelHeight = 0;
         _labelsView = [[NSMutableArray alloc] init];
        [_labelsView addObject:label];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            label.frame = CGRectMake(5, labelHeight+2, 630, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(630, 9999)].height);
          //  label.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2-74, label.center.y);
        }
        else
        {
            //287
            label.frame = CGRectMake(0, labelHeight+2, 265, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(265, 9999)].height);
           /* if([UIScreen mainScreen].bounds.size.height == 736)/*
            {
                label.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2-40, label.center.y);
            }
            else if([UIScreen mainScreen].bounds.size.height == 667)
            {
                label.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2-33, label.center.y);
            }*/
            //label.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2-40, label.center.y);
        }
        label.numberOfLines = 0;
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        labelHeight = label.frame.size.height;
        
        //label.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:label];
        self.labelHeight = labelHeight;
        [label setCenter:self.contentView.center];
       // self.frame = CGRectMake(5, 0, 310, labelHeight);
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}


- (id)initWithLine:(UILabel *)label
{
    self = [super initWithFrame:CGRectMake(0, 0, 320, 0)];
    if (self) {
        int labelHeight = 0;
        _labelsView = [[NSMutableArray alloc] init];
        [_labelsView addObject:label];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            label.frame = CGRectMake(5, labelHeight+2, 685, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(685, 9999)].height);
        }
        else
        {
            //287
            label.frame = CGRectMake(0, labelHeight+2, 287, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(287, 9999)].height);
           /* if([UIScreen mainScreen].bounds.size.height == 736)
            {
                label.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2-40, label.center.y);
            }
            else if([UIScreen mainScreen].bounds.size.height == 667)
            {
                label.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2-33, label.center.y);
            }*/
        }
        label.numberOfLines = 0;
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        labelHeight += label.frame.size.height;
        [self.contentView addSubview:label];
        self.labelHeight = labelHeight;
        [label setCenter:self.contentView.center];
       // self.frame = CGRectMake(5, 0, 310, labelHeight);
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}
- (id)initWithLabels:(NSArray *)labels {
    
    self = [super initWithFrame:CGRectMake(0, 0, 310, 0)];
    if (self) {
        int labelHeight = 0;
        _labelsView = [[NSMutableArray alloc] initWithArray:labels];
        
        for (UILabel *label in labels) {
            
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                
                label.frame = CGRectMake(5  , labelHeight+2, 685, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(685, 9999)].height);
                label.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2-74, label.center.y);
            }
            else
            {
                //287
                label.frame = CGRectMake(0, labelHeight+2, self.frame.size.width, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(287, 9999)].height);
                
            }
            
            
            label.numberOfLines = 0;
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            labelHeight += label.frame.size.height;
            [self.contentView addSubview:label];
            self.labelHeight = labelHeight;
            [label setCenter:self.contentView.center];
            //            self.frame = CGRectMake(0 , 0, 310, labelHeight + 10);
            self.backgroundColor = [UIColor clearColor];
        }
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    int labelHeight = 0;
    for (UILabel *label in self.labelsView) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            if (labelHeight == 0 && self.labelsView.count > 1 ) {
                label.frame = CGRectMake(5, labelHeight+12, self.frame.size.width, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(685, 9999)].height);

            }
            else {
                label.frame = CGRectMake(5, labelHeight+2, self.frame.size.width, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(685, 9999)].height);

            }
        }
        else
        {
            //287
            if (labelHeight == 0 && self.labelsView.count > 1) {
                label.frame = CGRectMake(0, labelHeight+12, self.frame.size.width, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(self.frame.size.width, 9999)].height);

            }
            else {
                label.frame = CGRectMake(0, labelHeight+2, self.frame.size.width, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(self.frame.size.width, 9999)].height);

            }
        }
        
         if (labelHeight == 0 && self.labelsView.count > 1) {
        labelHeight += label.frame.size.height + 12;
         }
         else {
             labelHeight += label.frame.size.height;
         }
        
    }
    
}
- (int)cellHeightWithArray:(NSArray *)labels {
    int labelHeight = 0;
    for (UILabel *label in labels) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            label.frame = CGRectMake(5, labelHeight+2, 685, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(685, 9999)].height);
        }
        else
        {
            //287
            label.frame = CGRectMake(0, labelHeight+2, 287, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(287, 9999)].height);
        }
        
        labelHeight += label.frame.size.height;
        
    }
    return labelHeight + 10;
    
}
- (float)cellHeight {
    return self.frame.size.height + 5;
}
- (CGSize)getHeightForString:(NSString *)string forFont:(UIFont *)font forConstraintSize:(CGSize)constrainSize {
    //    NSLog(@"string height %f",[string sizeWithFont:font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByWordWrapping].height);
    return [string sizeWithFont:font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByWordWrapping];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
