//
//  ISGSearchResultsViewController.h
//  iSearchGujral
//
//  Created by mac on 4/24/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISGDataModel.h"
#import "ISGSearchDetailViewController.h"
#import "FMDatabase.h"
#import "SVProgressHUD.h"
@interface ISGSearchResultsViewController : UIViewController<UITableViewDelegate,UIAlertViewDelegate, UITableViewDataSource>
{
    IBOutlet UIImageView *imageBg;
    NSMutableArray *heightsCell;

    IBOutlet UITableView *tableSearch;
    UILongPressGestureRecognizer *lpHandler;
    int fontSize;
    FMDatabase *database;
    NSString *fontStyle;
    NSString *sggsData;
    int shabd,dataID;
    NSString *script;
    NSString *strShabd,*strId,*strPage,*page;
    NSIndexPath *index;
    UITableViewCell *cell;
}
extern int selectedShabd;
@property (nonatomic,strong) NSMutableArray *arrayItems;
@property (nonatomic,strong) NSString *lang;
@property (nonatomic, strong) NSString * querry;
@property (nonatomic, strong) NSString * searchedString;
@end
