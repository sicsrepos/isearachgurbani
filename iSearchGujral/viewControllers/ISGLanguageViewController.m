//
//  ISGLanguageViewController.m
//  iSearchGujral
//
//  Created by mac on 4/26/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGLanguageViewController.h"
#import "ISGTheme.h"
#import <QuartzCore/CALayer.h>
#import "GzColors.h"


@interface ISGLanguageViewController ()
{
    ISGTheme *theme;
    
}
@end

@implementation ISGLanguageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.title=@"Languages";
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
   /* if ((screenHeight > 850) && (screenHeight < 1000)) {
        containerView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2-145, 80 , 290, 382);
    }
    if(screenHeight==1024.000000)
    {
        containerView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2-145, [UIScreen mainScreen].bounds.size.height/4 , 290, 382);
    }
    else if(screenHeight==568.000000)
    {
        containerView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2-145, 40 , 290, 382);
    }
    else if(screenHeight==480.000000)
    {
        containerView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2-145, 10 , 290, 345);
    }
    else if(screenHeight == 736)
    {
        containerView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2-145, 80 , 290, 382);
    }
    else
    {
        containerView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2-145, 60 , 290, 382);
    }*/
    
//    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
//    [containerView setFrame:CGRectMake(100, 100, 200, 400)];
    [self initialSettings];
    NSLog(@"frame %f",self.view.frame.size.width);
    // Do any additional setup after loading the view from its nib.
}
-(void)initialSettings
{
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    backGroundImage = theme.backgroundImage;
    background=theme.background;
    fontSize=theme.fontSize;
    fontStyle=theme.fontStyle;
    
    language1=theme.lang1;
    language2=theme.lang2;
    language3=theme.lang3;
    language4=theme.lang4;
    language5=theme.lang5;
    language6=theme.lang6;
    //language7=theme.lang7;
    
    strGurmukhiSize=theme.gurmukhiSize;
    strRomanSize=theme.romanSize;
    strHindiSize=theme.hindiSize;
    strEnglishSize=theme.englishSize;
    strTeekaSize=theme.teekaSize;
    strAttributeSize=theme.attributeSize;
    
    gurmukhiColor = theme.gurmukhiColor;
    romanColor = theme.romanColor;
    hindiColor = theme.hindiColor;
    englishColor = theme.englishColor;
    teekaColor = theme.teekaColor;
    attributeColor = theme.attributeColor;
    
    if([language1 isEqualToString:@"gurmukhi"])
    {
        [imgGurmukhi  setImage:[UIImage imageNamed:@"tick.png"]];
        [imgPunctuation setImage:[UIImage imageNamed:@"untick.png"]];

    }
    if([language1 isEqualToString:@"punctuation"])
    {
        [imgPunctuation  setImage:[UIImage imageNamed:@"tick.png"]];
        [imgGurmukhi setImage:[UIImage imageNamed:@"untick.png"]];
        
    }
    if([language1 isEqualToString:@"lareevar"])
    {
        [imgLareevar setImage:[UIImage imageNamed:@"tick.png"]];
        buttonLareevar.selected=TRUE;
    }
    if([language2 isEqualToString:@"roman"])
    {
        //NSLog(@"lang %@",language2);
        [imgRoman  setImage:[UIImage imageNamed:@"tick.png"]];
        [imgTrans setImage:[UIImage imageNamed:@"untick.png"]];
        
    }
    if([language2 isEqualToString:@"translit"])
    {
        //NSLog(@"lang %@",language2);
        [imgTrans  setImage:[UIImage imageNamed:@"tick.png"]];
        [imgRoman setImage:[UIImage imageNamed:@"untick.png"]];
        
    }
    // set another buttons
    if([language3 isEqualToString:@"gurmukhi"])
    {
         [imgHindi setImage:[UIImage imageNamed:@"tick.png"]];
        buttonHindi.selected=TRUE;
    }
    if([language4 isEqualToString:@"english"])
    {
         [imgEnglish setImage:[UIImage imageNamed:@"tick.png"]];
        buttonEnglish.selected=TRUE;
    }
    if([language5 isEqualToString:@"teeka"])
    {
         [imgTeeka setImage:[UIImage imageNamed:@"tick.png"]];
        buttonTeeka.selected=TRUE;
    }
    if([language6 isEqualToString:@"attributes"])
    {
         [imgAttributes setImage:[UIImage imageNamed:@"tick.png"]];
       // [buttonAttributes setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        buttonAttributes.selected=TRUE;
    }
    
    //null check
    if(background.length<=7)
    {
        background=@"LightPattern";
    }
    
    if (!backGroundImage)
    {
        [imageBg setImage:[UIImage imageNamed:background]];
    }
    else
        imageBg.image = backGroundImage;
    
    [self changeFontColor:background];
    theme=[[ISGTheme alloc]init];
}
-(void)changeFontColor:(NSString *)pattern
{
    
    if([pattern isEqualToString:@"LightPattern"]||[pattern isEqualToString:@"WhitePattern"])
    {
        [labelTransliteration setTextColor:[UIColor blackColor]];
        [labelGurumukhi setTextColor:[UIColor blackColor]];
        [labelPunctuation setTextColor:[UIColor blackColor]];
        [labelRoman setTextColor:[UIColor blackColor]];
        [labelAttributes setTextColor:[UIColor blackColor]];
        [labelEnglish setTextColor:[UIColor blackColor]];
        [labelHindi setTextColor:[UIColor blackColor]];
        [labelTeeks setTextColor:[UIColor blackColor]];
        [labelLareevar setTextColor:[UIColor blackColor]];
    }
    else
    {
        [labelTransliteration setTextColor:[UIColor whiteColor]];
        [labelGurumukhi setTextColor:[UIColor whiteColor]];
        [labelPunctuation setTextColor:[UIColor whiteColor]];
        [labelRoman setTextColor:[UIColor whiteColor]];
        [labelAttributes setTextColor:[UIColor whiteColor]];
        [labelEnglish setTextColor:[UIColor whiteColor]];
        [labelHindi setTextColor:[UIColor whiteColor]];
        [labelTeeks setTextColor:[UIColor whiteColor]];
        [labelLareevar setTextColor:[UIColor whiteColor]];
    }
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void)setCurrentValues
{
    theme.lang1=language1;
    theme.lang2=language2;
    theme.lang3=language3;
    theme.lang4=language4;
    theme.lang5=language5;
    theme.lang6=language6;
    //theme.lang7=language7;
    
    theme.backgroundImage = backGroundImage;
    theme.background=background;
    theme.fontStyle=fontStyle;
    theme.fontSize=fontSize;
    theme.gurmukhiSize=strGurmukhiSize;
    theme.romanSize=strRomanSize;
    theme.hindiSize=strHindiSize;
    theme.englishSize=strEnglishSize;
    theme.teekaSize=strTeekaSize;
    theme.attributeSize=strAttributeSize;
    
    theme.gurmukhiColor = gurmukhiColor;
    theme.romanColor = romanColor;
    theme.hindiColor = hindiColor;
    theme.englishColor = englishColor;
    theme.teekaColor = teekaColor;
    theme.attributeColor = attributeColor;
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:theme] forKey:@"theme"];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"refresh" object:Nil];
}
-(void)setLanguage:(NSString *)language
{
   // NSLog(@"selected lang :%@",language);
    [self setCurrentValues];
    
    if([language isEqualToString:@"punctuation"] || [language isEqualToString:@"gurmukhi"] || [language isEqualToString:@"lareevar"])
    {
        theme.lang1=language;
        
    }
    else  if([language isEqualToString:@"roman"] || [language isEqualToString:@"translit"])
    {
        theme.lang2=language;
    }
    else  if([language isEqualToString:@"hindi"])
    {
        theme.lang3=@"gurmukhi";
       
    }
    else  if([language isEqualToString:@"english"])
    {
        theme.lang4=language;
       
    }
    else  if([language isEqualToString:@"teeka"])
    {
        theme.lang5=language;
            }
    else  if([language isEqualToString:@"attributes"])
    {
        theme.lang6=language;
       
    }
    else  if([language isEqualToString:@"no_roman"]||[language isEqualToString:@"no_translit"])
    {
        theme.lang2=@"";
        
    }
    
    
    theme.background=background;
    theme.fontStyle=fontStyle;
    theme.fontSize=fontSize;
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:theme] forKey:@"theme"];
    [defaults synchronize];
    [self initialSettings];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"refresh" object:Nil];
}
-(IBAction)btnPunctuation:(id)sender
{
    [imgPunctuation  setImage:[UIImage imageNamed:@"tick.png"]];
    [imgGurmukhi setImage:[UIImage imageNamed:@"untick.png"]];
    [imgLareevar setImage:[UIImage imageNamed:@"untick.png"]];
    [self setLanguage:@"punctuation"];
}
-(IBAction)btnTransliteration:(id)sender
{
    buttonTransliteration.selected = !buttonTransliteration.selected;
    if (buttonTransliteration.selected)
    {
        [imgTrans  setImage:[UIImage imageNamed:@"tick.png"]];
        [imgRoman setImage:[UIImage imageNamed:@"untick.png"]];

    [self setLanguage:@"translit"];
    buttonRoman.selected=NO;
    }
   else
   {
       [imgTrans setImage:[UIImage imageNamed:@"untick.png"]];
       [self setLanguage:@"no_translit"];
       buttonRoman.selected=NO;
       buttonTransliteration.selected=NO;
       
   }
}
-(IBAction)btnRoman:(id)sender
{
     buttonRoman.selected = !buttonRoman.selected;
     if (buttonRoman.selected)
     {
         [imgRoman  setImage:[UIImage imageNamed:@"tick.png"]];
         [imgTrans setImage:[UIImage imageNamed:@"untick.png"]];
         [self setLanguage:@"roman"];
         buttonTransliteration.selected=NO;
   }
    else
    {
        [imgRoman setImage:[UIImage imageNamed:@"untick.png"]];
        [self setLanguage:@"no_roman"];
        buttonTransliteration.selected=NO;
        buttonRoman.selected=NO;
    }
    
}
-(IBAction)btnGurumukhi:(id)sender
{
    [imgGurmukhi  setImage:[UIImage imageNamed:@"tick.png"]];
    [imgPunctuation setImage:[UIImage imageNamed:@"untick.png"]];
    [imgLareevar setImage:[UIImage imageNamed:@"untick.png"]];
    [self setLanguage:@"gurmukhi"];
}
-(IBAction)btnHindi:(id)sender
{
    buttonHindi.selected = !buttonHindi.selected; 
    if (buttonHindi.selected)
    {
        [imgHindi setImage:[UIImage imageNamed:@"tick.png"]];
         [self setLanguage:@"hindi"];
        
    }
    else
    {
        [imgHindi setImage:[UIImage imageNamed:@"untick.png"]];
        language3=@"";
        [self setCurrentValues];
        
    }
}
-(IBAction)btnEnglish:(id)sender
{
    buttonEnglish.selected = !buttonEnglish.selected;
    if (buttonEnglish.selected)
    {
         [imgEnglish setImage:[UIImage imageNamed:@"tick.png"]];
         [self setLanguage:@"english"];
    }
    else
    {
         [imgEnglish setImage:[UIImage imageNamed:@"untick.png"]];
        language4=@"";
         [self setCurrentValues];
    }
}
-(IBAction)btnTeeka:(id)sender
{

    buttonTeeka.selected = !buttonTeeka.selected;
    if (buttonTeeka.selected)
    {
         [imgTeeka setImage:[UIImage imageNamed:@"tick.png"]];
         [self setLanguage:@"teeka"];
    }
    else
    {
         [imgTeeka setImage:[UIImage imageNamed:@"untick.png"]];
        language5=@"";
        [self setCurrentValues];
    }

}
-(IBAction)btnAttributes:(id)sender
{
    buttonAttributes.selected = !buttonAttributes.selected;
    if (buttonAttributes.selected)
    {
         [imgAttributes setImage:[UIImage imageNamed:@"tick.png"]];
         [self setLanguage:@"attributes"];
    }
    else
    {
         [imgAttributes setImage:[UIImage imageNamed:@"untick.png"]];
         language6=@"";
         [self setCurrentValues];
    }
}

-(IBAction)btnLareevar:(id)sender
{
    [imgLareevar setImage:[UIImage imageNamed:@"tick.png"]];
    [imgGurmukhi  setImage:[UIImage imageNamed:@"untick.png"]];
    [imgPunctuation setImage:[UIImage imageNamed:@"untick.png"]];
    [self setLanguage:@"lareevar"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
