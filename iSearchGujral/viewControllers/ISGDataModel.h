//
//  ISGDataModel.h
//  iSearchGujral
//
//  Created by mac on 4/18/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMResultSet.h"
@interface ISGDataModel : NSObject

@property (nonatomic,strong) NSString *gurmukhi;
@property (nonatomic,strong) NSString *english;
@property (nonatomic,strong) NSString *roman;
@property (nonatomic,strong) NSString *dataID;
@property (nonatomic,strong) NSString *shabd;
@property (nonatomic,strong) NSString *script;
@property (nonatomic,strong) NSString *wgamma;
@property (nonatomic,strong) NSString *gamma;
@property (nonatomic,strong) NSString *alpha;
@property (nonatomic,strong) NSString *walpha;
@property (nonatomic,strong) NSString *page;
@property (nonatomic,strong) NSString *attributes;
@property (nonatomic,strong) NSString *translit;
@property (nonatomic,strong) NSString *punctuation;
@property (nonatomic,strong) NSString *tab;
@property (nonatomic,strong) NSString *lang;
@property (nonatomic,strong) NSString *teeka;
@property (nonatomic,strong) NSString *line;
@property (nonatomic,strong) NSString *indexValue;
@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) NSString *chapterID;
@property (nonatomic,strong) NSString *startingline;
@property (nonatomic,strong) NSString *endingLine;
@property (nonatomic,strong) NSString *pageID;
@property (nonatomic,strong) NSString *shabdE;
@property (nonatomic,strong) NSString *shabdG;
@property (nonatomic,strong) NSString *parentID;
@property (nonatomic,strong) NSString *genreID;
@property (nonatomic,strong) NSString *shabdID;
@property (nonatomic,strong) NSString *authorID;
@property (nonatomic,strong) NSString *raagID;
@property (nonatomic,strong) NSString *count;

-(id) initWithResult:(FMResultSet *)result;
-(id) generateQuote:(FMResultSet *)result;
-(id) initWithShabd:(FMResultSet *)result;
-(id) initWithChapters:(FMResultSet *)result;
-(id) initWithSubChapters:(FMResultSet *)result;
-(id) initWithFavouriteList:(FMResultSet *)result;
-(id) initWithShabdIndex :(FMResultSet *)result;
-(id) initWithAuthors:(FMResultSet *)result;
-(id) initWithScriptures:(FMResultSet *)result;
-(id) initWithRaags:(FMResultSet *)result;
-(id) initWithCount:(FMResultSet *)result;
@end
