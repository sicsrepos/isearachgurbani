//
//  ISGTabViewController.h
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISGHomeViewController.h"
#import "ISGReadSGGSViewController.h"
#import "ISGSearchViewController.h"
#import "ISGBaniViewController.h"
#import "ISGPreferenceViewController.h"
#import "ISGFavouritesViewController.h"
#import "ISGHelpViewController.h"
#import "ISGExploreViewController.h"

@interface ISGTabViewController : UITabBarController<UITabBarControllerDelegate,UINavigationControllerDelegate>
@property (nonatomic,strong) ISGHomeViewController *HomeViewController;
@property (nonatomic,strong) ISGReadSGGSViewController *ReadSGGSViewController;
@property (nonatomic,strong) ISGSearchViewController *SearchViewController;
@property (nonatomic,strong) ISGBaniViewController *BaniViewController;
@property (nonatomic,strong) ISGPreferenceViewController *PreferenceViewController;
@property (nonatomic,strong) ISGFavouritesViewController *FavouritesViewController;
@property (nonatomic,strong) ISGHelpViewController *HelpViewController;
@property (nonatomic,strong) ISGExploreViewController * ExploreViewController;
@end
