//
//  ISGCell.h
//  iSearchGujral
//
//  Created by mac on 6/1/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISGCell : UITableViewCell

@property (nonatomic,readonly) float cellHeight;
@property (nonatomic) float labelHeight;
@property (nonatomic) NSMutableArray * labelsView;
- (id)initWithLabels:(NSArray *)labels size:(CGFloat ) size sizeX:(CGFloat)sizex;
- (id)initWithLabels:(NSArray *)labels;

- (id)initWithLine:(UILabel *)label;
- (id)initWithQuote:(UILabel *)label;
@end
