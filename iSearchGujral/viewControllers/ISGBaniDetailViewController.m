//
//  ISGBaniDetailViewController.m
//  iSearchGujral
//
//  Created by mac on 4/22/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGBaniDetailViewController.h"
#import "FMDatabase.h"
#import "ISGDataModel.h"
#import <MessageUI/MessageUI.h>
#import "MBProgressHUD.h"
#import "ISGAppDelegate.h"
#import <Twitter/Twitter.h>
#import "ISGCell.h"
#import "HMSideMenu.h"
#import "KGModal.h"
#import "BookMarkFolders.h"
#import "BookMarkPages.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "ScrollViewToPDF.h"
#import "ISGCustomAlertViewController.h"

@interface ISGBaniDetailViewController ()<MBProgressHUDDelegate,MFMailComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate, UIPrintInteractionControllerDelegate>
{
    NSString *bundlePath;
    NSString *dbFilePath;
    FMDatabase *database;
    MBProgressHUD *HUD;
    ISGAppDelegate *appDelegate;
    HMSideMenu *sideMenu;
    NSDictionary *dict;
    IBOutlet UIGestureRecognizer * leftSwipGesture ,* rightSwipeGesture;
    CGFloat x, y;
    NSArray * arrayFolders;
    NSMutableArray * arrayNames;
    UITableView * tableList;
    BookMarkPages * bookMarkPages;
    NSString * queryExecuted;
    UIImage * imageForPrint;
    UIPrintInteractionController * printController;
    int * value;
}

@end

@implementation ISGBaniDetailViewController
@synthesize bani,arrayData,arrayIndex,strFavour,arrayBaani,key,rootClass;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [tableBaani setBackgroundColor:[UIColor clearColor]];
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    arrayLanguages=[[NSMutableArray alloc]init];
    
    //x = table.frame.origin.x;
    // arrayData=[[NSMutableArray alloc]init];
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    CGFloat screenWidth = screenSize.width;
    
    //NSLog(@"screen height %f",screenHeight);
    /* if ((screenHeight > 850)  && (screenHeight < 1000)) {
     table.frame = CGRectMake(40, 60 ,screenWidth-80,screenHeight-450);
     }
     if(screenHeight==1024.000000)
     {
     table.frame = CGRectMake(40, 45, screenWidth-80,screenHeight-500);
     }
     else if(screenHeight==568.000000)
     {
     table.frame = CGRectMake(40, 25, screenWidth-80,screenHeight-40);
     }
     else if(screenHeight==480.000000)
     {
     table.frame = CGRectMake(40, 15, screenWidth-80,screenHeight-40);
     }
     else if(screenHeight == 736)
     {
     table.frame = CGRectMake(40, 20, screenWidth-80, screenHeight-240);
     }
     else if(screenHeight == 667)
     {
     table.frame = CGRectMake(40, 20, screenSize.width-80, screenHeight-120);
     }
     else if(screenHeight==1366.000)
     {
     table.frame = CGRectMake(40, 50, screenWidth-80,screenHeight-600);
     }
     */
    
    
    x = table.frame.origin.x;
    y = table.frame.origin.y;
    
    [self setTheme];
    database = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).database;
    self.navigationController.navigationBarHidden = NO;
    arrayItems=[[NSMutableArray alloc]init];
    /* scroll.delegate=self;
     scroll.maximumZoomScale=4.0;
     scroll.minimumZoomScale=1.0;*/
    if(arrayData.count==0)
    {
        buttonNext.hidden=TRUE;
        buttonPrev.hidden=TRUE;
        [self.view removeGestureRecognizer:leftSwipGesture];
        [self.view removeGestureRecognizer:rightSwipeGesture];
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"refresh" object:Nil];
    HUD=[[MBProgressHUD alloc]initWithView:self.tabBarController.view];
    [self.tabBarController.view addSubview:HUD];
    HUD.delegate=self;
    [self setSliderMenu];
    NSString *plistPath=[[NSBundle mainBundle] pathForResource:@"Baani" ofType:@"plist"];
    dict=[NSDictionary dictionaryWithContentsOfFile:plistPath];
    [self fetchData];
    if(arrayBaani.count!=0)
    {
        if(arrayIndex!=[arrayBaani count]-1)
        {
            buttonPrev.hidden=FALSE;
            buttonNext.hidden=FALSE;
            [self.view addGestureRecognizer:leftSwipGesture];
            [self.view addGestureRecognizer:rightSwipeGesture];
            
        }
        if(arrayIndex==0)
        {
            buttonPrev.hidden=TRUE;
            buttonNext.hidden=FALSE;
            [self.view removeGestureRecognizer:rightSwipeGesture];
            [self.view addGestureRecognizer:leftSwipGesture];
        }
        else if(arrayIndex==[arrayBaani count]-1)
        {
            buttonNext.hidden=TRUE;
            buttonPrev.hidden=FALSE;
            [self.view addGestureRecognizer:rightSwipeGesture];
            [self.view removeGestureRecognizer:leftSwipGesture];
        }
    }
    else
    {
        buttonNext.hidden=TRUE;
        buttonPrev.hidden=TRUE;
        [self.view removeGestureRecognizer:leftSwipGesture];
        [self.view removeGestureRecognizer:rightSwipeGesture];
    }
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandler:)];
    tapGesture.numberOfTapsRequired = 1;
    [table addGestureRecognizer:tapGesture];
    
    arrayNames = [NSMutableArray new];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    bookMarkPages = [[BookMarkPages alloc] initWithEntity:[NSEntityDescription entityForName:@"BookMarkPages" inManagedObjectContext:managedObjectContext] insertIntoManagedObjectContext:managedObjectContext];
}
-(void)fetchData
{
    NSLog(@"root cladd :%@",rootClass);
    NSLog(@"Key : %@",key);
    NSLog(@"index :%d",arrayIndex);
    NSLog(@"strFav :%@",strFavour);
    NSLog(@"arrBaani :%@",arrayBaani);
    if([rootClass isEqualToString:@"ISGBaniViewController"])
    {
        arrayData=[dict objectForKey:key];
    }
    else
    {
        if([strFavour isEqualToString:@"fav"]) arrayBaani=[[[dict objectForKey:key]allKeys]sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)].mutableCopy;
    }
    
    // NSLog(@"check : %@",[[[dict objectForKey:key]allKeys]sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]);
    if(arrayBaani.count!=0)arrayData=[[dict objectForKey:key]objectForKey:[arrayBaani objectAtIndex:arrayIndex]];
    // else arrayData=[[dict objectForKey:key]objectForKey:[arrayBaani objectAtIndex:arrayIndex]];
    
    NSString *str=[arrayBaani objectAtIndex:arrayIndex];
    NSArray *arr=[str componentsSeparatedByString:@"_"];
    if(arrayBaani)
        self.title=[arr objectAtIndex:1];
    else self.title=bani;
    
    // NSLog(@"arrayyy %@",arrayData);
    // NSLog(@"switch :%d",arrayData.count);
    switch (arrayData.count) {
        case 2:[self getData:[arrayData objectAtIndex:0]:[arrayData objectAtIndex:1]];
            break;
        case 4: [self getData1:[arrayData objectAtIndex:0] :[arrayData objectAtIndex:1] :[arrayData objectAtIndex:2] :[arrayData objectAtIndex:3]];
            break;
        case 6: [self getData3:arrayData];
            break;
        case 10: [self getData2:arrayData];
            break;
        default:
            [self getData4:arrayData];
            break;
    }
    [table reloadData];
}
-(void)setSliderMenu
{
    int x1 = 0;
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) x1=20;
    UIView *twitterItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+x1, 40+x1)];
    [twitterItem setMenuActionWithBlock:^{
        [self twitterShare];
        [sideMenu close];
    }];
    UIImageView *twitterIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+x1, 40+x1)];
    [twitterIcon setImage:[UIImage imageNamed:@"tw.png"]];
    [twitterItem addSubview:twitterIcon];
    
    UIView *emailItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+x1, 40+x1)];
    [emailItem setMenuActionWithBlock:^{
        [self emailShare];
        [sideMenu close];
    }];
    UIImageView *emailIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+x1 , 40+x1)];
    [emailIcon setImage:[UIImage imageNamed:@"mail.png"]];
    [emailItem addSubview:emailIcon];
    
    UIView *facebookItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+x1, 40+x1)];
    [facebookItem setMenuActionWithBlock:^{
        [self facebookShare];
        [sideMenu close];
        
    }];
    UIImageView *facebookIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+x1, 36+x1)];
    [facebookIcon setImage:[UIImage imageNamed:@"fb.png"]];
    [facebookItem addSubview:facebookIcon];
    
    UIView *browserItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+x1, 40+x1)];
    [browserItem setMenuActionWithBlock:^{
        [sideMenu close];
    }];
    UIImageView *browserIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+x1, 40+x1)];
    [browserIcon setImage:[UIImage imageNamed:@"closeButton.png"]];
    [browserItem addSubview:browserIcon];
    
    UIView *messageItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+x1, 37+x1)];
    [messageItem setMenuActionWithBlock:^{
        NSLog(@"mms item");
        [self sendMms];
        [sideMenu close];
    }];
    UIImageView *messageIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+x1, 36+x1)];
    [messageIcon setImage:[UIImage imageNamed:@"mmsButton.png"]];
    [messageItem addSubview:messageIcon];
    
    sideMenu = [[HMSideMenu alloc] initWithItems:@[twitterItem, emailItem, facebookItem ,messageItem,browserItem]];
    [sideMenu setItemSpacing:5.0f];
    [self.view addSubview:sideMenu];
}

//-(void)showFbView
//{
//    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//
//        //[controller setInitialText:@"First post from my iPhone app"];
//        [controller addImage:attachmentImage];
//        [self presentViewController:controller animated:YES completion:Nil];
//    }
//}

-(IBAction)btnProceed:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.persistent = YES;
    pasteboard.image =attachmentImage;
    NSString *phoneToCall = @"sms:";
    NSString *phoneToCallEncoded = [phoneToCall stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:phoneToCallEncoded];
    [[UIApplication sharedApplication] openURL:url];
    
}
-(void)sendMms
{
    [[KGModal sharedInstance]showWithContentView:popView andAnimated:YES];
}

-(void)setTheme
{
    [arrayLanguages removeAllObjects];
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    ISGTheme *theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    stringBackgroud=theme.background;
    fontSize=[theme.fontSize intValue];
    fontStyle=theme.fontStyle;
    
    language1=theme.lang1;
    language2=theme.lang2;
    language3=theme.lang3;
    language4=theme.lang4;
    language5=theme.lang5;
    language6=theme.lang6;
    //    language7=theme.lang7;
    
    gurmukhiColor=theme.gurmukhiColor;
    romanColor=theme.romanColor;
    hindiColor=theme.hindiColor;
    englishColor=theme.englishColor;
    teekaColor=theme.teekaColor;
    attributeColor=theme.attributeColor;
    
    gurmukhiSize=[theme.gurmukhiSize intValue];
    romanSize=[theme.romanSize intValue];
    hindiSize=[theme.hindiSize intValue];
    englishSize=[theme.englishSize intValue];
    teekaSize=[theme.teekaSize intValue];
    attributeSize=[theme.attributeSize intValue];
    
    if(language1.length>0) [arrayLanguages addObject:language1];
    if(language2.length>0) [arrayLanguages addObject:language2];
    if(language3.length>0) [arrayLanguages addObject:language3];
    if(language4.length>0) [arrayLanguages addObject:language4];
    if(language5.length>0) [arrayLanguages addObject:language5];
    if(language6.length>0) [arrayLanguages addObject:language6];
    if(language7.length>0) [arrayLanguages addObject:language7];
    
    if(fontSize==0)fontSize=14;
    if(stringBackgroud.length<=7) stringBackgroud=@"LightPattern";
    
    if (!theme.backgroundImage)
    {
        [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
    }
    else
        imageBg.image = theme.backgroundImage;
    
}

- (void)tapGestureHandler:(UITapGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:table];
    NSIndexPath *indexPath = [table indexPathForRowAtPoint:p];
    UITableViewCell *selectedCell=(UITableViewCell *)[table cellForRowAtIndexPath:indexPath];
    ISGDataModel *data = (ISGDataModel *)[arrayItems objectAtIndex:indexPath.row];
    shabd=[data.shabd intValue];
    dataID=[data.dataID intValue];
    sggsData=data.gurmukhi;
    strShabd=data.shabd;
    strId=data.dataID;
    
    if([stringBackgroud isEqualToString:@"LightPattern"])
    {
        selectedCell.backgroundColor=[UIColor yellowColor];
    }
    else if([stringBackgroud isEqualToString:@"WhitePattern"])
    {
        selectedCell.backgroundColor=[UIColor whiteColor];
    }
    else if([stringBackgroud isEqualToString:@"BlackPattern"])
    {
        selectedCell.backgroundColor=[UIColor blackColor];
    }
    else if([stringBackgroud isEqualToString:@"DarkPattern"])
    {
        selectedCell.backgroundColor=[UIColor redColor];
    }
    
    CGFloat wd;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        wd = 290;
    }
    else
    {
        wd = 680;
    }
    
    UIGraphicsBeginImageContext(CGSizeMake(wd, selectedCell.frame.size.height));
    CGRect tableViewFrame = selectedCell.frame;
    selectedCell.frame = CGRectMake(0.0, 0.0, selectedCell.frame.size.width, table.contentSize.height);
    selectedCell.backgroundColor = [UIColor clearColor];
    [selectedCell.layer renderInContext:UIGraphicsGetCurrentContext()];
    attachmentImage = UIGraphicsGetImageFromCurrentImageContext();
    
    selectedCell.frame = tableViewFrame;
    UIGraphicsEndImageContext();
    selectedCell.backgroundColor=[UIColor clearColor];
    
    
    bookMarkPages.pageNo = [NSString stringWithFormat:@"%d",page];
    bookMarkPages.scriptID = data.script;
    bookMarkPages.cellImage = UIImageJPEGRepresentation(attachmentImage, 1.0);
    bookMarkPages.query = queryExecuted;
    bookMarkPages.title = self.title;
    
    
    /*  UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Options" message:Nil delegate:self cancelButtonTitle:Nil otherButtonTitles:@"Add to favourites",@"Share",@"Print this page",@"Cancel" ,nil];
     [alert show];
     alert.tag=3;*/
    
    
    /* metest    UIView *customAlert = [[NSBundle mainBundle] loadNibNamed:@"ISGCustomAlert" owner:self options:nil][0];
     [[KGModal sharedInstance] showWithContentView:customAlert andAnimated:YES]; metest*/
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Ipad" bundle:nil];
        ISGCustomAlertViewController *customAlertVC = [storyboard instantiateViewControllerWithIdentifier:@"ISGCustomAlertViewController"];
        customAlertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        customAlertVC.modalTransitionStyle   = UIModalTransitionStyleCrossDissolve;
        customAlertVC.heading = self.title;
        customAlertVC.pageNo = [NSString stringWithFormat:@"%d",page];
        customAlertVC.scriptID = data.script;
        customAlertVC.cellImage = UIImageJPEGRepresentation(attachmentImage, 1.0);
        customAlertVC.query = queryExecuted;
        customAlertVC.imageData = [ScrollViewToPDF pdfDataOfScrollView:table];
        customAlertVC.imageSMS = attachmentImage;
        
        [self presentViewController:customAlertVC animated:YES completion:nil];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        ISGCustomAlertViewController *customAlertVC = [storyboard instantiateViewControllerWithIdentifier:@"ISGCustomAlertViewController"];
        customAlertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        customAlertVC.modalTransitionStyle   = UIModalTransitionStyleCrossDissolve;
        customAlertVC.heading = self.title;
        customAlertVC.pageNo = [NSString stringWithFormat:@"%d",page];
        customAlertVC.scriptID = data.script;
        customAlertVC.cellImage = UIImageJPEGRepresentation(attachmentImage, 1.0);
        customAlertVC.query = queryExecuted;
        customAlertVC.imageData = [ScrollViewToPDF pdfDataOfScrollView:table];
        customAlertVC.imageSMS = attachmentImage;
        
        [self presentViewController:customAlertVC animated:YES completion:nil];
    }
    
    
    
    
    
    
    
}

-(void)longPressHandler:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
    {
        UILabel * copyLabel = (UILabel *)gestureRecognizer.view;
        copyLabel.backgroundColor = [UIColor blueColor];
        [copyLabel becomeFirstResponder];
        CGPoint point = [gestureRecognizer locationInView:copyLabel];
        UIMenuController * menu = [UIMenuController sharedMenuController];
        [menu setTargetRect:CGRectMake(point.x, point.y, 0.0f, 0.0F) inView:copyLabel];
        message = copyLabel.text;
        UIMenuItem * copy = [[UIMenuItem alloc] initWithTitle:@"copy" action:@selector(copyMessage)];
        menu.menuItems = @[copy];
        [menu setMenuVisible:YES animated:YES];
        menu.arrowDirection = UIMenuControllerArrowUp;
    }
}

- (void)copyMessage
{
    UIPasteboard * clipBoard = [UIPasteboard generalPasteboard];
    clipBoard.string = message;
}

-(BOOL)canBecomeFirstResponder
{
    return YES;
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    return (action == @selector(copyMessage));
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==2 && buttonIndex==0)
    {
        //[self insertBookmark];
        [self showBookMarks];
    }
    else if(alertView.tag==3 && buttonIndex==0)
    {
        UIAlertView *alertBookmark = [[UIAlertView alloc] initWithTitle:@"Are you sure to add this to your favourites?"
                                                                message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
        UITextField *txtAlert = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 80.0, 260.0, 30.0)];
        [txtAlert setBackgroundColor:[UIColor whiteColor]];
        txtAlert.text=sggsData;
        txtAlert.font=[UIFont fontWithName:@"GurbaniLipiBold" size:22];
        txtAlert.userInteractionEnabled=NO;
        // [alertBookmark addSubview:txtAlert];
        [alertBookmark show];
        alertBookmark.tag=2;
    }
    else if(alertView.tag==3 && buttonIndex==1)
    {
        [sideMenu open];
    }
    else if(alertView.tag==3 && buttonIndex==2)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            //imageForPrint = [self captureView:table];
            NSData *imageData = [ScrollViewToPDF pdfDataOfScrollView:table]; //UIImagePNGRepresentation(imageForPrint);
            [self printItem:imageData];
        });
    }
    else if (alertView.tag == 100 && buttonIndex == 1)
    {
        UITextField * textField = [alertView textFieldAtIndex:0];
        if (textField.text.length == 0)
        {
            [[[UIAlertView alloc] initWithTitle:@"Invalid text" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        else
        {
            [BookMarkFolders addBookMarkFolderWithName:textField.text index:(int)arrayNames.count];
            [self showBookMarks];
        }
    }
    else if(alertView.tag==2 && buttonIndex==0)
    {
        [self insertBookmark];
    }
}

- (UIImage *)captureView:(UIScrollView *)view
{
    UIImage* image = nil;
    
    CGPoint savedContentOffset = view.contentOffset;
    CGRect savedFrame = view.frame;
    
    UIGraphicsBeginImageContextWithOptions(view.contentSize, 0, 0);
    //UIGraphicsBeginImageContext(view.contentSize);
    view.contentOffset = CGPointZero;
    view.frame = CGRectMake(0, 0, view.contentSize.width, view.contentSize.height);
    
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    view.contentOffset = savedContentOffset;
    view.frame = savedFrame;
    
    UIGraphicsEndImageContext();
    
    // after all of this, crop image to needed size
    return image;
}

- (void)showBookMarks
{
    arrayFolders = [BookMarkFolders fetchAllBookMarkFolders];
    [arrayNames removeAllObjects];
    [arrayFolders enumerateObjectsUsingBlock:^(BookMarkFolders * obj, NSUInteger idx, BOOL *stop)
     {
         [arrayNames addObject:obj.folderName];
     }];
    if (arrayNames.count == 0)
    {
        [self createFolder];
    }
    else
    {
        UIView * kgView = [[UIView alloc] initWithFrame:CGRectMake(40, 100, 240, 200)];
        tableList = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kgView.frame.size.width, kgView.frame.size.height - 40) style:UITableViewStylePlain];
        tableList.delegate = self;
        tableList.dataSource = self;
        tableList.backgroundColor = [UIColor clearColor];
        [kgView addSubview:tableList];
        
        UIButton * buttonCreateFolder = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(tableList.frame), kgView.frame.size.width, 40)];
        [buttonCreateFolder setTitle:@"Create folder" forState:UIControlStateNormal];
        [buttonCreateFolder setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [buttonCreateFolder setBackgroundColor:[UIColor whiteColor]];
        [buttonCreateFolder addTarget:self action:@selector(createFolder) forControlEvents:UIControlEventTouchUpInside];
        
        [kgView addSubview:buttonCreateFolder];
        
        [[KGModal sharedInstance] showWithContentView:kgView andAnimated:YES];
    }
}

-(void)createFolder
{
    [[KGModal sharedInstance] hideAnimated:YES];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"No BookMark Folders" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Create", nil];
    alert.tag = 100;
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) [self facebookShare];
    else if (buttonIndex==1) [self twitterShare];
    else if (buttonIndex==2) [self emailShare];
}

-(void)emailShare
{
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailCompose = [[MFMailComposeViewController alloc] init];
        mailCompose.mailComposeDelegate = self;
        NSData *data = [NSData dataWithData:UIImageJPEGRepresentation(attachmentImage, 1.0)];
        [mailCompose setSubject:@"iSearchGurbani share"];
        [mailCompose addAttachmentData:data
                              mimeType:@"jpg"
                              fileName:@"iSearchGurbani.jpg"];
        [self presentViewController:mailCompose animated:YES completion:Nil];
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"Please configure a mail account in your device and try again later" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    NSString *message=@"Your email has been discarded";
    switch (result) {
            
        case MFMailComposeResultFailed:
            message=@"Your email has failed to send";
            break;
            
        case MFMailComposeResultSent :
            message=@"Your email has sent Successfully";
            break;
            
        case MFMailComposeResultCancelled :
            NSLog(@"cancelled");
            break;
            
        case MFMailComposeResultSaved :
            message=@"Your email has been saved to draft";
            break;
        default:
            break;
    }
    [[[UIAlertView alloc]initWithTitle:@"Mail Status" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
    [self dismissViewControllerAnimated:YES completion:Nil];
}

-(void)facebookShare
{
    HUD.labelText=@"Sharing...";
    [HUD show:YES];
    //    appDelegate = (ISGAppDelegate *)[[UIApplication sharedApplication] delegate] ;
    //    if (!appDelegate.session)
    //    {
    //        NSLog(@"enterd");
    //        appDelegate.session = [[FBSession alloc]initWithPermissions:[NSArray arrayWithObjects:@"publish_stream",nil]];
    //    }
    //    if (![appDelegate.session isOpen])
    //    {
    //        NSLog(@"open");
    //        [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
    //         {
    //             if (!error)
    //             {
    //                 [self fbShare];
    //             }
    //         }];
    //    }
    //    else{
    [self fbShare];
    //    }
    
}
-(void) fbShare
{
    //    [FBSession setActiveSession:appDelegate.session];
    //    NSData *dataImage=UIImageJPEGRepresentation(attachmentImage,0.5);
    
    //    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    //    content.contentURL = [NSURL URLWithString:@"http://www.searchgurbani.com/"];
    //    content.contentTitle = @"iSearchGurbani";
    //    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
    
    SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [mySLComposerSheet setInitialText:@"iSearchGurbaani"];
    
    [mySLComposerSheet addImage:attachmentImage];
    
    [mySLComposerSheet addURL:[NSURL URLWithString:@"http://www.searchgurbani.com/"]];
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sharing canceled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                break;
            case SLComposeViewControllerResultDone:
                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sucessfully shared to facebook" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                break;
                
            default:
                break;
        }
    }];
    
    [self presentViewController:mySLComposerSheet animated:YES completion:^{
        [HUD hide:YES];
    }];
    //    }
    //    else
    //    {
    //        [HUD hide:YES];
    //    }
    
    //    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    //    photo.image = attachmentImage;
    //    photo.userGenerated = YES;
    //    photo.caption = @"http://www.searchgurbani.com/";
    //    FBSDKSharePhotoContent *content1 = [[FBSDKSharePhotoContent alloc] init];
    //    content1.photos = @[photo];
    //
    ////    [FBSDKShareDialog showFromViewController:self withContent:content1 delegate:self];
    //    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc]init];
    //    dialog.fromViewController = self;
    //    dialog.delegate = self;
    //    dialog.shareContent = content1;
    //    dialog.mode = FBSDKShareDialogModeBrowser;
    //    [dialog show];
    
    
    //    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    //    [parameters setObject:@"iSearchGurbani" forKey:@"name"];
    //    [parameters setObject:@"http://www.searchgurbani.com/" forKey:@"caption"];
    ////    [parameters setObject:[UIImage imageWithData:dataImage] forKey:@"picture"];
    //
    //    [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession] parameters:parameters handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
    //        [HUD hide:YES];
    //        if(error == nil)
    //        {
    //            if(result != FBWebDialogResultDialogNotCompleted && resultURL.query != nil)
    //            {
    //                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sucessfully shared to facebook" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    //            }
    //            else
    //            {
    //                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sharing canceled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    //            }
    //        }
    //        else
    //        {
    //            [[[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    //        }
    //    }];
    //    FBRequest *request=[FBRequest requestWithGraphPath:@"me/photos" parameters:parameters HTTPMethod:@"POST"];
    //
    //    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    //     {
    //         [HUD hide:YES];
    //         if(!error)
    //         {
    //             [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sucessfully shared to facebook" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    //         }
    //         else
    //         {
    //             [[[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    //         }
    //     }];
}

-(void)twitterShare
{
    //    if([SLComposeViewController instanceMethodForSelector:@selector(isAvailableForServiceType)] != nil)
    //    {
    //        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    //        {
    //            SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    //            //[composeViewController setInitialText:@"hai"];
    //            [composeViewController addImage:attachmentImage];
    //            composeViewController.completionHandler=^(TWTweetComposeViewControllerResult result){
    //                if(result==SLComposeViewControllerResultDone)
    //                {
    //                    [[[UIAlertView alloc]initWithTitle:@"Success" message:@"The Tweet was posted successfully" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    //                }
    //            };
    //
    //            [self presentViewController:composeViewController animated:YES completion:nil];
    //        }
    //        else
    //        {
    //            [[[UIAlertView alloc]initWithTitle:@"Sorry" message:@"Twitter is only supported in iOS5 or above" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    //        }
    //    }
    TWTweetComposeViewController *tweetView = [[TWTweetComposeViewController alloc] init];
    
    TWTweetComposeViewControllerCompletionHandler
    completionHandler =
    ^(TWTweetComposeViewControllerResult result) {
        
        if(result == TWTweetComposeViewControllerResultDone) {
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"The Tweet was posted successfully." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        if(result == TWTweetComposeViewControllerResultCancelled) {
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You cancelled posting the Tweet." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    };
    [tweetView setCompletionHandler:completionHandler];
    
    //[tweetView setInitialText:@"Try this App: Design Studio"];
    [tweetView addImage:attachmentImage];
    //[tweetView addURL:url];
    [self presentViewController:tweetView animated:YES completion:nil];
}


-(void)insertBookmark
{
    [database open];
    NSLog(@"sggs data :%@",sggsData);
    NSLog(@"bani :%@",bani);
    if([rootClass isEqualToString:@"ISGBaniViewController"])
    {
        [database executeUpdate:@"INSERT INTO bookmark (markText,scrpt,indexValue,key,tab) VALUES (?,?,?,?,'Baani2');",sggsData,bani,[NSString stringWithFormat:@"%d",arrayIndex],key];
    }
    else
        
    {
        [database executeUpdate:@"INSERT INTO bookmark (markText,scrpt,indexValue,key,tab) VALUES (?,?,?,?,'Baani1');",sggsData,bani,[NSString stringWithFormat:@"%d",arrayIndex],key];
    }
    //    NSLog(@"%@",[database lastErrorMessage]);
    [database close];
    [SVProgressHUD showSuccessWithStatus:@"Bookmarked successfully"];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"favourites" object:Nil];
}

-(void)SetDefaultValues
{
    stringBackgroud=@"LightPattern";
    fontSize=14;
    fontStyle=@"GurbaniLipiBold";
}
-(void)nextData
{
    if(arrayBaani.count!=0)
    {
        if(arrayIndex!=[arrayBaani count]-1)
        {
            buttonPrev.hidden=FALSE;
            buttonNext.hidden=FALSE;
            [self.view addGestureRecognizer:leftSwipGesture];
            [self.view addGestureRecognizer:rightSwipeGesture];
            arrayIndex++;
            [self fetchData];
        }
        if(arrayIndex==0)
        {
            buttonPrev.hidden=TRUE;
            buttonNext.hidden=FALSE;
            [self.view addGestureRecognizer:leftSwipGesture];
            [self.view removeGestureRecognizer:rightSwipeGesture];
        }
        else if(arrayIndex==[arrayBaani count]-1)
        {
            buttonNext.hidden=TRUE;
            buttonPrev.hidden=FALSE;
            [self.view removeGestureRecognizer:leftSwipGesture];
            [self.view addGestureRecognizer:rightSwipeGesture];
        }
    }
    else
    {
        buttonNext.hidden=TRUE;
        buttonPrev.hidden=TRUE;
        [self.view removeGestureRecognizer:leftSwipGesture];
        [self.view removeGestureRecognizer:rightSwipeGesture];
    }
    
}
-(void)previousData
{
    if(arrayBaani.count!=0)
    {
        if(arrayIndex!=0)
        {
            buttonNext.hidden=FALSE;
            buttonPrev.hidden=FALSE;
            [self.view addGestureRecognizer:leftSwipGesture];
            [self.view addGestureRecognizer:rightSwipeGesture];
            arrayIndex--;
            [self fetchData];
        }
        if(arrayIndex==0)
        {
            buttonPrev.hidden=TRUE;
            buttonNext.hidden=FALSE;
            [self.view addGestureRecognizer:leftSwipGesture];
            [self.view removeGestureRecognizer:rightSwipeGesture];
        }
        else if(arrayIndex==[arrayBaani count]-1)
        {
            buttonPrev.hidden=FALSE;
            buttonNext.hidden=TRUE;
            [self.view removeGestureRecognizer:leftSwipGesture];
            [self.view addGestureRecognizer:rightSwipeGesture];
        }
    }
    else
    {
        buttonNext.hidden=TRUE;
        buttonPrev.hidden=TRUE;
        [self.view removeGestureRecognizer:leftSwipGesture];
        [self.view removeGestureRecognizer:rightSwipeGesture];
    }
}
-(IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender
{
    if(arrayData.count>0)
    {
        // table.frame = CGRectMake(table.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
        [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
            // table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
        } completion:^(BOOL finished) {
            table.alpha = 1.0;
        }];
        
        [self nextData];
    }
    
    
}
-(IBAction)swipeRight:(UISwipeGestureRecognizer *)sender
{
    if(arrayData.count>0)
    {
        //table.frame = CGRectMake(x- self.view.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
        [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
            // table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
        } completion:^(BOOL finished) {
            table.alpha = 1.0;
        }];
        [self previousData];
    }
}
-(IBAction)btnNext:(id)sender
{
    // table.frame = CGRectMake(table.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
        //table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    } completion:^(BOOL finished) {
        table.alpha = 1.0;
    }];
    
    [self nextData];
}
-(IBAction)btnPrev:(id)sender
{
    //table.frame = CGRectMake(x- self.view.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
        // table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    } completion:^(BOOL finished) {
        table.alpha = 1.0;
    }];
    [self previousData];
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return table;
}
-(void)refresh:(NSNotification *)notification
{
    [self setTheme];
    [self buildCells];
    [tableBaani reloadData];
}
-(void)getData2:(NSArray *)array
{
    [database open];
    NSString *querry=[NSString stringWithFormat:@"SELECT ID,shabd,gurmukhi,roman,english,roman,attributes,translit,punctuation,scrpt,teeka,page FROM gurbani where ID >='%@' AND ID<='%@' OR ID >='%@' AND ID<='%@' OR ID >='%@' AND ID<='%@' OR ID >='%@' AND ID<='%@' OR ID >='%@' AND ID<='%@'",[array objectAtIndex:0],[array objectAtIndex:1],[array objectAtIndex:2],[array objectAtIndex:3],[array objectAtIndex:4],[array objectAtIndex:5],[array objectAtIndex:6],[array objectAtIndex:7],[array objectAtIndex:8],[array objectAtIndex:9]];
    NSLog(@"%@",querry);
    queryExecuted = querry;
    FMResultSet *results = [database executeQuery:querry];
    [arrayItems removeAllObjects];
    while([results next])
    {
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithShabd:results];
        [arrayItems addObject:dataModel];
    }
    [database close];
    [self buildCells];
    [table reloadData];
}


-(void)getData3:(NSArray *)array
{
    [database open];
    NSString *querry=[NSString stringWithFormat:@"SELECT ID,shabd,gurmukhi,roman,english,roman,attributes,translit,punctuation,scrpt,teeka,page FROM gurbani where ID >='%@' AND ID<='%@' OR ID >='%@' AND ID<='%@' OR ID >='%@' AND ID<='%@'",[array objectAtIndex:0],[array objectAtIndex:1],[array objectAtIndex:2],[array objectAtIndex:3],[array objectAtIndex:4],[array objectAtIndex:5]];
    NSLog(@"%@",querry);
    queryExecuted = querry;
    FMResultSet *results = [database executeQuery:querry];
    [arrayItems removeAllObjects];
    while([results next])
    {
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithShabd:results];
        [arrayItems addObject:dataModel];
    }
    [database close];
    [self buildCells];
    [table reloadData];
}
-(void)getData4:(NSArray *)array
{
    [database open];
    //     ID >='%@' AND ID<='%@' OR ID >='%@' AND ID<='%@' OR ID >='%@' AND ID<='%@'"
    NSString *querry=[NSString stringWithFormat:@"SELECT ID,shabd,gurmukhi,roman,english,roman,attributes,translit,punctuation,scrpt,teeka,page FROM gurbani where"];
    int i = 0;
    while (i < array.count) {
        if(i == 0)
        {
            querry = [NSString stringWithFormat:@"%@ ID >= '%@' AND ID <= '%@'", querry, array[i], array[i+1]];
            i = i+2;
        }
        else{
            querry = [NSString stringWithFormat:@"%@ OR ID >= '%@' AND ID <= '%@'", querry, array[i], array[i+1]];
            i = i+2;
        }
    }
    NSLog(@"%@",querry);
    queryExecuted = querry;
    FMResultSet *results = [database executeQuery:querry];
    [arrayItems removeAllObjects];
    while([results next])
    {
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithShabd:results];
        [arrayItems addObject:dataModel];
    }
    [database close];
    [self buildCells];
    [table reloadData];
}

-(void)getData1:(NSString *)first:(NSString *)second:(NSString *)third:(NSString *)fourth
{
    [database open];
    NSString *querry=[NSString stringWithFormat:@"SELECT ID,shabd,gurmukhi,roman,english,roman,attributes,translit,punctuation,scrpt,teeka,page FROM gurbani where ID >='%@' AND ID<='%@' OR ID >='%@' AND ID<='%@'",first,second,third,fourth];
    NSLog(@"%@",querry);
    queryExecuted = querry;
    FMResultSet *results = [database executeQuery:querry];
    [arrayItems removeAllObjects];
    while([results next])
    {
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithShabd:results];
        [arrayItems addObject:dataModel];
        
    }
    [database close];
    [self buildCells];
    [table reloadData];
}

-(void)getData:(NSString *)first:(NSString *)last
{
    [database open];
    NSString *querry=[NSString stringWithFormat:@"SELECT ID,shabd,gurmukhi,roman,english,roman,attributes,translit,punctuation,scrpt,teeka,page FROM gurbani where ID >='%@' AND ID<='%@'",first,last];
    NSLog(@"%@",querry);
    queryExecuted = querry;
    FMResultSet *results = [database executeQuery:querry];
    NSLog(@"FMResultSet %@",results);
    [arrayItems removeAllObjects];
    while([results next])
    {
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithShabd:results];
        [arrayItems addObject:dataModel];
        
    }
    [database close];
    [self buildCells];
    [table reloadData];
    
}
- (void)buildCells {
    // clear existing data
    if (cells) [cells removeAllObjects]; else cells = [[NSMutableArray alloc] init];
    if (heightsCell) [heightsCell removeAllObjects]; else heightsCell = [[NSMutableArray alloc] init];
    NSMutableArray *arrayLabels;
    for (ISGDataModel *data in arrayItems) {
        // NSLog(@"page :%@",data.page);
        if (arrayLabels) [arrayLabels removeAllObjects]; else arrayLabels = [[NSMutableArray alloc] init];
        for(int index=1;index<=arrayLanguages.count;index++)
        {
            UILabel *label = [[UILabel alloc] init];
            
            if (![[arrayLanguages objectAtIndex:index-1] isEqualToString:@"lareevar"])
            {
                [label setText:[data valueForKey:[arrayLanguages objectAtIndex:index-1]]];
            }
            
            label.textColor = [UIColor whiteColor];
            // setting font and size
            if(index==1 && [[arrayLanguages objectAtIndex:index-1] isEqualToString:@"gurmukhi"])
            {
                [label setFont:[UIFont fontWithName:fontStyle size:gurmukhiSize]];
                label.textColor = gurmukhiColor;
            }
            else if (index!=1 && [[arrayLanguages objectAtIndex:index-1] isEqualToString:@"gurmukhi"])
            {
                [label setFont:[UIFont fontWithName:@"Gurbanihindi" size:hindiSize]];
                label.textColor = hindiColor;
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"teeka"])
            {
                [label setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:teekaSize]];
                label.textColor = teekaColor;
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"attributes"])
            {
                [label setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:attributeSize]];
                label.textColor = attributeColor;
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"english"])
            {
                label.font=[UIFont boldSystemFontOfSize:englishSize];
                label.textColor = englishColor;
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"roman"])
            {
                label.font=[UIFont boldSystemFontOfSize:romanSize];
                //label.font = [UIFont fontWithName:@"Rosemary-Roman" size:romanSize];
                label.textColor = romanColor;
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"punctuation"])
            {
                [label setFont:[UIFont fontWithName:fontStyle size:gurmukhiSize]];
                label.textColor = gurmukhiColor;
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"translit"])
            {
                [label setFont:[UIFont boldSystemFontOfSize:romanSize]];
                //label.font = [UIFont fontWithName:@"Rosemary-Roman" size:romanSize];
                [label setTextColor:romanColor];
                
            }
            if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"lareevar"])
            {
                NSString * str = [data valueForKey:@"gurmukhi"];
                NSString * lareevar = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
                [label setFont:[UIFont fontWithName:fontStyle size:gurmukhiSize]];
                [label setTextColor:gurmukhiColor];
                label.text = lareevar;
            }
            
            lpHandler = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressHandler:)];
            lpHandler.minimumPressDuration = 0.5;
            label.userInteractionEnabled = YES;
            [label addGestureRecognizer:lpHandler];
            
            [arrayLabels addObject:label];
        }
        static NSString *cellIdentifier = @"ISGCell";
        UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            
            cell = [[ISGCell alloc] initWithLabels:arrayLabels];
        }
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell.layer setMasksToBounds:YES];
        
        [cells addObject:cell];
    }
    table.separatorColor=[UIColor grayColor];
}


#pragma mark tableview delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tableList)
    {
        return arrayNames.count;
    }
    else
    {
        [tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        return cells.count;
    }
}
-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tableList)
    {
        UITableViewCell * cellList = [[UITableViewCell alloc] initWithStyle:UITableViewStylePlain reuseIdentifier:@"myCell"];
        if (cellList == nil)
        {
            cellList = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
        }
        cellList.textLabel.text = arrayNames[indexPath.row];
        cellList.imageView.image = [UIImage imageNamed:@"folders"];
        return cellList;
    }
    else {
        
        return [cells objectAtIndex:indexPath.row];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tableList)
    {
        return 40;
    }
    else
    {
        ISGCell * cell = cells[indexPath.row];
        NSLog(@"cell:%f",cell.labelHeight);
        [cell layoutSubviews];
        
        return  cell.labelHeight + 24;
        //        [[heightsCell objectAtIndex:(int)[indexPath row]] intValue];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tableList)
    {
        BookMarkFolders * fol = arrayFolders[indexPath.row];
        NSSet * set = fol.relationshipToPages;
        NSArray * array = [NSArray arrayWithArray:set.allObjects];
        int cnt = 0;
        
        bookMarkPages.index = [NSString stringWithFormat:@"%lu",(unsigned long)array.count];
        
        for (BookMarkPages * obj in array)
        {
            if ([bookMarkPages.pageNo isEqualToString:obj.pageNo] && [bookMarkPages.scriptID isEqualToString:obj.scriptID] && [bookMarkPages.cellImage isEqualToData:obj.cellImage] && [bookMarkPages.query isEqualToString:obj.query])
            {
                cnt++;
            }
        }
        if (cnt > 0)
        {
            [SVProgressHUD showErrorWithStatus:@"Already added to the list"];
        }
        else
        {
            [BookMarkPages addBookMarkPage:bookMarkPages inBookMarkFolder:fol];
            [SVProgressHUD showSuccessWithStatus:@"Added"];
        }
        [[KGModal sharedInstance] hideAnimated:YES];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Printing
-(void)printItem :(NSData*)data {
    printController = [UIPrintInteractionController sharedPrintController];
    if(printController && [UIPrintInteractionController canPrintData:data]) {
        printController.delegate = self;
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [NSString stringWithFormat:@""];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        printController.printInfo = printInfo;
        printController.showsPageRange = YES;
        printController.printingItem = data;
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                //NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
            }
        };
        //[printController presentFromBarButtonItem:self.item animated:YES completionHandler:completionHandler];
        //[printController presentAnimated:YES completionHandler:completionHandler];
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            [printController presentFromRect:CGRectMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2, 0, 0) inView:self.view animated:YES completionHandler:completionHandler];
        }
        else
        {
            [printController presentAnimated:YES completionHandler:completionHandler];
        }
    }
}



@end
