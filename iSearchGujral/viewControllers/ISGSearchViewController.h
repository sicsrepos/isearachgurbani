//
//  ISGSearchViewController.h
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISGHelpViewController.h"
#import "FMDatabase.h"
#import "ISGDataModel.h"
#import "PKCustomKeyboard.h"
#import "PMCustomKeyboard.h"
#import "ISGSearchResultsViewController.h"
#import "MBProgressHUD.h"
@interface ISGSearchViewController : UIViewController<UITextFieldDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    int click,textLength,itration,searchFlag,filterFlag,cnt;
    ISGDataModel *dataModel;
    FMDatabase *database;
    IBOutlet UIImageView *imageCheck, *imageAdvancedSearchCheck;
    IBOutlet UITextField *txtSearch;
    IBOutlet UIButton *buttonCheck,*buttonSearch,*buttonHelp, *buttonAdvanceSearch, *buttonAuthor, *buttonRaag, *buttonScripture;
    IBOutlet UIButton *buttonCriteria,*buttonLanguage;
    NSMutableArray *arrayResults,*arrayLetterLenght;
    NSString *language,*englistFormString,*selectedCategory;
    BOOL wildCard,isAdvancedSearch;
    NSString *stringInput,*temp;
    NSMutableArray *arrayAdditionalResults,*arrayFilter;
    UITabBarItem *home, *explore, *Banis, *more;
    IBOutlet UILabel *labelPhrase,*labelcriteria,*labelLanguage;
    NSMutableArray *arrayAuthors, *arrayScriptures, *arrayRaags;
    BOOL searchMoreData;
    UIButton *buttonSelected;
    
    NSString *stringBackgroud;
    int fontSize;
    NSString *fontStyle;
    IBOutlet UIImageView *backgroundIng;
    

}
-(IBAction)btnCheck:(id)sender;
-(IBAction)btnCriteria:(id)sender;
-(IBAction)btnLanguage:(id)sender;
-(IBAction)btnHelp:(id)sender;
-(IBAction)btnSearch:(id)sender;
-(IBAction)keyClose:(id)sender;
-(IBAction)textChange:(id)sender;
-(IBAction)btnAuthor:(id)sender;
-(IBAction)btnRaag:(id)sender;
-(IBAction)btnScripture:(id)sender;
-(IBAction)btnAdvancedSearch:(id)sender;
@end
