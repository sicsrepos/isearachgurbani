//
//  ISGLanguageViewController.h
//  iSearchGujral
//
//  Created by mac on 4/26/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISGLanguageViewController : UIViewController
{
    IBOutlet UIButton *buttonPunctuation;
    IBOutlet UIButton *buttonTransliteration;
    IBOutlet UIButton *buttonRoman;
    IBOutlet UIButton *buttonGurumukhi;
    IBOutlet UIView *containerView;
    __weak IBOutlet UIButton *buttonHindi;
    __weak IBOutlet UIButton *buttonEnglish;
    __weak IBOutlet UIButton *buttonTeeka;
    __weak IBOutlet UIButton *buttonAttributes;
    __weak IBOutlet UIButton *buttonLareevar;
    IBOutlet UIImageView *imageBg;
    __weak IBOutlet UILabel *labelGurumukhi;
    __weak IBOutlet UILabel *labelPunctuation;
    __weak IBOutlet UILabel *labelTransliteration;
    __weak IBOutlet UILabel *labelRoman;
    __weak IBOutlet UILabel *labelHindi;
    __weak IBOutlet UILabel *labelEnglish;
    __weak IBOutlet UILabel *labelTeeks;
    __weak IBOutlet UILabel *labelAttributes;
    __weak IBOutlet UILabel *labelLareevar;
    UIImage * backGroundImage;
    NSString *background,*fontStyle,*fontSize,*language1,*language2,*language3,*language4,*language5,*language6,*strGurmukhiSize,*strRomanSize,*strHindiSize,*strEnglishSize,*strTeekaSize,*strAttributeSize;
    UIColor *gurmukhiColor,*romanColor,*hindiColor,*englishColor,*teekaColor,*attributeColor;
    __weak IBOutlet UIImageView *imgGurmukhi;
    __weak IBOutlet UIImageView *imgPunctuation;
    __weak IBOutlet UIImageView *imgLareevar;
    __weak IBOutlet UIImageView *imgAttributes;
    __weak IBOutlet UIImageView *imgTeeka;
    __weak IBOutlet UIImageView *imgEnglish;
    __weak IBOutlet UIImageView *imgHindi;
    __weak IBOutlet UIImageView *imgRoman;
    __weak IBOutlet UIImageView *imgTrans;
}
-(IBAction)btnPunctuation:(id)sender;
-(IBAction)btnTransliteration:(id)sender;
-(IBAction)btnRoman:(id)sender;
-(IBAction)btnGurumukhi:(id)sender;
-(IBAction)btnHindi:(id)sender;
-(IBAction)btnEnglish:(id)sender;
-(IBAction)btnTeeka:(id)sender;
-(IBAction)btnAttributes:(id)sender;
-(IBAction)btnLareevar:(id)sender;
@end
