//
//  ISGFontViewController.m
//  iSearchGujral
//
//  Created by mac on 4/26/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGFontViewController.h"
#import "ISGTheme.h"

@interface ISGFontViewController ()
{
    ISGTheme *theme;
    UIImage * backgroundImage;
}
@end

@implementation ISGFontViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.title=@"Fonts";
    [scroll setContentSize:CGSizeMake(320, 200)];
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    backgroundImage = theme.backgroundImage;
    stringBackgroud=theme.background;
    fontStyle=theme.fontStyle;
    language1=theme.lang1;
    language2=theme.lang2;
    language3=theme.lang3;
    language4=theme.lang4;
    language5=theme.lang5;
    language6=theme.lang6;
//    language7=theme.lang7;
    
    strGurmukhiSize=theme.gurmukhiSize;
    strRomanSize=theme.romanSize;
    strHindiSize=theme.hindiSize;
    strEnglishSize=theme.englishSize;
    strTeekaSize=theme.teekaSize;
    strAttributeSize=theme.attributeSize;
    
    gurmukhiColor = theme.gurmukhiColor;
    romanColor = theme.romanColor;
    hindiColor = theme.hindiColor;
    englishColor = theme.englishColor;
    teekaColor = theme.teekaColor;
    attributeColor = theme.attributeColor;
    
    gurmukhiSlider.value=[strGurmukhiSize floatValue];
    romanSlider.value=[strRomanSize floatValue];
    hindiSlider.value=[strHindiSize floatValue];
    englishSlider.value=[strEnglishSize floatValue];
    teekaSlider.value=[strTeekaSize floatValue];
    attributeSlider.value=[strAttributeSize floatValue];
    
    if(stringBackgroud.length<=7)
    {
        stringBackgroud=@"LightPattern";
    }
    
    if (!backgroundImage)
    {
        [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
    }
    else
        imageBg.image = backgroundImage;
    
    [self changeFontColor:stringBackgroud];
    int font=[theme.fontSize intValue];
       switch (font) {
        case 10:
        {
            [buttonSmall setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
            [buttonLarge setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
            [buttonMedium setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
            [buttonNormal setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        }
            break;
        case 15:
        {
            [buttonMedium setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
            [buttonSmall setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
            [buttonNormal setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
            [buttonLarge setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

        }
            break;
        case 17:
        {
            [buttonLarge setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
            [buttonSmall setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
            [buttonMedium setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
            [buttonNormal setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

        }
            break;
        default:
        {
            [buttonNormal setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
            [buttonSmall setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
            [buttonMedium setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
            [buttonLarge setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

        }
            break;
    }
    theme=[[ISGTheme alloc]init];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    labelPreview.text=@"";
}
-(void)changeFontColor:(NSString *)pattern
{
    
    [labelPreview setTextColor:gurmukhiColor];
    [labelGurumukhi setTextColor:gurmukhiColor];
    [labelRoman setTextColor:romanColor];
    [labelAttributes setTextColor:attributeColor];
    [labelEnglish setTextColor:englishColor];
    [labelHindi setTextColor:hindiColor];
    [labelTeeks setTextColor:teekaColor];
    
}


-(void)setFont
{
    theme.fontStyle=fontStyle;
    theme.background=stringBackgroud;
    theme.backgroundImage = backgroundImage;
    
    theme.lang1=language1;
    theme.lang2=language2;
    theme.lang3=language3;
    theme.lang4=language4;
    theme.lang5=language5;
    theme.lang6=language6;
//    theme.lang7=language7;
    
    theme.gurmukhiSize=strGurmukhiSize;
    theme.romanSize=strRomanSize;
    theme.hindiSize=strHindiSize;
    theme.englishSize=strEnglishSize;
    theme.teekaSize=strTeekaSize;
    theme.attributeSize=strAttributeSize;
    
    theme.gurmukhiColor = gurmukhiColor;
    theme.romanColor = romanColor;
    theme.hindiColor = hindiColor;
    theme.englishColor = englishColor;
    theme.teekaColor = teekaColor;
    theme.attributeColor = attributeColor;
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:theme] forKey:@"theme"];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"refresh" object:Nil];
}

#pragma mark slider
-(IBAction)setSliderValue:(id)sender
{
    [self setFont];
}
-(IBAction)sliderGurumukhi:(id)sender
{
    UISlider *slider=(UISlider *)sender;
    NSLog(@"%f",slider.value);
    labelPreview.text=@"] jpu ]";
    labelPreview.textColor = gurmukhiColor;
    [labelPreview setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:slider.value]];
    strGurmukhiSize=[NSString stringWithFormat:@"%f",slider.value];
}
-(IBAction)sliderRoman:(id)sender
{
    UISlider *slider=(UISlider *)sender;
    labelPreview.text=@"|| Jap ||";
    labelPreview.textColor = romanColor;
    [labelPreview setFont:[UIFont boldSystemFontOfSize:slider.value]];
    strRomanSize=[NSString stringWithFormat:@"%f",slider.value];
}
-(IBAction)sliderHindi:(id)sender
{
    UISlider *slider=(UISlider *)sender;
    labelPreview.text=@"] jpu ]";
    labelPreview.textColor = hindiColor;
    [labelPreview setFont:[UIFont fontWithName:@"Gurbanihindi" size:slider.value]];
    strHindiSize=[NSString stringWithFormat:@"%f",slider.value];
}
-(IBAction)sliderEnglish:(id)sender
{
    UISlider *slider=(UISlider *)sender;
    labelPreview.text=@"Chant And Meditate:";
    labelPreview.textColor = englishColor;
    [labelPreview setFont:[UIFont boldSystemFontOfSize:slider.value]];
    strEnglishSize=[NSString stringWithFormat:@"%f",slider.value];
}
-(IBAction)sliderTeeka:(id)sender
{
    UISlider *slider=(UISlider *)sender;
    labelPreview.text=@"(bwxI dw nwm hY)";
    labelPreview.textColor = teekaColor;
    [labelPreview setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:slider.value]];
    strTeekaSize=[NSString stringWithFormat:@"%f",slider.value];
}
-(IBAction)sliderAttributes:(id)sender
{
    UISlider *slider=(UISlider *)sender;
    labelPreview.text=@"jpu (m: 1) gurU gRMQ swihb - AMg 1 pN 3";
    labelPreview.textColor = attributeColor;
    [labelPreview setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:slider.value]];
    strAttributeSize=[NSString stringWithFormat:@"%f",slider.value];
}

#pragma mark buttons
-(IBAction)btnSmall:(id)sender
{
    //[self setFont:@"10"];
   

}
-(IBAction)btnLarge:(id)sender
{
    //[self setFont:@"17"];
  

}
-(IBAction)btnNormal:(id)sender
{
    //[self setFont:@"13"];
  
    
   
}
-(IBAction)btnMedium:(id)sender
{
   // [self setFont:@"15"];
   

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
