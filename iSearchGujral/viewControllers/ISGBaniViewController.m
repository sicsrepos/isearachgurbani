//
//  ISGBaniViewController.m
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGBaniViewController.h"
#import "ISGTheme.h"

@interface ISGBaniViewController ()
{
    NSDictionary *dict;
    NSArray *items;
}
@end

@implementation ISGBaniViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [tableBaani setBackgroundColor:[UIColor clearColor]];
    
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self setTheme];
    self.title=@"Baanis";
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    CGFloat screenWidth = screenSize.width;
    NSLog(@"screen height %f",screenHeight);
   /* if ((screenHeight > 850) && (screenHeight < 1000)) {
        tableBaani.frame = CGRectMake(40, 60 ,screenWidth-80,screenHeight-500);
    }
    else if(screenHeight==1024.000000)
    {
        tableBaani.frame = CGRectMake(40, 44, screenWidth-80,screenHeight-500);
    }
    else if(screenHeight==1366.000)
    {
        tableBaani.frame = CGRectMake(40, 50, screenWidth-80,screenHeight-500);
    }
    tableBaani.center = self.view.center;*/

    baniArray=[[NSMutableArray alloc]init];
    NSString *plistPath=[[NSBundle mainBundle] pathForResource:@"Baani" ofType:@"plist"];
    dict=[NSDictionary dictionaryWithContentsOfFile:plistPath];
    items=[[dict allKeys].mutableCopy sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    for (NSString *str in items) {
        NSArray *components=[str componentsSeparatedByString:@"_"];
        [baniArray addObject:[components objectAtIndex:1]];
    }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"refresh" object:Nil];
    // Do any additional setup after loading the view from its nib.
}

-(void)refresh:(NSNotification *)notification
{
    [self setTheme];
    [tableBaani reloadData];
}
-(void)setTheme
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    ISGTheme *theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    stringBackgroud=theme.background;
    fontSize=[theme.fontSize intValue];
    if(fontSize==0)
    {
        fontSize=14;
    }
    if(stringBackgroud.length<=7)
    {
        stringBackgroud=@"LightPattern";
    }
    if (!theme.backgroundImage)
    {
        [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
    }
    else
        imageBg.image = theme.backgroundImage;
    
}
-(void)SetDefaultValues
{
    stringBackgroud=@"LightPattern";
    fontSize=14;
    //fontStyle=@"gurbaniwebthick";
}
-(void)viewWillAppear:(BOOL)animated{
     self.navigationController.navigationBarHidden = NO;
}
-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    return [baniArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 60;
    }
    else
        return 44;
}

-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    fontSize = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 24 : fontSize;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.font=[UIFont boldSystemFontOfSize:fontSize];
    if([stringBackgroud isEqualToString:@"LightPattern"]||[stringBackgroud isEqualToString:@"WhitePattern"])
    {
        [cell.textLabel setTextColor:[UIColor blackColor]];
    }
    else
    {
        [cell.textLabel setTextColor:[UIColor whiteColor]];
    }
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.numberOfLines=2;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.text=[baniArray objectAtIndex:indexPath.row];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( [[dict objectForKey:[items objectAtIndex:indexPath.row]] isKindOfClass:[NSDictionary class]])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGBaniListViewController * baniListViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGBaniListViewController"];
        //ISGBaniListViewController *baniListViewController=[[ISGBaniListViewController alloc]initWithNibName:@"ISGBaniListViewController" bundle:Nil];
        baniListViewController.arrayItems=[[[dict objectForKey:[items objectAtIndex:indexPath.row]] allKeys]sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)].mutableCopy;
        baniListViewController.key=[items objectAtIndex:indexPath.row];
        baniListViewController.bani=[baniArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:baniListViewController animated:YES];
    }
    else
    {
        NSLog(@"%@",[dict objectForKey:[items objectAtIndex:indexPath.row]]);
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGBaniDetailViewController * baniDetailViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGBaniDetailViewController"];
        //ISGBaniDetailViewController *baniDetailViewController=[[ISGBaniDetailViewController alloc]initWithNibName:@"ISGBaniDetailViewController" bundle:Nil];
        baniDetailViewController.bani=baniArray[indexPath.row];
        baniDetailViewController.key=items[indexPath.row];
        baniDetailViewController.rootClass=@"ISGBaniViewController";
        baniDetailViewController.arrayData=[dict objectForKey:[items objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:baniDetailViewController animated:YES];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
