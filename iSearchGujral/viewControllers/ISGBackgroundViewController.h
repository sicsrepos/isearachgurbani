//
//  ISGBackgroundViewController.h
//  iSearchGujral
//
//  Created by mac on 4/26/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISGBackgroundViewController : UIViewController
{
    IBOutlet UIButton *buttonDarkPattern;
    IBOutlet UIButton *buttonLightPattern;
    IBOutlet UIButton *buttonBlackPattern;
    IBOutlet UIButton *buttonWhitePattern;
    IBOutlet UIButton *buttonCustomBackground;
    IBOutlet UILabel *labelDarkPattern;
    IBOutlet UILabel *labelLightPattern;
    IBOutlet UILabel *labelBlackPattern;
    IBOutlet UILabel *labelWhitePattern;
    IBOutlet UIImageView *imageBg;
    IBOutlet UIView *containerView;
    NSString *fontSize;
    NSString *fontStyle;
    NSString *language1;
    NSString *language2,*language3,*language4,*language5,*language6,*strGurmukhiSize,*strRomanSize,*strHindiSize,*strEnglishSize,*strTeekaSize,*strAttributeSize;
    UIColor *gurmukhiColor,*romanColor,*hindiColor,*englishColor,*teekaColor,*attributeColor;
}
-(IBAction)btnDarkPattern:(id)sender;
-(IBAction)btnLightPattern:(id)sender;
-(IBAction)btnBlackPattern:(id)sender;
-(IBAction)btnWhitePattern:(id)sender;
-(IBAction)btnCustomBackground:(id)sender;
@end
