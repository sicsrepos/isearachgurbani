//
//  ISGTheme.m
//  iSearchGujral
//
//  Created by mac on 4/30/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGTheme.h"

@implementation ISGTheme
static ISGTheme *theme;
@synthesize background,backgroundImage,font,fontSize,fontStyle,lang1,lang2,lang3,lang4,lang5,lang6,romanSize,gurmukhiSize,hindiSize,englishSize,teekaSize,attributeSize,gurmukhiColor,hindiColor,englishColor,teekaColor,attributeColor,romanColor;
+(id)getTheme
{
    if(theme==Nil)
    {
        theme=[[self alloc]init];
    }
    return theme;
}
- (void) encodeWithCoder:(NSCoder*)encoder {
    // If parent class also adopts NSCoding, include a call to
    // [super encodeWithCoder:encoder] as the first statement.
    
    [encoder encodeObject:backgroundImage forKey:@"backgroundImage"];
    [encoder encodeObject:background forKey:@"background"];
    [encoder encodeObject:fontSize forKey:@"fontSize"];
    [encoder encodeObject:fontStyle forKey:@"fontStyle"];
    
    [encoder encodeObject:lang1 forKey:@"language1"];
    [encoder encodeObject:lang2 forKey:@"language2"];
    [encoder encodeObject:lang3 forKey:@"language3"];
    [encoder encodeObject:lang4 forKey:@"language4"];
    [encoder encodeObject:lang5 forKey:@"language5"];
    [encoder encodeObject:lang6 forKey:@"language6"];
    //[encoder encodeObject:lang7 forKey:@"language7"];
    
    [encoder encodeObject:gurmukhiSize forKey:@"gurmukhiSize"];
    [encoder encodeObject:romanSize forKey:@"romanSize"];
    [encoder encodeObject:hindiSize forKey:@"hindiSize"];
    [encoder encodeObject:englishSize forKey:@"englishSize"];
    [encoder encodeObject:teekaSize forKey:@"teekaSize"];
    [encoder encodeObject:attributeSize forKey:@"attributeSize"];
    
    [encoder encodeObject:gurmukhiColor forKey:@"gurmukhiColor"];
    [encoder encodeObject:romanColor forKey:@"romanColor"];
    [encoder encodeObject:hindiColor forKey:@"hindiColor"];
    [encoder encodeObject:englishColor forKey:@"englishColor"];
    [encoder encodeObject:teekaColor forKey:@"teekaColor"];
    [encoder encodeObject:attributeColor forKey:@"attributeColor"];
    
    
}

- (id) initWithCoder:(NSCoder*)decoder {
    if (self = [super init]) {
        // If parent class also adopts NSCoding, replace [super init]
        // with [super initWithCoder:decoder] to properly initialize.
        // NOTE: Decoded objects are auto-released and must be retained
        
        backgroundImage = [decoder decodeObjectForKey:@"backgroundImage"];
        background = [decoder decodeObjectForKey:@"background"];
        fontSize = [decoder decodeObjectForKey:@"fontSize"];
        fontStyle = [decoder decodeObjectForKey:@"fontStyle"];
        
        lang1 = [decoder decodeObjectForKey:@"language1"];
        lang2 = [decoder decodeObjectForKey:@"language2"];
        lang3 = [decoder decodeObjectForKey:@"language3"];
        lang4 = [decoder decodeObjectForKey:@"language4"];
        lang5 = [decoder decodeObjectForKey:@"language5"];
        lang6 = [decoder decodeObjectForKey:@"language6"];
        //lang7 = [decoder decodeObjectForKey:@"language7"];
        
        gurmukhiSize = [decoder decodeObjectForKey:@"gurmukhiSize"];
        romanSize = [decoder decodeObjectForKey:@"romanSize"];
        hindiSize = [decoder decodeObjectForKey:@"hindiSize"];
        englishSize = [decoder decodeObjectForKey:@"englishSize"];
        teekaSize = [decoder decodeObjectForKey:@"teekaSize"];
        attributeSize = [decoder decodeObjectForKey:@"attributeSize"];
        
        gurmukhiColor = [decoder decodeObjectForKey:@"gurmukhiColor"];
        romanColor = [decoder decodeObjectForKey:@"romanColor"];
        hindiColor = [decoder decodeObjectForKey:@"hindiColor"];
        englishColor = [decoder decodeObjectForKey:@"englishColor"];
        teekaColor = [decoder decodeObjectForKey:@"teekaColor"];
        attributeColor = [decoder decodeObjectForKey:@"attributeColor"];
        
       
    }
    return self;
}
@end
