//
//  ISGSearchViewController.m
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGSearchViewController.h"
#import "NIDropDown.h"
#import "ISGAppDelegate.h"
#import "ISGTabViewController.h"
#import "ISGSearchHistoryViewController.h"
#import "SearchHistory.h"

@interface ISGSearchViewController ()<NIDropDownDelegate, UITabBarDelegate>
{
    NIDropDown *dropDown;
    BOOL isDeleteButtonPressed,isDropDownCategory;
    NSArray * arrayButtons;
    NSString * subQuerry;
    NSString * queryExecuted, *queryCount, *queryWildCard;
    UIBarButtonItem * buttonHistory;
}

@end

@implementation ISGSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    ISGAppDelegate * appDelegate = (ISGAppDelegate *)[[UIApplication sharedApplication]delegate];
    ISGTabViewController * tabs = (ISGTabViewController *)[appDelegate.window rootViewController];
    home = tabs.tabBar.items[0];
    explore = tabs.tabBar.items[1];
    Banis = tabs.tabBar.items[3];
    more = tabs.tabBar.items[4];
    
    wildCard=NO;
    isAdvancedSearch = NO;
    englistFormString=@"";
    isDeleteButtonPressed=NO;
    arrayLetterLenght=[[NSMutableArray alloc]init];
    database = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).database;
    txtSearch.delegate=self;
    arrayResults=[[NSMutableArray alloc]init];
    arrayAdditionalResults=[[NSMutableArray alloc]init];
    arrayAuthors = [NSMutableArray new];
    arrayRaags = [NSMutableArray new];
    arrayScriptures = [NSMutableArray new];
    queryWildCard = @"";
    
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"searchPreference"])
    {
        NSDictionary * dict = (NSDictionary *)[[NSUserDefaults standardUserDefaults]valueForKey:@"searchPreference"];
        [buttonCriteria setTitle:[dict valueForKey:@"selectedCriteria"] forState:UIControlStateNormal];
        [buttonLanguage setTitle:[dict valueForKey:@"selectedLanguage"] forState:UIControlStateNormal];
        [self setkeyboardForLanguage:[dict valueForKey:@"selectedLanguage"]];
    }
    else
    {
        [self setkeyboardForLanguage:@"Gurmukhi"];
    }
    
    click=0;
    cnt=0;
    textLength=0;
    language=@"roman";
    self.title=@"Search";
    [imageCheck setImage:[UIImage imageNamed:@"untick.png"]];
    
    isAdvancedSearch=NO;
    buttonAuthor.enabled=NO;
    buttonRaag.enabled=NO;
    buttonScripture.enabled=NO;
    
    [self enableButtons];
    
    //txtSearch.font=[UIFont fontWithName:@"gurbaniwebthick" size:14];
    txtSearch.text=@"";
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        txtSearch.font=[UIFont systemFontOfSize:20];
      //  [labelcriteria setFrame:CGRectMake(60, 100, 180, 32)];
      //  [labelLanguage setFrame:CGRectMake(60, 140, 180, 32)];
      //  [labelPhrase setFrame:CGRectMake(60, 60, 180, 32)];
      //  [txtSearch setFrame:CGRectMake(20, 80, 280, 35)];
      //  [buttonCriteria setFrame:CGRectMake(20, 120, 280, 31)];
     //   [buttonLanguage setFrame:CGRectMake(20, 160, 280, 31)];
      //  [buttonSearch setFrame:CGRectMake(85, 312, 100, 31)];
      //  [buttonHelp setFrame:CGRectMake(135, 312, 100, 31)];
      //  [buttonCheck setFrame:CGRectMake(80, 190, 150, 31)];
       // [imageCheck setFrame:CGRectMake(120, 190, 30, 30)];
       // [buttonAdvanceSearch setFrame:CGRectMake(80, 210, 170, 31)];
       // [imageAdvancedSearchCheck setFrame:CGRectMake(120, 210, 30, 30)];
        //[buttonAuthor setFrame:CGRectMake(20, 230, 280, 31)];
       // [buttonRaag setFrame:CGRectMake(20, 255, 280, 31)];
       // [buttonScripture setFrame:CGRectMake(20, 280, 280, 31)];
        
        
        [labelcriteria setFrame:CGRectMake(100, 100, 180, 32)];
        [labelLanguage setFrame:CGRectMake(100, 140, 180, 32)];
        [labelPhrase setFrame:CGRectMake(100, 60, 180, 32)];
        [txtSearch setFrame:CGRectMake(50, 80, 280, 35)];
        [buttonCriteria setFrame:CGRectMake(50, 120, 280, 31)];
        [buttonLanguage setFrame:CGRectMake(50, 160, 280, 31)];
        
        [buttonCheck setFrame:CGRectMake(100, 190, 150, 31)];
        [imageCheck setFrame:CGRectMake(130, 190, 30, 30)];
        [buttonAdvanceSearch setFrame:CGRectMake(100, 220, 170, 31)];
        [imageAdvancedSearchCheck setFrame:CGRectMake(130, 220, 30, 30)];
        
        [buttonAuthor setFrame:CGRectMake(50, 250, 280, 31)];
        [buttonRaag setFrame:CGRectMake(50, 280, 280, 31)];
        [buttonScripture setFrame:CGRectMake(50, 310, 280, 31)];
        
        [buttonHelp setFrame:CGRectMake(180, 360, 100, 31)];
        [buttonSearch setFrame:CGRectMake(110, 360, 100, 31)];
        
        
        
        
    }
    HUD=[[MBProgressHUD alloc]initWithView:self.tabBarController.view];
    [self.tabBarController.view addSubview:HUD];
    HUD.delegate=self;
    HUD.labelText=@"Searching...";
    arrayScriptures = [NSMutableArray arrayWithObjects:@"Sri Guru Granth Sahib",@"Bhai Gurdas Vaaran",@"Kabit Bhai Gurdas",@"Sri Dasam Granth Sahib",@"Baani Bhai Nand Lal",@"Unknown", nil];
    
    arrayButtons = [NSArray arrayWithObjects:buttonScripture,buttonRaag,buttonAuthor, nil];
    subQuerry = @"";
    
    [txtSearch setDelegate:self];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deleteNotification:) name:@"delete" object:Nil];
    
    buttonHistory = [[UIBarButtonItem alloc] initWithTitle:@"History" style:UIBarButtonItemStyleBordered target:self action:@selector(actionButtonHistory:)];
    [buttonHistory setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = buttonHistory;
    
    
    
    
}



-(void)deleteNotification:(NSNotification *)notification
{
    ////NSLog(@"delete me...");
    if(txtSearch.text.length>0)
    {
        isDeleteButtonPressed=YES;
    }
    
}

-(void)actionButtonHistory:(UIButton *) sender
{  dispatch_async(dispatch_get_main_queue(), ^{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    
    ISGSearchHistoryViewController * searchHistory  = [storyboard instantiateViewControllerWithIdentifier:@"ISGSearchHistoryViewController"];
    //ISGSearchHistoryViewController * searchHistory = [[ISGSearchHistoryViewController alloc] initWithNibName:@"ISGSearchHistoryViewController" bundle:nil];
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:searchHistory];
    [[self navigationController] presentViewController:nav animated:YES completion:nil];
    });
}
-(void)characterConvertion
{
    
    //    if(txtSearch.text.length==0)
    //    {
    //        txtSearch.text=@"";
    //        [arrayAdditionalResults removeAllObjects];
    //        [arrayResults removeAllObjects];
    //        englistFormString=@"";
    //        stringInput=@"";
    //        englistFormString=@"";
    //        temp=@"";
    //        txtSearch.text=@"";
    //        textLength=0;
    //    }
    if([buttonLanguage.titleLabel.text isEqualToString:@"Gurmukhi"])
    {
        ////NSLog(@"eng form letters: %@",englistFormString);
        NSString *letter;
        NSString *text=txtSearch.text;
        if(text.length>0)
        {
            letter =[text substringFromIndex:[text length] - 1];
        }
        NSString *path = [[NSBundle mainBundle] pathForResource: @"Language" ofType: @"plist"];
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile: path];
        NSString *englishLetter=[dict objectForKey:letter];
        if(englishLetter.length==0 || englishLetter.length>4)
            englishLetter=@"";
        ////NSLog(@"o/p %@",englishLetter);
        if(arrayLetterLenght.count<2)
        {
            [arrayLetterLenght addObject:[NSString stringWithFormat:@"%d",englishLetter.length]];
        }
        else
        {
            NSString *s=[arrayLetterLenght objectAtIndex:1];
            [arrayLetterLenght setObject:s atIndexedSubscript:0];
            [arrayLetterLenght setObject:[NSString stringWithFormat:@"%d",englishLetter.length] atIndexedSubscript:1];
        }
        //NSLog(@"array==%@",arrayLetterLenght);
        if(englistFormString.length==0) englistFormString=@"";
        
        
        //        if(txtSearch.text.length>=textLength && temp.length==0)
        //        {
        //            textLength=txtSearch.text.length;
        //            englistFormString=[NSString stringWithFormat:@"%@%@",englistFormString,englishLetter];
        //        }
        
        if(!isDeleteButtonPressed)
        {
            englistFormString=[NSString stringWithFormat:@"%@%@",englistFormString,englishLetter];
        }
        if(isDeleteButtonPressed)
        {
            NSString *newString=@"";
            NSString *tem=[arrayLetterLenght objectAtIndex:0];
            int x=[tem intValue];
            //NSLog(@"len to del :%d",x);
            [arrayLetterLenght setObject:@"1" atIndexedSubscript:0];
            if(englistFormString.length>0)
            {
                newString = [englistFormString substringToIndex:[englistFormString length]-x];
            }
            //NSLog(@"new string %@",newString);
            //            if(newString.length==0)
            //            {
            //                newString=@"";
            //            }
            //            else
            //            {
            englistFormString=[NSString stringWithFormat:@"%@",newString];
            // }
        }
        stringInput=englistFormString;
        // stringInput=@"p rm k";//foo
        isDeleteButtonPressed=NO;
        //NSLog(@"check %@",stringInput);
    }
}

-(IBAction)keyClose:(id)sender
{
    
}
-(IBAction)textChange:(id)sender
{
    //    NSString *text=txtSearch.text;
    //    NSMutableString *buffer = [text mutableCopy];
    //    CFMutableStringRef bufferRef = (__bridge CFMutableStringRef)buffer;
    //    CFStringTransform(bufferRef, NULL, kCFStringTransformToLatin, false);
    //    //NSLog(@"%@", buffer);
    [self characterConvertion];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self enableButtons];
}

- (void)enableButtons
{
    self.navigationController.navigationBarHidden=NO;
    itration=0;
    [arrayAdditionalResults removeAllObjects];
    [arrayResults removeAllObjects];
    ////NSLog(@"english string :%@",englistFormString);
    
    buttonSearch.enabled = YES;
    buttonHelp.enabled = YES;
    buttonCriteria.enabled = YES;
    buttonLanguage.enabled = YES;
    txtSearch.enabled = YES;
    buttonHistory.enabled = YES;
    
    home.enabled = YES;
    explore.enabled = YES;
    Banis.enabled = YES;
    more.enabled = YES;
    buttonAdvanceSearch.enabled = YES;
    
    [self checkAdvanceSearch];
    
    if(txtSearch.text.length==0)
    {
        englistFormString=@"";
        stringInput=@"";
    }
}

-(void)removeUnwnatedBlankSpace  
{
    unichar first = [txtSearch.text characterAtIndex:0];
    unichar last;
    if (txtSearch.text.length > 1)
    {
        last = [txtSearch.text characterAtIndex:[txtSearch.text length] - 1];
        if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:first])
        {
            txtSearch.text = [txtSearch.text substringFromIndex:1];
            [self removeUnwnatedBlankSpace];
        }
        else if([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:last])
        {
            txtSearch.text = [txtSearch.text substringToIndex:txtSearch.text.length-1];
            [self removeUnwnatedBlankSpace];
        }
    }
    else if(txtSearch.text.length == 1 && [txtSearch.text isEqualToString:@" "])
    {
        txtSearch.text = @"";
    }
    //unichar last = [txtSearch.text characterAtIndex:[txtSearch.text length] - 1];
    
}

-(IBAction)btnSearch:(id)sender
{
    [txtSearch resignFirstResponder];
    if(txtSearch.text.length != 0)
    [self removeUnwnatedBlankSpace];
    if(isAdvancedSearch == YES)
    {
        [arrayButtons enumerateObjectsUsingBlock:^(UIButton * obj, NSUInteger idx, BOOL *stop)
         {
             if(obj.enabled == YES)
             {
                 if(obj == buttonScripture && ![obj.titleLabel.text isEqualToString:@"Select Scripture"])
                 {
                     int i = (int)[arrayScriptures indexOfObject:obj.titleLabel.text];
                     subQuerry = [NSString stringWithFormat:@"%@ AND scrpt = '%d'",subQuerry,i+1];
                 }
                 if (obj == buttonRaag && ![obj.titleLabel.text isEqualToString:@"Select Raag"])
                 {
                     subQuerry = [NSString stringWithFormat:@"%@ AND raagID = '%@'",subQuerry,obj.titleLabel.text];
                 }
                 if (obj == buttonAuthor && ![obj.titleLabel.text isEqualToString:@"Select Writer"])
                 {
                     subQuerry = [NSString stringWithFormat:@"%@ AND authorID = '%@'",subQuerry,obj.titleLabel.text];
                 }
             }
         }];
        
        //NSLog(@"%@", subQuerry);
    }
    
    buttonSearch.enabled = NO;
    buttonHelp.enabled = NO;
    buttonCriteria.enabled = NO;
    buttonLanguage.enabled = NO;
    txtSearch.enabled = NO;
    home.enabled = NO;
    explore.enabled = NO;
    Banis.enabled = NO;
    more.enabled = NO;
    buttonHistory.enabled = NO;
    
    buttonAdvanceSearch.enabled = NO;
    
    [self checkAdvanceSearch];
    
    itration=0;
    searchFlag=0;
    [arrayResults removeAllObjects];
    [arrayFilter removeAllObjects];
    searchMoreData=NO;
    
    if (isAdvancedSearch == YES && buttonSelected == nil)
    {
        [[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select a category to search" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        buttonSearch.enabled = YES;
        buttonHelp.enabled = YES;
        buttonCriteria.enabled = YES;
        buttonLanguage.enabled = YES;
        txtSearch.enabled = YES;
        home.enabled = YES;
        explore.enabled = YES;
        Banis.enabled = YES;
        more.enabled = YES;
        buttonHistory.enabled = YES;
        buttonAdvanceSearch.enabled = YES;
        subQuerry = @"";
        [self checkAdvanceSearch];
    }
    else if([buttonLanguage.titleLabel.text isEqualToString:@"Gurmukhi"])
    {
        if(txtSearch.text.length!=0)
        {
            unichar first = [txtSearch.text characterAtIndex:0];
            unichar last = [txtSearch.text characterAtIndex:[txtSearch.text length] - 1];
            if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:first])
            {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Search text contains unwanted blank spaces" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
                buttonSearch.enabled = YES;
                buttonHelp.enabled = YES;
                buttonCriteria.enabled = YES;
                buttonLanguage.enabled = YES;
                txtSearch.enabled = YES;
                home.enabled = YES;
                explore.enabled = YES;
                Banis.enabled = YES;
                more.enabled = YES;
                buttonHistory.enabled = YES;
                
                buttonAdvanceSearch.enabled = YES;
                subQuerry = @"";
                
                [self checkAdvanceSearch];
            }
            else  if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:last])
            {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Search text contains unwanted blank spaces" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
                buttonSearch.enabled = YES;
                buttonHelp.enabled = YES;
                buttonCriteria.enabled = YES;
                buttonLanguage.enabled = YES;
                txtSearch.enabled = YES;
                home.enabled = YES;
                explore.enabled = YES;
                Banis.enabled = YES;
                more.enabled = YES;
                buttonHistory.enabled = YES;
                
                buttonAdvanceSearch.enabled = YES;
                subQuerry = @"";
                
                [self checkAdvanceSearch];
            }
            else
            {
                [txtSearch resignFirstResponder];
                [HUD showWhileExecuting:@selector(getDataGurmukhi) onTarget:self withObject:Nil animated:YES];
            }
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Please enter the text for Gurumukhi search" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            buttonSearch.enabled = YES;
            buttonHelp.enabled = YES;
            buttonCriteria.enabled = YES;
            buttonLanguage.enabled = YES;
            txtSearch.enabled = YES;
            home.enabled = YES;
            explore.enabled = YES;
            Banis.enabled = YES;
            more.enabled = YES;
            buttonHistory.enabled = YES;
            
            buttonAdvanceSearch.enabled = YES;
            subQuerry = @"";
            
            [self checkAdvanceSearch];
        }
    
    }
    else
    {
        if(txtSearch.text.length!=0)
        {
            unichar first = [txtSearch.text characterAtIndex:0];
            unichar last = [txtSearch.text characterAtIndex:[txtSearch.text length] - 1];
            if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:first])
            {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Search text contains unwanted blank spaces" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
                buttonSearch.enabled = YES;
                buttonHelp.enabled = YES;
                buttonCriteria.enabled = YES;
                buttonLanguage.enabled = YES;
                txtSearch.enabled = YES;
                home.enabled = YES;
                explore.enabled = YES;
                Banis.enabled = YES;
                more.enabled = YES;
                buttonHistory.enabled = YES;
                subQuerry = @"";
                
                buttonAdvanceSearch.enabled = YES;
                
                [self checkAdvanceSearch];
            }
            else if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:last])
            {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Search text contains unwanted blank spaces" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
                buttonSearch.enabled = YES;
                buttonHelp.enabled = YES;
                buttonCriteria.enabled = YES;
                buttonLanguage.enabled = YES;
                txtSearch.enabled = YES;
                home.enabled = YES;
                explore.enabled = YES;
                Banis.enabled = YES;
                more.enabled = YES;
                buttonHistory.enabled = YES;
                subQuerry = @"";
                
                buttonAdvanceSearch.enabled = YES;
                
                [self checkAdvanceSearch];
            }
            else
            {
                [txtSearch resignFirstResponder];
               [HUD showWhileExecuting:@selector(getDataRomanNew) onTarget:self withObject:Nil animated:YES];
               
            }
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Please enter the text for Roman search" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            buttonSearch.enabled = YES;
            buttonHelp.enabled = YES;
            buttonCriteria.enabled = YES;
            buttonLanguage.enabled = YES;
            txtSearch.enabled = YES;
            home.enabled = YES;
            explore.enabled = YES;
            Banis.enabled = YES;
            more.enabled = YES;
            buttonHistory.enabled = YES;
            
            buttonAdvanceSearch.enabled = YES;
            subQuerry = @"";
            
            [self checkAdvanceSearch];
        }
            
    }
    
}

-(void)filterAdditionalResultsForGurmukhi:(NSString *)filter
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    int counter;
    //NSLog(@"filterAdditionalResultsForGurmukhi %@",filter);
    NSMutableArray *arrayComponent;
    for (ISGDataModel *data in arrayResults)
    {
        if(data.gurmukhi.length>0)
        {
            NSArray *arrayWords=[data.gurmukhi componentsSeparatedByString:@" "];
            //NSLog(@"array %@",arrayWords);
            arrayComponent=[[NSMutableArray alloc]init];
            counter=0;
            // //NSLog(@"words in array : %@",arrayWords);
            if(arrayWords.count>filter.length)
            {
                for(int i=0;i<filter.length;i++)
                {
                    [arrayComponent removeAllObjects];
                    NSString *arrayElement=[arrayWords objectAtIndex:i];
                    if(arrayElement.length>0)
                    {
                        [arrayComponent addObject:arrayElement];
                    }
                    else [arrayComponent addObject:@"xxxxxxxxx"];
                    //NSLog(@"word =%@",arrayComponent);
                    //NSLog(@"letter =%c",[filter characterAtIndex:i]);
                    NSPredicate *pred = [NSPredicate predicateWithFormat:@"self BEGINSWITH[c]  %@",[NSString stringWithFormat:@"%c",[filter characterAtIndex:i]]];
                    NSArray *arr = [arrayComponent filteredArrayUsingPredicate:pred];
                    if(arr.count>0)
                    {
                        counter++;
                    }
                    //NSLog(@"conter =%d",counter);
                }
                if(counter==filter.length)
                {
                    if(![arrayFilter containsObject:data])[arrayFilter addObject:data];
                }
            }
        }
    }
    //NSLog(@"arrayFilter filterAdditionalResults count = %d",arrayFilter.count);
    //NSLog(@"arrayResults filterAdditionalResults count = %d",arrayResults.count);
    if(arrayResults.count==0)//additional change in Roman
    {
        arrayResults=[[NSMutableArray alloc]initWithArray:arrayFilter];
        [self superFilter];
    }
    //NSLog(@"arrayResults count after chenage = %d",arrayResults.count);
}


-(void)filterAdditionalResults:(NSString *)filter
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    int counter;
    //NSLog(@"filterAdditionalResults %@",filter);
    NSMutableArray *arrayComponent;
    for (ISGDataModel *data in arrayResults)
    {
        if(data.roman.length>0)
        {
            NSArray *arrayWords=[data.roman componentsSeparatedByString:@" "];
            arrayComponent=[[NSMutableArray alloc]init];
            counter=0;
            // //NSLog(@"words in array : %@",arrayWords);
            if(arrayWords.count>filter.length)
            {
                for(int i=0;i<filter.length;i++)
                {
                    [arrayComponent removeAllObjects];
                    NSString *arrayElement=[arrayWords objectAtIndex:i];
                    if(arrayElement.length>0)
                    {
                        [arrayComponent addObject:arrayElement];
                    }
                    else [arrayComponent addObject:@"xxxxxxxxx"];
                    //NSLog(@"word =%@",arrayComponent);
                    //NSLog(@"letter =%c",[filter characterAtIndex:i]);
                    NSPredicate *pred = [NSPredicate predicateWithFormat:@"self BEGINSWITH[c]  %@",[NSString stringWithFormat:@"%c",[filter characterAtIndex:i]]];
                    NSArray *arr = [arrayComponent filteredArrayUsingPredicate:pred];
                    if(arr.count>0)
                    {
                        counter++;
                    }
                    //NSLog(@"conter =%d",counter);
                }
                if(counter==filter.length)
                {
                    [arrayFilter addObject:data];
                }
            }
        }
    }
    ////NSLog(@"arrayFilter filterAdditionalResults count = %d",arrayFilter.count);
   ////NSLog(@"arrayResults filterAdditionalResults count = %d",arrayResults.count);
    if(arrayResults.count==0)//additional change in Roman
    {
        arrayResults=[[NSMutableArray alloc]initWithArray:arrayFilter];
        [self superFilter];
    }
    ////NSLog(@"arrayResults count after chenage = %d",arrayResults.count);
    
//    //NSLog(@"The Filtered Results are:");
//    [arrayFilter enumerateObjectsUsingBlock:^(ISGDataModel * obj, NSUInteger idx, BOOL *stop) {
//        //NSLog(@"%@",obj.roman);
//    }];
    
}
-(void)superFilter
{
    NSString *filter=stringInput;
    //NSLog(@"%s and filfer %@",__func__,filter);
    [arrayFilter removeAllObjects];
    for (ISGDataModel *data in arrayResults)
    {
        if([buttonLanguage.titleLabel.text isEqualToString:@"Roman"])
        {
            if([data.walpha componentsSeparatedByString:filter].count>1){
                ////NSLog(@"walpha : %@",data.walpha);
                [arrayFilter addObject:data];
            }
        }
        else
        {
            if([data.wgamma componentsSeparatedByString:filter].count>1){
                ////NSLog(@"wgamma : %@",data.wgamma);
                [arrayFilter addObject:data];
            }
        }
    }
    ////NSLog(@"array superFilter %d",arrayFilter.count);
}

-(void)filterResults:(NSString *)filter
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    int counter;
    //NSLog(@"Filter %@",filter);
    if([filter isEqualToString:@"nana"]) return;// nana found exception. So added this
    arrayFilter=[[NSMutableArray alloc]init];
    NSMutableArray *arrayComponent;
    for (ISGDataModel *data in arrayResults)
    {
        if(data.roman.length>0)
        {
            NSArray *arrayWords=[data.roman componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            arrayComponent=[[NSMutableArray alloc]init];
            counter=0;
            for(int i=0;i<filter.length;i++)
            {
                [arrayComponent removeAllObjects];
                NSString *arrayElement=[arrayWords objectAtIndex:i];
                if(arrayElement.length>0)
                {
                    [arrayComponent addObject:arrayElement];
                }
                else [arrayComponent addObject:@"xxxxxxxxx"];
                //        //NSLog(@"word =%@",arrayComponent);
                //        //NSLog(@"letter =%c",[filter characterAtIndex:i]);
                NSPredicate *pred = [NSPredicate predicateWithFormat:@"self BEGINSWITH[c]  %@",[NSString stringWithFormat:@"%c",[filter characterAtIndex:i]]];
                NSArray *arr = [arrayComponent filteredArrayUsingPredicate:pred];
                if(arr.count>0)
                {
                    counter++;
                }
                // //NSLog(@"conter =%d",counter);
            }
            if(counter==filter.length)
            {
                [arrayFilter addObject:data];
            }
        }
    }
    //NSLog(@"arrayFilter filterResults count = %d",arrayFilter.count);
    //NSLog(@"arrayResults filterResults count = %d",arrayResults.count);
}
-(void)filterResultsWith:(NSString *)filter
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    //NSLog(@"filter == %@",filter);
    NSString *indexPattern;
    filterFlag=1;
    arrayFilter=[[NSMutableArray alloc]init];
    if([buttonLanguage.titleLabel.text isEqualToString:@"Roman"])
    {
        for (ISGDataModel *data in arrayResults)
        {
            if(searchFlag==0) indexPattern=data.walpha;
            else indexPattern=data.alpha;
            if([indexPattern componentsSeparatedByString:filter].count >1)
            {
                //NSLog(@"indexPattern roman :%@",indexPattern);
                [arrayFilter addObject:data];
            }
        }
    }
    else
    {
        for (ISGDataModel *data in arrayResults)
        {
            //NSLog(@"check :%@",data.gamma);
            indexPattern=data.gamma;
            if([indexPattern componentsSeparatedByString:filter].count >1)
            {
                [arrayFilter addObject:data];
            }
        }
    }
    //NSLog(@"arrayFilter filterResultsWith count = %d",arrayFilter.count);
    //NSLog(@"arrayResults filterResultsWith count = %d",arrayResults.count);
    //NSLog(@"lang : %@",buttonLanguage.titleLabel.text);
    if(searchMoreData && [buttonLanguage.titleLabel.text isEqualToString:@"Roman"])[self getDataByQuerryRoman];
    
    //    else
    //    {
    //        //[self getDataByQuerryGurmukhi];
    //        [self filterAdditionalResultsForGurmukhi:filter];
    //    }
}

-(void)getDataByQuerryGurmukhi
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    NSString *str=@"* ";
    NSString *querryPattern=@"";
    NSString *newQuerry=@"";
    //if([buttonLanguage.titleLabel.text isEqualToString:@"Gurmukhi"])stringInput=txtSearch.text;
    NSArray *arrayPattern=[stringInput componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    for (NSString *pattern in arrayPattern) {
        querryPattern=[NSString stringWithFormat:@"%@%@%@",querryPattern,pattern,str];
    }
    unichar last = [querryPattern characterAtIndex:[querryPattern length] - 1];
    if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:last]) {
        newQuerry=[querryPattern substringToIndex:[querryPattern length]-1];
    } else newQuerry=querryPattern;
    
    NSString *querry;
    if (isAdvancedSearch == YES && subQuerry != nil)
    {
        querry = [NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,wgamma,gamma,plineID FROM gurbani WHERE wgamma GLOB '%@' %@ ORDER BY ID ASC",newQuerry,subQuerry];
        
        queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@' %@ ORDER BY ID ASC",newQuerry,subQuerry];
    }
    else
    {
        querry = [NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,wgamma,gamma,plineID FROM gurbani WHERE wgamma GLOB '%@'ORDER BY ID ASC",newQuerry];
        
        queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@' %@ ORDER BY ID ASC",newQuerry,subQuerry];
    }
   // NSLog(@"querryAdditional :%@",querry);
    FMResultSet *results = [database executeQuery:querry];
    [arrayResults removeAllObjects];
    while([results next])
    {
        dataModel=[[ISGDataModel alloc]initWithResult:results];
        if(![arrayResults containsObject:dataModel]) [arrayResults addObject:dataModel];
    }
    
    queryWildCard = querry;
    NSString *pattern=@"";
    NSString *checkedString=@"";
    stringInput=[stringInput stringByReplacingOccurrencesOfString:@"*" withString:@""];
    //NSLog(@"i/p :%@",stringInput);
    unichar lastLetter = [stringInput characterAtIndex:[stringInput length] - 1];
    if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:lastLetter]) {
        checkedString=[stringInput substringToIndex:stringInput.length-1];
    } else checkedString=stringInput;
    
    NSArray *array=[checkedString componentsSeparatedByString:@" "];
    int j=0;
    for (NSString *item in array) {
        pattern=[NSString stringWithFormat:@"%@%@",pattern,[NSString stringWithFormat:@"%c",[item characterAtIndex:0]]];
        j++;
    }
    //NSLog(@"pattern : %@",pattern);
    [self filterAdditionalResultsForGurmukhi:pattern];
}


-(void)getDataByQuerryRoman
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    NSString *str=@"% ";
    NSString *querryPattern=@"";
    NSString *newQuerry=@"";
    if([buttonLanguage.titleLabel.text isEqualToString:@"Roman"])stringInput=txtSearch.text;
    NSArray *arrayPattern=[stringInput componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    for (NSString *pattern in arrayPattern) {
        querryPattern=[NSString stringWithFormat:@"%@%@%@",querryPattern,pattern,str];
    }
    unichar last = [querryPattern characterAtIndex:[querryPattern length] - 1];
    if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:last]) {
        newQuerry=[querryPattern substringToIndex:[querryPattern length]-1];
    } else newQuerry=querryPattern;
    
    NSString *querry;
    if (isAdvancedSearch == YES && subQuerry != nil)
    {
        querry = [NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,plineID,wgamma FROM gurbani WHERE walpha LIKE '%@' %@ ORDER BY ID ASC",newQuerry, subQuerry];
        queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@' %@ ORDER BY ID ASC",newQuerry,subQuerry];
    }
    else
    {
        querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,plineID,wgamma FROM gurbani WHERE walpha LIKE '%@'ORDER BY ID ASC",newQuerry];
        queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@'ORDER BY ID ASC",newQuerry];
    }
   // NSLog(@"querryAdditional :%@",querry);
    FMResultSet *results = [database executeQuery:querry];
    [arrayResults removeAllObjects];
    while([results next])
    {
        dataModel=[[ISGDataModel alloc]initWithResult:results];
        [arrayResults addObject:dataModel];
    }
    queryWildCard = querry;
    NSString *pattern=@"";
    if([buttonLanguage.titleLabel.text isEqualToString:@"Roman"])
    {
        NSString *checkedString;
        unichar lastLetter = [stringInput characterAtIndex:[stringInput length] - 1];
        if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:lastLetter]) {
            checkedString=[stringInput substringToIndex:stringInput.length-1];
        } else checkedString=stringInput;
        
        NSArray *array=[checkedString componentsSeparatedByString:@" "];
        int j=0;
        for (NSString *item in array) {
            pattern=[NSString stringWithFormat:@"%@%@",pattern,[NSString stringWithFormat:@"%c",[item characterAtIndex:0]]];
            j++;
        }
    }
    else
    {
        pattern=[newQuerry stringByReplacingOccurrencesOfString:@"%" withString:@""];
    }
//    //NSLog(@"The Results are:");
//    [arrayResults enumerateObjectsUsingBlock:^(ISGDataModel * obj, NSUInteger idx, BOOL *stop) {
//        //NSLog(@"%@",obj.roman);
//    }];
    
    [self filterAdditionalResults:pattern];
}

-(void)getDataRomanNew
{
    searchFlag=0;
    NSString *querry;
    NSString *str=@"%";
    stringInput=[txtSearch.text lowercaseString];
    [database open];
    
    if([buttonCriteria.titleLabel.text isEqualToString:@"First Letter Anywhere"])
    {
        if ([stringInput componentsSeparatedByString:@" "].count > 1 && itration==0)
        {
            NSString *filterPattern=@"";
            NSString *querryPattern=@"";
            NSArray *arrayPattern=[stringInput componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            for (NSString *pattern in arrayPattern) {
                querryPattern=[NSString stringWithFormat:@"%@ %@%@",querryPattern,pattern,str];
                filterPattern=[NSString stringWithFormat:@"%@%c",filterPattern,[pattern characterAtIndex:0]];
            }
            //filterPattern=[NSString stringWithFormat:@" %@",txtSearch.text];
            NSArray *ary=[stringInput componentsSeparatedByString:@" "];
            int wordLen=0;
            for (NSString *item in ary) {
                if(item.length>wordLen) wordLen=item.length;
            }
            if(wordLen==1)
            {
                stringInput=[stringInput stringByReplacingOccurrencesOfString:@" " withString:@""];
                if (isAdvancedSearch == YES && subQuerry != nil)
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,wgamma,plineID FROM gurbani WHERE walpha LIKE '%@%@' %@ ORDER BY ID ASC",str,querryPattern,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@' %@ ORDER BY ID ASC",str,querryPattern,subQuerry];
                }
                else
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,wgamma,plineID FROM gurbani WHERE walpha LIKE '%@%@'ORDER BY ID ASC",str,querryPattern];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@'ORDER BY ID ASC",str,querryPattern];
                }
                searchFlag=1;
                
            }
            else
            {
                if (isAdvancedSearch == YES && subQuerry != nil)
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,walpha,alpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,wgamma,plineID FROM gurbani WHERE walpha LIKE '%@%@' %@ ORDER BY ID ASC",str,querryPattern,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@' %@ ORDER BY ID ASC",str,querryPattern,subQuerry];
                }
                else
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,walpha,alpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,wgamma,plineID FROM gurbani WHERE walpha LIKE '%@%@'ORDER BY ID ASC",str,querryPattern];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@'ORDER BY ID ASC",str,querryPattern];
                }
                searchFlag=2;
            }
            FMResultSet *results = [database executeQuery:querry];
            [arrayResults removeAllObjects];
            while([results next])
            {
                dataModel=[[ISGDataModel alloc]initWithResult:results];
                [arrayResults addObject:dataModel];
            }
            if (arrayResults.count > 0)
            {
                queryExecuted = querry;
            }
            searchMoreData=YES;
            if(!wildCard)[self filterResultsWith:filterPattern];
            
        }
        else  if ([stringInput componentsSeparatedByString:@"*"].count > 1 && itration==0)
        {
            if(wildCard)
            {
                stringInput=[stringInput stringByReplacingOccurrencesOfString:@"*" withString:@"% "];
                if (isAdvancedSearch == YES && subQuerry != nil)
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,wgamma,plineID FROM gurbani WHERE walpha LIKE '%@%@' %@ ORDER BY ID ASC",stringInput,str,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@' %@ ORDER BY ID ASC",stringInput,str,subQuerry];
                }
                else
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,wgamma,plineID FROM gurbani WHERE walpha LIKE '%@%@'ORDER BY ID ASC",stringInput,str];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@'ORDER BY ID ASC",stringInput,str];
                }
                FMResultSet *results = [database executeQuery:querry];
                [arrayResults removeAllObjects];
                while([results next])
                {
                    dataModel=[[ISGDataModel alloc]initWithResult:results];
                    // //NSLog(@"id= %@",dataModel.dataID);
                    [arrayResults addObject:dataModel];
                }
                if (arrayResults.count > 0)
                {
                    queryExecuted = querry;
                }
            }
        }
        else if(itration==0)
        {
            NSString *filterPattern=txtSearch.text;
            //            NSArray *arrayPattern=[stringInput componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            //            for (NSString *pattern in arrayPattern) {
            //                filterPattern=[NSString stringWithFormat:@"%@%c",filterPattern,[pattern characterAtIndex:0]];
            //            }
            
            if (isAdvancedSearch == YES && subQuerry != nil)
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,alpha,walpha,gurmukhi,english,translit,punctuation,scrpt,page,line,attributes,teeka,page,gamma,wgamma,plineID FROM gurbani  where walpha LIKE '%@%@%@' %@ ORDER BY ID ASC",str,stringInput,str,subQuerry];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@%@' %@ ORDER BY ID ASC",str,stringInput,str,subQuerry];
                if([stringInput isEqualToString:@"a"] || [stringInput isEqualToString:@"o"] || [stringInput isEqualToString:@"u"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,alpha,walpha,gurmukhi,english,translit,punctuation,scrpt,page,line,attributes,teeka,page,gamma,wgamma,plineID FROM gurbani  where walpha LIKE '%@a%@' OR walpha LIKE %@o%@ OR walpha LIKE %@u%@ %@ ORDER BY ID ASC",str,str,str,str,str,str,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@a%@' OR walpha LIKE %@o%@ OR walpha LIKE %@u%@ %@ ORDER BY ID ASC",str,str,str,str,str,str,subQuerry];
                }
                else if([stringInput isEqualToString:@"v"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,alpha,walpha,gurmukhi,english,translit,punctuation,scrpt,page,line,attributes,teeka,page,gamma,wgamma,plineID FROM gurbani  where walpha LIKE '%@v%@' OR walpha LIKE '%@b%@' %@ ORDER BY ID ASC",str,str,str,str,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@v%@' OR walpha LIKE '%@b%@' %@ ORDER BY ID ASC",str,str,str,str,subQuerry];
                }
                else if([stringInput isEqualToString:@"f"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,alpha,walpha,gurmukhi,english,translit,punctuation,scrpt,page,line,attributes,teeka,page,gamma,wgamma,plineID FROM gurbani  where walpha LIKE '%@f%@' OR walpha LIKE '%@p%@' %@ ORDER BY ID ASC",str,str,str,str,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@f%@' OR walpha LIKE '%@p%@' %@ ORDER BY ID ASC",str,str,str,str,subQuerry];
                }
            }
            else
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,alpha,walpha,gurmukhi,english,translit,punctuation,scrpt,page,line,attributes,teeka,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE '%@%@%@'ORDER BY ID ASC",str,stringInput,str];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@%@'ORDER BY ID ASC",str,stringInput,str];
                if([stringInput isEqualToString:@"a"] || [stringInput isEqualToString:@"o"] || [stringInput isEqualToString:@"u"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,alpha,walpha,gurmukhi,english,translit,punctuation,scrpt,page,line,attributes,teeka,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE '%@a%@' OR walpha LIKE '%@o%@' OR walpha LIKE '%@u%@' ORDER BY ID ASC",str,str,str,str,str,str];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@a%@' OR walpha LIKE '%@o%@' OR walpha LIKE '%@u%@' ORDER BY ID ASC",str,str,str,str,str,str];
                }
                else if([stringInput isEqualToString:@"v"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,alpha,walpha,gurmukhi,english,translit,punctuation,scrpt,page,line,attributes,teeka,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE '%@v%@' OR walpha LIKE '%@b%@' ORDER BY ID ASC",str,str,str,str];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@v%@' OR walpha LIKE '%@b%@' ORDER BY ID ASC",str,str,str,str];
                }
                else if([stringInput isEqualToString:@"f"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,alpha,walpha,gurmukhi,english,translit,punctuation,scrpt,page,line,attributes,teeka,page,gamma,wgamma,plineID FROM gurbani  where walpha LIKE '%@f%@' OR walpha LIKE '%@p%@' ORDER BY ID ASC",str,str,str,str];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@f%@' OR walpha LIKE '%@p%@' ORDER BY ID ASC",str,str,str,str];
                }
            }
            
            FMResultSet *results = [database executeQuery:querry];
            [arrayResults removeAllObjects];
            while([results next])
            {
                dataModel=[[ISGDataModel alloc]initWithResult:results];
                // //NSLog(@"id= %@",dataModel.dataID);
                [arrayResults addObject:dataModel];
            }
            if (arrayResults.count > 0)
            {
                queryExecuted = querry;
            }
            if(!wildCard) [self filterResultsWith:filterPattern];
        }
    }
    else if ([buttonCriteria.titleLabel.text isEqualToString:@"First Letter Beginning"])
    {
        NSString *type;
        int wordLenght=0;
        NSString *checkedString;
        unichar last = [txtSearch.text characterAtIndex:[txtSearch.text length] - 1];
        if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:last]) {
            checkedString=[txtSearch.text substringToIndex:txtSearch.text.length-1];
        } else checkedString=txtSearch.text;
        NSArray *words = [checkedString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (words.count > 1)
        {
            wordLenght = words.count;
        }
        else
        {
            for (NSString *item in words) {
                if(item.length>wordLenght) wordLenght=item.length;
            }
        }
        
        if(wordLenght==1) type=@"single"; else type=@"more";
        if ([stringInput componentsSeparatedByString:@"*"].count > 1 && itration==0)
        {
            //NSLog(@"type :%@",type);
            if(wildCard)
            {
                if([type isEqualToString:@"single"])
                {
                    stringInput=[stringInput stringByReplacingOccurrencesOfString:@" " withString:@""];
                    stringInput=[stringInput stringByReplacingOccurrencesOfString:@"*" withString:@"%"];
                    if (isAdvancedSearch == YES && subQuerry != nil)
                    {
                        querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,wgamma,plineID FROM gurbani WHERE walpha LIKE '%@' %@ ORDER BY ID ASC",stringInput,subQuerry];
                        queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@' %@ ORDER BY ID ASC",stringInput,subQuerry];
                    }
                    else
                    {
                        querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,wgamma,plineID FROM gurbani WHERE walpha LIKE '%@'ORDER BY ID ASC",stringInput];
                        queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@'ORDER BY ID ASC",stringInput];
                    }
                }
                else
                {
                    stringInput=[stringInput stringByReplacingOccurrencesOfString:@" " withString:@"%"];
                    stringInput=[stringInput stringByReplacingOccurrencesOfString:@"*" withString:@"%"];
                    if (isAdvancedSearch == YES && subQuerry != nil)
                    {
                        querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE '%@' %@ ORDER BY ID ASC",stringInput,subQuerry];
                        queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@' %@ ORDER BY ID ASC",stringInput,subQuerry];
                    }
                    else
                    {
                        querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE '%@'ORDER BY ID ASC",stringInput];
                        queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@'ORDER BY ID ASC",stringInput];
                    }
                }
                FMResultSet *results = [database executeQuery:querry];
                [arrayResults removeAllObjects];
                while([results next])
                {
                    dataModel=[[ISGDataModel alloc]initWithResult:results];
                    // //NSLog(@"id= %@",dataModel.dataID);
                    [arrayResults addObject:dataModel];
                }
                if (arrayResults.count > 0)
                {
                    queryExecuted = querry;
                }
            }
        }
        else if([type isEqualToString:@"single"])
        {
            NSString *formatedString=[stringInput stringByReplacingOccurrencesOfString:@" " withString:@""];
            if (isAdvancedSearch == YES && subQuerry != nil)
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE '%@%@' %@ ORDER BY ID ASC",formatedString,str,subQuerry];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@' %@ ORDER BY ID ASC",formatedString,str,subQuerry];
                if([stringInput isEqualToString:@"a"] || [stringInput isEqualToString:@"o"] || [stringInput isEqualToString:@"u"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE 'a%@' OR walpha LIKE 'o%@' OR walpha LIKE 'u%@' %@ ORDER BY ID ASC",str,str,str,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE 'a%@' OR walpha LIKE 'o%@' OR walpha LIKE 'u%@' %@ ORDER BY ID ASC",str,str,str,subQuerry];
                }
                else if([stringInput isEqualToString:@"v"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE 'v%@' OR walpha LIKE 'b%@' %@ ORDER BY ID ASC",str,str,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE 'v%@' OR walpha LIKE 'b%@' %@ ORDER BY ID ASC",str,str,subQuerry];
                }
                else if([stringInput isEqualToString:@"f"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE 'f%@' OR walpha LIKE 'p%@' %@ ORDER BY ID ASC",str,str,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE 'f%@' OR walpha LIKE 'p%@' %@ ORDER BY ID ASC",str,str,subQuerry];
                }
            }
            else
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE '%@%@'ORDER BY ID ASC",formatedString,str];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@'ORDER BY ID ASC",formatedString,str];
                if([stringInput isEqualToString:@"a"] || [stringInput isEqualToString:@"o"] || [stringInput isEqualToString:@"u"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE 'a%@' OR walpha LIKE 'o%@' OR walpha LIKE 'u%@'ORDER BY ID ASC",str,str,str];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE 'a%@' OR walpha LIKE 'o%@' OR walpha LIKE 'u%@'ORDER BY ID ASC",str,str,str];
                }
                else if([stringInput isEqualToString:@"v"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE 'v%@' OR walpha LIKE 'b%@' ORDER BY ID ASC",str,str];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE 'v%@' OR walpha LIKE 'b%@' ORDER BY ID ASC",str,str];
                }
                else if([stringInput isEqualToString:@"f"])
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE 'f%@' OR walpha LIKE 'p%@' ORDER BY ID ASC",str,str];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE 'f%@' OR walpha LIKE 'p%@' ORDER BY ID ASC",str,str];
                }
            }
            FMResultSet *results = [database executeQuery:querry];
            [arrayResults removeAllObjects];
            while([results next])
            {
                dataModel=[[ISGDataModel alloc]initWithResult:results];
                // //NSLog(@"id= %@",dataModel.dataID);
                [arrayResults addObject:dataModel];
            }
            if (arrayResults.count > 0)
            {
                queryExecuted = querry;
            }
            if(!wildCard)[self filterResults:formatedString];
        }
        else
        {
            NSString *pattern=@"";
            NSString *formatedString=@"";
            NSArray *array=[checkedString componentsSeparatedByString:@" "];
            int j=0;
            for (NSString *item in array) {
                pattern=[NSString stringWithFormat:@"%@%@",pattern,[NSString stringWithFormat:@"%c",[item characterAtIndex:0]]];
                j++;
            }
            for(int i=0;i<array.count;i++)
            {
                NSString *space;
                if(i>=0 && i!=array.count-1) space=@"% ";
                else if(i==array.count-1) space=@""; else space=@" ";
                formatedString=[NSString stringWithFormat:@"%@%@%@",formatedString,[array objectAtIndex:i],space];
            }
            
            if (isAdvancedSearch == YES && subQuerry != nil)
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE '%@%@' %@ ORDER BY ID ASC",formatedString,str,subQuerry];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@' %@ ORDER BY ID ASC",formatedString,str,subQuerry];
            }
            else
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where walpha LIKE '%@%@'ORDER BY ID ASC",formatedString,str];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE walpha LIKE '%@%@'ORDER BY ID ASC",formatedString,str];
            }
            
            FMResultSet *results = [database executeQuery:querry];
            [arrayResults removeAllObjects];
            while([results next])
            {
                dataModel=[[ISGDataModel alloc]initWithResult:results];
                // //NSLog(@"id= %@",dataModel.dataID);
                [arrayResults addObject:dataModel];
            }
            if (arrayResults.count > 0)
            {
                queryExecuted = querry;
            }
            if(!wildCard) [self filterResults:pattern];
        }
    }
    else
    {
        if (isAdvancedSearch == YES && subQuerry != nil)
        {
            querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,roman,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where roman LIKE '%@%@%@' %@ ORDER BY ID ASC",str,stringInput,str,subQuerry];
            queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE roman LIKE '%@%@%@' %@ ORDER BY ID ASC",str,stringInput,str,subQuerry];
            if([stringInput isEqualToString:@"a"] || [stringInput isEqualToString:@"o"] || [stringInput isEqualToString:@"u"])
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,roman,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where roman LIKE '%@a%@' OR roman LIKE '%@o%@' OR roman LIKE '%@u%@' %@ ORDER BY ID ASC",str,str,str,str,str,str,subQuerry];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE roman LIKE '%@a%@' OR roman LIKE '%@o%@' OR roman LIKE '%@u%@' %@ ORDER BY ID ASC",str,str,str,str,str,str,subQuerry];
            }
            else if([stringInput isEqualToString:@"v"])
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,roman,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where roman LIKE '%@v%@' OR roman LIKE '%@b%@' %@ ORDER BY ID ASC",str,str,str,str,subQuerry];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE roman LIKE '%@v%@' OR roman LIKE '%@b%@' %@ ORDER BY ID ASC",str,str,str,str,subQuerry];
            }
            else if([stringInput isEqualToString:@"f"])
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,roman,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where roman LIKE '%@f%@' OR roman LIKE '%@p%@' %@ ORDER BY ID ASC",str,str,str,str,subQuerry];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE roman LIKE '%@f%@' OR roman LIKE '%@p%@' %@ ORDER BY ID ASC",str,str,str,str,subQuerry];
            }
        }
        else
        {
            querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,roman,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where roman LIKE '%@%@%@'ORDER BY ID ASC",str,stringInput,str];
            queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE roman LIKE '%@%@%@'ORDER BY ID ASC",str,stringInput,str];
            if([stringInput isEqualToString:@"a"] || [stringInput isEqualToString:@"o"] || [stringInput isEqualToString:@"u"])
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,roman,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where roman LIKE '%@a%@' OR roman LIKE '%@o%@' OR roman LIKE '%@u%@' ORDER BY ID ASC",str,str,str,str,str,str];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE roman LIKE '%@a%@' OR roman LIKE '%@o%@' OR roman LIKE '%@u%@' ORDER BY ID ASC",str,str,str,str,str,str];
            }
            else if([stringInput isEqualToString:@"v"])
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,roman,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where roman LIKE '%@v%@' OR roman LIKE '%@b%@' ORDER BY ID ASC",str,str,str,str];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE roman LIKE '%@v%@' OR roman LIKE '%@b%@' ORDER BY ID ASC",str,str,str,str];
            }
            else if([stringInput isEqualToString:@"f"])
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,roman,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where roman LIKE '%@f%@' OR roman LIKE '%@p%@' ORDER BY ID ASC",str,str,str,str];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE roman LIKE '%@f%@' OR roman LIKE '%@p%@' ORDER BY ID ASC",str,str,str,str];
            }
        }
        FMResultSet *results = [database executeQuery:querry];
        [arrayResults removeAllObjects];
        while([results next])
        {
            dataModel=[[ISGDataModel alloc]initWithResult:results];
            ////NSLog(@"id= %@",dataModel.dataID);
            [arrayResults addObject:dataModel];
        }
        if (arrayResults.count > 0)
        {
            queryExecuted = querry;
        }
    }
  //  NSLog(@" getDataRomanNew %@",querry);
    [database close];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGSearchResultsViewController * searchResultsViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGSearchResultsViewController"];
   // ISGSearchResultsViewController *searchResultsViewController=[[ISGSearchResultsViewController alloc]initWithNibName:@"ISGSearchResultsViewController" bundle:Nil];
    searchResultsViewController.searchedString = txtSearch.text;
    subQuerry = @"";
    
    if(arrayResults.count==0 )
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"No record found" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
        
        
        buttonSearch.enabled = YES;
        buttonHelp.enabled = YES;
        buttonCriteria.enabled = YES;
        buttonLanguage.enabled = YES;
        txtSearch.enabled = YES;
        home.enabled = YES;
        explore.enabled = YES;
        Banis.enabled = YES;
        more.enabled = YES;
        buttonHistory.enabled = YES;
        buttonAdvanceSearch.enabled = YES;
        subQuerry = @"";
        
        if (isAdvancedSearch == YES && [buttonScripture.titleLabel.text isEqualToString:@"Sri Guru Granth Sahib"])
        {
            buttonAuthor.enabled=YES;
            buttonRaag.enabled=YES;
        }
        else
        {
            buttonAuthor.enabled=NO;
            buttonRaag.enabled=NO;
        }
    }
    else if(arrayFilter.count>0 || arrayResults.count>0)
    {
        if(arrayFilter.count>0)
        {
            NSComparisonResult (^sortBlock)(id, id) = ^(ISGDataModel *obj1, ISGDataModel *obj2) {
                if ([obj1.dataID intValue] > [obj2.dataID intValue]) {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                if ([obj1.dataID intValue] < [obj2.dataID intValue]) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                return (NSComparisonResult)NSOrderedSame;
            };
            NSArray *sorted = [arrayFilter sortedArrayUsingComparator:sortBlock];
            searchResultsViewController.arrayItems=sorted.mutableCopy;
            searchResultsViewController.lang=@"roman";
            searchResultsViewController.querry = queryExecuted;
            NSArray * arraySearchHistory = [SearchHistory fetchAllSearchHistory];
            if(arraySearchHistory.count > 9)
            {
                NSMutableArray * arr = [NSMutableArray new];
                [arr removeAllObjects];
                for (int i = 0; i < arraySearchHistory.count; i++)
                {
                    SearchHistory * obj = (SearchHistory *)arraySearchHistory[i];
                    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
                    [dict setObject:obj.searchText forKey:@"text"];
                    [dict setObject:obj.searchLanguage forKey:@"laguage"];
                    [dict setObject:obj.searchQuery forKey:@"query"];
                    [dict setObject:obj.searchCategory forKey:@"category"];
                    [dict setObject:obj.searchQueryWildcard forKey:@"queryWildcard"];
                    [arr addObject:dict];
                }
                [arr removeObjectAtIndex:0];
                [SearchHistory deleteAllSearchHistory];
                [SearchHistory updateSearchHistory:arr];
            }
            [SearchHistory addSearchHistoryForText:txtSearch.text Language:@"roman" Query:queryExecuted Category:buttonCriteria.titleLabel.text Type:wildCard ? @"Wildcard" : @"notWildcard" andWildCardQuery:queryWildCard];
            [self.navigationController pushViewController:searchResultsViewController animated:YES];
            
        }
        else
        {
            searchResultsViewController.arrayItems=arrayResults;
            searchResultsViewController.lang=@"roman";
            searchResultsViewController.querry = queryExecuted;
            NSArray * arraySearchHistory = [SearchHistory fetchAllSearchHistory];
            if(arraySearchHistory.count > 9)
            {
                NSMutableArray * arr = [NSMutableArray new];
                [arr removeAllObjects];
                for (int i = 0; i < arraySearchHistory.count; i++)
                {
                    SearchHistory * obj = (SearchHistory *)arraySearchHistory[i];
                    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
                    [dict setObject:obj.searchText forKey:@"text"];
                    [dict setObject:obj.searchLanguage forKey:@"laguage"];
                    [dict setObject:obj.searchQuery forKey:@"query"];
                    [dict setObject:obj.searchCategory forKey:@"category"];
                    [dict setObject:obj.searchQueryWildcard forKey:@"queryWildcard"];
                    [arr addObject:dict];
                }
                [arr removeObjectAtIndex:0];
                [SearchHistory deleteAllSearchHistory];
                [SearchHistory updateSearchHistory:arr];
            }
            [SearchHistory addSearchHistoryForText:txtSearch.text Language:@"roman" Query:queryExecuted Category:buttonCriteria.titleLabel.text Type:wildCard ? @"Wildcard" : @"notWildcard" andWildCardQuery:queryWildCard];
            [self.navigationController pushViewController:searchResultsViewController animated:YES];
        }
        
    }
        });
}

-(void)getDataGurmukhi
{
    NSString *querry;
    NSString *str=@"*";
    [database open];
    if([buttonCriteria.titleLabel.text isEqualToString:@"First Letter Anywhere"])
    {
        if ([stringInput componentsSeparatedByString:@" "].count > 1 && itration==0)
        {
            NSString *filterPattern=@"";
            NSString *querryPattern=@"";
            NSArray *arrayPattern=[stringInput componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            for (NSString *pattern in arrayPattern) {
                querryPattern=[NSString stringWithFormat:@"%@%@%@",querryPattern,pattern,str];
                filterPattern=[NSString stringWithFormat:@"%@%c",filterPattern,[pattern characterAtIndex:0]];
            }
            NSArray *ary=[stringInput componentsSeparatedByString:@" "];
            int wordLen=0;
            for (NSString *item in ary) {
                if(item.length>wordLen) wordLen=(int)item.length;
            }
            if(wordLen==1)
            {
                stringInput=[stringInput stringByReplacingOccurrencesOfString:@" " withString:@""];
                if (isAdvancedSearch == YES && subQuerry != nil)
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,wgamma,gamma,plineID FROM gurbani WHERE wgamma GLOB '%@%@' %@ ORDER BY ID ASC",str,querryPattern,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@' %@ ORDER BY ID ASC",str,querryPattern,subQuerry];
                }
                else
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,wgamma,gamma,plineID FROM gurbani WHERE wgamma GLOB '%@%@'ORDER BY ID ASC",str,querryPattern];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@'ORDER BY ID ASC",str,querryPattern];
                }
            }
            else
            { 
                if (isAdvancedSearch == YES && subQuerry != nil)
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,wgamma,gamma,plineID FROM gurbani WHERE wgamma GLOB '%@%@' %@ ORDER BY ID ASC",str,querryPattern,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@' %@ ORDER BY ID ASC",str,querryPattern,subQuerry];
                }
                else
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,wgamma,gamma,plineID FROM gurbani WHERE wgamma GLOB '%@%@'ORDER BY ID ASC",str,querryPattern];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@'ORDER BY ID ASC",str,querryPattern];
                }
                
            }
            
            FMResultSet *results = [database executeQuery:querry];
            [arrayResults removeAllObjects];
            while([results next])
            {
                dataModel=[[ISGDataModel alloc]initWithResult:results];
                [arrayResults addObject:dataModel];
            }
            if (arrayResults.count > 0)
            {
                queryExecuted = querry;
            }
            searchMoreData=YES;
            //NSLog(@"querry %@",querry);
            //NSLog(@"called FLA 1");
            //NSLog(@"filterPattern in FLA %@",filterPattern);
            if(!wildCard) [self filterResultsWith:filterPattern];
        }
        else  if ([stringInput componentsSeparatedByString:@"*"].count > 1 && itration==0)
        {
            if(wildCard)
            {
                stringInput=[stringInput stringByReplacingOccurrencesOfString:@"*" withString:@"% "];
                if (isAdvancedSearch == YES && subQuerry != nil)
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,wgamma,gamma,plineID FROM gurbani WHERE wgamma GLOB '%@%@' %@ ORDER BY ID ASC",stringInput,str,subQuerry];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@' %@ ORDER BY ID ASC",stringInput,str,subQuerry];
                }
                else
                {
                    querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,english,page,wgamma,gamma,plineID FROM gurbani WHERE wgamma GLOB '%@%@'ORDER BY ID ASC",stringInput,str];
                    queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@'ORDER BY ID ASC",stringInput,str];
                }
                
                FMResultSet *results = [database executeQuery:querry];
                [arrayResults removeAllObjects];
                while([results next])
                {
                    dataModel=[[ISGDataModel alloc]initWithResult:results];
                    [arrayResults addObject:dataModel];
                }
                if (arrayResults.count > 0)
                {
                    queryExecuted = querry;
                }
            }
        }
        else if(itration==0)
        {
            if (isAdvancedSearch == YES && subQuerry != nil)
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,translit,punctuation,scrpt,page,line,attributes,teeka,page,gamma,wgamma,plineID FROM gurbani  WHERE wgamma GLOB '%@%@%@' %@ ORDER BY ID ASC",str,stringInput,str,subQuerry];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@%@' %@ ORDER BY ID ASC",str,stringInput,str,subQuerry];
            }
            else
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,translit,punctuation,scrpt,page,line,attributes,teeka,page,gamma,wgamma,plineID FROM gurbani WHERE wgamma GLOB '%@%@%@'ORDER BY ID ASC",str,stringInput,str];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@%@'ORDER BY ID ASC",str,stringInput,str];
            }
            
            FMResultSet *results = [database executeQuery:querry];
            [arrayResults removeAllObjects];
            while([results next])
            {
                dataModel=[[ISGDataModel alloc]initWithResult:results];
                [arrayResults addObject:dataModel];
            }
            if (arrayResults.count > 0)
            {
                queryExecuted = querry;
            }
        }
    }
    else if ([buttonCriteria.titleLabel.text isEqualToString:@"First Letter Beginning"])
    {
        
        NSString *type;
        int wordLenght=0;
        // //NSLog(@"txt len %d",txtSearch.text.length);
        NSString *checkedString;
        unichar last = [txtSearch.text characterAtIndex:[txtSearch.text length] - 1];
        if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:last]) {
            checkedString=[txtSearch.text substringToIndex:txtSearch.text.length-1];
            
        } else checkedString=txtSearch.text;
        // //NSLog(@"check len %d",checkedString.length);
        NSArray *words = [checkedString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//        NSLog(@"array words : %@",words);
        for (NSString *item in words) {
            NSLog(@"len in loop :%lu",(unsigned long)item.length);
            if(item.length>wordLenght) wordLenght=(int)item.length;
        }
        
        //NSLog(@"word len :%d",wordLenght);
        if(wordLenght==1) type=@"single"; else type=@"more";
        ////NSLog(@"type : %@",type);
        if([type isEqualToString:@"single"])
        {
            NSString *formatedString=[stringInput stringByReplacingOccurrencesOfString:@" " withString:@""];
            if (isAdvancedSearch == YES && subQuerry != nil)
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,roman,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani WHERE gamma GLOB '%@%@' %@ ORDER BY ID ASC",formatedString,str,subQuerry];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@' %@ ORDER BY ID ASC",formatedString,str,subQuerry];
            }
            else
            {

                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,roman,alpha,walpha,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani WHERE gamma GLOB '%@%@' ORDER BY ID ASC",formatedString,str];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@'ORDER BY ID ASC",formatedString,str];
                
            }
            
            FMResultSet *results = [database executeQuery:querry];
            [arrayResults removeAllObjects];
            while([results next])
            {
                dataModel=[[ISGDataModel alloc]initWithResult:results];
                [arrayResults addObject:dataModel];
            }
            if (arrayResults.count > 0)
            {
                queryExecuted = querry;
            }
            if(!wildCard)[self filterResults:formatedString];
        }
        else
        {
            NSString *pattern=@"";
            NSString *formatedString=@"";
            
            NSArray *array=[stringInput componentsSeparatedByString:@" "];
            int j=0;
            for (NSString *item in array)
            {
                if (item.length > 0)
                pattern=[NSString stringWithFormat:@"%@%@",pattern,[NSString stringWithFormat:@"%c",[item characterAtIndex:0]]];
                j++;
            }
            for(int i=0;i<array.count;i++)
            {
                NSString *space;
                if(i>=0 && i!=array.count-1) space=@"* ";
                else if(i==array.count-1) space=@""; else space=@" ";
                formatedString=[NSString stringWithFormat:@"%@%@%@",formatedString,[array objectAtIndex:i],space];
            }
            if (isAdvancedSearch == YES && subQuerry != nil)
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where wgamma GLOB '%@%@' %@ ORDER BY ID ASC",formatedString,str,subQuerry];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@' %@ ORDER BY ID ASC",formatedString,str,subQuerry];
            }
            else
            {
                querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where wgamma GLOB '%@%@'ORDER BY ID ASC",formatedString,str];
                queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE wgamma GLOB '%@%@'ORDER BY ID ASC",formatedString,str];
            }
            
            
            FMResultSet *results = [database executeQuery:querry];
            [arrayResults removeAllObjects];
            while([results next])
            {
                dataModel=[[ISGDataModel alloc]initWithResult:results];
                [arrayResults addObject:dataModel];
            }
            if (arrayResults.count > 0)
            {
                queryExecuted = querry;
            }
            if(!wildCard) [self filterResults:pattern];
        }
    }
    else
    {
        if (isAdvancedSearch == YES && subQuerry != nil)
        {
            querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where gurmukhi GLOB '%@%@%@' %@ ORDER BY ID ASC",str,stringInput,str,subQuerry];
            queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE gurmukhi GLOB '%@%@%@' %@ ORDER BY ID ASC",str,stringInput,str,subQuerry];
        }
        else
        {
            querry=[NSString stringWithFormat:@"SELECT roman,ID,shabd,gurmukhi,english,alpha,walpha,roman,translit,punctuation,scrpt,page,teeka,attributes,line,page,gamma,wgamma,plineID FROM gurbani where gurmukhi GLOB '%@%@%@'ORDER BY ID ASC",str,stringInput,str];
            queryCount = [NSString stringWithFormat:@"SELECT COUNT(1) FROM gurbani WHERE gurmukhi GLOB '%@%@%@'ORDER BY ID ASC",str,stringInput,str];
        }
        
        
        FMResultSet *results = [database executeQuery:querry];
        [arrayResults removeAllObjects];
        while([results next])
        {
            dataModel=[[ISGDataModel alloc]initWithResult:results];
            [arrayResults addObject:dataModel];
        }
        if (arrayResults.count > 0)
        {
            queryExecuted = querry;
        }
    }
   // NSLog(@" getDataGurmukhi %@",querry);
    [database close];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGSearchResultsViewController * searchResultsViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGSearchResultsViewController"];
    //ISGSearchResultsViewController *searchResultsViewController=[[ISGSearchResultsViewController alloc]initWithNibName:@"ISGSearchResultsViewController" bundle:Nil];
    subQuerry = @"";
    searchResultsViewController.searchedString = txtSearch.text;
    
    if(arrayResults.count==0 )
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"No record found" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
        
        buttonSearch.enabled = YES;
        buttonHelp.enabled = YES;
        buttonCriteria.enabled = YES;
        buttonLanguage.enabled = YES;
        txtSearch.enabled = YES;
        home.enabled = YES;
        explore.enabled = YES;
        Banis.enabled = YES;
        more.enabled = YES;
        buttonHistory.enabled = YES;
        
        buttonAdvanceSearch.enabled = YES;
        
        if (isAdvancedSearch == YES && [buttonScripture.titleLabel.text isEqualToString:@"Sri Guru Granth Sahib"])
        {
            buttonAuthor.enabled=YES;
            buttonRaag.enabled=YES;
        }
        else
        {
            buttonAuthor.enabled=NO;
            buttonRaag.enabled=NO;
        }
    }
    else if(arrayFilter.count>0 || arrayResults.count>0)
    {
        if(arrayFilter.count>0)
        {
            NSComparisonResult (^sortBlock)(id, id) = ^(ISGDataModel *obj1, ISGDataModel *obj2) {
                if ([obj1.dataID intValue] > [obj2.dataID intValue]) {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                if ([obj1.dataID intValue] < [obj2.dataID intValue]) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                return (NSComparisonResult)NSOrderedSame;
            };
            NSArray *sorted = [arrayFilter sortedArrayUsingComparator:sortBlock];
            searchResultsViewController.arrayItems=sorted.mutableCopy;
        }
        else
        {
            searchResultsViewController.arrayItems=arrayResults;
        }
        searchResultsViewController.lang=@"gurmukhi";
        searchResultsViewController.querry = queryExecuted;
        NSArray * arraySearchHistory = [SearchHistory fetchAllSearchHistory];
        if(arraySearchHistory.count > 9)
        {
            NSMutableArray * arr = [NSMutableArray new];
            [arr removeAllObjects];
            for (int i = 0; i < arraySearchHistory.count; i++)
            {
                SearchHistory * obj = (SearchHistory *)arraySearchHistory[i];
                NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
                [dict setObject:obj.searchText forKey:@"text"];
                [dict setObject:obj.searchLanguage forKey:@"laguage"];
                [dict setObject:obj.searchQuery forKey:@"query"];
                [dict setObject:obj.searchCategory forKey:@"category"];
                [dict setObject:obj.searchQueryWildcard forKey:@"queryWildcard"];
                [arr addObject:dict];
            }
            [arr removeObjectAtIndex:0];
            [SearchHistory deleteAllSearchHistory];
            [SearchHistory updateSearchHistory:arr];
        }
        [SearchHistory addSearchHistoryForText:txtSearch.text Language:@"gurmukhi" Query:queryExecuted Category:buttonCriteria.titleLabel.text Type:wildCard ? @"Wildcard" : @"notWildcard" andWildCardQuery:queryWildCard];
        [self.navigationController pushViewController:searchResultsViewController animated:YES];
        //        home.enabled = YES;
        //        explore.enabled = YES;
        //        Banis.enabled = YES;
        //        more.enabled = YES;
        
    }
    });
    
}

//-(NSString *)getTotalNumberOfResults
//{
//    [database open];
//    NSString * count;
//    FMResultSet *results = [database executeQuery:queryCount];
//    while([results next])
//    {
//        ISGDataModel *data=[[ISGDataModel alloc]initWithCount:results];
//        count = data.count;
//    }
//    [database close];
//    return count;
//}

-(IBAction)btnCriteria:(id)sender
{
    isDropDownCategory = YES;
    NSMutableArray *arrayCriteria=[[NSMutableArray alloc]initWithObjects:@"First Letter Beginning",@"First Letter Anywhere",@"Phrase Search", nil];
    
    if(dropDown == nil) {
        CGFloat f = 165;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arrayCriteria];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}


-(void)rel{
    dropDown = nil;
    ////NSLog(@"Qid : %d",qID);
    /*
     if(!qID)
     {
     txtSearch.text=@"";
     [txtSearch resignFirstResponder];
     [arrayAdditionalResults removeAllObjects];
     [arrayResults removeAllObjects];
     englistFormString=@"";
     stringInput=@"";
     englistFormString=@"";
     temp=@"";
     txtSearch.text=@"";
     textLength=0;
     [self setkeyboardForLanguage:@"Gurmukhi"];
     }
     else
     {
     txtSearch.text=@"";
     [txtSearch resignFirstResponder];
     [arrayAdditionalResults removeAllObjects];
     [arrayResults removeAllObjects];
     englistFormString=@"";
     stringInput=@"";
     englistFormString=@"";
     temp=@"";
     txtSearch.text=@"";
     textLength=0;
     if(isDropDownCategory && [buttonLanguage.titleLabel.text isEqualToString:@"Gurmukhi"])
     {
     [self setkeyboardForLanguage:@"Gurmukhi"];
     }
     else
     {
     [self setkeyboardForLanguage:@"Roman"];
     }
     }
     
     */
    
   // txtSearch.text=@"";
    [txtSearch resignFirstResponder];
    [arrayAdditionalResults removeAllObjects];
    [arrayResults removeAllObjects];
    englistFormString=@"";
    stringInput=@"";
    englistFormString=@"";
    temp=@"";
   // txtSearch.text=@"";
    textLength=0;
    [self performSelector:@selector(checkLang) withObject:nil afterDelay:1];
    [self performSelector:@selector(checkAdvanceSearch) withObject:nil afterDelay:0.5];
    [self performSelector:@selector(checkSearchPreference) withObject:nil afterDelay:0.5];
    
}

- (void)checkSearchPreference
{
    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
    [dict setObject:buttonLanguage.titleLabel.text forKey:@"selectedLanguage"];
    [dict setObject:buttonCriteria.titleLabel.text forKey:@"selectedCriteria"];
    
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:dict forKey:@"searchPreference"];
    [userDefault synchronize];
}

- (void)checkAdvanceSearch
{
    if (isAdvancedSearch == YES && ([buttonScripture.titleLabel.text isEqualToString:@"Sri Guru Granth Sahib"] || [buttonScripture.titleLabel.text isEqualToString:@"Select Scripture"]))
    {
        buttonAuthor.enabled=YES;
        buttonRaag.enabled=YES;
    }
    else
    {
        buttonAuthor.enabled=NO;
        buttonRaag.enabled=NO;
        
    }
}

- (void)checkLang
{
    if([buttonLanguage.titleLabel.text isEqualToString:@"Gurmukhi"])
    {
        [self setkeyboardForLanguage:@"Gurmukhi"];
    }
    else
    {
        [self setkeyboardForLanguage:@"Roman"];
    }
}

-(void)setkeyboardForLanguage:(NSString *)lang
{
    // //NSLog(@"selected kb type : %@",lang);
    
    if([lang isEqualToString:@"Gurmukhi"])
    {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
        {
            PMCustomKeyboard *customKeyboard = [[PMCustomKeyboard alloc] init];
            [customKeyboard setTextView:txtSearch];
        }
        else
        {
            PKCustomKeyboard *customKeyboard = [[PKCustomKeyboard alloc] init];
            [customKeyboard setTextView:txtSearch];
        }
    }
    else
    {
        [txtSearch setInputView:nil];
        [txtSearch reloadInputViews];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(IBAction)btnHelp:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGHelpViewController * HelpViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGHelpViewController"];
    //ISGHelpViewController *HelpViewController=[[ISGHelpViewController alloc]initWithNibName:@"ISGHelpViewController" bundle:Nil];
           buttonSearch.enabled = NO;
             buttonHelp.enabled = NO;
         buttonCriteria.enabled = NO;
         buttonLanguage.enabled = NO;
              txtSearch.enabled = NO;
          buttonHistory.enabled = NO;
    buttonAdvanceSearch.enabled = NO;
    
    [self checkAdvanceSearch];
    [self.navigationController pushViewController:HelpViewController animated:YES];
    
    });
}
-(IBAction)btnLanguage:(id)sender
{
    isDropDownCategory = NO;
    NSMutableArray *arrayLanguage=[[NSMutableArray alloc]initWithObjects:@"Gurmukhi",@"Roman", nil];
    
    if(dropDown == nil) {
        CGFloat f = 110;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arrayLanguage];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
    [self checkLang];
    
}
-(IBAction)btnCheck:(id)sender
{
    click++;
    if(click%2!=0)
    {
        [imageCheck setImage:[UIImage imageNamed:@"tick.png"]];
        wildCard=YES;
    }
    else
    {
        [imageCheck setImage:[UIImage imageNamed:@"untick.png"]];
        wildCard=NO;
    }
}

-(void)btnAdvancedSearch:(id)sender
{
    cnt++;
    
    if (arrayAuthors.count == 0 || arrayRaags.count == 0)
    {
        [HUD showWhileExecuting:@selector(getListOfAuthorsRaagsAndSriptures) onTarget:self withObject:nil animated:YES];
    }
    
    [buttonScripture setTitle:@"Select Scripture" forState:UIControlStateNormal];
    [buttonRaag setTitle:@"Select Raag" forState:UIControlStateNormal];
    [buttonAuthor setTitle:@"Select Writer" forState:UIControlStateNormal];
    
    if(cnt%2==1)
    {
        [imageAdvancedSearchCheck setImage:[UIImage imageNamed:@"tick.png"]];
        isAdvancedSearch=YES;
        buttonScripture.enabled=YES;
        if (isAdvancedSearch == YES && ([buttonScripture.titleLabel.text isEqualToString:@"Sri Guru Granth Sahib"] || [buttonScripture.titleLabel.text isEqualToString:@"Select Scripture"]))
        {
            buttonAuthor.enabled=YES;
            buttonRaag.enabled=YES;
        }
        else
        {
            buttonAuthor.enabled=NO;
            buttonRaag.enabled=NO;
        }
    }
    else
    {
        [imageAdvancedSearchCheck setImage:[UIImage imageNamed:@"untick.png"]];
        isAdvancedSearch=NO;
        buttonAuthor.enabled=NO;
        buttonRaag.enabled=NO;
        buttonScripture.enabled=NO;
    }
}

-(void)btnAuthor:(id)sender
{
    if(dropDown == nil)
    {
        CGFloat f = 110;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arrayAuthors];
        dropDown.delegate = self;
        buttonSelected = sender;
    }
    else
    {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
    
}

-(void)btnRaag:(id)sender
{
    if(dropDown == nil)
    {
        CGFloat f = 110;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arrayRaags];
        dropDown.delegate = self;
        buttonSelected = sender;
    }
    else
    {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
}

-(void)btnScripture:(id)sender
{
    if(dropDown == nil)
    {
        CGFloat f = 110;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arrayScriptures];
        dropDown.delegate = self;
        buttonSelected = sender;
    }
    else
    {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getListOfAuthorsRaagsAndSriptures
{
    [self getAuthorsList];
    [self getRaagsList];
    //[self getScriptureList];
}

#pragma mark Get Authors, Scriptures and Raags

- (void)getAuthorsList
{
    [database open];
    
    NSString * querry = @"SELECT DISTINCT authorID FROM gurbani";
    
    ////NSLog(@"%@",querry);
    FMResultSet *results = [database executeQuery:querry];
    [arrayAuthors removeAllObjects];
    
    while([results next])
    {
        
        ISGDataModel *data=[[ISGDataModel alloc]initWithAuthors:results];
        if (data.authorID != nil && ![data.authorID isEqualToString: @""] && ![data.authorID isEqualToString: @"42"])
        {
            [arrayAuthors addObject:data.authorID];
        }
    }
    [database close];
}

- (void)getRaagsList
{
    [database open];
    
    NSString * querry = @"SELECT DISTINCT raagID FROM gurbani";
    
    ////NSLog(@"%@",querry);
    FMResultSet *results = [database executeQuery:querry];
    [arrayRaags removeAllObjects];
    
    while([results next])
    {
        ISGDataModel *data=[[ISGDataModel alloc]initWithRaags:results];
        if (data.raagID != nil && ![data.raagID isEqualToString: @""])
        {
            [arrayRaags addObject:data.raagID];
        }
    }
    
    [database close];
}

@end
