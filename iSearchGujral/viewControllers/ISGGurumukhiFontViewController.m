//
//  ISGGurumukhiFontViewController.m
//  iSearchGujral
//
//  Created by mac on 4/26/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGGurumukhiFontViewController.h"
#import "ISGTheme.h"
@interface ISGGurumukhiFontViewController ()
{
    ISGTheme *theme;
    UIImage * backgroundImage;
}
@end

@implementation ISGGurumukhiFontViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    //if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)[containerView setFrame:CGRectMake(100, 100, 200, 200)];
    self.title=@"Gurumukhi Fonts";
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    backgroundImage = theme.backgroundImage;
    stringBackgroud=theme.background;
    language1=theme.lang1;
    language2=theme.lang2;
    language3=theme.lang3;
    language4=theme.lang4;
    language5=theme.lang5;
    language6=theme.lang6;
//    language7=theme.lang7;
    
    strGurmukhiSize=theme.gurmukhiSize;
    strRomanSize=theme.romanSize;
    strHindiSize=theme.hindiSize;
    strEnglishSize=theme.englishSize;
    strTeekaSize=theme.teekaSize;
    strAttributeSize=theme.attributeSize;
    
    gurmukhiColor = theme.gurmukhiColor;
    romanColor = theme.romanColor;
    hindiColor = theme.hindiColor;
    englishColor = theme.englishColor;
    teekaColor = theme.teekaColor;
    attributeColor = theme.attributeColor;
    
    NSString *fontStyle=theme.fontStyle;
    fontSize=theme.fontSize;
    if(stringBackgroud.length<=7)
    {
        stringBackgroud=@"LightPattern";
        fontStyle=@"GurbaniLipiBold";
    }
    [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
    [self changeFontColor:stringBackgroud with:fontStyle];
    theme=[[ISGTheme alloc]init];
    // Do any additional setup after loading the view from its nib.
}

-(void)setFont:(NSString *)style
{
    theme.fontStyle=style;
    theme.fontSize=fontSize;
    theme.background=stringBackgroud;
    theme.backgroundImage = backgroundImage;
    
    theme.lang1=language1;
    theme.lang2=language2;
    theme.lang3=language3;
    theme.lang4=language4;
    theme.lang5=language5;
    theme.lang6=language6;
//    theme.lang7=language7;
    
    theme.gurmukhiSize=strGurmukhiSize;
    theme.romanSize=strRomanSize;
    theme.hindiSize=strHindiSize;
    theme.englishSize=strEnglishSize;
    theme.teekaSize=strTeekaSize;
    theme.attributeSize=strAttributeSize;
    
    theme.gurmukhiColor = gurmukhiColor;
    theme.romanColor = romanColor;
    theme.hindiColor = hindiColor;
    theme.englishColor = englishColor;
    theme.teekaColor = teekaColor;
    theme.attributeColor = attributeColor;
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:theme] forKey:@"theme"];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"refresh" object:Nil];
}
-(void)changeFontColor:(NSString *)pattern with:(NSString *)style
{
    NSLog(@"pattern :%@ & style :%@",pattern,style);
    if([style isEqualToString:@"Raaj Script Medium"])
    {
        [buttonRaaj setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonDefault setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonPrabhki setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonRaaja setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

    }
    else if([style isEqualToString:@"Prabhki"])
    {
        [buttonPrabhki setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonRaaj setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonRaaja setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonDefault setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

    }
    else if([style isEqualToString:@"Raajaa Script Medium"])
    {
        [buttonRaaja setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonRaaj setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonPrabhki setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonDefault setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

    }
    else
    {
        [buttonDefault setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonRaaj setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonPrabhki setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonRaaja setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

    }
    
    if([pattern isEqualToString:@"LightPattern"]||[pattern isEqualToString:@"WhitePattern"])
    {
        [labelDefault setTextColor:[UIColor blackColor]];
        [labelPrabhki setTextColor:[UIColor blackColor]];
        [labelRaaj setTextColor:[UIColor blackColor]];
        [labelRaaja setTextColor:[UIColor blackColor]];
    }
    else
    {
        [labelDefault setTextColor:[UIColor whiteColor]];
        [labelPrabhki setTextColor:[UIColor whiteColor]];
        [labelRaaj setTextColor:[UIColor whiteColor]];
        [labelRaaja setTextColor:[UIColor whiteColor]];
    }
    
}

-(IBAction)btnRaaj:(id)sender
{
    [self setFont:@"Raaj Script Medium"];
    [buttonRaaj setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
    [buttonDefault setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [buttonPrabhki setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [buttonRaaja setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

}
-(IBAction)btnDefault:(id)sender
{
    [self setFont:@"GurbaniLipiBold"];
    [buttonDefault setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
    [buttonRaaj setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [buttonPrabhki setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [buttonRaaja setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

}
-(IBAction)btnRaaja:(id)sender
{
    [self setFont:@"Raajaa Script Medium"];
    [buttonRaaja setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
    [buttonRaaj setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [buttonPrabhki setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [buttonDefault setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

}
-(IBAction)btnPrabhki:(id)sender
{
    [self setFont:@"Prabhki"];
    [buttonPrabhki setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
    [buttonRaaj setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [buttonRaaja setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [buttonDefault setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
