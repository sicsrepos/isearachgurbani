//
//  ISGBaniViewController.h
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISGBaniListViewController.h"
#import "ISGBaniDetailViewController.h"
@interface ISGBaniViewController : UIViewController
{
    NSMutableArray *baniArray;
    IBOutlet UIImageView *imageBg;
    NSString *stringBackgroud;
    IBOutlet UITableView *tableBaani;
    int fontSize;
    NSString *fontStyle;
}
@end
