//
//  ISGFavouritesViewController.m
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGFavouritesViewController.h"
#import "Favourites.h"
#import "ISGAppDelegate.h"
#import "FMDatabase.h"
#import "ISGSearchDetailViewController.h"
#import "BookMarkFolders.h"
#import "ISGFavouriteCollectionViewCell.h"
#import "BookMarkPages.h"
#import "ISGBookMarkListViewController.h"


@interface ISGFavouritesViewController ()<UIAlertViewDelegate>
{
    FMDatabase *database;
    NSMutableArray *arrayFavourites;
    IBOutlet UIView * collectionView;
    NSMutableArray * arrayNames;
    NSMutableArray * arrayFolders;
    NSMutableArray * arrayIndex;
    int times;
    BOOL check;
    NSMutableArray * arraySortedFolders;
}
@end

@implementation ISGFavouritesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    database = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).database;
    arrayFavourites=[[NSMutableArray alloc]init];
    //[self getFavouritesData];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"favourites" object:Nil];
    UIBarButtonItem * button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addFolder:)];
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationController.navigationBar.translucent = NO;
        [button setTintColor:[UIColor whiteColor]];
    }
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMe:)];
    tapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:tapGesture];
    
    self.navigationItem.rightBarButtonItem=button;
    
    
    //[collectionFavourite registerNib:[UINib nibWithNibName:@"ISGFavouriteCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"myCell"];
    arrayNames = [NSMutableArray new];
    arrayFolders = [NSMutableArray new];
    arrayIndex = [NSMutableArray new];
    arraySortedFolders = [NSMutableArray new];
    
    //uzysGridView = [[UzysGridView alloc] init];
    
    [self getBookmarkFolders];
    
    [self setBookMarkFolders];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeBG:) name:@"refresh" object:Nil];
   
    
    // Do any additional setup after loading the view from its nib.
}
-(void)changeBG:(NSNotification *)notification
{
    [self setTheme];
}
-(void)setTheme
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    ISGTheme *theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    stringBackgroud=theme.background;
    fontSize=[theme.fontSize intValue];
    if(fontSize==0)
    {
        fontSize=14;
    }
    if(stringBackgroud.length<=7)
    {
        stringBackgroud=@"LightPattern";
    }
    if (!theme.backgroundImage)
    {
        [backgroundIng setImage:[UIImage imageNamed:stringBackgroud]];
    }
    else
        backgroundIng.image = theme.backgroundImage;
    
}



- (void)tapMe:(UITapGestureRecognizer *)gestureRecognizer
{
    ISGFavouriteCollectionViewCell * cell = [[ISGFavouriteCollectionViewCell alloc] initWithFrame:CGRectNull];
    cell.ButtonDelete.hidden = YES;
    check = NO;
    [uzysGridView reloadData];
}

-(void) getBookmarkFolders
{
    [arrayFolders removeAllObjects];
    arrayFolders = [[BookMarkFolders fetchAllBookMarkFolders]mutableCopy];
    [arrayNames removeAllObjects];
    [arrayIndex removeAllObjects];
    [arraySortedFolders removeAllObjects];
    
    for (int i = (int)arrayFolders.count; i>=0; i--)
    {
        int count = 0;
        for (BookMarkFolders * folder in arrayFolders)
        {
            if (i == [folder.index intValue])
            {
                [arraySortedFolders addObject:arrayFolders[count]];
            }
            else
                count++;
        }
    }
 /*   for (int j=0; j<arraySortedFolders.count-1; j++)
    {
        BookMarkFolders *obj;
        [arrayNames addObject:obj.folderName];
        [arrayIndex addObject:obj.index];
    }*/

    [arraySortedFolders enumerateObjectsUsingBlock:^(BookMarkFolders *obj, NSUInteger idx, BOOL *stop)
    {
        [arrayNames addObject:obj.folderName];
        [arrayIndex addObject:obj.index];
    }];
    
    times++;
    
    [uzysGridView reloadData];
    
}

-(void)setBookMarkFolders
{
    uzysGridView = [[UzysGridView alloc] initWithFrame:collectionView.frame numOfRow:10 numOfColumns:4 cellMargin:7];
    uzysGridView.delegate = self;
    uzysGridView.dataSource = self;
    [self.view addSubview:uzysGridView];
}

-(void)addFolder:(id)sender
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Folder Name" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.tag = 2;
    [alert show];
    ISGFavouriteCollectionViewCell * cell = [[ISGFavouriteCollectionViewCell alloc] initWithFrame:CGRectNull];
    cell.ButtonDelete.hidden = YES;
    check = NO;
    [uzysGridView reloadData];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 2 && buttonIndex == 1)
    {
        UITextField * textFiled = [alertView textFieldAtIndex:0];
        if (textFiled.text.length > 0)
        {
         if (UIScreen.mainScreen.bounds.size.height == 736)
            {
             if (textFiled.text.length > 8)
                {
                    [[[UIAlertView alloc] initWithTitle:@"Folder name should be less than 8 characters" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
                }
                else
                {
                    int cnt = 0;
                    for (NSString * name in arrayNames)
                    {
                        if ([textFiled.text isEqualToString:name])
                        {
                            cnt++;
                        }
                    }
                    if (cnt>0)
                    {
                        [SVProgressHUD showErrorWithStatus:@"Folder already exist"];
                    }
                    else
                    {
                        [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
                        [self getBookmarkFolders];
                    }

                }
           }
        else if ((UIScreen.mainScreen.bounds.size.height == 667))
        {
            if (textFiled.text.length > 7)
            {
             [[[UIAlertView alloc] initWithTitle:@"Folder name should be less than 7 characters" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];

            }
            else
            {
                int cnt = 0;
                for (NSString * name in arrayNames)
                {
                    if ([textFiled.text isEqualToString:name])
                    {
                        cnt++;
                    }
                }
                if (cnt>0)
                {
                    [SVProgressHUD showErrorWithStatus:@"Folder already exist"];
                }
                else
                {
                    [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
                    [self getBookmarkFolders];
                }
                
            }
                
        }
       else if ((UIScreen.mainScreen.bounds.size.height == 568))
        {
        if (textFiled.text.length > 6)
            {
            [[[UIAlertView alloc] initWithTitle:@"Folder name should be less than 6 characters" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
            }
            else
            {
                int cnt = 0;
                for (NSString * name in arrayNames)
                {
                    if ([textFiled.text isEqualToString:name])
                    {
                        cnt++;
                    }
                }
                if (cnt>0)
                {
                    [SVProgressHUD showErrorWithStatus:@"Folder already exist"];
                }
                else
                {
                    [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
                    [self getBookmarkFolders];
                }
                
            }
            
        }
      else if ((UIScreen.mainScreen.bounds.size.height == 1024))
        {
        if (textFiled.text.length > 15)
        {
         [[[UIAlertView alloc] initWithTitle:@"Folder name should be less than 15 characters" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        else
        {
            int cnt = 0;
            for (NSString * name in arrayNames)
            {
                if ([textFiled.text isEqualToString:name])
                {
                    cnt++;
                }
            }
            if (cnt>0)
            {
                [SVProgressHUD showErrorWithStatus:@"Folder already exist"];
            }
            else
            {
                [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
                [self getBookmarkFolders];
            }
            
        }
            
        }
      else
      {
            int cnt = 0;
            for (NSString * name in arrayNames)
            {
                  if ([textFiled.text isEqualToString:name])
                  {
                      cnt++;
                  }
              }
              if (cnt>0)
              {
                  [SVProgressHUD showErrorWithStatus:@"Folder already exist"];
              }
              else
              {
                  [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
                  [self getBookmarkFolders];
              }
              
          
          
      }
   
            
            
            
            
        }
            
        
            
            
        
        else
            [[[UIAlertView alloc] initWithTitle:@"Invalid Text" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        
    }
   
    //[collectionFavourite reloadData];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    
    self.title=@"My Favourites";
    [super viewWillAppear:YES];
    [self getBookmarkFolders];
    ISGFavouriteCollectionViewCell * cell = [[ISGFavouriteCollectionViewCell alloc] initWithFrame:CGRectNull];
    check = NO;
    cell.ButtonDelete.hidden = YES;

    [uzysGridView reloadData];
    //[collectionFavourite reloadData];
}

-(void)refresh:(NSNotification *)notification
{
    [self getFavouritesData];
}
-(void)clearData
{
    [database open];
    [database executeUpdate:@"DELETE FROM bookmark;"];
//    NSLog(@"%@",[database lastErrorMessage]);
    [database close];
    [arrayFavourites removeAllObjects];
    //[tableFavourites reloadData];
}
-(void)getFavouritesData
{
   
    [arrayFavourites removeAllObjects];
    [database open];
    NSString *querry=[NSString stringWithFormat:@"SELECT * FROM bookmark"];
    FMResultSet *results = [database executeQuery:querry];
    while([results next])
	{
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithFavouriteList:results];
        [arrayFavourites addObject:dataModel];
    }
    [database close];
    if(arrayFavourites.count==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"You have no favourite list" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        //[tableFavourites reloadData];
    }
}

#pragma mark tableview delegates
-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%d",(int)arrayFavourites.count);
    return [arrayFavourites count];
}
-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"favouritesCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil)
    {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
             NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FavouriteCell_iPad" owner:self options:nil];
            if ([nib count]>0)
            {
                cell = [nib objectAtIndex:0];
            }
        }
        else
        {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FavouriteCell" owner:self options:nil];
            if ([nib count]>0)
            {
                cell = [nib objectAtIndex:0];
            }
        }
        
    }
    ISGDataModel *data=[arrayFavourites objectAtIndex:indexPath.row];
    UILabel *labelBook=(UILabel *)[cell viewWithTag:1];
    UILabel *labelText=(UILabel *)[cell viewWithTag:2];
    //UILabel *labelAng=(UILabel *)[cell viewWithTag:3];
    labelBook.text=data.script;
    labelText.text=data.gurmukhi;
   // labelAng.text=[NSString stringWithFormat:@"Ang: %@",favourites.ang];
    if(![data.lang isEqualToString:@"roman"])
    [labelText setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:14]];
    else [labelText setFont:[UIFont systemFontOfSize:14]];
    if([data.script isEqualToString:@"Sri Guru Granth Sahib"])
    {
       [labelBook setTextColor :[UIColor colorWithRed:63.0/255 green:55.0/255 blue:136.0/255 alpha:1]];
       [labelText setTextColor :[UIColor colorWithRed:63.0/255 green:55.0/255 blue:136.0/255 alpha:1]];
    }
    else if([data.script isEqualToString:@"Bhai Gurdas Vaaran"])
    {
        [labelBook setTextColor:[UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0]];
        [labelText setTextColor:[UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0]];
    }
    else if([data.script isEqualToString:@"Kabit Bhai Gurdas"])
    {
        labelBook.textColor=[UIColor grayColor];
        labelText.textColor=[UIColor grayColor];
    }
    else if([data.script isEqualToString:@"Baani Bhai Nand Lal"])
    {
        [labelBook setTextColor:[UIColor colorWithRed:113.0/255 green:2.0/255 blue:67.0/255 alpha:1 ]];
        [labelText setTextColor:[UIColor colorWithRed:113.0/255 green:2.0/255 blue:67.0/255 alpha:1 ]];
    }
    else if([data.script isEqualToString:@"Sri Dasam Granth Sahib"])
    {
        [labelBook setTextColor:[UIColor colorWithRed:52.0/255 green:96.0/255 blue:68.0/255 alpha:1 ]];
        [labelText setTextColor:[UIColor colorWithRed:52.0/255 green:96.0/255 blue:68.0/255 alpha:1 ]];
    }
    else
    {
        labelBook.textColor=[UIColor brownColor];
        labelText.textColor=[UIColor brownColor];
    }
    cell.backgroundColor=[UIColor clearColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.title=@"Back";
    ISGDataModel *data=[arrayFavourites objectAtIndex:indexPath.row];
    if([data.tab isEqualToString:@"ISGRead"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGReadSGGSViewController * readSGGSViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGReadSGGSViewController"];
        //ISGReadSGGSViewController *readSGGSViewController=[[ISGReadSGGSViewController alloc]initWithNibName:@"ISGReadSGGSViewController" bundle:Nil];
        readSGGSViewController.page=[data.page intValue];
        readSGGSViewController.strFavour=@"fav";
        NSLog(@"check ISGRead %@",data.page);
        [self.navigationController pushViewController:readSGGSViewController animated:YES];
    }
    else if([data.tab isEqualToString:@"Baani1"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGBaniDetailViewController * baniDetailViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGBaniDetailViewController"];
        //ISGBaniDetailViewController *baniDetailViewController=[[ISGBaniDetailViewController alloc]initWithNibName:@"ISGBaniDetailViewController" bundle:Nil];
        baniDetailViewController.bani=data.script;
        baniDetailViewController.key=data.key;
        baniDetailViewController.strFavour=@"fav";
        baniDetailViewController.arrayIndex=[data.indexValue intValue];
        [self.navigationController pushViewController:baniDetailViewController animated:YES];

    }
   
    else if([data.tab isEqualToString:@"Search"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGSearchDetailViewController * searchDetailViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGSearchDetailViewController"];
        //ISGSearchDetailViewController *searchDetailViewController=[[ISGSearchDetailViewController alloc]initWithNibName:@"ISGSearchDetailViewController" bundle:Nil];
       // NSLog(@"data :%@",data.script);
        searchDetailViewController.ang=data.page;
        searchDetailViewController.language=data.lang;
        
        [self.navigationController pushViewController:searchDetailViewController animated:YES];
    }
    else 
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGBaniDetailViewController * baniDetailViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGBaniDetailViewController"];
       // ISGBaniDetailViewController *baniDetailViewController=[[ISGBaniDetailViewController alloc]initWithNibName:@"ISGBaniDetailViewController" bundle:Nil];
        baniDetailViewController.bani=data.script;
        baniDetailViewController.key=data.key;
        baniDetailViewController.strFavour=@"fav";
        baniDetailViewController.rootClass=@"ISGBaniViewController";
        baniDetailViewController.arrayIndex=[data.indexValue intValue];
        [self.navigationController pushViewController:baniDetailViewController animated:YES];
        
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 62;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        ISGDataModel *data=[arrayFavourites objectAtIndex:indexPath.row];
        [database open];
        [database executeUpdate:@"DELETE FROM bookmark WHERE markText=?;",data.gurmukhi];
//        NSLog(@"%@",[database lastErrorMessage]);
        [database close];
        [arrayFavourites removeObjectAtIndex:indexPath.row];
        //[tableFavourites reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrayFolders.count;
}



#pragma mark -UzysGridViewDataSource

-(NSInteger)numberOfCellsInGridView:(UzysGridView *)gridview
{
    return arrayNames.count;
}

-(UzysGridViewCell *)gridView:(UzysGridView *)gridview cellAtIndex:(NSUInteger)index
{
    ISGFavouriteCollectionViewCell * cell = [[ISGFavouriteCollectionViewCell alloc] initWithFrame:CGRectNull];
    cell.labelName.text = arrayNames[index];
    if (check)
    {
        cell.ButtonDelete.hidden = NO;
    }
    return cell;
}

-(void)gridView:(UzysGridView *)gridview moveAtIndex:(NSUInteger)fromindex toIndex:(NSUInteger)toIndex
{
    NSString * str = arrayNames[fromindex];
    
    [arrayNames removeObjectAtIndex:fromindex];
    [arrayNames insertObject:str atIndex:toIndex];
    
    [BookMarkFolders updateBookMarkIndex:arrayIndex andFolders:arrayNames];
}


-(void) gridView:(UzysGridView *)gridView didSelectCell:(UzysGridViewCell *)cell atIndex:(NSUInteger)index
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    
    ISGBookMarkListViewController * listOfBookMarks  = [storyboard instantiateViewControllerWithIdentifier:@"ISGBookMarkListViewController"];
   // ISGBookMarkListViewController * listOfBookMarks = [[ISGBookMarkListViewController alloc] initWithNibName:@"ISGBookMarkListViewController" bundle:nil];
    listOfBookMarks.folderName = arraySortedFolders[index];
    listOfBookMarks.titleName = arrayNames[index];
    [self.navigationController pushViewController:listOfBookMarks animated:YES];
    
    
}

- (void) gridView:(UzysGridView *)gridview deleteAtIndex:(NSUInteger)index
{
    
//    [self getBookmarkFolders];
    [self arrangeIndex:(int)index];
    check = YES;
    [BookMarkFolders deleteBookMarkFolderWithName:arrayNames[index]];
    
    
    [uzysGridView reloadData];
   // [gridview reloadData];
}



-(void) arrangeIndex:(int)index
{
    NSMutableArray * sortedIndex = [NSMutableArray new];
    NSMutableArray * sortedArray = [NSMutableArray new];
    [sortedIndex removeAllObjects];
    [sortedArray removeAllObjects];
    for (int i=(int)arrayIndex.count-2; i>=0; i--)
    {
        [sortedIndex addObject:[NSString stringWithFormat:@"%d",i]];
        
    }
    for (int j=0; j<arrayNames.count; j++)
    {
        if (j!=index)
        {
            [sortedArray addObject:arrayNames[j]];
        }
    }
    
    
    [BookMarkFolders updateBookMarkIndex:sortedIndex andFolders:sortedArray];
    [self getBookmarkFolders];

}

@end
