//
//  ISGHelpViewController.h
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISGHelpViewController : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIWebView *webView;
    
}

@end
