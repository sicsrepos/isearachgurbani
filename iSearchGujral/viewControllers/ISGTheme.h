//
//  ISGTheme.h
//  iSearchGujral
//
//  Created by mac on 4/30/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISGTheme : NSObject<NSCoding>


@property(nonatomic, retain)NSString *background;
@property(nonatomic, retain)UIImage * backgroundImage;
@property(nonatomic, retain)NSString *font;
@property(nonatomic, retain)NSString *fontStyle;
@property(nonatomic, retain)NSString *fontSize;

@property(nonatomic, retain)NSString *lang1;
@property(nonatomic, retain)NSString *lang2;
@property(nonatomic, retain)NSString *lang3;
@property(nonatomic, retain)NSString *lang4;
@property(nonatomic, retain)NSString *lang5;
@property(nonatomic, retain)NSString *lang6;
//@property(nonatomic, retain)NSString *lang7;

@property(nonatomic, retain)NSString *gurmukhiSize;
@property(nonatomic, retain)NSString *romanSize;
@property(nonatomic, retain)NSString *hindiSize;
@property(nonatomic, retain)NSString *englishSize;
@property(nonatomic, retain)NSString *teekaSize;
@property(nonatomic, retain)NSString *attributeSize;

@property(nonatomic, retain)UIColor *gurmukhiColor;
@property(nonatomic, retain)UIColor *romanColor;
@property(nonatomic, retain)UIColor *hindiColor;
@property(nonatomic, retain)UIColor *englishColor;
@property(nonatomic, retain)UIColor *teekaColor;
@property(nonatomic, retain)UIColor *attributeColor;

+(id)getTheme;
- (void) encodeWithCoder:(NSCoder*)encoder;
- (id) initWithCoder:(NSCoder*)decoder;
@end
