//
//  ISGPreferenceViewController.m
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGPreferenceViewController.h"
#import "ISGTheme.h"
#import "SVProgressHUD.h"
#import "ISGFontColorViewController.h"
#import "ISGCustomSettingsViewController.h"
#import "CustomSettings.h"


@interface ISGPreferenceViewController ()
{
    IBOutlet UITableView *tablePreference;
    UIBarButtonItem * buttonSubmit;
}
@end

@implementation ISGPreferenceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationController.navigationBar.translucent = NO;
    }
    self.title=@"Preferences";
    
    buttonSubmit = [[UIBarButtonItem alloc] initWithTitle:@"Save Settings" style:UIBarButtonItemStyleBordered target:self action:@selector(actionButtonCustomSettings)];
    [buttonSubmit setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = buttonSubmit;
    
    arrayList=[[NSMutableArray alloc]initWithObjects:@"Select Display Language",@"Select Font Size",@"Select Gurmukhi Font Style",@"Select Font Color",@"Select Background Theme",@"Reset to Default", @"Custom Settings", nil];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self checkCustomSettings];
    [tablePreference reloadData];
}

-(void)actionBackButton
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayList count];
}
-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    float fontSize = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 26 : 16;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text=[arrayList objectAtIndex:indexPath.row];
    cell.textLabel.font=[UIFont systemFontOfSize:fontSize];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 60;
    }
    else
        return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        switch (indexPath.row) {
            case 0:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGLanguageViewController * languageViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGLanguageViewController"];
                //ISGLanguageViewController *languageViewController=[[ISGLanguageViewController alloc]initWithNibName:@"ISGLanguageViewController" bundle:Nil];
                [self.navigationController pushViewController:languageViewController animated:YES];
            }
            break;
            case 1:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGFontViewController * fontViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGFontViewController"];
                //ISGFontViewController *fontViewController=[[ISGFontViewController alloc]initWithNibName:@"ISGFontViewController" bundle:Nil];
                [self.navigationController pushViewController:fontViewController animated:YES];
            }
            break;
            case 2:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGGurumukhiFontViewController * gurumukhiFontViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGGurumukhiFontViewController"];
                //ISGGurumukhiFontViewController *gurumukhiFontViewController=[[ISGGurumukhiFontViewController alloc]initWithNibName:@"ISGGurumukhiFontViewController" bundle:Nil];
                [self.navigationController pushViewController:gurumukhiFontViewController animated:YES
                 ];
            }
            break;
            case 3:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGFontColorViewController * fontColorViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGFontColorViewController"];
               // ISGFontColorViewController *fontColorViewController=[[ISGFontColorViewController alloc]initWithNibName:@"ISGFontColorViewController" bundle:Nil];
                [self.navigationController pushViewController:fontColorViewController animated:YES];
            }
            break;
            case 4:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGBackgroundViewController * backgroundViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGBackgroundViewController"];
               // ISGBackgroundViewController *backgroundViewController=[[ISGBackgroundViewController alloc]initWithNibName:@"ISGBackgroundViewController" bundle:Nil];
                [self.navigationController pushViewController:backgroundViewController animated:YES];
            }
            break;
            case 5:
            {
                
                [[[UIAlertView alloc]initWithTitle:Nil message:@"Are you sure you want to reset the settings ?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil]show];
            }
            break;
            case 6:
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
                
                ISGCustomSettingsViewController * customSettingsViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGCustomSettingsViewController"];
               // ISGCustomSettingsViewController *customSettingsViewController=[[ISGCustomSettingsViewController alloc]initWithNibName:@"ISGCustomSettingsViewController" bundle:Nil];
                [self.navigationController pushViewController:customSettingsViewController animated:YES];
            }
                break;
        default:
            break;
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [self setDefault];
        [SVProgressHUD showSuccessWithStatus:@"Changed to defalut values"];
    }
    [self checkCustomSettings];
    [tablePreference reloadData];
}
-(void)setDefault
{
    ISGTheme *theme = [[ISGTheme alloc]init];
    theme.background = @"LightPattern";
    theme.fontStyle = @"GurbaniLipiBold";
    theme.fontSize = @"16";
    theme.lang1 = @"gurmukhi";
    theme.lang2 = @"roman";
    theme.lang3 = @"";
    theme.lang4 = @"english";
    theme.lang5 = @"";
    theme.lang6=@"attributes";
    theme.gurmukhiSize=@"22";
    theme.romanSize=@"14";
    theme.hindiSize=@"17";
    theme.englishSize=@"14";
    theme.teekaSize=@"17";
    theme.attributeSize=@"14";
    
    theme.gurmukhiColor = [UIColor blackColor];
    theme.teekaColor = [UIColor colorWithRed:83.0/255 green:54.0/255 blue:25.0/255 alpha:1];
    theme.hindiColor = [UIColor colorWithRed:113.0/255 green:2.0/255 blue:67.0/255 alpha:1];
    theme.romanColor = [UIColor colorWithRed:51.0/255 green:0 blue:64.0/255 alpha:1];
    theme.englishColor = [UIColor colorWithRed:110.0/255 green:0 blue:37/255 alpha:1];
    theme.attributeColor = [UIColor darkGrayColor];


    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:theme] forKey:@"theme"];
    [defaults synchronize];
    NSUserDefaults * defaults1 = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults1 objectForKey:@"theme"];
    theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"refresh" object:Nil];
    
}

-(void)actionButtonCustomSettings
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = (NSData *)[defaults objectForKey:@"theme"];
    [CustomSettings addCustomSettingName:@"Custom" andSettings:data];
    [self checkCustomSettings];
}

-(void)checkCustomSettings
{
    BOOL ishidden = NO;
    NSArray * arr = [CustomSettings fetchAllCustomSettings];
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    for (int i = 0; i < arr.count; i++)
    {
        CustomSettings * setting = (CustomSettings *)arr[i];
        if ([setting.setting isEqualToData:data])
        {
            ishidden = YES;
        }
    }
    
    if (ishidden)
    {
        buttonSubmit.enabled = NO;
    }
    else
    {
        buttonSubmit.enabled = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
