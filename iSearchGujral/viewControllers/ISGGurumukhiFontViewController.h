//
//  ISGGurumukhiFontViewController.h
//  iSearchGujral
//
//  Created by mac on 4/26/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISGGurumukhiFontViewController : UIViewController
{
    IBOutlet UIButton *buttonRaaj,*buttonPrabhki;
    IBOutlet UIButton *buttonDefault,*buttonRaaja;
    IBOutlet UIImageView *imageBg;
    IBOutlet UIView *containerView;
    __weak IBOutlet UILabel *labelRaaj;
    __weak IBOutlet UILabel *labelPrabhki;
    __weak IBOutlet UILabel *labelRaaja;
    __weak IBOutlet UILabel *labelDefault;
    NSString *stringBackgroud;
    NSString *fontSize;
    NSString *language1;
    NSString *language2,*language3,*language4,*language5,*language6,*strGurmukhiSize,*strRomanSize,*strHindiSize,*strEnglishSize,*strTeekaSize,*strAttributeSize;
    UIColor *gurmukhiColor,*romanColor,*hindiColor,*englishColor,*teekaColor,*attributeColor;
}
-(IBAction)btnRaaj:(id)sender;
-(IBAction)btnDefault:(id)sender;
-(IBAction)btnRaaja:(id)sender;
-(IBAction)btnPrabhki:(id)sender;
@end
