//
//  ISGHomeViewController.m
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGHomeViewController.h"
#import "FMDatabase.h"
#import "ISGDataModel.h"
#import "ISGTheme.h"
#import "ISGSearchDetailViewController.h"
#import "ISGSearchResultsViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "MBProgressHUD.h"
#import "ISGCell.h"
@interface ISGHomeViewController ()<EGORefreshTableHeaderDelegate,MBProgressHUDDelegate,UITableViewDelegate,UITableViewDataSource>

{
    
    UILongPressGestureRecognizer *lpHandler;
    int randomIndex;
    BOOL push;
    int size;
    NSMutableDictionary *selectedData;
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
    NSMutableArray *cells;
    UITableViewCell *cell;
}
@end

@implementation ISGHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    push=YES;
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    CGFloat screenWidth = screenSize.width;

   /* dispatch_async(dispatch_get_main_queue(), ^{
    if (screenHeight == 736)
    {
      tableHome.frame = CGRectMake(30, 60, [UIScreen mainScreen].bounds.size.width - 60, screenHeight-180);
    }
    else if (screenHeight == 667)
    {
       tableHome.frame = CGRectMake(30, 60, [UIScreen mainScreen].bounds.size.width - 60, screenHeight-400);
    }
    else if (screenHeight == 736)
    {
         tableHome.frame = CGRectMake(30, 60, [UIScreen mainScreen].bounds.size.width - 60, screenHeight-450);
    }
    else if (screenHeight > 850)
    {
        tableHome.frame = CGRectMake(80, 60, [UIScreen mainScreen].bounds.size.width - 160, screenHeight-450);
    }
    else if (UIScreen.mainScreen.bounds.size.height == 568.000000)
    {
        tableHome.frame = CGRectMake(40, 60, [UIScreen mainScreen].bounds.size.width - 80, screenHeight-30);
    }
    else if (UIScreen.mainScreen.bounds.size.height == 1366.000)
    {
        tableHome.frame = CGRectMake(44, 50, [UIScreen mainScreen].bounds.size.width - 88,screenHeight-200);
    }
   
    
    else
    {
        tableHome.frame = CGRectMake(30, 60, [UIScreen mainScreen].bounds.size.width - 60, [UIScreen mainScreen].bounds.size.height-300);
    }
        tableHome.center = self.view.center;

    });*/
    //  tableHome.frame = CGRectMake(20,40,[UIScreen mainScreen].bounds.size.width - 40,[UIScreen mainScreen].bounds.size.height - 130);
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
         [self.tabBarController.tabBar setTranslucent:NO];
    }
    arrayQuotes=[[NSMutableArray alloc]init];
    float low_bound = 350;
    float high_bound = 900;

    float rndValue = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
    randomIndex = (int)(rndValue + 0.5);
    [self generateQuote];
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    int key = [userDefaults integerForKey:@"firstLaunch"];
    userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:1 forKey:@"firstLaunch"];
    [userDefaults synchronize];
    if(key==0)
    {
        ISGTheme *theme=[[ISGTheme alloc]init];
        theme.background=@"LightPattern";
        theme.fontStyle=@"GurbaniLipiBold";
        theme.fontSize=@"16";
        
        theme.lang1=@"gurmukhi";
        theme.lang2=@"roman";
        theme.lang3=@"";
        theme.lang4=@"english";
        theme.lang5=@"";
        theme.lang6=@"attributes";
//        theme.lang7=@"";
        
        theme.gurmukhiSize=@"22";
        theme.romanSize=@"14";
        theme.hindiSize=@"17";
        theme.englishSize=@"14";
        theme.teekaSize=@"17";
        theme.attributeSize=@"14";
        
        theme.gurmukhiColor = [UIColor blackColor];
        theme.teekaColor = [UIColor colorWithRed:83.0/255 green:54.0/255 blue:25.0/255 alpha:1];
        theme.hindiColor = [UIColor colorWithRed:113.0/255 green:2.0/255 blue:67.0/255 alpha:1];
        theme.romanColor = [UIColor colorWithRed:51.0/255 green:0 blue:64.0/255 alpha:1];
        theme.englishColor = [UIColor colorWithRed:110.0/255 green:0 blue:37/255 alpha:1];
        theme.attributeColor = [UIColor darkGrayColor];
        
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:theme] forKey:@"theme"];
        [defaults synchronize];
        NSUserDefaults * defaults1 = [NSUserDefaults standardUserDefaults];
        NSData *data = [defaults1 objectForKey:@"theme"];
        theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    NSLog(@"The screen height is %f",[UIScreen mainScreen].bounds.size.height);
    lpHandler = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressHandler:)];
    lpHandler.minimumPressDuration = 0.5;
    [tableHome addGestureRecognizer:lpHandler];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pin:) name:@"pin" object:selectedData];
     if (_refreshHeaderView == nil) {
        
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - tableHome.bounds.size.height, tableHome.bounds.size.width, tableHome.bounds.size.height - 50)];
        [view setBackgroundColor:[UIColor clearColor]];
        if([UIScreen mainScreen].bounds.size.height == 736)
        {
            view.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2 + 17/*-40*/, view.center.y);
        }
        else if ([UIScreen mainScreen].bounds.size.height == 667)
        {
            view.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2/*-33*/, view.center.y);
        }
         if([UIScreen mainScreen].bounds.size.height > 736)
         {
             view.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2 /*-40*/, view.center.y);
         }
         
         else{
             view.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2/*-33*/, view.center.y);
 
         }
         

         
        view.delegate = self;
        [tableHome addSubview:view];
        _refreshHeaderView = view;
    }
    
    [_refreshHeaderView refreshLastUpdatedDate];
    
    
    
    
}







-(void)pin:(NSNotification *)notification
{
    randomIndex=selectedShabd;
    [self generateQuote];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    push=YES;
}
-(void)longPressHandler:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if(push)
    {
    push=NO;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGSearchDetailViewController * searchDetailViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGSearchDetailViewController"];
    //ISGSearchDetailViewController *searchDetailViewController=[[ISGSearchDetailViewController alloc]initWithNibName:@"ISGSearchDetailViewController" bundle:Nil];
    searchDetailViewController.ang=[NSString stringWithFormat:@"%d",randomIndex];//shabad page display is based on shabad no:, not ang(page)
    [self.navigationController pushViewController:searchDetailViewController animated:YES];
    }

}
-(void)generateQuote
{
   
    FMDatabase *database = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).database;
    [database open];
    //NSString *querry=[NSString stringWithFormat:@"SELECT gurmukhi,english,roman FROM gurbani WHERE shabd='%d'",randomIndex];
    NSString *querry=[NSString stringWithFormat:@"SELECT gurmukhi,english,roman FROM gurbani INDEXED BY homeIndex WHERE shabd='%d'",randomIndex];
    NSLog(@"%@",querry);
    FMResultSet *results = [database executeQuery:querry];
    
    [arrayQuotes removeAllObjects];
    while([results next])
	{
        ISGDataModel *dataModel=[[ISGDataModel alloc]generateQuote:results];
        [arrayQuotes addObject:dataModel];
    }
    [database close];
    [self buildCells];
}

-(void)buildCells
{
    (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?(size=26):(size=20);
    if (cells) [cells removeAllObjects]; else cells = [[NSMutableArray alloc] init];
    if (heightsCell) [heightsCell removeAllObjects]; else heightsCell = [[NSMutableArray alloc] init];

    for (ISGDataModel *dataModel in arrayQuotes) {
        UILabel *label = [[UILabel alloc] init];
        label.text=dataModel.gurmukhi;
        label.textColor=[UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0];
        label.font=[UIFont fontWithName:@"GurbaniLipiBold" size:size];
        label.numberOfLines=10;
        label.lineBreakMode=NSLineBreakByWordWrapping;
        label.textAlignment = NSTextAlignmentCenter;
        label.center = tableHome.center;
        static NSString *cellIdentifier = @"ISGCell";
        cell = [tableHome dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[ISGCell alloc] initWithQuote:label];
            int labelHeight = 0;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                label.frame = CGRectMake(5, labelHeight+2, 630, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(630, 9999)].height);
            }
            else
            {
                //287
                label.frame = CGRectMake(0, labelHeight+2, 265, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(265, 9999)].height);
                
                //label.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2-40, label.center.y);
            }
            
            labelHeight = label.frame.size.height;
            NSNumber *someNumber = [NSNumber numberWithInt:labelHeight + 10];
            [heightsCell addObject:someNumber];
        }
        cell.center = tableHome.center;
        //cell.backgroundColor = [UIColor redColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell.layer setMasksToBounds:YES];
        [cells addObject:cell];
    }
    tableHome.separatorColor=[UIColor clearColor];
    [tableHome reloadData];
}

- (CGSize)getHeightForString:(NSString *)string forFont:(UIFont *)font forConstraintSize:(CGSize)constrainSize {
    //    NSLog(@"string height %f",[string sizeWithFont:font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByWordWrapping].height);
    return [string sizeWithFont:font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByWordWrapping];
}
#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
    
    //  should be calling your tableviews data source model to reload
    //  put here just for demo
    MBProgressHUD *HUD=[[MBProgressHUD alloc] initWithView:self.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    [HUD showWhileExecuting:@selector(parseRefresh) onTarget:self withObject:nil animated:YES];
    [tableHome reloadData];
    [tableHome setUserInteractionEnabled:NO];
    _reloading = YES;
    
}

- (void)doneLoadingTableViewData{
    
    //  model should call this when its done loading
    [tableHome setUserInteractionEnabled:YES];
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:tableHome];
    
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    
}


#pragma mark tableview delegates
-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    return cells.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ISGCell * cell = cells[indexPath.row];
    NSLog(@"cell:%f",cell.labelHeight);
    [cell layoutSubviews];
    
    return  cell.labelHeight + 10;
   // return [[cells objectAtIndex:indexPath.row] cellHeight];
   // return [[heightsCell objectAtIndex:(int)[indexPath row]] intValue];

}
-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     return [cells objectAtIndex:indexPath.row];
}
#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
    [arrayQuotes removeAllObjects];
    float low_bound = 350;
    float high_bound = 900;
   
    float rndValue = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
    randomIndex = (int)(rndValue + 0.5);
    [self generateQuote];
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
    return _reloading; // should return if data source model is reloading
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
