//
//  ISGFontViewController.h
//  iSearchGujral
//
//  Created by mac on 4/26/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISGFontViewController : UIViewController
{
    IBOutlet UIButton *buttonSmall;
    IBOutlet UIButton *buttonNormal;
    IBOutlet UIButton *buttonLarge;
    IBOutlet UIButton *buttonMedium;
    IBOutlet UIImageView *imageBg;
    __weak IBOutlet UILabel *labelSmall;
    __weak IBOutlet UILabel *labelNormal;
    __weak IBOutlet UILabel *labelMedium;
    __weak IBOutlet UILabel *labelLarge;
    IBOutlet UILabel *labelGurumukhi;
    IBOutlet UILabel *labelRoman;
    IBOutlet UILabel *labelHindi;
    IBOutlet UILabel *labelEnglish;
    IBOutlet UILabel *labelTeeks;
    IBOutlet UILabel *labelAttributes;
    IBOutlet UILabel *labelPreview;
    IBOutlet UIScrollView *scroll;
    IBOutlet UISlider *gurmukhiSlider;
    IBOutlet UISlider *romanSlider;
    IBOutlet UISlider *hindiSlider;
    IBOutlet UISlider *englishSlider;
    IBOutlet UISlider *teekaSlider;
    IBOutlet UISlider *attributeSlider;
    NSString *fontStyle;
    NSString *stringBackgroud,*strGurmukhiSize,*strRomanSize,*strHindiSize,*strEnglishSize,*strTeekaSize,*strAttributeSize;
    NSString *language1,*gurmukhiSize,*romanSize,*hindiSize,*englishSize,*teekaSize,*attributeSize;
    NSString *language2,*language3,*language4,*language5,*language6;
    UIColor *gurmukhiColor,*romanColor,*hindiColor,*englishColor,*teekaColor,*attributeColor;
}
-(IBAction)sliderGurumukhi:(id)sender;
-(IBAction)setSliderValue:(id)sender;
-(IBAction)sliderRoman:(id)sender;
-(IBAction)sliderHindi:(id)sender;
-(IBAction)sliderEnglish:(id)sender;
-(IBAction)sliderTeeka:(id)sender;
-(IBAction)sliderAttributes:(id)sender;
-(IBAction)btnSmall:(id)sender;
-(IBAction)btnLarge:(id)sender;
-(IBAction)btnNormal:(id)sender;
-(IBAction)btnMedium:(id)sender;
@end
