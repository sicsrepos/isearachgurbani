//
//  ISGSearchResultsViewController.m
//  iSearchGujral
//
//  Created by mac on 4/24/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGSearchResultsViewController.h"
#import "ISGTheme.h"
#import "ISGCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ISGTabViewController.h"
#import "BookMarkFolders.h"
#import "BookMarkPages.h"
#import "KGModal.h"

int selectedShabd;
@interface ISGSearchResultsViewController () <UITabBarDelegate>
{
    NSString *stringBackgroud;
    NSMutableArray *cells,*arrayLabels,*arrayLoadedItems;
    UITabBarItem *home, *explore, *Banis, *more;
    int loadData;
    BOOL isLoadMoreData;
    int cnt, arrayCount;
    NSArray * arrayFolders;
    NSMutableArray * arrayNames;
    UITableView * TableList;
    BookMarkPages * bookMarkPage;
}
@end

@implementation ISGSearchResultsViewController
@synthesize arrayItems,lang, querry, searchedString;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [tableSearch setBackgroundColor:[UIColor clearColor]];
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self setTheme];
    
    loadData = 1;
    cnt = 1;
    arrayLoadedItems = [NSMutableArray new];
    cells = [[NSMutableArray alloc] init];
    [cells removeAllObjects];
    
    ISGAppDelegate * appDelegate = (ISGAppDelegate *)[[UIApplication sharedApplication]delegate];
    ISGTabViewController * tabs = (ISGTabViewController *)[appDelegate.window rootViewController];
    home = tabs.tabBar.items[0];
    explore = tabs.tabBar.items[1];
    Banis = tabs.tabBar.items[3];
    more = tabs.tabBar.items[4];
    
    home.enabled = YES;
    explore.enabled = YES;
    Banis.enabled = YES;
    more.enabled = YES;
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    CGFloat screenWidth = screenSize.width;

    NSLog(@"screen height %f",screenHeight);
        dispatch_async(dispatch_get_main_queue(), ^{
   /* if ((screenHeight > 850) && (screenHeight < 1000)) {
        tableSearch.frame = CGRectMake(40, 60 ,screenWidth-80,screenHeight-380);
    }
    else if(screenHeight==1024.000000)
    {
        tableSearch.frame = CGRectMake(40, 30, screenWidth-80,screenHeight-300);
    }
    else if(screenHeight==568.000000)
    {
        tableSearch.frame = CGRectMake(40, 20, screenWidth-80,screenHeight-175);
    }
    else if(screenHeight==480.000000)
    {
        tableSearch.frame = CGRectMake(40, 20, screenWidth-80,screenHeight-80);
    }
    else if(screenHeight == 736)
    {
        tableSearch.frame = CGRectMake(40, 20, screenWidth-80, screenHeight-380);
    }
    else
    {
        tableSearch.frame = CGRectMake(40, 20, screenSize.width-80,screenHeight-200);
    }
            tableSearch.center = self.view.center;*/

});
    self.navigationController.navigationBarHidden = NO;
    database = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).database;
    if(arrayItems.count==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"No data found" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [self loadDataForTableView:loadData];
    }
    lpHandler = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressHandler:)];
    lpHandler.minimumPressDuration = 0.5;
    [tableSearch addGestureRecognizer:lpHandler];
      [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"refresh" object:Nil];
    
    self.title = [NSString stringWithFormat:@"Search Result : Found %lu rows for '%@'",(unsigned long)arrayItems.count,searchedString];
    arrayNames = [NSMutableArray new];
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    bookMarkPage = [[BookMarkPages alloc] initWithEntity:[NSEntityDescription entityForName:@"BookMarkPages" inManagedObjectContext:managedObjectContext] insertIntoManagedObjectContext:managedObjectContext];
}

-(void) loadDataForTableView:(int)num
{
    int limit;
    if (arrayItems.count >= num * 30)
    {
        limit = 30;
        isLoadMoreData = YES;
    }
    else if(arrayItems.count < 30)
    {
        limit = (int)arrayItems.count;
        isLoadMoreData = NO;
    }
    else
    {
        limit = (int)arrayItems.count - (int)cells.count;
        isLoadMoreData = NO;
    }
    
    [arrayLoadedItems removeAllObjects];
    for (int i = (int)cells.count; i < limit+cells.count; i++)
    {
        [arrayLoadedItems addObject:arrayItems[i]];
    }
    
    [self buildCells];
}

-(void)viewWillAppear:(BOOL)animated
{
   //self.title=[NSString stringWithFormat:@"%d Search Results",[arrayItems count]];
    //self.title = titleName;
}
-(void)refresh:(NSNotification *)notification
{
    [self setTheme];
    [self buildCells];
    [tableSearch reloadData];
}
-(void)insertBookmark
{
    [database open];
    NSLog(@"shabd = %@",strShabd);
    [database executeUpdate:@"INSERT INTO bookmark (markText,page,scrpt,language,tab) VALUES (?,?,?,?,'Search');",sggsData,strShabd,script,lang];
//    NSLog(@"%@",[database lastErrorMessage]);
    [database close];
    [SVProgressHUD showSuccessWithStatus:@"Bookmarked successfully"];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"favourites" object:Nil];
}


-(void)longPressHandler:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if(UIGestureRecognizerStateBegan ==gestureRecognizer.state)
	{
        CGPoint p = [gestureRecognizer locationInView:tableSearch];
        index = [tableSearch indexPathForRowAtPoint:p];
        UITableViewCell * selectedCell = (UITableViewCell *)[tableSearch cellForRowAtIndexPath:index];
        ISGDataModel *data=[arrayItems objectAtIndex:index.row];
        shabd=[data.shabd intValue];
        dataID=[data.dataID intValue];
        sggsData=data.gurmukhi;
        strShabd=data.shabd;
        strId=data.dataID;
        page=data.page;
        
        if([data.script isEqualToString:@"sggs"])
        {
            script=@"Sri Guru Granth Sahib";
        }
        else if([data.script isEqualToString:@"bgv"])
        {
            script=@"Bhai Gurdas Vaaran";
        }
        else if([data.script isEqualToString:@"kbg"])
        {
            script=@"Kabit Bhai Gurdas";
        }
        else if([data.script isEqualToString:@"bnl"])
        {
            script=@"Baani Bhai Nand Lal";
        }
        else if([data.script isEqualToString:@"dgs"])
        {
            script=@"Sri Dasam Granth Sahib";
        }
        
        CGFloat wd;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            wd = 290;
        }
        else
        {
            wd = 680;
        }
        
        UIGraphicsBeginImageContext(CGSizeMake(wd, selectedCell.frame.size.height));
        CGRect tableViewFrame = selectedCell.frame;
        selectedCell.frame = CGRectMake(0, 0, selectedCell.frame.size.width, tableSearch.contentSize.height);
        [selectedCell.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage * attachmentImage = UIGraphicsGetImageFromCurrentImageContext();
        selectedCell.frame = tableViewFrame;
        UIGraphicsEndImageContext();
        selectedCell.backgroundColor = [UIColor clearColor];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Options" message:Nil delegate:self cancelButtonTitle:Nil otherButtonTitles:@"Open Ang",@"Favourites",@"Pin to Home",@"Cancel",nil];
        [alert show];
        alert.tag=1;
        
        bookMarkPage.pageNo = page;
        bookMarkPage.scriptID = data.script;
        bookMarkPage.cellImage = UIImageJPEGRepresentation(attachmentImage, 1.0);
        bookMarkPage.query = [NSString stringWithFormat:@"SELECT ID,shabd,gurmukhi,english,roman,page,scrpt,attributes,translit,punctuation,teeka FROM gurbani  WHERE shabd ='%@' ORDER BY ROWID ASC",data.shabd];
        [self setNavTitle:data.script forPage:data.page];
    }
}

-(void)setNavTitle:(NSString *)title forPage:(NSString *)pages
{
    
    if(title.length>0)
    {
        if([title isEqualToString:@"1"])
        {
            title=@"SRI GURU GRANTH SAHIB";
        }
        else if([title isEqualToString:@"2"])
        {
            title=@"BHAI GURDAS VAAR";
        }
        else if([title isEqualToString:@"3"])
        {
            title=@"BHAI GURDAS KABIT";
        }
        else if([title isEqualToString:@"4"])
        {
            title=@"SRI DASAM GRANTH PAGE";
        }
        else if([title isEqualToString:@"5"])
        {
            self.title=@"BAANi BHAI NAND LAL";
        }
        else
        {
            //title = [NSString stringWithFormat:@"SGGS Ang %@", title];
            title = [NSString stringWithFormat:@"Ang"];
        }
        
        bookMarkPage.title = [NSString stringWithFormat:@"%@ '%@'",title,pages];
    }
    
}

-(void)showBookmarkAlert
{
    UIAlertView *alertBookmark = [[UIAlertView alloc] initWithTitle:@"Are you sure to add this to your favourites?"
                                                            message:@"\n\n\n" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
    UITextField *txtAlert = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 80.0, 260.0, 30.0)];
    [txtAlert setBackgroundColor:[UIColor whiteColor]];
    txtAlert.text=sggsData;
    txtAlert.font=[UIFont fontWithName:@"GurbaniLipiBold" size:22];
    txtAlert.userInteractionEnabled=NO;
    //[alertBookmark addSubview:txtAlert];
    [alertBookmark show];
     alertBookmark.tag=2;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1 && buttonIndex==0)
    {   dispatch_async(dispatch_get_main_queue(), ^{
    
        ISGDataModel *dataModel=[arrayItems objectAtIndex:index.row];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGSearchDetailViewController * searchDetailViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGSearchDetailViewController"];
       // ISGSearchDetailViewController *searchDetailViewController=[[ISGSearchDetailViewController alloc]initWithNibName:@"ISGSearchDetailViewController" bundle:Nil];
        searchDetailViewController.ang=dataModel.shabd;//shabad page display is based on shabad no:, not ang(page)
        if([lang isEqualToString:@"gurmukhi"])
        {
            searchDetailViewController.selectedText=dataModel.gurmukhi;
        }
        else  if([lang isEqualToString:@"roman"])
        {
            searchDetailViewController.selectedText=dataModel.roman;
        }
        else
        {
            searchDetailViewController.selectedText=dataModel.gurmukhi;
        }

        searchDetailViewController.language=lang;
        [self.navigationController pushViewController:searchDetailViewController animated:YES];
        });
    }
    else if(alertView.tag==1 && buttonIndex==1)
    {
        //[self showBookmarkAlert];
        UIAlertView *alertBookmark = [[UIAlertView alloc] initWithTitle:@"Are you sure to add this to your favourites?"
                                                                message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
//        UITextField *txtAlert = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 80.0, 260.0, 30.0)];
//        [txtAlert setBackgroundColor:[UIColor whiteColor]];
//        txtAlert.text=sggsData;
//        if([language isEqualToString:@"roman"])
//            txtAlert.font=[UIFont systemFontOfSize:20];
//        else txtAlert.font=[UIFont fontWithName:@"GurbaniLipiBold" size:22];
//        txtAlert.userInteractionEnabled=NO;
        //[alertBookmark addSubview:txtAlert];
        [alertBookmark show];
        alertBookmark.tag=2;
    }
    else if(alertView.tag==1 && buttonIndex==2)
    {
        selectedShabd=shabd;
        [[NSNotificationCenter defaultCenter]postNotificationName:@"pin" object:Nil];
        self.tabBarController.selectedIndex = 0;
    }
    else if(alertView.tag==2 && buttonIndex==0)
    {
        //[self insertBookmark];
        [self showFolderList];
    }
    else if (alertView.tag == 100 && buttonIndex == 1)
    {
        UITextField * textField = [alertView textFieldAtIndex:0];
        if (textField.text.length == 0)
        {
            [[[UIAlertView alloc] initWithTitle:@"Invalid text" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        else
        {
            [BookMarkFolders addBookMarkFolderWithName:textField.text index:(int)arrayNames.count];
            [self showFolderList];
        }
    }

}

-(void) showFolderList
{
    arrayFolders = [BookMarkFolders fetchAllBookMarkFolders];
    [arrayNames removeAllObjects];
    [arrayFolders enumerateObjectsUsingBlock:^(BookMarkFolders * obj, NSUInteger idx, BOOL *stop)
     {
         [arrayNames addObject:obj.folderName];
     }];
    if (arrayNames.count == 0)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"No Bookmark Folders" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Create", nil];
        alert.tag = 100;
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alert show];
    }
    else
    {
        UIView * kgView = [[UIView alloc] initWithFrame:CGRectMake(40, 100, 240, 200)];
        TableList = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kgView.frame.size.width, kgView.frame.size.height) style:UITableViewStylePlain];
        TableList.delegate = self;
        TableList.dataSource = self;
        TableList.backgroundColor = [UIColor clearColor];
        [kgView addSubview:TableList];
        [[KGModal sharedInstance] showWithContentView:kgView andAnimated:YES];
    }
}

-(void)setTheme
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    ISGTheme *theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    stringBackgroud=theme.background;
    //fontSize=[theme.fontSize intValue];
    fontStyle=theme.fontStyle;
   // if(fontSize==0)
    {
        fontSize=23;
    }
    if(stringBackgroud.length<=7)
    {
        stringBackgroud=@"LightPattern";
    }
    if (!theme.backgroundImage)
    {
        [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
    }
    else
        imageBg.image = theme.backgroundImage;
    
}
-(void)SetDefaultValues
{
    stringBackgroud=@"LightPattern";
    fontSize=23;
    fontStyle=@"GurbaniLipiBold";
}
-(void)buildCells
{
    arrayLabels=[[NSMutableArray alloc]init];
    if (heightsCell) [heightsCell removeAllObjects]; else heightsCell = [[NSMutableArray alloc] init];

    //if (cells) [cells removeAllObjects]; else cells = [[NSMutableArray alloc] init];
    for (ISGDataModel *dataModel in arrayLoadedItems) {
        UILabel *label = [[UILabel alloc] init];
        if([lang isEqualToString:@"gurmukhi"])
        {
            label.text=dataModel.gurmukhi;
            label.font=[UIFont fontWithName:@"GurbaniLipiBold" size:fontSize];
        }
        else  if([lang isEqualToString:@"roman"])
        {
            label.text=dataModel.roman;
            label.font=[UIFont boldSystemFontOfSize:fontSize-2];
        }
        else
        {
            label.text=dataModel.gurmukhi;
            label.font=[UIFont fontWithName:fontStyle size:fontSize+2];
        }
        [label setTextAlignment:NSTextAlignmentCenter];
        if([stringBackgroud isEqualToString:@"LightPattern"]||[stringBackgroud isEqualToString:@"WhitePattern"])
        {
            [label setTextColor:[UIColor blackColor]];
        }
        else
        {
            [label setTextColor:[UIColor whiteColor]];
        }
        label.numberOfLines=0;
        label.lineBreakMode=NSLineBreakByWordWrapping;
        [arrayLabels addObject:label];
    
    static NSString *cellIdentifier = @"ISGCell";
    cell = [tableSearch dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[ISGCell alloc] initWithLine:label];
        int labelHeight = 0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            label.frame = CGRectMake(5, labelHeight+2, 685, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(685, 9999)].height);
        }
        else
        {
            //287
            label.frame = CGRectMake(0, labelHeight+2, 287, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(287, 9999)].height);
            if([UIScreen mainScreen].bounds.size.height == 736)
            {
                label.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2-40, label.center.y);
            }
            else if([UIScreen mainScreen].bounds.size.height == 667)
            {
                label.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2-33, label.center.y);
            }
        }
        
        labelHeight += label.frame.size.height;
        NSNumber *someNumber = [NSNumber numberWithInt:labelHeight + 10];
        [heightsCell addObject:someNumber];
    }
    
    cell.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell.layer setMasksToBounds:YES];
    [cells addObject:cell];
    }
    tableSearch.separatorColor=[UIColor grayColor];
    arrayCount = (int)cells.count;
    [tableSearch reloadData];
}
- (CGSize)getHeightForString:(NSString *)string forFont:(UIFont *)font forConstraintSize:(CGSize)constrainSize {
    //    NSLog(@"string height %f",[string sizeWithFont:font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByWordWrapping].height);
    return [string sizeWithFont:font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByWordWrapping];
}
-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == TableList)
    {
        return arrayNames.count;
    }
    else
        NSLog(@"Search Count %lu",(unsigned long)cells.count);
        return [cells count];
}


-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == TableList)
    {
        UITableViewCell * cellList = [[UITableViewCell alloc] initWithStyle:UITableViewStylePlain reuseIdentifier:@"myCell"];
        if (cellList == nil)
        {
            cellList = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
        }
        cellList.textLabel.text = arrayNames[indexPath.row];
        cellList.imageView.image = [UIImage imageNamed:@"folders"];
        return cellList;
    }
    else
        return [cells objectAtIndex:indexPath.row];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == TableList)
    {
        BookMarkFolders * fol = arrayFolders[indexPath.row];
        NSSet * set = fol.relationshipToPages;
        NSArray * array = [NSArray arrayWithArray:set.allObjects];
        int cntt = 0;
        bookMarkPage.index = [NSString stringWithFormat:@"%lu",(unsigned long)array.count];
        for (BookMarkPages * obj in array)
        {
            if ([bookMarkPage.pageNo isEqualToString:obj.pageNo] && [bookMarkPage.scriptID isEqualToString:obj.scriptID] && [bookMarkPage.cellImage isEqualToData:obj.cellImage] && [bookMarkPage.query isEqualToString:obj.query])
            {
                cntt++;
            }
        }
        if (cntt>0)
        {
            [SVProgressHUD showErrorWithStatus:@"Already added to the list"];
        }
        else
        {
            [BookMarkPages addBookMarkPage:bookMarkPage inBookMarkFolder:fol];
            [SVProgressHUD showSuccessWithStatus:@"Added"];
        }
        [[KGModal sharedInstance] hideAnimated:YES];
    }
    else
    {
        ISGDataModel *dataModel=[arrayItems objectAtIndex:indexPath.row];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGSearchDetailViewController * searchDetailViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGSearchDetailViewController"];
       // ISGSearchDetailViewController *searchDetailViewController=[[ISGSearchDetailViewController alloc]initWithNibName:@"ISGSearchDetailViewController" bundle:Nil];
        // searchDetailViewController.shabd=dataModel.shabd;
        // NSLog(@"selected id= %@",dataModel.dataID);
        searchDetailViewController.selectedId=dataModel.dataID;
        searchDetailViewController.language=lang;
        searchDetailViewController.author=dataModel.script;
        searchDetailViewController.raag=dataModel.line;
        searchDetailViewController.ang=dataModel.shabd;//shabad page display is based on shabad no:, not ang(page)
        searchDetailViewController.selectedLang=lang;
        if([lang isEqualToString:@"gurmukhi"])
        {
            searchDetailViewController.selectedText=dataModel.gurmukhi;
        }
        else  if([lang isEqualToString:@"roman"])
        {
            searchDetailViewController.selectedText=dataModel.roman;
        }
        else
        {
            searchDetailViewController.selectedText=dataModel.gurmukhi;
        }
        [self.navigationController pushViewController:searchDetailViewController animated:YES];
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == TableList)
    {
        return 40;
    }
    else{
        ISGCell * cell = cells[indexPath.row];
        NSLog(@"cell:%f",cell.labelHeight);
        [cell layoutSubviews];
        
        return  cell.labelHeight + 24;
    }
       //  return [[heightsCell objectAtIndex:(int)[indexPath row]] intValue];
       // return [[cells objectAtIndex:indexPath.row] cellHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView.contentOffset.y + scrollView.frame.size.height >= scrollView.contentSize.height && arrayLoadedItems.count > 0 && isLoadMoreData == YES)
    {
        loadData++;
        [self loadDataForTableView:loadData];
//        if(arrayItems.count == loadData*30)
//        {
//            [self loadMoreSearchResult];
//        }
    }
}

- (void)loadMoreSearchResult
{
    [database open];
    FMResultSet *results = [database executeQuery:[NSString stringWithFormat:@"%@ LIMIT %d,60",querry,cnt*60]];
    while([results next])
    {
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithResult:results];
        [arrayItems addObject:dataModel];
    }
    [database close];
    cnt ++;
    self.title = [NSString stringWithFormat:@"Search Result : Found %lu rows for '%@'",(unsigned long)arrayItems.count, searchedString];
}

@end
