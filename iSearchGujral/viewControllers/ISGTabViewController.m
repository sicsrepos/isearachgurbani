//
//  ISGTabViewController.m
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGTabViewController.h"

@interface ISGTabViewController ()
{
    int check;
}

@end

@implementation ISGTabViewController
@synthesize HomeViewController,ReadSGGSViewController,SearchViewController,BaniViewController,PreferenceViewController,FavouritesViewController,HelpViewController,ExploreViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)init {
    
    self = [super init];
    if (self) {
        
        self.delegate = self;
        self.navigationItem.hidesBackButton=YES;
        check = 0;
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        HomeViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGHomeViewController"];
        
        //HomeViewController=[[ISGHomeViewController alloc]initWithNibName:@"ISGHomeViewController" bundle:Nil];
        UINavigationController *navHomeViewController=[[UINavigationController alloc]initWithRootViewController:HomeViewController];
        
        navHomeViewController.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Home" image:[UIImage imageNamed:@"Home.png"] tag:1];
    
//        ReadSGGSViewController=[[ISGReadSGGSViewController alloc]initWithNibName:@"ISGReadSGGSViewController" bundle:Nil];
//        ReadSGGSViewController.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Read SGGS" image:[UIImage imageNamed:@"Read SGGS.png"] tag:2];
        ExploreViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGExploreViewController"];

       // ExploreViewController=[[ISGExploreViewController alloc]initWithNibName:@"ISGExploreViewController" bundle:Nil];
        UINavigationController *navExplore = [[UINavigationController alloc]initWithRootViewController:ExploreViewController];
        navExplore.tabBarItem=[[UITabBarItem alloc]initWithTitle:@"Explore" image:[UIImage imageNamed:@"Read SGGS.png"] tag:8];
        
        SearchViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGSearchViewController"];

       // SearchViewController=[[ISGSearchViewController alloc]initWithNibName:@"ISGSearchViewController" bundle:Nil];
        UINavigationController *navSearchViewController=[[UINavigationController alloc]initWithRootViewController:SearchViewController];
        navSearchViewController.tabBarItem=[[UITabBarItem alloc]initWithTitle:@"Search" image:[UIImage imageNamed:@"Search.png"] tag:3];
        
        BaniViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGBaniViewController"];

       // BaniViewController=[[ISGBaniViewController alloc]initWithNibName:@"ISGBaniViewController" bundle:Nil];
         UINavigationController *navBaniViewController=[[UINavigationController alloc]initWithRootViewController:BaniViewController];
        navBaniViewController.tabBarItem=[[UITabBarItem alloc]initWithTitle:@"Baanis" image:[UIImage imageNamed:@"NewBannis.png"] tag:4];
        
        PreferenceViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGPreferenceViewController"];

       // PreferenceViewController=[[ISGPreferenceViewController alloc]initWithNibName:@"ISGPreferenceViewController" bundle:Nil];
        UINavigationController *navPreference=[[UINavigationController alloc]initWithRootViewController:PreferenceViewController];
        navPreference.tabBarItem=[[UITabBarItem alloc]initWithTitle:@"Preference" image:[UIImage imageNamed:@"Settings.png"] tag:5];
        
        FavouritesViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGFavouritesViewController"];

       // FavouritesViewController=[[ISGFavouritesViewController alloc]initWithNibName:@"ISGFavouritesViewController" bundle:Nil];
        UINavigationController *navFavourites=[[UINavigationController alloc]initWithRootViewController:FavouritesViewController];
        navFavourites.tabBarItem=[[UITabBarItem alloc]initWithTitle:@"Favourites" image:[UIImage imageNamed:@"Favorites-1.png"] tag:6];
        HelpViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGHelpViewController"];

        //HelpViewController=[[ISGHelpViewController alloc]initWithNibName:@"ISGHelpViewController" bundle:Nil];
        UINavigationController *navHelp = [[UINavigationController alloc]initWithRootViewController:HelpViewController];
        navHelp.tabBarItem=[[UITabBarItem alloc]initWithTitle:@"Help" image:[UIImage imageNamed:@"Help.png"] tag:7];
        
        self.viewControllers = [NSArray arrayWithObjects:navHomeViewController,navExplore,navSearchViewController,navBaniViewController,navPreference,navFavourites,navHelp, nil];
        self.moreNavigationController.delegate = self;
        
         if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7)
         {
             navBaniViewController.navigationBar.translucent=NO;
             navFavourites.navigationBar.translucent=NO;
             navHomeViewController.navigationBar.translucent=NO;
             navPreference.navigationBar.translucent=NO;
             navSearchViewController.navigationBar.translucent=NO;
            [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0]];
             self.navigationController.navigationBar.translucent = NO;
         }
         else
         {
             [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0]];
         }
            
    }
     return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [self performSelector:@selector(setMoreNavigationControllerBackground) withObject:nil afterDelay:0.0];
}

-(void)setMoreNavigationControllerBackground
{
    if ([self.moreNavigationController.topViewController.view isKindOfClass:[UITableView class]])
    {
        UITableView * table = (UITableView *)self.moreNavigationController.topViewController.view;
        if (check == 0)
        {
            table.contentInset = UIEdgeInsetsMake(120, 0, 0, 0);
            for (UITableViewCell * cell in [table visibleCells])
            {
                cell.backgroundColor = [UIColor clearColor];
                [cell.contentView setBackgroundColor:[UIColor clearColor]];
                cell.textLabel.font = [UIFont systemFontOfSize:16];
                cell.imageView.hidden = YES;
                cell.textLabel.hidden = YES;
                CGRect frameImage = cell.imageView.frame;
                frameImage.origin.x += 60;
                
                CGRect frameLabel = cell.textLabel.frame;
                frameLabel.origin.x = CGRectGetMaxX(frameImage)+10;
                
                UIImageView * imageView = [[UIImageView alloc] initWithFrame:frameImage];
                imageView.image = cell.imageView.image;
                [cell addSubview:imageView];
                
                UILabel * label = [[UILabel alloc] initWithFrame:frameLabel];
                label.text = cell.textLabel.text;
                [cell addSubview:label];
                [cell setSelectionStyle:UITableViewCellEditingStyleNone];
            }
        }
        
        check++;
        
        table.separatorStyle = UITableViewCellSeparatorStyleNone;
        table.backgroundColor = [UIColor clearColor];
        
    }
}

@end

