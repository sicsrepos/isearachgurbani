//
//  ISGBackgroundViewController.m
//  iSearchGujral
//
//  Created by mac on 4/26/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGBackgroundViewController.h"
#import "ISGTheme.h"

@interface ISGBackgroundViewController ()<UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    ISGTheme *theme;
    UIImage *imagecheck;
}
@end

@implementation ISGBackgroundViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.title=@"Backgrounds";
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *stringBackgroud=theme.background;
    fontSize=theme.fontSize;
    fontStyle=theme.fontStyle;
    
    language1=theme.lang1;
    language2=theme.lang2;
    language3=theme.lang3;
    language4=theme.lang4;
    language5=theme.lang5;
    language6=theme.lang6;
//    language7=theme.lang7;
    
    strGurmukhiSize=theme.gurmukhiSize;
    strRomanSize=theme.romanSize;
    strHindiSize=theme.hindiSize;
    strEnglishSize=theme.englishSize;
    strTeekaSize=theme.teekaSize;
    strAttributeSize=theme.attributeSize;
    
    gurmukhiColor = theme.gurmukhiColor;
    romanColor = theme.romanColor;
    hindiColor = theme.hindiColor;
    englishColor = theme.englishColor;
    teekaColor = theme.teekaColor;
    attributeColor = theme.attributeColor;
    
    
    
    if (theme.backgroundImage)
    {
        imageBg.image = theme.backgroundImage;
    }
    else
    {
        [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
        [self setCheckMark:stringBackgroud];
    }
    if(stringBackgroud.length<=7)
    {
        stringBackgroud=@"LightPattern";
    }
    [self changeFontColor:stringBackgroud];
    
    theme=[[ISGTheme alloc]init];
    //if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)[containerView setFrame:CGRectMake(100, 100, 200, 200)];
    // Do any additional setup after loading the view from its nib.
}

-(void)setThemeWithPattern:(NSString *)pattern
{
    if ([pattern isEqualToString:@""])
    {
        theme.backgroundImage = imagecheck;
        theme.background=nil;
    }
    else
    {
        theme.backgroundImage = nil;
        theme.background=pattern;
    }
    
    
    
    theme.fontSize=fontSize;
    theme.fontStyle=fontStyle;
    
    theme.lang1=language1;
    theme.lang2=language2;
    theme.lang3=language3;
    theme.lang4=language4;
    theme.lang5=language5;
    theme.lang6=language6;
//    theme.lang7=language7;
    
    theme.gurmukhiSize=strGurmukhiSize;
    theme.romanSize=strRomanSize;
    theme.hindiSize=strHindiSize;
    theme.englishSize=strEnglishSize;
    theme.teekaSize=strTeekaSize;
    theme.attributeSize=strAttributeSize;
    
    if([pattern isEqualToString:@"LightPattern"]||[pattern isEqualToString:@"WhitePattern"])
    {
        theme.gurmukhiColor = [UIColor blackColor];
        theme.teekaColor = [UIColor colorWithRed:83.0/255 green:54.0/255 blue:25.0/255 alpha:1];
        theme.hindiColor = [UIColor colorWithRed:113.0/255 green:2.0/255 blue:67.0/255 alpha:1];
        theme.romanColor = [UIColor colorWithRed:51.0/255 green:0 blue:64.0/255 alpha:1];
        theme.englishColor = [UIColor colorWithRed:110.0/255 green:0 blue:37/255 alpha:1];
        theme.attributeColor = [UIColor darkGrayColor];
    }
    else
    {
        theme.gurmukhiColor = [UIColor greenColor];
        theme.teekaColor = [UIColor colorWithRed:255.0/255 green:254.0/255 blue:178.0/255 alpha:1];
        theme.hindiColor = [UIColor colorWithRed:255.0/255 green:189.0/255 blue:85.0/255 alpha:1];
        theme.romanColor = [UIColor colorWithRed:159.0/255 green:255.0/255 blue:193.0/255 alpha:1];
        theme.englishColor = [UIColor colorWithRed:56.0/255 green:255.0/255 blue:255.0/255 alpha:1];
        theme.attributeColor = [UIColor whiteColor];
    }
    
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:theme] forKey:@"theme"];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"refresh" object:Nil];
}
-(void)setCheckMark:(NSString *)background
{
    if([background isEqualToString:@"BlackPattern"])
    {
        [buttonBlackPattern setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonDarkPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonLightPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonWhitePattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

    }
    else if([background isEqualToString:@"WhitePattern"])
    {
        [buttonWhitePattern setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonBlackPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonDarkPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonLightPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

    }
    else if([background isEqualToString:@"DarkPattern"])
    {
        [buttonDarkPattern setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonBlackPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonLightPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonWhitePattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];

    }
    else
    {
        [buttonLightPattern setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonDarkPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonWhitePattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonBlackPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    }
    
}
-(void)changeFontColor:(NSString *)pattern
{
    if([pattern isEqualToString:@"LightPattern"]||[pattern isEqualToString:@"WhitePattern"])
    {
        [labelBlackPattern setTextColor:[UIColor blackColor]];
        [labelDarkPattern setTextColor:[UIColor blackColor]];
        [labelLightPattern setTextColor:[UIColor blackColor]];
        [labelWhitePattern setTextColor:[UIColor blackColor]];
        [buttonCustomBackground setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        

    }
    else
    {
        [labelBlackPattern setTextColor:[UIColor whiteColor]];
        [labelDarkPattern setTextColor:[UIColor whiteColor]];
        [labelLightPattern setTextColor:[UIColor whiteColor]];
        [labelWhitePattern setTextColor:[UIColor whiteColor]];
        [buttonCustomBackground setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"refresh" object:Nil];
}
-(IBAction)btnDarkPattern:(id)sender
{
    [self setThemeWithPattern:@"DarkPattern"];
    [imageBg setImage:[UIImage imageNamed:@"DarkPattern.png"]];
  
        [buttonDarkPattern setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonBlackPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonLightPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonWhitePattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
   [self changeFontColor:theme.background];
}
-(IBAction)btnLightPattern:(id)sender
{
    [self setThemeWithPattern:@"LightPattern"];
    [imageBg setImage:[UIImage imageNamed:@"LightPattern.png"]];
  
        [buttonLightPattern setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonDarkPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonWhitePattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonBlackPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [self changeFontColor:theme.background];
    

}
-(IBAction)btnBlackPattern:(id)sender
{
    
    [self setThemeWithPattern:@"BlackPattern"];
    [imageBg setImage:[UIImage imageNamed:@"BlackPattern.png"]];
   
        [buttonBlackPattern setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonDarkPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonLightPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonWhitePattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
   [self changeFontColor:theme.background];

}
-(IBAction)btnWhitePattern:(id)sender
{
    [self setThemeWithPattern:@"WhitePattern"];
    [imageBg setImage:[UIImage imageNamed:@"WhitePattern.png"]];
 
        [buttonWhitePattern setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
        [buttonBlackPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonDarkPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
        [buttonLightPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [self changeFontColor:theme.background];

}
-(IBAction)btnCustomBackground:(id)sender
{
    UIActionSheet * actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Photo Album", nil];
    UIAlertController *alertController = [actionSheet valueForKey:@"_alertController"];
    if ([alertController isKindOfClass:[UIAlertController class]])
    {
        alertController.view.tintColor = [UIColor blueColor];
    }
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerController *picker=[[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    switch (buttonIndex)
    {
        case 0:
        {
            BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
            picker.sourceType = hasCamera ? UIImagePickerControllerSourceTypeCamera :    UIImagePickerControllerSourceTypePhotoLibrary;
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self presentViewController:picker animated:YES completion:nil];
            }];
//            [self presentViewController:picker animated:YES completion:nil];
        }
            break;
        case 1:
        {
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self presentViewController:picker animated:YES completion:nil];
            }];
        }
            break;
            
        case 2:
            NSLog(@"cancel");
            break;
        default:
            break;
    }
}

#pragma mark ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    imagecheck=[info objectForKey:UIImagePickerControllerEditedImage];
    imageBg.image = imagecheck;

    
    [labelBlackPattern setTextColor:[UIColor blackColor]];
    [labelDarkPattern setTextColor:[UIColor blackColor]];
    [labelLightPattern setTextColor:[UIColor blackColor]];
    [labelWhitePattern setTextColor:[UIColor blackColor]];
    
    [buttonDarkPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [buttonBlackPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [buttonLightPattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    [buttonWhitePattern setImage:[UIImage imageNamed:@"untick.png"] forState:UIControlStateNormal];
    
    [self setThemeWithPattern:@""];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
