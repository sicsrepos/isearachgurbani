//
//  ISGDataModel.m
//  iSearchGujral
//
//  Created by mac on 4/18/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGDataModel.h"

@implementation ISGDataModel
@synthesize gurmukhi,english,roman,dataID,shabd,script,attributes,wgamma,translit,punctuation,tab,teeka,line,gamma,indexValue,key, count;
-(id)initWithResult : (FMResultSet *)result
{
    self = [super init];
    if (self)
	{
        self.gurmukhi = [result stringForColumn:@"gurmukhi"];
        self.english = [result stringForColumn:@"english"];
        self.roman = [result stringForColumn:@"roman"];
        self.dataID = [result stringForColumn:@"ID"];
        self.shabd = [result stringForColumn:@"shabd"];
        self.translit = [result stringForColumn:@"translit"];
        self.punctuation = [result stringForColumn:@"punctuation"];
        self.script = [result stringForColumn:@"scrpt"];
        self.teeka = [result stringForColumn:@"teeka"];
        self.attributes = [result stringForColumn:@"attributes"];
        self.line = [result stringForColumn:@"line"];
        self.page = [result stringForColumn:@"page"];
        self.alpha = [result stringForColumn:@"alpha"];
        self.walpha = [result stringForColumn:@"walpha"];
        self.gamma = [result stringForColumn:@"gamma"];
        self.wgamma=[result stringForColumn:@"wgamma"];
        self.pageID=[result stringForColumn:@"plineID"];
       
	}
    return self;
}
-(id)initWithFavouriteList : (FMResultSet *)result
{
    self = [super init];
    if (self)
	{
        self.gurmukhi = [result stringForColumn:@"markText"];
        self.script = [result stringForColumn:@"scrpt"];
        self.tab = [result stringForColumn:@"tab"];
        self.page= [result stringForColumn:@"page"];
        self.lang=[result stringForColumn:@"language"];
        self.indexValue=[result stringForColumn:@"indexValue"];
        self.key=[result stringForColumn:@"key"];
	}
    return self;
}
-(id)initWithShabd : (FMResultSet *)result
{
    self = [super init];
    if (self)
	{
        self.gurmukhi = [result stringForColumn:@"gurmukhi"];
        self.english = [result stringForColumn:@"english"];
        self.roman = [result stringForColumn:@"roman"];
        self.dataID = [result stringForColumn:@"ID"];
        self.shabd = [result stringForColumn:@"shabd"];
        self.page = [result stringForColumn:@"page"];
        self.script = [result stringForColumn:@"scrpt"];
        self.attributes = [result stringForColumn:@"attributes"];
        self.translit = [result stringForColumn:@"translit"];
        self.punctuation = [result stringForColumn:@"punctuation"];
        self.teeka = [result stringForColumn:@"teeka"];
	}
    return self;
}

- (id)initWithChapters:(FMResultSet *)result
{
    self = [super init];
    if (self)
    {
        self.english = [result stringForColumn:@"chapterE"];
        self.dataID = [result stringForColumn:@"chapterID"];
    }
    return self;
}

- (id)initWithSubChapters:(FMResultSet *)result
{
    self = [super init];
    if (self)
    {
        self.english = [result stringForColumn:@"chapterE"];
        self.page = [result stringForColumn:@"pageID"];
        self.startingline = [result stringForColumn:@"lineID"];
        self.endingLine = [result stringForColumn:@"endID"];
    }
    return self;
}

-(id)generateQuote: (FMResultSet *)result
{
    if (self)
	{
        self.gurmukhi = [result stringForColumn:@"gurmukhi"];
        self.english = [result stringForColumn:@"english"];
        self.roman = [result stringForColumn:@"roman"];
        
	}
    return self;
}

-(id)initWithShabdIndex:(FMResultSet *)result
{
    self = [super init];
    if (self)
    {
        self.dataID = [result stringForColumn:@"ID"];
        self.page = [result stringForColumn:@"pageID"];
        self.startingline = [result stringForColumn:@"lineID"];
        self.endingLine = [result stringForColumn:@"endID"];
        self.shabdE = [result stringForColumn:@"shabdE"];
        self.shabdG = [result stringForColumn:@"shabdG"];
        self.parentID = [result stringForColumn:@"parentID"];
        self.genreID = [result stringForColumn:@"genreID"];
        self.shabdID = [result stringForColumn:@"shabdID"];
        self.authorID = [result stringForColumn:@"authorID"];
        self.raagID = [result stringForColumn:@"raagID"];
    }
    return self;
}

-(id)initWithAuthors:(FMResultSet *)result
{
    self = [super init];
    if (self)
    {
        self.authorID = [result stringForColumn:@"authorID"];
    }
    return self;
}

-(id)initWithScriptures:(FMResultSet *)result
{
    self = [super init];
    if (self)
    {
        self.script = [result stringForColumn:@"scrpt"];
    }
    return self;
}

-(id)initWithRaags:(FMResultSet *)result
{
    self = [super init];
    if (self)
    {
        self.raagID = [result stringForColumn:@"raagID"];
    }
    return self;
}

-(id)initWithCount:(FMResultSet *)result
{
    self = [super init];
    if (self)
    {
        self.count = [result stringForColumn:@"COUNT(1)"];
    }
    return self;
}

@end
