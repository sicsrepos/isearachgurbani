//
//  ISGSearchDetailViewController.h
//  iSearchGujral
//
//  Created by mac on 4/24/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISGTheme.h"
#import "ISGAppDelegate.h"
#import <MessageUI/MessageUI.h>
@interface ISGSearchDetailViewController : UIViewController<UIScrollViewDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate>
{
    UILongPressGestureRecognizer *lpHandler;
    NSMutableArray *arrayItems,*cells;
    NSMutableArray *heightsCell;

    IBOutlet UIScrollView *scroll;
    IBOutlet UITableView *table;
    int shabdValue;
    IBOutlet UIButton *buttonLeft;
    IBOutlet UIButton *buttonRight;
    NSString *stringBackgroud,*message;
    IBOutlet UIImageView *imageBg;
    int fontSize;
    NSString *fontStyle;
    NSString  *language1,*language2,*language3,*language4,*language5,*language6,*language7;
    NSMutableArray *arrayLanguages;
    int size,gurmukhiSize,romanSize,hindiSize,englishSize,teekaSize,attributeSize;
    UIColor *gurmukhiColor,*romanColor,*hindiColor,*englishColor,*teekaColor,*attributeColor;
    BOOL showRoman;
    int shabad,dataID;
    FMDatabase *database;
    ISGAppDelegate *appDelegate;
    UIImage *attachmentImage;
    NSString *strShabd,*strId,*strPage,*sggsData,*script,*pageNumber;
    IBOutlet UIView *popView;
    UITapGestureRecognizer * tapGesture;
}
@property (nonatomic,strong) NSString *shabd;
@property (nonatomic,strong) NSString *language;
@property (nonatomic,strong) NSString *author;
@property (nonatomic,strong) NSString *raag;
@property (nonatomic,strong) NSString *ang;
@property (nonatomic,strong) NSString *selectedText;
@property (nonatomic,strong) NSString *selectedId;
@property (nonatomic,strong) NSString *selectedLang;

-(IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender;
-(IBAction)swipeRight:(UISwipeGestureRecognizer *)sender;
-(IBAction)btnLeft:(id)sender;
-(IBAction)btnRight:(id)sender;
-(IBAction)btnProceed:(id)sender;
@end
