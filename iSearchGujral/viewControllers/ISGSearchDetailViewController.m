//
//  ISGSearchDetailViewController.m
//  iSearchGujral
//
//  Created by mac on 4/24/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGSearchDetailViewController.h"
#import "FMDatabase.h"
#import "ISGDataModel.h"
#import "ISGCell.h"
#import <QuartzCore/QuartzCore.h>
#import "SVProgressHUD.h"
#import "MBProgressHUD.h"
#import <Twitter/Twitter.h> 
#import "HMSideMenu.h"
#import "KGModal.h"
#import "UIColor+Convert.h"
#import "BookMarkFolders.h"
#import "BookMarkPages.h"
#import "ISGTabViewController.h"
#import "ScrollViewToPDF.h"
#import "ISGCustomAlertViewController.h"

@interface ISGSearchDetailViewController ()<MBProgressHUDDelegate, UITableViewDataSource, UITableViewDelegate, UITabBarDelegate, UIPrintInteractionControllerDelegate>
{
    MBProgressHUD *HUD;
    HMSideMenu *sideMenu;
    CGFloat x;
    NSArray * arrayFolders;
    NSMutableArray * arrayNames;
    UITableView * TableList;
    BookMarkPages * bookMarkPage;
    NSString * stringQuery;
    UIImage * imageForPrint;
    UIPrintInteractionController * printController;
}
@end

@implementation ISGSearchDetailViewController
@synthesize shabd,language,author,raag,ang,selectedText,selectedId;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [table setBackgroundColor:[UIColor clearColor]];
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    showRoman=YES;
    
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    CGFloat screenWidth = screenSize.width;

    NSLog(@"screen height %f",screenHeight);
    //database = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).database;
 /*   if ((screenHeight > 850) && (screenHeight < 1000)) {
        table.frame = CGRectMake(40, 60 ,screenWidth-80,screenHeight-150);
    }
    if(screenHeight==1024.000000)
    {
        table.frame = CGRectMake(40, 120, UIScreen.mainScreen.bounds.size.width-80,screenHeight-450);
    }
    else if(screenHeight==568.000000)
    {
      table.frame = CGRectMake(40, 100, UIScreen.mainScreen.bounds.size.width-80,screenHeight);
    }

    else if(screenHeight==480.000000)
    {
        table.frame = CGRectMake(40, 100, screenWidth-80,screenHeight-80);
    }
    else if(screenHeight == 736)
    {
        table.frame = CGRectMake(60, 120, screenSize.width-120, screenHeight-150);
    }
    else if(screenHeight == 667)
    {
        table.frame = CGRectMake(40, 120, screenSize.width-80, screenHeight-300);
    }
    else
    {
        table.frame = CGRectMake(60, 120, screenSize.width-120, screenHeight-100);
    }
    table.center = self.view.center;*/

    
    x = table.frame.origin.x;

    NSLog(@"seletced land= %@ and ang=%@",language,ang);
    arrayLanguages=[[NSMutableArray alloc]init];
  /*  scroll.maximumZoomScale=4.0;
    scroll.minimumZoomScale=1.0;*/
    self.navigationController.navigationBarHidden=NO;
    arrayItems=[[NSMutableArray alloc]init];
    shabdValue=[shabd intValue];
    [self setTheme];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"refresh" object:Nil];
    database = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).database;
//    HUD=[[MBProgressHUD alloc]initWithView:self.tabBarController.view];
//    [self.tabBarController.view addSubview:HUD];
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.delegate=self;
    [self setSliderMenu];
    
    [self getData];
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandler:)];
    tapGesture.numberOfTapsRequired = 1;
    [table addGestureRecognizer:tapGesture];
    
    arrayNames = [NSMutableArray new];
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    bookMarkPage = [[BookMarkPages alloc] initWithEntity:[NSEntityDescription entityForName:@"BookMarkPages" inManagedObjectContext:managedObjectContext] insertIntoManagedObjectContext:managedObjectContext];
    
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem * btnBack = [[UIBarButtonItem alloc] initWithTitle:@"<Back" style:UIBarButtonItemStyleBordered target:self action:@selector(backAction)]; //[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backButton"] style:UIBarButtonItemStyleBordered target:self action:@selector(backAction)];
    btnBack.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = btnBack;
    // Do any additional setup after loading the view from its nib.
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setSliderMenu
{
    int xx = 0;
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) xx=20;
    UIView *twitterItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [twitterItem setMenuActionWithBlock:^{
        [self twitterShare];
        [sideMenu close];
    }];
    UIImageView *twitterIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [twitterIcon setImage:[UIImage imageNamed:@"tw.png"]];
    [twitterItem addSubview:twitterIcon];
    
    UIView *emailItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [emailItem setMenuActionWithBlock:^{
        [self emailShare];
        [sideMenu close];
    }];
    UIImageView *emailIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx , 40+xx)];
    [emailIcon setImage:[UIImage imageNamed:@"mail.png"]];
    [emailItem addSubview:emailIcon];
    
    UIView *facebookItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [facebookItem setMenuActionWithBlock:^{
        [self facebookShare];
        [sideMenu close];
    }];
    UIImageView *facebookIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 36+xx)];
    [facebookIcon setImage:[UIImage imageNamed:@"fb.png"]];
    [facebookItem addSubview:facebookIcon];
    
    UIView *browserItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [browserItem setMenuActionWithBlock:^{
        [sideMenu close];
    }];
    UIImageView *browserIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [browserIcon setImage:[UIImage imageNamed:@"closeButton.png"]];
    [browserItem addSubview:browserIcon];
    
    UIView *messageItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 37+xx)];
    [messageItem setMenuActionWithBlock:^{
        NSLog(@"mms item");
        [self sendMms];
        [sideMenu close];
    }];
    UIImageView *messageIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 36+xx)];
    [messageIcon setImage:[UIImage imageNamed:@"mmsButton.png"]];
    [messageItem addSubview:messageIcon];
    
    sideMenu = [[HMSideMenu alloc] initWithItems:@[twitterItem, emailItem, facebookItem ,messageItem,browserItem]];
    [sideMenu setItemSpacing:5.0f];
    [self.view addSubview:sideMenu];
}
-(IBAction)btnProceed:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.persistent = YES;
    pasteboard.image =attachmentImage;
    NSString *phoneToCall = @"sms:";
    NSString *phoneToCallEncoded = [phoneToCall stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:phoneToCallEncoded];
    [[UIApplication sharedApplication] openURL:url];
}
-(void)sendMms
{
    [[KGModal sharedInstance]showWithContentView:popView andAnimated:YES];
}

-(void)tapGestureHandler:(UITapGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:table];
    NSIndexPath *indexPath = [table indexPathForRowAtPoint:p];
    UITableViewCell *selectedCell=(UITableViewCell *)[table cellForRowAtIndexPath:indexPath];
    ISGDataModel *data = (ISGDataModel *)[arrayItems objectAtIndex:indexPath.row];
    shabad=[data.shabd intValue];
    dataID=[data.dataID intValue];
    if([language isEqualToString:@"gurmukhi"])
        sggsData=data.gurmukhi;
    else sggsData=data.roman;
    strShabd=data.shabd;
    strId=data.dataID;
    
    if([stringBackgroud isEqualToString:@"LightPattern"])
    {
        selectedCell.backgroundColor=[UIColor clearColor];
    }
    else if([stringBackgroud isEqualToString:@"WhitePattern"])
    {
        selectedCell.backgroundColor=[UIColor clearColor];
    }
    else if([stringBackgroud isEqualToString:@"BlackPattern"])
    {
        selectedCell.backgroundColor=[UIColor clearColor];
    }
    else if([stringBackgroud isEqualToString:@"DarkPattern"])
    {
        selectedCell.backgroundColor=[UIColor clearColor];
    }
    
    CGFloat wd;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        wd = 290;
    }
    else
    {
        wd = 680;
    }
    
    UIGraphicsBeginImageContext(CGSizeMake(wd, selectedCell.frame.size.height));
    CGRect tableViewFrame = selectedCell.frame;
    selectedCell.frame = CGRectMake(0.0, 0.0, selectedCell.frame.size.width, table.contentSize.height);
   // selectedCell.backgroundColor = [UIColor clearColor];
    [selectedCell.layer renderInContext:UIGraphicsGetCurrentContext()];
    attachmentImage = UIGraphicsGetImageFromCurrentImageContext();
    selectedCell.frame = tableViewFrame;
    UIGraphicsEndImageContext();
    selectedCell.backgroundColor=[UIColor clearColor];
    
    bookMarkPage.pageNo = ang;
    bookMarkPage.scriptID = data.script;
    bookMarkPage.cellImage = UIImageJPEGRepresentation(attachmentImage, 1.0);
    bookMarkPage.query = stringQuery;
    bookMarkPage.title = self.title;

    
    /*UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Options" message:Nil delegate:self cancelButtonTitle:Nil otherButtonTitles:@"Add to favourites",@"Share",@"Print this page",@"Cancel" ,nil];
    [alert show];
    alert.tag=1;*/
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Ipad" bundle:nil];
        ISGCustomAlertViewController *customAlertVC = [storyboard instantiateViewControllerWithIdentifier:@"ISGCustomAlertViewController"];
        customAlertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        customAlertVC.modalTransitionStyle   = UIModalTransitionStyleCrossDissolve;
        customAlertVC.heading = self.title;
        customAlertVC.pageNo = ang;
        customAlertVC.scriptID = data.script;
        customAlertVC.cellImage = UIImageJPEGRepresentation(attachmentImage, 1.0);
        customAlertVC.query = stringQuery;
        customAlertVC.imageSMS = attachmentImage;
        customAlertVC.imageData = [ScrollViewToPDF pdfDataOfScrollView:table];
        
        [self presentViewController:customAlertVC animated:YES completion:nil];

    }
    else{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    ISGCustomAlertViewController *customAlertVC = [storyboard instantiateViewControllerWithIdentifier:@"ISGCustomAlertViewController"];
    
    customAlertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    customAlertVC.modalTransitionStyle   = UIModalTransitionStyleCrossDissolve;
    customAlertVC.heading = self.title;
    customAlertVC.pageNo = ang;
    customAlertVC.scriptID = data.script;
    customAlertVC.cellImage = UIImageJPEGRepresentation(attachmentImage, 1.0);
    customAlertVC.query = stringQuery;
    customAlertVC.imageSMS = attachmentImage;
    customAlertVC.imageData = [ScrollViewToPDF pdfDataOfScrollView:table];

    [self presentViewController:customAlertVC animated:YES completion:nil];
    }

}

-(void)longPressHandler:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
    {
        UILabel * copyLable = (UILabel *)gestureRecognizer.view;
        CGPoint point = [gestureRecognizer locationInView:copyLable];
        [copyLable becomeFirstResponder];
        UIMenuController * menu = [UIMenuController sharedMenuController];
        [menu setTargetRect:CGRectMake(point.x, point.y, 0.0f, 0.0f) inView:copyLable];
        message = copyLable.text;
        UIMenuItem * copy = [[UIMenuItem alloc] initWithTitle:@"copy" action:@selector(copyMessage)];
        menu.menuItems = @[copy];
        [menu setMenuVisible:YES animated:YES];
        menu.arrowDirection = UIMenuControllerArrowDown;
    }
}

- (void)copyMessage
{
    UIPasteboard * clipBoard = [UIPasteboard generalPasteboard];
    clipBoard.string = message;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL) canPerformAction:(SEL)action withSender:(id)sender
{
    return (action == @selector(copyMessage));
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==2 && buttonIndex==0)
    {
        //[self insertBookmark];
        [self showFolderList];
    }
    else if(alertView.tag==1 && buttonIndex==0)
    {
        UIAlertView *alertBookmark = [[UIAlertView alloc] initWithTitle:@"Are you sure to add this to your favourites?"
                                                                message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
        UITextField *txtAlert = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 80.0, 260.0, 30.0)];
        [txtAlert setBackgroundColor:[UIColor whiteColor]];
        txtAlert.text=sggsData;
        if([language isEqualToString:@"roman"])
            txtAlert.font=[UIFont systemFontOfSize:20];
        else txtAlert.font=[UIFont fontWithName:@"GurbaniLipiBold" size:22];
        txtAlert.userInteractionEnabled=NO;
        //[alertBookmark addSubview:txtAlert];
        [alertBookmark show];
        alertBookmark.tag=2;
    }
    else if(alertView.tag==1 && buttonIndex==1)
    {
        [sideMenu open];
    }
    else if(alertView.tag==1 && buttonIndex==2)
    {
       // imageForPrint = [self captureView:table];
        NSData *imageData = [ScrollViewToPDF pdfDataOfScrollView:table]; //UIImagePNGRepresentation(imageForPrint);
        [self printItem:imageData];
    }
    else if (alertView.tag == 100 && buttonIndex == 1)
    {
        UITextField * textField = [alertView textFieldAtIndex:0];
        if (textField.text.length == 0)
        {
            [[[UIAlertView alloc] initWithTitle:@"Invalid text" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        else
        {
            [BookMarkFolders addBookMarkFolderWithName:textField.text index:(int)arrayNames.count];
            [self showFolderList];
        }
    }
}

- (UIImage *)captureView:(UIScrollView *)view
{
    UIImage* image = nil;
    
    CGPoint savedContentOffset = view.contentOffset;
    CGRect savedFrame = view.frame;
    
    UIGraphicsBeginImageContextWithOptions(view.contentSize, 0, 0);
    //UIGraphicsBeginImageContext(view.contentSize);
    view.contentOffset = CGPointZero;
    view.frame = CGRectMake(0, 0, view.contentSize.width, view.contentSize.height);
    
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    view.contentOffset = savedContentOffset;
    view.frame = savedFrame;
    
    UIGraphicsEndImageContext();
    
    // after all of this, crop image to needed size
    return image;
}

-(void) showFolderList
{
    arrayFolders = [BookMarkFolders fetchAllBookMarkFolders];
    [arrayNames removeAllObjects];
    [arrayFolders enumerateObjectsUsingBlock:^(BookMarkFolders * obj, NSUInteger idx, BOOL *stop)
    {
        [arrayNames addObject:obj.folderName];
    }];
    if (arrayNames.count == 0)
    {
        [self createFolder];
    }
    else
    {
        UIView * kgView = [[UIView alloc] initWithFrame:CGRectMake(40, 100, 240, 240)];
        TableList = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kgView.frame.size.width, kgView.frame.size.height - 40) style:UITableViewStylePlain];
        TableList.delegate = self;
        TableList.dataSource = self;
        TableList.backgroundColor = [UIColor clearColor];
        
        [kgView addSubview:TableList];
        
        UIButton * buttonCreateFolder = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(TableList.frame), kgView.frame.size.width, 40)];
        [buttonCreateFolder setTitle:@"Create folder" forState:UIControlStateNormal];
        [buttonCreateFolder setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [buttonCreateFolder setBackgroundColor:[UIColor whiteColor]];
        [buttonCreateFolder addTarget:self action:@selector(createFolder) forControlEvents:UIControlEventTouchUpInside];
        [kgView addSubview:buttonCreateFolder];
        
        [[KGModal sharedInstance] showWithContentView:kgView andAnimated:YES];
    }
}

-(void)createFolder
{
    [[KGModal sharedInstance] hideAnimated:YES];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"No BookMark Folders" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Create", nil];
    alert.tag = 100;
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}


-(void)insertBookmark
{
    [database open];
    script=self.title;
    [database executeUpdate:@"INSERT INTO bookmark (markText,page,scrpt,language,tab) VALUES (?,?,?,?,'Search');",sggsData,strShabd,script,language];
//    NSLog(@"%@",[database lastErrorMessage]);
    [database close];
    [SVProgressHUD showSuccessWithStatus:@"Bookmarked successfully"];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"favourites" object:Nil];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) [self facebookShare];
    else if (buttonIndex==1) [self twitterShare];
    else if (buttonIndex==2) [self emailShare];
}

-(void)emailShare
{
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailCompose = [[MFMailComposeViewController alloc] init];
        mailCompose.mailComposeDelegate = self;
        NSData *data = [NSData dataWithData:UIImageJPEGRepresentation(attachmentImage, 1.0)];
        [mailCompose setSubject:@"iSearchGurbani share"];
        [mailCompose addAttachmentData:data
                              mimeType:@"jpg"
                              fileName:@"iSearchGurbani.jpg"];
        [self presentViewController:mailCompose animated:YES completion:Nil];
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"Please configure a mail account in your device and try again later" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    NSString *message=@"Your email has been discarded";
	switch (result) {
            
        case MFMailComposeResultFailed:
            message=@"Your email has failed to send";
            break;
            
        case MFMailComposeResultSent :
            message=@"Your email has sent Successfully";
            break;
            
        case MFMailComposeResultCancelled :
            NSLog(@"cancelled");
            break;
            
        case MFMailComposeResultSaved :
            message=@"Your email has been saved to draft";
            break;
        default:
            break;
    }
    [[[UIAlertView alloc]initWithTitle:@"Mail Status" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
    [self dismissViewControllerAnimated:YES completion:Nil];
}

-(void)facebookShare
{
    HUD.labelText=@"Sharing...";
    [HUD show:YES];
//    appDelegate = (ISGAppDelegate *)[[UIApplication sharedApplication] delegate] ;
//    if (!appDelegate.session)
//    {
//        NSLog(@"enterd");
//        appDelegate.session = [[FBSession alloc]initWithPermissions:[NSArray arrayWithObjects:@"publish_stream",nil]];
//    }
//    if (![appDelegate.session isOpen])
//    {
//        NSLog(@"open");
//        [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
//         {
//             if (!error)
//             {
//                 [self fbShare];
//             }
//         }];
//    }
//    else{
        [self fbShare];
//    }
    
}
-(void) fbShare
{
//    [FBSession setActiveSession:appDelegate.session];
//    NSData *dataImage=UIImageJPEGRepresentation(attachmentImage,1.0);
//    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
//    [parameters setObject:@"iSearchGurbani" forKey:@"name"];
//    [parameters setObject:@"http://www.searchgurbani.com/" forKey:@"caption"];
//    [parameters setObject:dataImage forKey:@"picture"];
    
//    [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession] parameters:parameters handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//        [HUD hide:YES];
//        if(error == nil)
//        {
//            if(result != FBWebDialogResultDialogNotCompleted && resultURL.query != nil)
//            {
//                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sucessfully shared to facebook" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
//            }
//            else
//            {
//                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sharing canceled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
//            }
//        }
//        else
//        {
//            [[[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
//        }
//    }];
    
    SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [mySLComposerSheet setInitialText:@"iSearchGurbaani"];
    
    [mySLComposerSheet addImage:attachmentImage];
    
    [mySLComposerSheet addURL:[NSURL URLWithString:@"http://www.searchgurbani.com/"]];
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sharing canceled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                break;
            case SLComposeViewControllerResultDone:
                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sucessfully shared to facebook" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                break;
                
            default:
                break;
        }
    }];
    
    [self presentViewController:mySLComposerSheet animated:YES completion:^{
        [HUD hide:YES];
    }];
}

-(void)twitterShare
{
    TWTweetComposeViewController *tweetView = [[TWTweetComposeViewController alloc] init];
    
    TWTweetComposeViewControllerCompletionHandler
    completionHandler =
    ^(TWTweetComposeViewControllerResult result) {
        
        if(result == TWTweetComposeViewControllerResultDone) {
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"The Tweet was posted successfully." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        if(result == TWTweetComposeViewControllerResultCancelled) {
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You cancelled posting the Tweet." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    };
    [tweetView setCompletionHandler:completionHandler];
    
    //[tweetView setInitialText:@"Try this App: Design Studio"];
    [tweetView addImage:attachmentImage];
    //[tweetView addURL:url];
    [self presentViewController:tweetView animated:YES completion:nil];
}

-(void)refresh:(NSNotification *)notification
{
    [arrayLanguages removeAllObjects];
    [self setTheme];
    [self buildCells];
    [table reloadData];

}
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return table;
}
-(IBAction)btnLeft:(id)sender
{
    if([ang intValue]!=1)
    {
        buttonRight.hidden=FALSE;
        //shabdValue--;
        ang=[NSString stringWithFormat:@"%d",[ang intValue] -1];
        
       // table.frame = CGRectMake(x- self.view.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
        [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
            //table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
        } completion:^(BOOL finished) {
            table.alpha = 1.0;
        }];
        
        [self getData];
    }
}
-(IBAction)btnRight:(id)sender
{
    buttonLeft.hidden=FALSE;
    ang=[NSString stringWithFormat:@"%d",[ang intValue] +1];
    //shabdValue++;
    
    //table.frame = CGRectMake(table.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
       // table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    } completion:^(BOOL finished) {
        table.alpha = 1.0;
    }];
    
    [self getData];
}
-(IBAction)swipeRight:(UISwipeGestureRecognizer *)sender
{
    if([ang intValue]!=1)
    {
        buttonRight.hidden=FALSE;
        ang=[NSString stringWithFormat:@"%d",[ang intValue] -1];
        //shabdValue--;
       // table.frame = CGRectMake(x- self.view.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
        [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
            //table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
        } completion:^(BOOL finished) {
            table.alpha = 1.0;
        }];
        
        
        [self getData];
    }
}
-(IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender
{
    buttonLeft.hidden=FALSE;
    ang=[NSString stringWithFormat:@"%d",[ang intValue] +1];
    //shabdValue++;
    
   // table.frame = CGRectMake(table.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
       // table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    } completion:^(BOOL finished) {
        table.alpha = 1.0;
    }];

    
    [self getData];
}
-(void)setNavTitle:(NSString *)title forPage:(NSString *)page
{
   
    if(title.length>0)
    {
        if([title isEqualToString:@"1"])
        {
            title=@"SRI GURU GRANTH SAHIB";
        }
        else if([title isEqualToString:@"2"])
        {
            title=@"BHAI GURDAS VAAR";
        }
        else if([title isEqualToString:@"3"])
        {
            title=@"BHAI GURDAS KABIT";
        }
        else if([title isEqualToString:@"4"])
        {
            title=@"SRI DASAM GRANTH PAGE";
        }
        else if([title isEqualToString:@"5"])
        {
            self.title=@"BAANi BHAI NAND LAL";
        }
        else
        {
             //title = [NSString stringWithFormat:@"SGGS Ang %@", title];
            title = [NSString stringWithFormat:@"Ang"];
        }
        
        self.title=[NSString stringWithFormat:@"%@ '%@'",title,page];
        //self.title = title;
        
    }

}
-(void)getData
{
    [arrayItems removeAllObjects];
    if([ang intValue]>0)
    {
        
        if([ang intValue]==1)
        {
            buttonLeft.hidden=TRUE;
        }
    
    
    NSString *querry=[NSString stringWithFormat:@"SELECT ID,shabd,gurmukhi,english,roman,page,scrpt,attributes,translit,punctuation,teeka FROM gurbani  WHERE shabd ='%@' ORDER BY ROWID ASC",ang];
    NSLog(@"%@",querry);
        stringQuery = querry;
    [database open];
    FMResultSet *results = [database executeQuery:querry];
    while([results next])
    {
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithShabd:results];
        [arrayItems addObject:dataModel];
    }
    [database close];
    if(arrayItems.count==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"No more data" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            ang=[NSString stringWithFormat:@"%d",[ang intValue]-1];
            buttonRight.hidden=TRUE;
        }
        else
        {
           
            [self buildCells];
            [table reloadData];
        }
    }
}
-(void)setTheme
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    ISGTheme *theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    stringBackgroud=theme.background;
    fontSize=[theme.fontSize intValue];
    fontStyle=theme.fontStyle;
    language1=theme.lang1;
    language2=theme.lang2;
    language3=theme.lang3;
    language4=theme.lang4;
    language5=theme.lang5;
    language6=theme.lang6;
//    language7=theme.lang7;
    
    gurmukhiSize=[theme.gurmukhiSize intValue];
    romanSize=[theme.romanSize intValue];
    hindiSize=[theme.hindiSize intValue];
    englishSize=[theme.englishSize intValue];
    teekaSize=[theme.teekaSize intValue];
    attributeSize=[theme.attributeSize intValue];
    
    gurmukhiColor = theme.gurmukhiColor;
    teekaColor = theme.teekaColor;
    hindiColor = theme.hindiColor;
    englishColor = theme.englishColor;
    romanColor = theme.romanColor;
    attributeColor = theme.attributeColor;
    
    if(language1.length>0) [arrayLanguages addObject:language1];
    if(language2.length>0) [arrayLanguages addObject:language2];
    if(language3.length>0) [arrayLanguages addObject:language3];
    if(language4.length>0) [arrayLanguages addObject:language4];
    if(language5.length>0) [arrayLanguages addObject:language5];
    if(language6.length>0) [arrayLanguages addObject:language6];
    if (language7.length>0)[arrayLanguages addObject:language7];
    if(fontSize==0)
    {
        fontSize=13;
    }
    if(stringBackgroud.length<=7)
    {
        stringBackgroud=@"LightPattern";
    }
    if (!theme.backgroundImage)
    {
        [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
    }
    else
        imageBg.image = theme.backgroundImage;

    
    if([arrayLanguages containsObject:language])
    {
        showRoman=NO;
    }
   // NSLog(@"%@",arrayLanguages);
}
-(void)SetDefaultValues
{
    stringBackgroud=@"LightPattern";
    fontSize=14;
    fontStyle=@"GurbaniLipiBold";
}

- (void)buildCells {
    // clear existing data
   
    if (cells) [cells removeAllObjects]; else cells = [[NSMutableArray alloc] init];
    if (heightsCell) [heightsCell removeAllObjects]; else heightsCell = [[NSMutableArray alloc] init];
    NSMutableArray *arrayLabels;
    for (ISGDataModel *data in arrayItems) {
       // NSLog(@"ID :%@",data.dataID);
        if (arrayLabels) [arrayLabels removeAllObjects]; else arrayLabels = [[NSMutableArray alloc] init];
        for(int index=1;index<=arrayLanguages.count;index++)
        {
            UILabel *label = [[UILabel alloc] init];
            if (![[arrayLanguages objectAtIndex:index-1] isEqualToString:@"lareevar"])
            {
                [label setText:[data valueForKey:[arrayLanguages objectAtIndex:index-1]]];
            }
            label.textColor = [UIColor whiteColor];
            
            // setting font and size
            if(index==1 && [[arrayLanguages objectAtIndex:index-1] isEqualToString:@"gurmukhi"])
            {
                [label setFont:[UIFont fontWithName:fontStyle size:gurmukhiSize]];
                [label setTextColor:gurmukhiColor];
                
            }
            else if (index!=1 && [[arrayLanguages objectAtIndex:index-1] isEqualToString:@"gurmukhi"])
            {
                [label setFont:[UIFont fontWithName:@"Gurbanihindi" size:hindiSize]];
                [label setTextColor:hindiColor];
                
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"teeka"])
            {
                [label setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:teekaSize]];
                [label setTextColor:teekaColor];
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"attributes"])
            {
                [label setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:attributeSize]];
                [label setTextColor:attributeColor];
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"english"])
            {
                label.font=[UIFont boldSystemFontOfSize:englishSize];
                [label setTextColor:englishColor];
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"roman"])
            {
                label.font=[UIFont boldSystemFontOfSize:romanSize];
                //label.font = [UIFont fontWithName:@"Rosemary-Roman" size:romanSize];
                [label setTextColor:romanColor];
              
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"punctuation"])
            {
                [label setFont:[UIFont fontWithName:fontStyle size:gurmukhiSize]];
                [label setTextColor:gurmukhiColor];
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"translit"])
            {
                [label setFont:[UIFont boldSystemFontOfSize:romanSize]];
                //label.font = [UIFont fontWithName:@"Rosemary-Roman" size:romanSize];
                [label setTextColor:romanColor];
                
            }
            if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"lareevar"])
            {
                NSString * str = [data valueForKey:@"gurmukhi"];
                NSString * lareevar = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
                [label setFont:[UIFont fontWithName:fontStyle size:gurmukhiSize]];
                [label setTextColor:gurmukhiColor];
                label.text = lareevar;
            }
            //highlighting search results
            if([stringBackgroud isEqualToString:@"LightPattern"]||[stringBackgroud isEqualToString:@"WhitePattern"])
            {
                if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"gurmukhi"]||[[arrayLanguages objectAtIndex:index-1] isEqualToString:@"punctuation"])
                {
                    if([selectedId isEqualToString:data.dataID] && [_selectedLang isEqualToString:@"gurmukhi"])
                    {
                        label.textColor=[UIColor  colorWithHexString:@"ff0000"];
                    }
                }
                else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"roman"]||[[arrayLanguages objectAtIndex:index-1] isEqualToString:@"translit"])
                {
                    if([selectedId isEqualToString:data.dataID] && [_selectedLang isEqualToString:@"roman"])
                    {
                        label.textColor=[UIColor  colorWithHexString:@"ff0000"];
                    }
                    
                }
            }
            else
            {
                if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"gurmukhi"]||[[arrayLanguages objectAtIndex:index-1] isEqualToString:@"punctuation"])
                {
                    if([selectedId isEqualToString:data.dataID] && [_selectedLang isEqualToString:@"gurmukhi"])
                    {
                        label.textColor=[UIColor  colorWithHexString:@"ff0000"];
                    }
                }
                else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"roman"]||[[arrayLanguages objectAtIndex:index-1] isEqualToString:@"translit"])
                {
                    if([selectedId isEqualToString:data.dataID] && [_selectedLang isEqualToString:@"roman"])
                    {
                        label.textColor=[UIColor  colorWithHexString:@"ff0000"];
                    }
                }
            }
            
            lpHandler = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressHandler:)];
            lpHandler.minimumPressDuration = 0.5;
            label.userInteractionEnabled = YES;
            [label addGestureRecognizer:lpHandler];
            
            
            [arrayLabels addObject:label];
        }
        static NSString *cellIdentifier = @"ISGCell";
        UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[ISGCell alloc] initWithLabels:arrayLabels ];
            int labelHeight = 0;
            for (UILabel *label in arrayLabels) {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    label.frame = CGRectMake(5, labelHeight+2, 685, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(685, 9999)].height);
                }
                else
                {
                    //287
                    label.frame = CGRectMake(0, labelHeight+2, 287, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(287, 9999)].height);
                }
                
                labelHeight += label.frame.size.height;
                
            }
            NSNumber *someNumber = [NSNumber numberWithInt:labelHeight + 10];
            [heightsCell addObject:someNumber];
        }
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell.layer setMasksToBounds:YES];
        [cells addObject:cell];
    }
    table.separatorColor=[UIColor grayColor];
    
}
- (CGSize)getHeightForString:(NSString *)string forFont:(UIFont *)font forConstraintSize:(CGSize)constrainSize {
    //    NSLog(@"string height %f",[string sizeWithFont:font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByWordWrapping].height);
    return [string sizeWithFont:font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByWordWrapping];
}

#pragma mark tableview delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == TableList)
    {
        return arrayNames.count;
    }
    else
    {
       // [tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        return [cells count];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == TableList)
    {
        return 40;
    }
    else {
    ISGCell * cell = cells[indexPath.row];
    NSLog(@"cell:%f",cell.labelHeight);
   [cell layoutSubviews];
    
    
    return  cell.labelHeight + 24;
    }
       // return [[cells objectAtIndex:indexPath.row] cellHeight];
       // return [[cells objectAtIndex:indexPath.row]frame].size.height;
       // return [[heightsCell objectAtIndex:(int)[indexPath row]] intValue];

}
-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == TableList)
    {
        UITableViewCell * cellList = [[UITableViewCell alloc] initWithStyle:UITableViewStylePlain reuseIdentifier:@"myCell"];
        if (cellList == nil)
        {
            cellList = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
        }
        cellList.textLabel.text = arrayNames[indexPath.row];
        cellList.imageView.image = [UIImage imageNamed:@"folders"];
        return cellList;
    }
    else
    {
        ISGDataModel *dataModel=arrayItems[indexPath.row];
        [self setNavTitle:dataModel.script forPage:dataModel.page];
        return [cells objectAtIndex:indexPath.row];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == TableList)
    {
        BookMarkFolders * fol = arrayFolders[indexPath.row];
        NSSet * set = fol.relationshipToPages;
        NSArray * array = [NSArray arrayWithArray:set.allObjects];
        int cnt = 0;
        bookMarkPage.index = [NSString stringWithFormat:@"%lu",(unsigned long)array.count];
        for (BookMarkPages * obj in array)
        {
            if ([bookMarkPage.pageNo isEqualToString:obj.pageNo] && [bookMarkPage.scriptID isEqualToString:obj.scriptID] && [bookMarkPage.cellImage isEqualToData:obj.cellImage] && [bookMarkPage.query isEqualToString:obj.query])
            {
                cnt++;
            }
        }
        if (cnt>0)
        {
            [SVProgressHUD showErrorWithStatus:@"Already added to the list"];
        }
        else
        {
            [BookMarkPages addBookMarkPage:bookMarkPage inBookMarkFolder:fol];
            [SVProgressHUD showSuccessWithStatus:@"Added"];
        }
        [[KGModal sharedInstance] hideAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Printing
-(void)printItem :(NSData*)data
{
    printController = [UIPrintInteractionController sharedPrintController];
    if(printController && [UIPrintInteractionController canPrintData:data])
    {
        printController.delegate = self;
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [NSString stringWithFormat:@""];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        printController.printInfo = printInfo;
        printController.showsPageRange = YES;
        printController.printingItem = data;
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error)
            {
                //NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
            }
        };
        //[printController presentFromBarButtonItem:self.item animated:YES completionHandler:completionHandler];
        //[printController presentAnimated:YES completionHandler:completionHandler];
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            [printController presentFromRect:CGRectMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2, 0, 0) inView:self.view animated:YES completionHandler:completionHandler];
        }
        else
        {
            [printController presentAnimated:YES completionHandler:completionHandler];
        }
    }
}


@end
