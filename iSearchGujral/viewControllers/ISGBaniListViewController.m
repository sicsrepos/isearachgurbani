//
//  ISGBaniListViewController.m
//  iSearchGujral
//
//  Created by mac on 4/22/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGBaniListViewController.h"
#define CASE(str) if ([__s__ isEqualToString:(str)])
#define SWITCH(s) for (NSString *__s__ = (s); ; )
#define DEFAULT
@interface ISGBaniListViewController ()
{
    NSDictionary *dict;
}
@end

@implementation ISGBaniListViewController
@synthesize bani,arrayItems,key;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [tableBaani setBackgroundColor:[UIColor clearColor]];
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
     self.navigationController.navigationBarHidden = NO;
    self.title=bani;
    [self setTheme];
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    CGFloat screenWidth = screenSize.width;

    NSLog(@"screen height %f",screenHeight);
  /*  if ((screenHeight > 850) && (screenHeight < 1000)) {
        tableBaani.frame = CGRectMake(40, 60 ,screenWidth-80,screenHeight-500);
    }
    if(screenHeight==1024.000000)
    {
        tableBaani.frame = CGRectMake(40, 44, screenWidth-80,screenHeight-500);
    }
    if(screenHeight==1366.000)
    {
        tableBaani.frame = CGRectMake(40, 50, screenWidth-80,screenHeight-500);
    }
    tableBaani.center = self.view.center;*/
    NSString *plistPath=[[NSBundle mainBundle] pathForResource:@"Baani" ofType:@"plist"];
    dict=[NSDictionary dictionaryWithContentsOfFile:plistPath];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"refresh" object:Nil];
    // Do any additional setup after loading the view from its nib.
}
-(void)refresh:(NSNotification *)notification
{
    [self setTheme];
    [tableBaani reloadData];
}
-(void)setTheme
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    ISGTheme *theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    stringBackgroud=theme.background;
    fontSize=[theme.fontSize intValue];
    if(fontSize==0)
    {
        fontSize=14;
    }
    if(stringBackgroud.length<=7)
    {
        stringBackgroud=@"LightPattern";
    }
    if (!theme.backgroundImage)
    {
        [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
    }
    else
        imageBg.image = theme.backgroundImage;
    
    
}
-(void)SetDefaultValues
{
    stringBackgroud=@"LightPattern";
    fontSize=14;
    //fontStyle=@"gurbaniwebthick";
}
#pragma tableview delegates
-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayItems count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 60;
    }
    else
        return 44;
}

-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    fontSize = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 24 : fontSize;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if([stringBackgroud isEqualToString:@"LightPattern"]||[stringBackgroud isEqualToString:@"WhitePattern"])
    {
        [cell.textLabel setTextColor:[UIColor blackColor]];
    }
    else
    {
        [cell.textLabel setTextColor:[UIColor whiteColor]];
    }
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.numberOfLines=2;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.font=[UIFont boldSystemFontOfSize:fontSize];
    NSArray *arr=[[arrayItems objectAtIndex:indexPath.row] componentsSeparatedByString:@"_"];
    cell.textLabel.text=[arr objectAtIndex:1];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    ISGBaniDetailViewController *BaniDetailViewController=[[ISGBaniDetailViewController alloc]initWithNibName:@"ISGBaniDetailViewController" bundle:Nil];
//    BaniDetailViewController.bani=[arrayItems objectAtIndex:indexPath.row];
//    BaniDetailViewController.arrayIndex=indexPath.row;
//    BaniDetailViewController.arrayData=arrayItems;
//    [self.navigationController pushViewController:BaniDetailViewController animated:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    
    ISGBaniDetailViewController * baniDetailViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGBaniDetailViewController"];
    //ISGBaniDetailViewController *baniDetailViewController=[[ISGBaniDetailViewController alloc]initWithNibName:@"ISGBaniDetailViewController" bundle:Nil];
   // baniDetailViewController.arrayData=[[dict objectForKey:key]objectForKey:[arrayItems objectAtIndex:indexPath.row]];
    baniDetailViewController.arrayBaani=arrayItems;
    baniDetailViewController.arrayIndex=indexPath.row;
    baniDetailViewController.key=key;
    baniDetailViewController.bani=bani;
    [self.navigationController pushViewController:baniDetailViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
