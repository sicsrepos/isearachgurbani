//
//  BookMarkPages.h
//  iSearchGujral
//
//  Created by SICS on 18/09/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BookMarkFolders.h"


@interface BookMarkPages : NSManagedObject

@property (nonatomic, retain) NSData * cellImage;
@property (nonatomic, retain) NSString * index;
@property (nonatomic, retain) NSString * pageNo;
@property (nonatomic, retain) NSString * scriptID;
@property (nonatomic, retain) NSString * query;
@property (nonatomic, retain) NSString * title;

+(void) addBookMarkPage:(BookMarkPages *)bookMarkPage inBookMarkFolder:(BookMarkFolders *)bookMarkFolder;
+(void) deleteBookMarkPage:(BookMarkPages *)bookMarkPage inBookMarkFolder:(BookMarkFolders *)bookMarkFolder;
+(void) deleteAllBookMarkPageinBookMarkFolder:(BookMarkFolders *)bookMarkFolder;
+(void) updateBookMarkIndex:(NSArray *)arrayIndex inBookMarkPages:(NSArray *)arrayBookMarkPage inBookMarkFolder:(BookMarkFolders *)bookMarkFolder;

@end
