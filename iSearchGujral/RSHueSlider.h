//
//  RSHueSlider.h
//  iSearchGujral
//
//  Created by SICS on 20/01/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSColorPickerView.h"

@interface RSHueSlider : UISlider

@property (nonatomic) RSColorPickerView * colorPicker;

@end
