//
//  ISGFavouriteCollectionViewCell.h
//  iSearchGujral
//
//  Created by SICS on 05/01/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "UzysGridViewCell.h"

@interface ISGFavouriteCollectionViewCell : UzysGridViewCell

@property (nonatomic, strong) UILabel * labelName;
@property (nonatomic, strong) UIImageView * imageFolder;

@end
