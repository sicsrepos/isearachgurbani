//
//  SearchHistory.h
//  iSearchGujral
//
//  Created by SICS on 17/08/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SearchHistory : NSManagedObject

@property (nonatomic, retain) NSString * searchLanguage;
@property (nonatomic, retain) NSString * searchQuery;
@property (nonatomic, retain) NSString * searchText;
@property (nonatomic, retain) NSString * searchCategory;
@property (nonatomic, retain) NSString * searchType;
@property (nonatomic, retain) NSString * searchQueryWildcard;

+(void) addSearchHistoryForText:(NSString *)text Language:(NSString *)language Query:(NSString *)query Category:(NSString *)category Type:(NSString *)type andWildCardQuery:(NSString *)queryWildcard;
+(void) deleteAllSearchHistory;
+(NSArray *) fetchAllSearchHistory;
+(void) updateSearchHistory:(NSArray *)arrayHistory;

@end
