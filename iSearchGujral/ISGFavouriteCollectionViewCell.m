//
//  ISGFavouriteCollectionViewCell.m
//  iSearchGujral
//
//  Created by SICS on 05/01/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "ISGFavouriteCollectionViewCell.h"

@implementation ISGFavouriteCollectionViewCell

@synthesize labelName,imageFolder;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        
        self.imageFolder = [[UIImageView alloc] initWithFrame:CGRectNull];
        self.imageFolder.image = [UIImage imageNamed:@"folders"];
        [self addSubview:self.imageFolder];
        
        self.labelName = [[UILabel alloc] initWithFrame:CGRectNull];
        self.labelName.textAlignment = NSTextAlignmentCenter;
        self.labelName.backgroundColor = [UIColor clearColor];
        self.labelName.textColor = [UIColor blackColor];
//        self.labelName.lineBreakMode = NSLineBreakByWordWrapping;
//        self.labelName.numberOfLines = 0;
        [self addSubview:self.labelName];
        
        [self bringSubviewToFront:self.ButtonDelete];
    }
    return self;
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
    if (UIScreen.mainScreen.bounds.size.height == 1024)
    {
        
        self.imageFolder.frame = CGRectMake(25, 20, 50, 50);
        self.imageFolder.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.labelName.font = [UIFont systemFontOfSize:14];

        self.labelName.frame = CGRectMake(13, 55, 150, 60);
        self.labelName.textAlignment = NSTextAlignmentLeft;
        self.labelName.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    else if (UIScreen.mainScreen.bounds.size.height == 568)
    {
        self.imageFolder.frame = CGRectMake(10, 10, 50, 50);
        self.imageFolder.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.labelName.frame = CGRectMake(0, 49, 50, 50);
        self.labelName.font = [UIFont systemFontOfSize:12];
        //self.labelName.textAlignment = NSTextAlignmentLeft;

        self.labelName.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
 
    }
   
    else if (UIScreen.mainScreen.bounds.size.height == 736)
    {
        self.imageFolder.frame = CGRectMake(10, 10, 50, 50);
        self.imageFolder.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.labelName.frame = CGRectMake(0, 49, 60, 50);
        self.labelName.font = [UIFont systemFontOfSize:12];
       // self.labelName.textAlignment = NSTextAlignmentLeft;

        self.labelName.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
    }
    else if (UIScreen.mainScreen.bounds.size.height == 667)
    {
        self.imageFolder.frame = CGRectMake(10, 10, 50, 50);
        self.imageFolder.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.labelName.frame = CGRectMake(0, 49, 50, 50);
        self.labelName.font = [UIFont systemFontOfSize:12];
        //self.labelName.textAlignment = NSTextAlignmentLeft;

        self.labelName.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
    }
    
   
    
    else
    {
        self.imageFolder.frame = CGRectMake(10, 10, 50, 50);
        self.imageFolder.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.labelName.frame = CGRectMake(0, 49, 50, 50);
        self.labelName.font = [UIFont systemFontOfSize:12];
       // self.labelName.textAlignment = NSTextAlignmentLeft;

        self.labelName.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
    }
    
    

    
    
    
    
    }

@end
