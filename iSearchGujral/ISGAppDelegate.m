//
//  ISGAppDelegate.m
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import "ISGAppDelegate.h"
#import "ISGTabViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Social/Social.h>
@implementation ISGAppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize database = _database;
//@synthesize session;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Fabric with:@[[Crashlytics class]]];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[UIColor blackColor]];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self setUpDatabase];
    ISGTabViewController *TabViewController=[[ISGTabViewController alloc] init];
    [self.window setRootViewController:TabViewController];
    UITableView *moreTableView =(UITableView *) [[TabViewController.moreNavigationController topViewController] view] ;
    [moreTableView setBackgroundColor:[UIColor clearColor]];
    [moreTableView setSeparatorColor:[UIColor blackColor]];
    
    UITableViewCell *cell = [moreTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    cell.backgroundColor = [UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0];
    cell.contentView.backgroundColor = [UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0];
    UITableViewCell *cell1 = [moreTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    cell1.backgroundColor = [UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0];
    cell1.contentView.backgroundColor = [UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0];
    UITableViewCell *cell2 = [moreTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    cell2.backgroundColor = [UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0];
    cell2.contentView.backgroundColor = [UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0];
    UITableViewCell *cell3 = [moreTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    cell3.backgroundColor = [UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0];
    cell3.contentView.backgroundColor = [UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0];
    
    [moreTableView setScrollEnabled:NO];
    
    TabViewController .customizableViewControllers = nil;
    TabViewController.moreNavigationController.navigationBarHidden = NO;
    [[TabViewController tabBar] setSelectedImageTintColor:[UIColor brownColor]];
   
        [[UINavigationBar appearance] setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:
          [UIColor whiteColor], UITextAttributeTextColor,
          [UIFont boldSystemFontOfSize:15.0], UITextAttributeFont,nil]];
    
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 8)
    {
        [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0]];
        [self.window setTintColor:[UIColor whiteColor]];
    }
    else
    {
        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0]];
    }
    
    UIGraphicsBeginImageContext(self.window.bounds.size);
    [[UIImage imageNamed:@"preference"] drawInRect:CGRectMake([[UIScreen mainScreen] bounds].origin.x, [[UIScreen mainScreen] bounds].origin.y+44, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-88)];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.window.backgroundColor = [UIColor colorWithPatternImage:image];    
    //self.window.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
   // [[UIBarButtonItem appearance] setTintColor: [UIColor colorWithRed:123.0/255.0 green:21.0/255.0 blue:0.0/255.0 alpha:1.0]];
    [[UIBarButtonItem appearance] setTintColor: [UIColor whiteColor]];
   
    
    [self.window makeKeyAndVisible];
    [application setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:0
                                                         diskCapacity:0
                                                             diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    databasePath = [documentDir stringByAppendingPathComponent:databaseName];
    
    return YES;
}
//-(void)check{
//    [_database open];
//    //[_database executeUpdate:@"INSERT INTO bookmark (asd) VALUES ('qwe');"];
//   // [_database executeUpdate:@"DELETE FROM bookmark WHERE bookmarkid = 1"];
//    NSLog(@"%@",[_database lastErrorMessage]);
//    [_database close];
//}

-(void)setUpDatabase{
   
//        NSString *dbFilePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"isg2013_final.db"];
//        NSData *databaseData = [NSData dataWithContentsOfFile:dbFilePath];
//        //NSLog(@"size %d",databaseData.length);
//        if ([databaseData writeToFile:documentsDatabasePath atomically:YES]) {
//           
//        }
//    }
//     _database = [FMDatabase databaseWithPath:dbFilePath];
    
    // Deleting the Old data base
    if (![[NSFileManager defaultManager] fileExistsAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"isg_final08.db"]])
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"isg_final.db"]])
        {
            [[NSFileManager defaultManager] removeItemAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"isg_final.db"] error:nil];
        }
        else
        for (int i = 1; i < 8; i++)
        {
            //NSLog(@"%@",[NSString  stringWithFormat:@"isg_final0%d.db", i]);
            if ([[NSFileManager defaultManager] fileExistsAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:[NSString  stringWithFormat:@"isg_final0%d.db", i]]])
            {
                [[NSFileManager defaultManager] removeItemAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:[NSString  stringWithFormat:@"isg_final0%d.db", i]] error:nil];
            }
        }
        //[[NSFileManager defaultManager] removeItemAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"isg_final02.db"] error:nil];
    }

    // Last uploaded db is isg_final07.db
    NSString *documentsDatabasePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"isg2019.db"];
    
    if (![[NSFileManager defaultManager]fileExistsAtPath:documentsDatabasePath])
    {
        NSError *error;
        NSString *unZipFilePath;
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *zipFilePath = [[NSBundle mainBundle] pathForResource:@"isg2019.db" ofType:@"zip"];
        unZipFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"isg_final08.zip"];
        NSLog(@"unZipFilePath = %@",unZipFilePath);
        if (![fileMgr fileExistsAtPath:unZipFilePath])
        {
            NSLog(@"file not exist");
            [fileMgr createDirectoryAtPath:unZipFilePath withIntermediateDirectories:NO
                                attributes:nil error:&error];
        }
        if([SSZipArchive unzipFileAtPath:zipFilePath toDestination:[documentsDatabasePath stringByDeletingLastPathComponent] delegate:self])
        {
            [fileMgr removeItemAtPath:unZipFilePath error:&error];
            
        }
        
    }
        
    
    
//    // Deleting the Old data base
//    if (![[NSFileManager defaultManager] fileExistsAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"isg_final08.db"]])
//    {
//        for (int i = 0; i < 8; i++)
//        {
//            if ([[NSFileManager defaultManager] fileExistsAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:[NSString  stringWithFormat:@"isg_final0%d.db", i]]])
//            {
//                [[NSFileManager defaultManager] removeItemAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:[NSString  stringWithFormat:@"isg_final0%d.db", i]] error:nil];
//            }
//        }
//        //[[NSFileManager defaultManager] removeItemAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"isg_final02.db"] error:nil];
//    }
    
    _database = [FMDatabase databaseWithPath:documentsDatabasePath];
        
}
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
//    return[self.session handleOpenURL:url];
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
//     [FBSession.activeSession handleDidBecomeActive];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [FBSession.activeSession closeAndClearTokenInformation];
    [self saveContext];
    
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"iSearchGujral" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"iSearchGujral.sqlite"];
    
    NSError *error = nil;
    
   
    //NSURL *storeURL = [applicationDocumentsDirectory URLByAppendingPathComponent:@"Done.sqlite"];
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption : @(YES), NSInferMappingModelAutomaticallyOption : @(YES) };
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
