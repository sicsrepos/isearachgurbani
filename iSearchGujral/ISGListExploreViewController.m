//
//  ISGListExploreViewController.m
//  iSearchGujral
//
//  Created by SICS on 11/12/14.
//  Copyright (c) 2014 sics. All rights reserved.
//

#import "ISGListExploreViewController.h"
#import "ISGTheme.h"
#import "ISGDataModel.h"
#import <Twitter/Twitter.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "ISGAppDelegate.h"
#import "MBProgressHUD.h"
#import "ISGCell.h"
#import "HMSideMenu.h"
#import "KGModal.h"
#import "ISGReadSGGSViewController.h"

@interface ISGListExploreViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView * tableList;
    NSIndexPath * selectedIndex;
    NSMutableArray * arrayIndexPath;
    NSMutableArray * arrayPageID;
    NSMutableArray * arrayTotalPage,*arrayDetails;
    BOOL isSubCategory;
    int select;
    NSMutableArray * arrayPages, *arrayTitles;
    NSString *stringTitle;
}

@end

@implementation ISGListExploreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [super viewDidLoad];
    [tableList setBackgroundColor:[UIColor clearColor]];
    
    database = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).database;
    
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self setTheme];
    self.title=_titleName;
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    CGFloat screenWidth = screenSize.width;

   /* //NSLog(@"screen height %f",screenHeight);
    if ((screenHeight > 850) && (screenHeight < 1000))  {
        tableList.frame = CGRectMake(40, 60 ,screenWidth-80,350);
    }
    
    if(screenHeight==1024.000000)
    {
        tableList.frame = CGRectMake(40, 44, screenWidth-80,screenHeight-500);
    }
    if(screenHeight==1366.000)
    {
        tableList.frame = CGRectMake(40, 50, screenWidth-80,300);
    }*/
    tableList.center = self.view.center;
    arraySubChapterItems = [NSMutableArray new];
    arrayItems = [NSMutableArray new];
    arrayIndexPath = [NSMutableArray new];
    arrayPages = [NSMutableArray new];
    arrayPageID = [NSMutableArray new];
    arrayTotalPage = [NSMutableArray new];
    arrayDetails = [NSMutableArray new];
    arrayTitles = [NSMutableArray new];
    if (![_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"] && ![_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    {
        [self getData];
    }
    else
        [self getSubChaptersFrom:0 to:1];
        
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"refresh" object:Nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)refresh:(NSNotification *)notification
{
    [self setTheme];
    //[tableList reloadData];
}


#pragma mark Set Themes
-(void)setTheme
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    ISGTheme *theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    stringBackgroud=theme.background;
    fontSize=[theme.fontSize intValue];
    fontStyle = theme.fontStyle;
    if(fontSize==0)
    {
        fontSize=13;
    }
    if(stringBackgroud.length<=7)
    {
            stringBackgroud=@"LightPattern";
    }
    if (!theme.backgroundImage)
    {
        [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
    }
    else
        imageBg.image = theme.backgroundImage;
    
}
-(void)SetDefaultValues
{
    stringBackgroud=@"LightPattern";
    fontSize=12;
    //fontStyle=@"gurbaniwebthick";
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
    if ([_titleName isEqualToString:@"SGGS Chapter index"] || [_titleName isEqualToString:@"Sri Dasam Granth Index"] || [_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    [self getpageID];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    BOOL retValue = NO;
    retValue = [super canPerformAction:action withSender:sender];
    return retValue;
}


#pragma Get Chapters
-(void)getData
{
    [database open];
    
    NSString *querry;
    if ([_titleName isEqualToString:@"SGGS Chapter index"])
    {
        querry=@"SELECT chapterID,chapterE FROM tbl_chapters WHERE pageID='0' AND scriptID = '1'";
    }
    else if ([_titleName isEqualToString:@"Bhai Gurdas Vaar index"])
    {
        querry = @"SELECT chapterID, chapterE FROM tbl_chapters WHERE scriptID = '2' AND pageID = '0' AND parentID != '0'";
    }
    else if ([_titleName isEqualToString:@"Sri Dasam Granth Index"])
    {
        querry=@"SELECT chapterID,chapterE FROM tbl_chapters WHERE scriptID = '4' AND ParentID = '1366'";
    }
    else if ([_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    {
        querry=@"SELECT chapterID,chapterE FROM tbl_chapters WHERE pageID='0' AND scriptID = '5'";
    }
    else if([_titleName isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [_titleName isEqualToString:@"SGGS Shabd Index Roman"] || [_titleName isEqualToString:@"SGGS Shabd Index Devanagri(Hindi)"])
    {
        querry = @"SELECT * FROM tbl_shabd WHERE raagID = '0'";
    }
    
    //NSLog(@"%@",querry);
    FMResultSet *results = [database executeQuery:querry];
    [arrayItems removeAllObjects];
    [arrayPages removeAllObjects];
    if([_titleName isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [_titleName isEqualToString:@"SGGS Shabd Index Roman"] || [_titleName isEqualToString:@"SGGS Shabd Index Devanagri(Hindi)"])
    {
        while([results next])
        {
            
            ISGDataModel *dataModel=[[ISGDataModel alloc]initWithShabdIndex:results];
            [arrayItems addObject:dataModel];
            
        }
    }
    else
    while([results next])
    {
        
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithChapters:results];
        [arrayItems addObject:dataModel];
        
    }
    [database close];
    [tableList reloadData];
}

#pragma mark getPageIDS
-(void)getpageID
{
    [database open];
    NSString *querry;
    
    if ([_titleName isEqualToString:@"SGGS Chapter index"])
    {
         querry=[NSString stringWithFormat:@"SELECT pageID,chapterE,lineID,endID FROM tbl_chapters WHERE  pageID != '0' AND scriptID = '1'"];
    }
    else if ([_titleName isEqualToString:@"Sri Dasam Granth Index"])
    {
        querry=[NSString stringWithFormat:@"SELECT pageID,chapterE,lineID,endID FROM tbl_chapters WHERE  pageID != '0' AND scriptID = '4'"];
    }
    else if ([_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    {
        querry=[NSString stringWithFormat:@"SELECT pageID,chapterE,lineID,endID FROM tbl_chapters WHERE  pageID != '0' AND scriptID = '5'"];
    }
    
   
    //NSLog(@"%@",querry);
    FMResultSet *results = [database executeQuery:querry];
    [arrayTotalPage removeAllObjects];
    [arrayPageID removeAllObjects];
    while([results next])
    {
        
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithSubChapters:results];
        [arrayTotalPage addObject:dataModel];
        
    }
    
    [arrayTotalPage enumerateObjectsUsingBlock:^(ISGDataModel *pages, NSUInteger idx, BOOL *stop)
     {
         [arrayPageID addObject:pages.page];
     }];
//    if ([_titleName isEqualToString:@"Bhai Nand Lal Baani"])
//    {
//        [arrayPageID addObject:@"144"];
//    }
    
    //NSLog(@"data count %d",arraySubChapterItems.count);
    [database close];
}

#pragma Get Sub Chapters
-(void)getSubChaptersFrom:(int)from to:(int)to
{
    [database open];
    NSString *querry;
    
    if([_titleName isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [_titleName isEqualToString:@"SGGS Shabd Index Roman"] || [_titleName isEqualToString:@"SGGS Shabd Index Devanagri(Hindi)"])
    {
        querry = [NSString stringWithFormat:@"SELECT * FROM tbl_shabd WHERE ID BETWEEN %d AND %d", from+1, to-1];
    }
    else if ([_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"])
    {
        querry = [NSString stringWithFormat:@"SELECT * FROM tbl_shabd WHERE raagID != '0' AND shabdE LIKE '%@' ORDER BY shabdE",_alphabet];
    }
    else if ([_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    {
        querry=[NSString stringWithFormat:@"SELECT pageID,chapterE,lineID,endID FROM tbl_chapters WHERE scriptID = '5' AND pageID != '0'"];
    }
    else
        querry=[NSString stringWithFormat:@"SELECT pageID,chapterE,lineID,endID FROM tbl_chapters WHERE  chapterID between %d  AND %d",from+1, to-1];
    //NSLog(@"%@",querry);
    FMResultSet *results = [database executeQuery:querry];
    [arraySubChapterItems removeAllObjects];
    [arrayPages removeAllObjects];
    [arrayDetails removeAllObjects];
    [arrayTitles removeAllObjects];
    if([_titleName isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [_titleName isEqualToString:@"SGGS Shabd Index Roman"] || [_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"] || [_titleName isEqualToString:@"SGGS Shabd Index Devanagri(Hindi)"])
    while([results next])
    {
            
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithShabdIndex:results];
        [arraySubChapterItems addObject:dataModel];
            
    }
    else
    while([results next])
    {
        
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithSubChapters:results];
        [arraySubChapterItems addObject:dataModel];
        
    }
    if ([_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"] && arraySubChapterItems.count == 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"No data Available" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
    }
    
    [arraySubChapterItems enumerateObjectsUsingBlock:^(ISGDataModel *pages, NSUInteger idx, BOOL *stop)
    {
        [arrayPages addObject:pages.page];
        NSDictionary * dict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:pages.page,pages.startingline,pages.endingLine, nil] forKeys:[NSArray arrayWithObjects:@"page",@"firstLine",@"lastLine", nil]];
        [arrayDetails addObject:dict];
         
        if ([_titleName isEqualToString:@"Bhai Gurdas Vaar index"])
        {
            [arrayTitles addObject:[NSString stringWithFormat:@"%@\n(Pauri %d of %@)",pages.english,idx+1,stringTitle]];
        }
        else if ([_titleName isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [_titleName isEqualToString:@"SGGS Shabd Index Roman"] || [_titleName isEqualToString:@"SGGS Shabd Index Devanagri(Hindi)"] || [_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"])
        {
            [arrayTitles addObject:[NSString stringWithFormat:@"This %@ shabd by %@ in %@ on Ang %@", pages.genreID,pages.authorID,pages.raagID,pages.page]];
        }
    }];
    
    //NSLog(@"data count %d",arraySubChapterItems.count);
    [database close];
}

#pragma mark Create TableCells
- (void) createTableCell:(int )index
{
    
    
    ISGDataModel *fromData=[arrayItems objectAtIndex:index];
    ISGDataModel *toData=[arrayItems objectAtIndex:index+1];
    
    

    if (([toData.dataID intValue]- [fromData.dataID intValue]) > 1)
    {
        [arrayIndexPath removeAllObjects];
        [arraySubChapterItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:select];
             [arrayIndexPath addObject:indexPath];
             if (idx == arraySubChapterItems.count - 1)
             {
                 [tableList insertRowsAtIndexPaths:arrayIndexPath withRowAnimation:UITableViewRowAnimationAutomatic];
             }
         }];
    
    }
    [tableList reloadData];
}


#pragma mark Remove TableCells
- (void) removeCells
{
    [arrayIndexPath removeAllObjects];
    [arraySubChapterItems removeAllObjects];
//    [arraySubChapterItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
//     {
//         NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:select];
//         [arrayIndexPath addObject:indexPath];
//         if (idx == arraySubChapterItems.count - 1)
//         {
//            [arraySubChapterItems removeAllObjects];
//             [tableList deleteRowsAtIndexPaths:arrayIndexPath withRowAnimation:nil];
//         }
//     }];
    
    isSubCategory = NO;
    
    
    
}

#pragma mark TableView DataSource 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (![_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"] && ![_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    {
        return arrayItems.count;
    }
    else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == select)
    {
        return arraySubChapterItems.count;
    }
    else
        return 0;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (![_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"] && ![_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    {
        ISGDataModel *data=[arrayItems objectAtIndex:section];
        return data.english;
    }
    else
        return nil;
    
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (![_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"] && ![_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(14, 0, tableView.frame.size.width-28, 40)];
        [btn setTag:section+1];
        ISGDataModel *data;
        data=[arrayItems objectAtIndex:section];
        NSString * string;
        //NSLog(@"Page Id's %@",data.dataID);
        
        
        if ([_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"])
        {
            string = @"";
        }
        if([_titleName isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [_titleName isEqualToString:@"SGGS Shabd Index Devanagri(Hindi)"])
        {
            string = data.shabdG;
        }
        else if ([_titleName isEqualToString:@"SGGS Shabd Index Roman"])
        {
            string = data.shabdE;
        }
        else
            string =data.english;
        btn.titleLabel.font=[UIFont boldSystemFontOfSize:fontSize];
        btn.layer.borderWidth = 0.25f;
        
        if([stringBackgroud isEqualToString:@"LightPattern"]||[stringBackgroud isEqualToString:@"WhitePattern"])
        {
            [btn setTitleColor:[UIColor colorWithRed:60.0/255.0 green:66.0/255.0 blue:65.0/255.0 alpha:1] forState:UIControlStateNormal];
        }
        else
        {
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        
        //btn.titleLabel.font = [UIFont systemFontOfSize:18];
        if([_titleName isEqualToString:@"SGGS Shabd Index Gurmukhi"])
        {
            btn.titleLabel.font = [UIFont fontWithName:fontStyle size:18];
        }
        else if ([_titleName isEqualToString:@"SGGS Shabd Index Devanagri(Hindi)"])
        {
            btn.titleLabel.font = [UIFont fontWithName:@"Gurbanihindi" size:18];
        }
        else
            btn.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        btn.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        [btn setTitle:string forState:UIControlStateNormal];
        [btn.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [btn addTarget:self action:@selector(sectionTapped:) forControlEvents:UIControlEventTouchDown];
        [view addSubview:btn];
        
        btn.titleLabel.numberOfLines = 2;
        btn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        
        [view setBackgroundColor:[UIColor clearColor]];
        return view;
 
    }
    else
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (![_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"] && ![_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    {
        return 40.0f;
    }
    else
        return 0;
}

#pragma mark Action For Header
- (void)sectionTapped:(UIButton*)btn
{
    //NSLog(@"Section index = %ld",(long)btn.tag);
    
    if (![_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"] && ![_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    {
        stringTitle = btn.titleLabel.text;
        if ((isSubCategory && btn.tag-1 == select) )
        {
            isSubCategory = NO;
            [self removeCells];
        }
        else
        {
            isSubCategory = NO;
            [self removeCells];
            
            int from, to;
            
            ISGDataModel *fromData=[arrayItems objectAtIndex:btn.tag-1];
            
            from = [fromData.dataID intValue];
            if ([_titleName isEqualToString:@"SGGS Chapter index"] && btn.tag == arrayItems.count)
            {
                to = 412;
            }
            else if ([_titleName isEqualToString:@"Sri Dasam Granth Index"] && btn.tag == arrayItems.count)
            {
                to = 1864;
            }
            else if ([_titleName isEqualToString:@"Bhai Gurdas Vaar index"] && btn.tag == arrayItems.count)
            {
                to = 1364;
            }
            else if ([_titleName isEqualToString:@"Bhai Nand Lal Baani"] && btn.tag == arrayItems.count)
            {
                to = 1870;
            }
            else if (([_titleName isEqualToString:@"SGGS Shabd Index Gurmukhi"]||[_titleName isEqualToString:@"SGGS Shabd Index Roman"]||[_titleName isEqualToString:@"SGGS Shabd Index Devanagri(Hindi)"]) && btn.tag == arrayItems.count)
            {
                to = 3013;
            }
            else
            {
                ISGDataModel *toData=[arrayItems objectAtIndex:btn.tag];
                to = [toData.dataID intValue];
            }
            
            select = btn.tag-1;
            if (to - from > 1)
            {
                isSubCategory = YES;
                [self getSubChaptersFrom:from to:to];
                //[self createTableCell:select];
                [tableList reloadData];
            }
            else if ([_titleName isEqualToString:@"Sri Dasam Granth Index"])
            {
                [self showDesamGranthIndex:from];
                return;
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"No subchapters available" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                
                [self removeCells];
                isSubCategory = NO;
            }
            
        }

    }
    else
    {
        select = btn.tag-1;
        [self getSubChaptersFrom:0 to:1];
    }
    
    
    [tableList reloadData];
}

-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.font=[UIFont boldSystemFontOfSize:fontSize];
    if([stringBackgroud isEqualToString:@"LightPattern"]||[stringBackgroud isEqualToString:@"WhitePattern"])
    {
        cell.textLabel.textColor = [UIColor colorWithRed:124.0/255.0 green:7.0/255.0 blue:65.0/255.0 alpha:1];
    }
    else
    {
        cell.textLabel.textColor = [UIColor colorWithRed:51.0/255 green:0 blue:64.0/255 alpha:1];
    }
    
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.numberOfLines=4;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    ISGDataModel *data=[arraySubChapterItems objectAtIndex:indexPath.row];
    if([_titleName isEqualToString:@"SGGS Shabd Index Gurmukhi"])
    {
        cell.textLabel.text=data.shabdG;
        cell.textLabel.font = [UIFont fontWithName:fontStyle size:fontSize];
    }
    else if ([_titleName isEqualToString:@"SGGS Shabd Index Devanagri(Hindi)"])
    {
        cell.textLabel.text=data.shabdG;
        cell.textLabel.font = [UIFont fontWithName:@"Gurbanihindi" size:fontSize];
    }
    else if ([_titleName isEqualToString:@"SGGS Shabd Index Roman"] || [_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"])
    {
        cell.textLabel.text=data.shabdE;
    }
    else
        cell.textLabel.text=data.english;
    cell.textLabel.userInteractionEnabled = YES;
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

#pragma mark TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{   dispatch_async(dispatch_get_main_queue(), ^{

    ISGDataModel *data=[arraySubChapterItems objectAtIndex:indexPath.row];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];

    ISGReadSGGSViewController * readSGGS  = [storyboard instantiateViewControllerWithIdentifier:@"ISGReadSGGSViewController"];
    //ISGReadSGGSViewController * readSGGS = [[ISGReadSGGSViewController alloc] initWithNibName:@"ISGReadSGGSViewController" bundle:nil];
//    if ([_titleName isEqualToString:@"SGGS Chapter index"] || [_titleName isEqualToString:@"Sri Dasam Granth Index"] || [_titleName isEqualToString:@"Bhai Nand Lal Baani"])
//    {
//        int currentPosition = [arrayPageID indexOfObject:data.page];
//        readSGGS.firstPage = [arrayPageID[currentPosition] intValue];
//        readSGGS.lastPage = [arrayPageID[currentPosition+1] intValue];
//    }
    if ([_titleName isEqualToString:@"SGGS Chapter index"])
    {
        int currentPosition = [arrayPageID indexOfObject:data.page];
        readSGGS.firstPage = [arrayPageID[currentPosition] intValue];
        readSGGS.lastPage = 1429;
    }
    else if ([_titleName isEqualToString:@"Sri Dasam Granth Index"])
    {
        int currentPosition = [arrayPageID indexOfObject:data.page];
        readSGGS.firstPage = [arrayPageID[currentPosition] intValue];
        readSGGS.lastPage = 2759;
    }
    else if ([_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    {
        int currentPosition = [arrayPageID indexOfObject:data.page];
        readSGGS.firstPage = [arrayPageID[currentPosition] intValue];
        readSGGS.lastPage = 144;
        readSGGS.titleName = _titleName;
    }
    else
    {
        int currentPosition = [arrayPages indexOfObject:data.page];
        if (currentPosition == arrayPages.count-1)
        {
            readSGGS.firstPage = [arrayPages[currentPosition] intValue];
            readSGGS.lastPage = [arrayPages[currentPosition] intValue];
        }
        else
        {
            readSGGS.firstPage = [arrayPages[currentPosition] intValue];
            readSGGS.lastPage = [arrayPages[currentPosition+1] intValue];
        }
    }
    
    readSGGS.page = [data.page intValue];
    readSGGS.arrayPages = arrayPages;
    readSGGS.arrayDetails = arrayDetails;
    readSGGS.dict = arrayDetails[indexPath.row];
    if ([_titleName isEqualToString:@"SGGS Chapter index"])
    {
        readSGGS.stringCategory = @"SGGS Chapter Index";
    }
    else if ([_titleName isEqualToString:@"Sri Dasam Granth Index"])
    {
        readSGGS.stringCategory = @"Sri Dasam Granth Index";
    }
    else if ([_titleName isEqualToString:@"Bhai Gurdas Vaar index"])
    {
        readSGGS.stringCategory = @"Bhai Gurdas Vaar index";
        readSGGS.page = [data.page intValue];
        readSGGS.titleName = arrayTitles[indexPath.row];
        readSGGS.arrayTitles = arrayTitles;
    }
    else if ([_titleName isEqualToString:@"Bhai Nand Lal Baani"])
    {
        readSGGS.stringCategory = @"Bhai Nand Lal Baani";
        readSGGS.titleName = @"Bhai Nand Lal Baani";
    }
    
    else if ([_titleName isEqualToString:@"SGGS Shabd Index Gurmukhi"]||[_titleName isEqualToString:@"SGGS Shabd Index Roman"] || [_titleName isEqualToString:@"SGGS Shabd Index Devanagri(Hindi)"] || [_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"])
    {
        readSGGS.titleName = arrayTitles[indexPath.row];
        readSGGS.arrayTitles = arrayTitles;
        readSGGS.shabdIndex = indexPath.row;
        if([_titleName isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [_titleName isEqualToString:@"SGGS Shabd Index Devanagri(Hindi)"])
        {
            readSGGS.stringCategory = @"SGGS Shabd Index Gurmukhi";
        }
        else if([_titleName isEqualToString:@"SGGS Shabd Index Roman"])
        {
            readSGGS.stringCategory = @"SGGS Shabd Index Roman";
        }
        else
        {
            readSGGS.stringCategory = @"SGGS Shabd Index Alphabetical";
        }
    }
//    else if ([_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"])
//    {
//        readSGGS.titleName = @"SGGS Shabd Index Alphabetical";
//        readSGGS.stringCategory = @"SGGS Shabd Index Alphabetical";
//        readSGGS.shabdIndex = indexPath.row;
//    }
    
    [self.navigationController pushViewController:readSGGS animated:YES];
     });
}

- (void)showDesamGranthIndex:(int)idx
{
    [database open];
    NSString * querry=[NSString stringWithFormat:@"SELECT pageID,chapterE,lineID,endID FROM tbl_chapters WHERE  chapterID = %d",idx];
    
    //NSLog(@"%@",querry);
    [arraySubChapterItems removeAllObjects];
    [arrayPages removeAllObjects];
    [arrayDetails removeAllObjects];
    FMResultSet *results = [database executeQuery:querry];
    while([results next])
    {
        ISGDataModel * dataModel =[[ISGDataModel alloc]initWithSubChapters:results];
        [arraySubChapterItems addObject:dataModel];
    }
    if ([_titleName isEqualToString:@"SGGS Shabd Index Alphabetical"] && arraySubChapterItems.count == 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"No data Available" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
    }
    
    [arraySubChapterItems enumerateObjectsUsingBlock:^(ISGDataModel *pages, NSUInteger idx, BOOL *stop)
     {
         [arrayPages addObject:pages.page];
         NSDictionary * dict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:pages.page,pages.startingline,pages.endingLine, nil] forKeys:[NSArray arrayWithObjects:@"page",@"firstLine",@"lastLine", nil]];
         [arrayDetails addObject:dict];
     }];
    
    [database close];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        
        ISGReadSGGSViewController * readSGGS  = [storyboard instantiateViewControllerWithIdentifier:@"ISGReadSGGSViewController"];
   // ISGReadSGGSViewController * readSGGS = [[ISGReadSGGSViewController alloc] initWithNibName:@"ISGReadSGGSViewController" bundle:nil];
    ISGDataModel * data = (ISGDataModel *)arraySubChapterItems[0];
    int currentPosition = [arrayPageID indexOfObject:data.page];
    readSGGS.firstPage = [arrayPageID[currentPosition] intValue];
    readSGGS.lastPage = 2759;
    
    readSGGS.page = [data.page intValue];
    readSGGS.arrayPages = arrayPages;
    readSGGS.arrayDetails = arrayDetails;
    readSGGS.dict = arrayDetails[0];
    readSGGS.stringCategory = @"Sri Dasam Granth Index";
    //readSGGS.titleName = @"Sri Dasam Granth Index";
    [self.navigationController pushViewController:readSGGS animated:YES];
     });
}


@end
