//
//  RSHueSlider.m
//  iSearchGujral
//
//  Created by SICS on 20/01/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "RSHueSlider.h"

@implementation RSHueSlider

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRoutine];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initRoutine];
    }
    return self;
}

- (void)initRoutine
{
    self.minimumValue = 0.0;
    self.maximumValue = 1.0;
    self.continuous = YES;
    self.enabled = YES;
    self.userInteractionEnabled = YES;
    [self addTarget:self action:@selector(myValueChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)myValueChanged:(id)noti
{
    _colorPicker.hue = self.value;
}

- (void) setColorPicker:(RSColorPickerView *)cp
{
    _colorPicker = cp;
    if (!_colorPicker)
    {
        return;
    }
    self.value = _colorPicker.hue;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    NSArray *colors = [[NSArray alloc] initWithObjects:
                       (id)[UIColor colorWithWhite:0 alpha:0].CGColor,
                       (id)[UIColor colorWithWhite:1 alpha:1].CGColor,nil];
    
    CGGradientRef myGradient = CGGradientCreateWithColors(space, (__bridge CFArrayRef)colors, NULL);
    
    CGContextDrawLinearGradient(ctx, myGradient, CGPointZero, CGPointMake(rect.size.width, 0), 0);
    CGGradientRelease(myGradient);
    CGColorSpaceRelease(space);
}


@end
