//
//  ISGBookMarkListViewController.m
//  iSearchGujral
//
//  Created by SICS on 12/01/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "ISGBookMarkListViewController.h"
#import "BookMarkPages.h"
#import "ISGReadSGGSViewController.h"
#import "ISGDataModel.h"
#import "iCloud.h"

@interface ISGBookMarkListViewController ()<iCloudDelegate>
{
    IBOutlet UITableView *tableFavourites;

    NSArray * array;
    UIImageView * imageCell;
    NSMutableArray * arraySortedList;
    NSMutableArray * arrayIndex;
    NSMutableArray * arrayPages;
    NSMutableArray * arrayScript;
    CGSize screenSize;
}

@end

@implementation ISGBookMarkListViewController
@synthesize folderName;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=_titleName;
    arraySortedList = [NSMutableArray new];
    arrayIndex = [NSMutableArray new];
    arrayPages = [NSMutableArray new];
    arrayScript = [NSMutableArray new];
    [self getBookMarkList];
    screenSize = [UIScreen mainScreen].bounds.size;
    tableFavourites.separatorColor = [UIColor clearColor];
    [[iCloud sharedCloud] setDelegate:self];
    [[iCloud sharedCloud] setVerboseLogging:YES];
    [[iCloud sharedCloud] setupiCloudDocumentSyncWithUbiquityContainer:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getBookMarkList];
    //[tableFavourites reloadData];
    if (array.count == 0)
    {
        UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:@"ISearchGurbani" message:@"No favourites added yet." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [Alert show];
    }

}

-(void) getBookMarkList
{
    NSSet * set = folderName.relationshipToPages;
    array = [[NSArray alloc] initWithArray:set.allObjects];
    [arraySortedList removeAllObjects];
    [arrayIndex removeAllObjects];
    [arrayPages removeAllObjects];
    [arrayScript removeAllObjects];
    
    for (int i = (int)array.count-1; i>=0; i--)
    {
        int count = 0;
        for (BookMarkPages * page in array)
        {
            if (i == [page.index intValue])
            {
                [arraySortedList addObject:array[count]];
            }
            else
                count++;
        }
        
    }
    
    [arraySortedList enumerateObjectsUsingBlock:^(BookMarkPages * obj, NSUInteger idx, BOOL *stop)
     {
         [arrayIndex addObject:obj.index];
         [arrayPages addObject:obj.pageNo];
         [arrayScript addObject:obj.scriptID];
    }];
    
    [tableFavourites reloadData];
    
    }

#pragma mark UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [self setReorderingEnabled:(array.count>1)];
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"myCell"];
    if (cell == nil)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    }
    UIImage * image = [UIImage imageWithData:((BookMarkPages *)arraySortedList[indexPath.row]).cellImage];
    
    imageCell = [[UIImageView alloc] initWithFrame:CGRectMake(25, 22, screenSize.width-50, image.size.height)];
    imageCell.image = image;

    
    cell.frame = CGRectMake(0, 25, screenSize.width-60, image.size.height);
    
    //imageCell.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    //imageCell.layer.borderWidth = 0.25;
    //imageCell.contentMode = UIViewContentModeScaleAspectFill;
    //cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:imageCell];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    //cell.textLabel.text = ((BookMarkPages *)array[indexPath.row]).pageNo;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //return imageCell.image.size.height;
    if (arraySortedList.count>0)
    {
        UIImage * image = [UIImage imageWithData:((BookMarkPages *)arraySortedList[indexPath.row]).cellImage];
        if (image.size.height > 40)
        {
            return image.size.height;
        }
        else {
            return 60;
        }
        
    }
    else
        return 60;
    
}

-(UITableViewCell *)cellIdenticalToCellAtIndexPath:(NSIndexPath *)indexPath forDragTableViewController:(ATSDragToReorderTableViewController *)dragTableViewController
{
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    UIImage * image = [UIImage imageWithData:((BookMarkPages *)arraySortedList[indexPath.row]).cellImage];
    
    imageCell = [[UIImageView alloc] initWithFrame:CGRectMake(25, 22, screenSize.width-50, image.size.height)];
    imageCell.image = image;
    cell.frame = CGRectMake(0, 25, screenSize.width-60, image.size.height);

    //imageCell.contentMode = UIViewContentModeScaleAspectFit;
    [cell.contentView addSubview:imageCell];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //cell.textLabel.text = ((BookMarkPages *)array[indexPath.row]).pageNo;
    return cell;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    BookMarkPages * page = arraySortedList[sourceIndexPath.row];
    [arraySortedList removeObjectAtIndex:sourceIndexPath.row];
    [arraySortedList insertObject:page atIndex:destinationIndexPath.row];
    [BookMarkPages updateBookMarkIndex:arrayIndex inBookMarkPages:arraySortedList inBookMarkFolder:folderName];
    [self getBookMarkList];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BookMarkPages * data = arraySortedList[indexPath.row];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    
    ISGReadSGGSViewController * readSGGS  = [storyboard instantiateViewControllerWithIdentifier:@"ISGReadSGGSViewController"];
    //ISGReadSGGSViewController * readSGGS = [[ISGReadSGGSViewController alloc] initWithNibName:@"ISGReadSGGSViewController" bundle:nil];
    readSGGS.page = [data.pageNo intValue];
    readSGGS.arrayPages = arrayPages;
    readSGGS.arrayScript = arrayScript;
    readSGGS.scriptID = data.scriptID;
    readSGGS.stringCategory = @"BookMarks";
    readSGGS.stringQuery = data.query;
    readSGGS.titleName = data.title;
    BOOL cloudIsAvailable = [[iCloud sharedCloud] checkCloudAvailability];
    if (cloudIsAvailable) {
        BOOL fileExists = [[iCloud sharedCloud] doesFileExistInCloud:@"isearch_Fav.csv"];
        if (fileExists == YES) {
          [self getBookMarkList];
        }
       
    }
    
    [self.navigationController pushViewController:readSGGS animated:YES];

}
-(void) getFileDataFromiCloud
{
    [[iCloud sharedCloud] retrieveCloudDocumentWithName:@"isearch_Fav.csv" completion:^(UIDocument *cloudDocument, NSData *documentData, NSError *error) {
        if (!error) {
            NSString *fileName = [cloudDocument.fileURL lastPathComponent];
            NSData *fileData = documentData;
        }
    }];
}
-(void) deleteFileDataFromiCloud
{
    [[iCloud sharedCloud] deleteDocumentWithName:@"isearch_Fav.csv" completion:^(NSError *error) {
        // Completion handler could be used to update your UI and tell the user that the document was deleted
    }];
}
-(void) saveFileDataToiCloud
{
    [[iCloud sharedCloud] saveAndCloseDocumentWithName:@"isearch_Fav.csv" withContent:[NSData data] completion:^(UIDocument *cloudDocument, NSData *documentData, NSError *error) {
        if (error == nil) {
            // Code here to use the UIDocument or NSData objects which have been passed with the completion handler
        }
        else{
            [[iCloud sharedCloud] uploadLocalOfflineDocumentsWithRepeatingHandler:^(NSString *fileName, NSError *error) {
                if (error == nil) {
                    // This code block is called repeatedly until all files have been uploaded (or an upload has at least been attempted). Code here to use the NSString (the name of the uploaded file) which have been passed with the repeating handler
                }
            } completion:^{
                // Completion handler could be used to tell the user that the upload has completed
            }];
        }
    }];
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    BookMarkPages * data = arraySortedList[indexPath.row];
//    [BookMarkPages deleteBookMarkPage:data inBookMarkFolder:folderName];
    [self arrangeIndex:(int)indexPath.row];
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//    [self getBookMarkList];
}

-(void) arrangeIndex:(int)index
{
    NSMutableArray * sortedIndex = [NSMutableArray new];
    NSMutableArray * sortedArray = [NSMutableArray new];
    [sortedIndex removeAllObjects];
    [sortedArray removeAllObjects];
    for (int i=(int)arrayIndex.count - 2; i>=0; i--)
    {
        [sortedIndex addObject:[NSString stringWithFormat:@"%d",i]];
        
    }
    for (int j=0; j<arraySortedList.count; j++)
    {
        if (j!=index)
        {
            [sortedArray addObject:arraySortedList[j]];
        }
    }
    [BookMarkPages updateBookMarkIndex:sortedIndex inBookMarkPages:sortedArray inBookMarkFolder:folderName];
    [self getBookMarkList];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
