//
//  ISGReadSGGSViewController.h
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"
#import "ISGAppDelegate.h"
#import "Favourites.h"
#import "SVProgressHUD.h"
#import "ISGBaniDetailViewController.h"
#define CELL_CONTENT_WIDTH 320.0f
#define CELL_CONTENT_MARGIN 10.0f
@interface ISGReadSGGSViewController : UIViewController<UITableViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UITableViewDataSource>
{
    FMDatabase *database;
    NSMutableArray *arrayItems;
    NSMutableArray *heightsCell;

    IBOutlet UITableView *table;
    int page;
    IBOutlet UIButton *buttonPrev,*buttonNext,*buttonTop;
    UIButton *labelPage;
    UITextField *textAng;
    IBOutlet UIScrollView *customScroll;
    UILongPressGestureRecognizer *lpHandler;
    UITapGestureRecognizer * tapGesture;
    UITextField *txtAlert;
    NSString *sggsData;
    int shabd,dataID;
    NSString *strShabd,*strId,*strPage;
    IBOutlet UIImageView *imageBg,*imageBg2;
    NSString *stringBackgroud;
    NSString *fontStyle,*message;
    NSString *language1;
    NSString *language2,*language3,*language4,*language5,*language6;
    NSString *script;
    int size1,gurmukhiSize,romanSize,hindiSize,englishSize,teekaSize,attributeSize;
    UIColor *gurmukhiColor,*romanColor,*hindiColor,*englishColor,*teekaColor,*attributeColor;
    NSMutableArray *arrayLanguages;
    int languageFlag,previousPage,totalSize,lineCount;
    UITableViewCell *cell;
    IBOutlet UIImageView *cellImage;
    UIImage *attachmentImage;
    float lastHeight;
    IBOutlet UIView *popView;
    
    IBOutlet UIGestureRecognizer * leftGesture, *rightGesture;
   
}
@property (nonatomic) int page;
@property (nonatomic) int firstPage;
@property (nonatomic) int lastPage;
@property (nonatomic) int shabdIndex;
@property (nonatomic, strong) NSString * titleName;
@property (nonatomic, strong) NSArray * arrayTitles;
@property (nonatomic, strong) NSDictionary * dict;
@property (nonatomic, strong) NSArray * arrayDetails;
@property (nonatomic, strong) NSString * scriptID;
@property (nonatomic, strong) NSMutableArray * arrayPages;
@property (nonatomic, strong) NSArray * arrayScript;
@property (nonatomic,strong) NSString *strFavour;
@property (nonatomic, strong) NSString * stringCategory;
@property (nonatomic, strong) NSString * stringQuery;
-(IBAction)btnNext:(id)sender;
-(IBAction)btnPrev:(id)sender;
-(IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender;
-(IBAction)swipeRight:(UISwipeGestureRecognizer *)sender;
-(IBAction)btnOpenAng:(id)sender;
-(IBAction)btnProceed:(id)sender;
@end
