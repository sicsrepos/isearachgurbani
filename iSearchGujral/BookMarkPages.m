//
//  BookMarkPages.m
//  iSearchGujral
//
//  Created by SICS on 18/09/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "BookMarkPages.h"
#import "ISGAppDelegate.h"
#define entity @"BookMarkPages"

@implementation BookMarkPages

@dynamic cellImage;
@dynamic index;
@dynamic pageNo;
@dynamic scriptID;
@dynamic query;
@dynamic title;

+(void) addBookMarkPage:(BookMarkPages *)bookMarkPage inBookMarkFolder:(BookMarkFolders *)bookMarkFolder
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    BookMarkPages * bookPage = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:managedObjectContext];
    bookPage.pageNo = bookMarkPage.pageNo;
    bookPage.scriptID = bookMarkPage.scriptID;
    bookPage.index = bookMarkPage.index;
    bookPage.cellImage = bookMarkPage.cellImage;
    bookPage.query = bookMarkPage.query;
    bookPage.title = bookMarkPage.title;
    [bookMarkFolder addRelationshipToPagesObject:bookPage];
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
    
    
}

+(void) deleteBookMarkPage:(BookMarkPages *)bookMarkPage inBookMarkFolder:(BookMarkFolders *)bookMarkFolder
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF==%@",bookMarkPage];
    [fetchRequest setPredicate:predicate];
    
    NSArray * array = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [array enumerateObjectsUsingBlock:^(BookMarkPages * obj, NSUInteger idx, BOOL *stop)
     {
         [bookMarkFolder removeRelationshipToPagesObject:obj];
         [managedObjectContext deleteObject:obj];
     }];
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
}

+(void) deleteAllBookMarkPageinBookMarkFolder:(BookMarkFolders *)bookMarkFolder
{
    NSError * error = nil;
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    NSArray * array = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [array enumerateObjectsUsingBlock:^(BookMarkPages * obj, NSUInteger idx, BOOL *stop)
     {
         [bookMarkFolder removeRelationshipToPagesObject:obj];
         [managedObjectContext deleteObject:obj];
     }];
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
}


+(void) updateBookMarkIndex:(NSArray *)arrayIndex inBookMarkPages:(NSArray *)arrayBookMarkPage inBookMarkFolder:(BookMarkFolders *)bookMarkFolder
{
    NSError * error = nil;
    NSMutableArray * bookPage = [NSMutableArray new];
    NSMutableArray * bookMarkImage = [NSMutableArray new];
    NSMutableArray * bookMarkScript = [NSMutableArray new];
    NSMutableArray * bookMarkQuery = [NSMutableArray new];
    NSMutableArray * bookMarktitle = [NSMutableArray new];
    
    [bookPage removeAllObjects];
    [bookMarkImage removeAllObjects];
    [bookMarkScript removeAllObjects];
    [bookMarkQuery removeAllObjects];
    [bookMarktitle removeAllObjects];
    
    [arrayBookMarkPage enumerateObjectsUsingBlock:^(BookMarkPages * obj, NSUInteger idx, BOOL *stop)
     {
         [bookPage addObject:obj.pageNo];
         [bookMarkScript addObject:obj.scriptID];
         [bookMarkImage addObject:obj.cellImage];
         [bookMarkQuery addObject:obj.query];
         [bookMarktitle addObject:obj.title];
     }];
    [self deleteAllBookMarkPageinBookMarkFolder:bookMarkFolder];
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    
    for (int i = 0; i < arrayIndex.count; i++)
    {
        BookMarkPages * bookMarkPage = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:managedObjectContext];
        bookMarkPage.scriptID = bookMarkScript[i];
        bookMarkPage.pageNo = bookPage[i];
        bookMarkPage.cellImage = bookMarkImage[i];
        bookMarkPage.index = arrayIndex[i];
        bookMarkPage.query = bookMarkQuery[i];
        bookMarkPage.title = bookMarktitle[i];
        [bookMarkFolder addRelationshipToPagesObject:bookMarkPage];
    }
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"%@",[error localizedDescription]);
    }
    
}
-(NSString *)dataFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"iSearchFav.csv"];
}
- (void)saveAsFileAction:(BookMarkPages *)bookMarkPage inBookMarkFolder:(BookMarkFolders *)bookMarkFolder {
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self dataFilePath]]) {
        [[NSFileManager defaultManager] createFileAtPath: [self dataFilePath] contents:nil attributes:nil];
        NSLog(@"Route creato");
    }
    
    NSMutableString *writeString = [NSMutableString stringWithCapacity:0]; //don't worry about the capacity, it will expand as necessary
    writeString = [NSMutableString string];
    
    
    NSString* newStr = [[NSString alloc] initWithData:bookMarkPage.cellImage encoding:NSUTF8StringEncoding];
    writeString = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@, \n",bookMarkPage.pageNo,bookMarkPage.pageNo,bookMarkPage.index,bookMarkPage.query, bookMarkPage.title,newStr]];
    //Moved this stuff out of the loop so that you write the complete string once and only once.
    NSLog(@"writeString :%@",writeString);
    
    NSFileHandle *handle;
    handle = [NSFileHandle fileHandleForWritingAtPath: [self dataFilePath] ];
    //say to handle where's the file fo write
    [handle truncateFileAtOffset:[handle seekToEndOfFile]];
    //position handle cursor to the end of file
    [handle writeData:[writeString dataUsingEncoding:NSUTF8StringEncoding]];
}

@end
