//
//  ISGSubChaptersViewController.m
//  iSearchGujral
//
//  Created by SICS on 11/12/14.
//  Copyright (c) 2014 sics. All rights reserved.
//

#import "ISGSubChaptersViewController.h"
#import "ISGTheme.h"
#import "ISGDataModel.h"
#import <Twitter/Twitter.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "ISGAppDelegate.h"
#import "MBProgressHUD.h"
#import "ISGCell.h"
#import "HMSideMenu.h"
#import "KGModal.h"
#import "ISGListExploreViewController.h"

@interface ISGSubChaptersViewController ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation ISGSubChaptersViewController
{
    NSArray * arrayAlphabets;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    arrayAlphabets = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K",@"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V",@"W", @"X", @"Y", @"Z"];
    [tableSubChapters setBackgroundColor:[UIColor clearColor]];
    
    //database = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).database;
    
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self setTheme];
    self.title=_titleName;
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    NSLog(@"screen height %f",screenHeight);
    self.title = @"SGGS Shabd Index Alphabetical";
   /* if ((screenHeight > 850) && (screenHeight < 1000)) {
        tableSubChapters.frame = CGRectMake(60, 60 ,240,400);
    }
    
    if(screenHeight==1024.000000)
    {
        tableSubChapters.frame = CGRectMake(44, 44, 240,screenHeight-500);
    }
    if(screenHeight==1024.000000)
    {
        tableSubChapters.frame = CGRectMake(44, 50, 240,screenHeight-500);
    }
    tableSubChapters.center = self.view.center;
*/
    
    arrayItems =[[NSMutableArray alloc]init];
    //[self getData];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"refresh" object:Nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)refresh:(NSNotification *)notification
{
    [self setTheme];
    [tableSubChapters reloadData];
}

-(void)setTheme
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    ISGTheme *theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    stringBackgroud=theme.background;
    fontSize=[theme.fontSize intValue];
    if(fontSize==0)
    {
        fontSize=13;
    }
    if(stringBackgroud.length<=7)
    {
        stringBackgroud=@"LightPattern";
    }
    [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
}
-(void)SetDefaultValues
{
    stringBackgroud=@"LightPattern";
    fontSize=12;
    //fontStyle=@"gurbaniwebthick";
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    BOOL retValue = NO;
    retValue = [super canPerformAction:action withSender:sender];
    return retValue;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayAlphabets.count;
}
-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.font=[UIFont boldSystemFontOfSize:fontSize];
    if([stringBackgroud isEqualToString:@"LightPattern"]||[stringBackgroud isEqualToString:@"WhitePattern"])
    {
        [cell.textLabel setTextColor:[UIColor blackColor]];
    }
    else
    {
        [cell.textLabel setTextColor:[UIColor whiteColor]];
    }
    
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.numberOfLines=2;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.text=arrayAlphabets[indexPath.row];
    cell.textLabel.userInteractionEnabled = YES;
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    
    ISGListExploreViewController * listVC  = [storyboard instantiateViewControllerWithIdentifier:@"ISGListExploreViewController"];
    //ISGListExploreViewController * listVC =[[ISGListExploreViewController alloc]initWithNibName:@"ISGListExploreViewController" bundle:Nil];
    listVC.titleName = @"SGGS Shabd Index Alphabetical";
    listVC.alphabet = [(NSString *)arrayAlphabets[indexPath.row] stringByAppendingString:@"%"];;
    [self.navigationController pushViewController:listVC animated:YES];
}

//-(void)getData
//{
//    [database open];
//    
//    NSString *querry=[NSString stringWithFormat:@"SELECT chapterE FROM tbl_chapters WHERE  ID between %d  AND %d",_from+1, _to-1];
//    NSLog(@"%@",querry);
//    FMResultSet *results = [database executeQuery:querry];
//    [arrayItems removeAllObjects];
//    while([results next])
//    {
//        
//        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithSubChapters:results];
//        [arrayItems addObject:dataModel];
//        
//    }
//    NSLog(@"data count %d",arrayItems.count);
//    [database close];
//    [tableSubChapters reloadData];
//}

@end
