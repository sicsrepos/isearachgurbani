//
//  ISGReadSGGSViewController.m
//  iSearchGujral
//
//  Created by mac on 4/17/13.
//  Copyright (c) 2013 sics. All rights reserved.


#define kAnimationDuration 0.2f

#import "ISGReadSGGSViewController.h"
#import "ISGDataModel.h"
#import "ISGTheme.h"
#import <Twitter/Twitter.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "ISGAppDelegate.h"
#import "MBProgressHUD.h"
#import "ISGCell.h"
#import "HMSideMenu.h"
#import "KGModal.h"
#import "GzColors.h"
#import "BookMarkFolders.h"
#import "BookMarkPages.h"
#import "ISGAppDelegate.h"
#import "ScrollViewToPDF.h"
#import "UIColor+Convert.h"
#import "ISGCustomAlertViewController.h"

@interface ISGReadSGGSViewController ()<MFMailComposeViewControllerDelegate,MBProgressHUDDelegate,UIPrintInteractionControllerDelegate>
{
    ISGAppDelegate *appDelegate;
    MBProgressHUD *HUD;
    NSMutableArray *cells;
    UITableView *nextTableView, *previousTableView;
    int currentIndex;
    UITableView * tableViewList;
    NSArray * arrayFolders;
    NSMutableArray * arrayNames;
    BookMarkPages * bookMarkPage;
    CGFloat x, y;
    UIButton * buttonAng;
    int indexpath;
    NSMutableArray * arraySortedFolders;
    UIImage * imageForPrint;
    UIPrintInteractionController * printController;
    UILabel *labelTitle;
    NSString * queryExecuted;
    
}
@property (nonatomic, strong) HMSideMenu *sideMenu;
@end
@implementation ISGReadSGGSViewController
@synthesize page,strFavour,arrayPages,stringCategory,scriptID,arrayScript,lastPage,firstPage,arrayDetails,dict,titleName,arrayTitles,shabdIndex,stringQuery;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [table setBackgroundColor:[UIColor clearColor]];
    
    
    labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 480, 44)];
    labelTitle.backgroundColor = [UIColor clearColor];
    labelTitle.numberOfLines = 2;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    labelTitle.font = [UIFont boldSystemFontOfSize: 12.0f];
    labelTitle.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    labelTitle.textAlignment = NSTextAlignmentCenter;
    labelTitle.textColor = [UIColor whiteColor];
    labelTitle.text = titleName;
    if ([stringCategory isEqualToString:@"Bhai Gurdas Vaar index"] || [stringCategory isEqualToString:@"SGGS Ang by Ang"] || [stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"]||[stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"])
    {
        self.navigationItem.titleView = labelTitle;
    }
    else if ([stringCategory isEqualToString:@"SGGS Chapter Index"])
    {
        labelTitle.text = @"Sri Guru Granth Sahib";
        self.navigationItem.titleView = labelTitle;
    }
    else if ([stringCategory isEqualToString:@"Sri Dasam Granth Page by Page"])
    {
        labelTitle.text = @"Sri Dasam Granth Index";
        self.navigationItem.titleView = labelTitle;
    }
    else
    {
        self.navigationItem.titleView = labelTitle;
    }
    
    
    
    self.navigationItem.hidesBackButton = YES;
    
    NSMutableArray * arrayButton = [[NSMutableArray alloc] init];
    [arrayButton removeAllObjects];
    
    UIBarButtonItem * btnBack = [[UIBarButtonItem alloc] initWithTitle:@"<Back" style:UIBarButtonItemStyleBordered target:self action:@selector(backAction)]; //[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backButton"] style:UIBarButtonItemStyleBordered target:self action:@selector(backAction)];
    btnBack.tintColor = [UIColor whiteColor];
    [btnBack setImageInsets:UIEdgeInsetsMake(0, -20, 0, -50)];
    [arrayButton addObject:btnBack];
    
    buttonAng = [[UIButton alloc] initWithFrame:CGRectMake(0, 40, 80, 35)];
    UIImage * image = [UIImage imageNamed:@"open_ang3.png"];
    [buttonAng setTitle:@"Open Ang" forState:UIControlStateNormal];
    buttonAng.titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    //[buttonAng setTitleEdgeInsets:UIEdgeInsetsMake(0, -50, 0, 20)];
    [buttonAng setBackgroundImage:image forState:UIControlStateNormal];
    
    //[buttonAng setImageEdgeInsets:UIEdgeInsetsMake(0, 0, -50, 20)];
    //[buttonAng setContentEdgeInsets:UIEdgeInsetsMake(0, -20, 0, -20)];
    [buttonAng addTarget:self action:@selector(btnOpenAng:) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem * btnOpen = [[UIBarButtonItem alloc] initWithCustomView:buttonAng];
    
    
    labelPage = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 15)];
    [labelPage setTitle:@"Hello" forState:UIControlStateNormal];
    labelPage.titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    [labelPage setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [labelPage setTitleEdgeInsets:UIEdgeInsetsMake(0, -40, 0, -75)];
    labelPage.enabled = NO;
    UIBarButtonItem * btn = [[UIBarButtonItem alloc] initWithCustomView:labelPage];
    self.navigationItem.rightBarButtonItem = btn;
    
    arrayNames = [NSMutableArray new];
    arraySortedFolders = [NSMutableArray new];
    
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    bookMarkPage = [[BookMarkPages alloc] initWithEntity:[NSEntityDescription entityForName:@"BookMarkPages" inManagedObjectContext:managedObjectContext] insertIntoManagedObjectContext:managedObjectContext];
    
    
    
    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
         if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
        [buttonTop setBackgroundImage:[UIImage imageNamed:@"newNav.png"] forState:UIControlStateNormal];
        //[buttonAng setBackgroundImage:Nil forState:UIControlStateNormal];
        }
    else
    {
        [buttonTop setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    languageFlag=0;
    arrayLanguages=[[NSMutableArray alloc]init];
    [self setTheme];
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    CGFloat screenWidth = screenSize.width;

   // NSLog(@"screen height %f",screenHeight);
    
    database = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).database;
  /*  if ((screenHeight > 850) && (screenHeight < 1000)) {
        table.frame = CGRectMake(40, 60 ,screenWidth-80,screenHeight-150);
    }
    if(screenHeight==1024.000000)
    {
        table.frame = CGRectMake(40, 50, UIScreen.mainScreen.bounds.size.width - 80,screenHeight-480);
    }
    else if(screenHeight==568.000000)
    {
        table.frame = CGRectMake(40, 40, UIScreen.mainScreen.bounds.size.width-80,screenHeight);
    }
    else if(screenHeight==480.000000)
    {
        table.frame = CGRectMake(40, 65, screenWidth-80,screenHeight);
    }
    else if(screenHeight == 736)
    {
        table.frame = CGRectMake(60, 50, screenSize.width-120, screenHeight-150);
    }
    else if(screenHeight == 667)
    {
        table.frame = CGRectMake(35, 40, screenSize.width-70, screenHeight-100);
    }
    else if(screenHeight==1366.000)
    {
        table.frame = CGRectMake(40, 50, screenWidth-80,300);
    }
    table.center = self.view.center;*/

    x = table.frame.origin.x;
    y = table.frame.origin.y;
    
    if(![strFavour isEqualToString:@"fav"])
    {
        if ([stringCategory isEqualToString:@"SGGS Ang by Ang"])
        {
            NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
            page= (int)[userDefaults integerForKey:@"pageMarker"];
        }
        else if ([stringCategory isEqualToString:@"Sri Dasam Granth Page by Page"])
        {
            NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
            page= (int)[userDefaults integerForKey:@"pageMarker For Sri Dasam Granth Page by Page"];
        }
        else if ([stringCategory isEqualToString:@"Kabit by Kabit"])
        {
            NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
            page= (int)[userDefaults integerForKey:@"pageMarker For Kabit by Kabit"];
        }

        //Sri Dasam Granth Index
        
        imageBg.hidden=FALSE;
        imageBg2.hidden=TRUE;
       
    }
    else
    {
        imageBg.hidden=TRUE;
        imageBg2.hidden=FALSE;
       /* if(screenHeight==568.000000)
        {
            table.frame = CGRectMake(55, 65, 290,screenHeight-115);
        }
        else if(screenHeight==480.000000)
        {
            table.frame = CGRectMake(55, 65, 290,screenHeight-40);
        }
        else
        {
            table.frame = CGRectMake(75, 75, 240,screenHeight-580);
        }*/
        
        buttonTop.hidden=TRUE;
        buttonAng.hidden=TRUE;
        labelPage.hidden=TRUE;
    }
    if(page==0)page++;
    arrayItems =[[NSMutableArray alloc]init];
    
    customScroll.delegate = self;
    customScroll.maximumZoomScale = 4.0;
    customScroll.minimumZoomScale = 1.0;
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandler:)];
    tapGesture.numberOfTapsRequired = 1;
    [table addGestureRecognizer:tapGesture];
    
//    lpHandler = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressHandler:)];
//    lpHandler.minimumPressDuration = 0.5;
//    [table addGestureRecognizer:lpHandler];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"refresh" object:Nil];
    HUD=[[MBProgressHUD alloc]initWithView:self.tabBarController.view];
    [self.tabBarController.view addSubview:HUD];
    HUD.delegate=self;
    [self setSliderMenu]; 
    
    if ([stringCategory isEqualToString:@"SGGS Ang by Ang"])
    {
        buttonAng.hidden = NO;
        [arrayButton addObject:btnOpen];
    }
    else if ([stringCategory isEqualToString:@"Kabit by Kabit"])
    {
        buttonAng.hidden = NO;
        [arrayButton addObject:btnOpen];
        [buttonAng setTitle:@"Open Kabit" forState:UIControlStateNormal];
    }
    else if ([stringCategory isEqualToString:@"Sri Dasam Granth Page by Page"])
    {
        buttonAng.hidden = NO;
        [arrayButton addObject:btnOpen];
        [buttonAng setTitle:@"Open Page" forState:UIControlStateNormal];
    }
    else
    {
        buttonAng.hidden = YES;
        [arrayButton removeObject:btnOpen];
    }
    
    
    if ([stringCategory isEqualToString:@"SGGS Chapter Index"] || [stringCategory isEqualToString:@"Sri Dasam Granth Index"])
    {
        page = firstPage;
    }
    else if([stringCategory isEqualToString:@"Bhai Gurdas Vaar index"] || [stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"] || [stringCategory isEqualToString:@"BookMarks"])
    {
        page = [dict[@"page"] intValue];
        firstPage = [dict[@"firstLine"] intValue];
        lastPage = [dict[@"lastLine"] intValue];
        labelPage.hidden = YES;
    }
    
    //[arrayButton addObject:btnOpen];
    
    self.navigationItem.leftBarButtonItems = arrayButton;
     NSString * num = [NSString stringWithFormat:@"%d",page];
    if ([stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"])
    {
        currentIndex = shabdIndex;
    }
    else
        currentIndex = (int)[arrayPages indexOfObject:num];
    
    [self getData];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSString * num = [NSString stringWithFormat:@"%d",page];
    if ([stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"])
    {
        currentIndex = shabdIndex;
    }
    else
        currentIndex = (int)[arrayPages indexOfObject:num];
}

-(void)setSliderMenu
{
    int xx = 0;
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) xx=20;
    UIView *twitterItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [twitterItem setMenuActionWithBlock:^{
        [self twitterShare];
        [self.sideMenu close];
    }];
    UIImageView *twitterIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [twitterIcon setImage:[UIImage imageNamed:@"tw.png"]];
    [twitterItem addSubview:twitterIcon];
    
    UIView *emailItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [emailItem setMenuActionWithBlock:^{
        [self emailShare];
        [self.sideMenu close];
    }];
    UIImageView *emailIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx , 40+xx)];
    [emailIcon setImage:[UIImage imageNamed:@"mail.png"]];
    [emailItem addSubview:emailIcon];
    
    UIView *facebookItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [facebookItem setMenuActionWithBlock:^{
        [self facebookShare];
        [self.sideMenu close];
    }];
    UIImageView *facebookIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 36+xx)];
    [facebookIcon setImage:[UIImage imageNamed:@"fb.png"]];
    [facebookItem addSubview:facebookIcon];
    
    UIView *browserItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [browserItem setMenuActionWithBlock:^{
        [self.sideMenu close];
    }];
    UIImageView *browserIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [browserIcon setImage:[UIImage imageNamed:@"closeButton.png"]];
    [browserItem addSubview:browserIcon];
    
    UIView *messageItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 37+xx)];
    [messageItem setMenuActionWithBlock:^{
        NSLog(@"mms item");
        [self sendMms];
        [self.sideMenu close];
    }];
    UIImageView *messageIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 36+xx)];
    [messageIcon setImage:[UIImage imageNamed:@"mmsButton.png"]];
    [messageItem addSubview:messageIcon];
    
    self.sideMenu = [[HMSideMenu alloc] initWithItems:@[twitterItem, emailItem, facebookItem ,messageItem,browserItem]];
    [self.sideMenu setItemSpacing:5.0f];
    [self.view addSubview:self.sideMenu];
}

-(IBAction)btnProceed:(id)sender
{
       UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
       pasteboard.persistent = YES;
       pasteboard.image =attachmentImage;
       NSString *phoneToCall = @"sms:";
       NSString *phoneToCallEncoded = [phoneToCall stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
       NSURL *url = [[NSURL alloc] initWithString:phoneToCallEncoded];
       [[UIApplication sharedApplication] openURL:url];

}
-(void)sendMms
{
     [[KGModal sharedInstance] showWithContentView:popView andAnimated:YES];
}


-(void)setTheme
{
    [arrayLanguages removeAllObjects];
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"theme"];
    ISGTheme *theme = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    stringBackgroud=theme.background;
    size1=[theme.fontSize intValue];
   
    language1=theme.lang1;
    language2=theme.lang2;
    language3=theme.lang3;
    language4=theme.lang4;
    language5=theme.lang5;
    language6=theme.lang6;
//    language7=theme.lang7;
    
    gurmukhiSize=[theme.gurmukhiSize intValue];
    romanSize=[theme.romanSize intValue];
    hindiSize=[theme.hindiSize intValue];
    englishSize=[theme.englishSize intValue];
    teekaSize=[theme.teekaSize intValue];
    attributeSize=[theme.attributeSize intValue];
    
    gurmukhiColor=theme.gurmukhiColor;
    romanColor=theme.romanColor;
    hindiColor=theme.hindiColor;
    englishColor=theme.englishColor;
    teekaColor=theme.teekaColor;
    attributeColor=theme.attributeColor;
    
    
    
    totalSize=gurmukhiSize+romanSize+hindiSize+englishSize+teekaSize+attributeSize;
    if(language1.length>0) [arrayLanguages addObject:language1];
    if(language2.length>0) [arrayLanguages addObject:language2];
    if(language3.length>0) [arrayLanguages addObject:language3];
    if(language4.length>0) [arrayLanguages addObject:language4];
    if(language5.length>0) [arrayLanguages addObject:language5];
    if(language6.length>0) [arrayLanguages addObject:language6];
//    if(language7.length>0) [arrayLanguages addObject:language7];
    
    NSLog(@"arrayLanguages = %@",arrayLanguages);
    if(size1==0)size1=13;
    if(stringBackgroud.length<=7)stringBackgroud=@"LightPattern.png";
    fontStyle=theme.fontStyle;
    
    if (!theme.backgroundImage)
    {
        [imageBg setImage:[UIImage imageNamed:stringBackgroud]];
        [imageBg2 setImage:[UIImage imageNamed:stringBackgroud]];
    }
    else
    {
        imageBg.image = theme.backgroundImage;
        imageBg2.image = theme.backgroundImage;
    }
    
    
    
    [table reloadData];
}

-(void)tapGestureHandler:(UITapGestureRecognizer *)gestureRecognizer
{
    if([arrayItems count] == 0)
    {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:table];
    NSIndexPath *indexPath = [table indexPathForRowAtPoint:p];
    UITableViewCell *selectedCell=(UITableViewCell *)[table cellForRowAtIndexPath:indexPath];
    ISGDataModel *data=[arrayItems objectAtIndex:indexPath.row];
    shabd=[data.shabd intValue];
    dataID=[data.dataID intValue];
    sggsData=data.gurmukhi;
    strShabd=data.shabd;
    strId=data.dataID;
    
    
    if([data.script isEqualToString:@"sggs"])
    {
        script=@"Sri Guru Granth Sahib";
    }
    else if([data.script isEqualToString:@"bgv"])
    {
        script=@"Bhai Gurdas Vaaran";
    }
    else if([data.script isEqualToString:@"kbg"])
    {
        script=@"Kabit Bhai Gurdas";
    }
    else if([data.script isEqualToString:@"bnl"])
    {
        script=@"Baani Bhai Nand Lal";
    }
    else if([data.script isEqualToString:@"dgs"])
    {
        script=@"Sri Dasam Granth Index";
    }
    strPage=[NSString stringWithFormat:@"%d",page];
    
    if([stringBackgroud isEqualToString:@"LightPattern"])
    {
        selectedCell.backgroundColor=[UIColor yellowColor];
    }
    else if([stringBackgroud isEqualToString:@"WhitePattern"])
    {
        selectedCell.backgroundColor=[UIColor whiteColor];
    }
    else if([stringBackgroud isEqualToString:@"BlackPattern"])
    {
        selectedCell.backgroundColor=[UIColor blackColor];
    }
    else if([stringBackgroud isEqualToString:@"DarkPattern"])
    {
        selectedCell.backgroundColor=[UIColor redColor];
    }
    
    CGFloat wd;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        wd = 290;
    }
    else
    {
        wd = 680;
    }
    
    UIGraphicsBeginImageContext(CGSizeMake(wd, selectedCell.frame.size.height));
    CGRect tableViewFrame = selectedCell.frame;
    selectedCell.frame = CGRectMake(0.0, 0.0, selectedCell.frame.size.width, table.contentSize.height);
    selectedCell.backgroundColor = [UIColor clearColor];
    [selectedCell.layer renderInContext:UIGraphicsGetCurrentContext()];
    attachmentImage = UIGraphicsGetImageFromCurrentImageContext();
    selectedCell.frame = tableViewFrame;
    UIGraphicsEndImageContext();
    selectedCell.backgroundColor=[UIColor clearColor];
  /*  UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Options" message:Nil delegate:self cancelButtonTitle:Nil otherButtonTitles:@"Add to favourites",@"Share",@"Print this page",@"Cancel" ,nil];
    [alert show];
    alert.tag=3;*/
    bookMarkPage.pageNo = [NSString stringWithFormat:@"%d",page];
    bookMarkPage.scriptID = data.script;
    bookMarkPage.cellImage = UIImageJPEGRepresentation(attachmentImage, 1.0);
    bookMarkPage.query = queryExecuted;
    bookMarkPage.title = labelTitle.text;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Ipad" bundle:nil];
        ISGCustomAlertViewController *customAlertVC = [storyboard instantiateViewControllerWithIdentifier:@"ISGCustomAlertViewController"];
    
    customAlertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    customAlertVC.modalTransitionStyle   = UIModalTransitionStyleCrossDissolve;
    customAlertVC.heading = labelTitle.text;
    customAlertVC.pageNo = [NSString stringWithFormat:@"%d",page];
    customAlertVC.scriptID = data.script;
    customAlertVC.cellImage = UIImageJPEGRepresentation(attachmentImage, 1.0);
    customAlertVC.query = queryExecuted;
    customAlertVC.imageData = [ScrollViewToPDF pdfDataOfScrollView:table];
    customAlertVC.imageSMS = attachmentImage;
        

    [self presentViewController:customAlertVC animated:YES completion:nil];
    }
    
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        ISGCustomAlertViewController *customAlertVC = [storyboard instantiateViewControllerWithIdentifier:@"ISGCustomAlertViewController"];
        customAlertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        customAlertVC.modalTransitionStyle   = UIModalTransitionStyleCrossDissolve;
        customAlertVC.heading = labelTitle.text;
        customAlertVC.pageNo = [NSString stringWithFormat:@"%d",page];
        customAlertVC.scriptID = data.script;
        customAlertVC.cellImage = UIImageJPEGRepresentation(attachmentImage, 1.0);
        customAlertVC.query = queryExecuted;
        customAlertVC.imageData = [ScrollViewToPDF pdfDataOfScrollView:table];
        customAlertVC.imageSMS = attachmentImage;
        [self presentViewController:customAlertVC animated:YES completion:nil];

    }
    
    
    
    
}


-(void)longPressHandler:(UILongPressGestureRecognizer *)gestureRecognizer
{
//    if(UIGestureRecognizerStateBegan ==gestureRecognizer.state)
//	{
//        
//        CGPoint p = [gestureRecognizer locationInView:table];
//        NSIndexPath *indexPath = [table indexPathForRowAtPoint:p];
//        UITableViewCell *selectedCell=(UITableViewCell *)[table cellForRowAtIndexPath:indexPath];
//        ISGDataModel *data=[arrayItems objectAtIndex:indexPath.row];
//        shabd=[data.shabd intValue];
//        dataID=[data.dataID intValue];
//        sggsData=data.gurmukhi;
//        strShabd=data.shabd;
//        strId=data.dataID;
//        
//        bookMarkPage.pageNo = [NSString stringWithFormat:@"%d",page];
//        bookMarkPage.scriptID = data.script;
//        
//        
//        if([data.script isEqualToString:@"sggs"])
//        {
//            script=@"Sri Guru Granth Sahib";
//        }
//        else if([data.script isEqualToString:@"bgv"])
//        {
//            script=@"Bhai Gurdas Vaaran";
//        }
//        else if([data.script isEqualToString:@"kbg"])
//        {
//            script=@"Kabit Bhai Gurdas";
//        }
//        else if([data.script isEqualToString:@"bnl"])
//        {
//            script=@"Baani Bhai Nand Lal";
//        }
//        else if([data.script isEqualToString:@"dgs"])
//        {
//            script=@"Sri Dasam Granth Index";
//        }
//        strPage=[NSString stringWithFormat:@"%d",page];
//        
//         if([stringBackgroud isEqualToString:@"LightPattern"])
//         {
//             selectedCell.backgroundColor=[UIColor yellowColor];
//         }
//        else if([stringBackgroud isEqualToString:@"WhitePattern"])
//        {
//            selectedCell.backgroundColor=[UIColor whiteColor];
//        }
//        else if([stringBackgroud isEqualToString:@"BlackPattern"])
//        {
//            selectedCell.backgroundColor=[UIColor blackColor];
//        }
//        else if([stringBackgroud isEqualToString:@"DarkPattern"])
//        {
//            selectedCell.backgroundColor=[UIColor redColor];
//        }
//        
//        UIGraphicsBeginImageContext(selectedCell.frame.size);
//        CGRect tableViewFrame = selectedCell.frame;
//        selectedCell.frame = CGRectMake(0.0, 0.0, selectedCell.frame.size.width, table.contentSize.height);
//        [selectedCell.layer renderInContext:UIGraphicsGetCurrentContext()];
//        attachmentImage = UIGraphicsGetImageFromCurrentImageContext();
//        selectedCell.frame = tableViewFrame;
//        UIGraphicsEndImageContext();
//         selectedCell.backgroundColor=[UIColor clearColor];
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Options" message:Nil delegate:self cancelButtonTitle:Nil otherButtonTitles:@"Bookmark",@"Share",@"Cancel" ,nil];
//        [alert show];
//        alert.tag=3;
//    }
    if(gestureRecognizer.state != UIGestureRecognizerStateEnded)
    {
        
        
        UILabel * copyLabel = (UILabel *)gestureRecognizer.view;
        CGPoint point = [gestureRecognizer locationInView:copyLabel];
        [copyLabel becomeFirstResponder];
        UIMenuController * menu = [UIMenuController sharedMenuController];
        [menu setTargetRect:CGRectMake(point.x, point.y, 0.0f, 0.0f)  inView:copyLabel];
        message = copyLabel.text;
        UIMenuItem * copy = [[UIMenuItem alloc] initWithTitle:@"copy" action:@selector(copyMessage)];
        menu.menuItems = @[copy];
        [menu setMenuVisible:YES animated:YES];
        
        menu.arrowDirection = UIMenuControllerArrowDown;
    }
}


-(void)copyMessage
{
    UIPasteboard *clipBoard = [UIPasteboard generalPasteboard];
    [clipBoard setString:message];
}

-(BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL) canPerformAction:(SEL)action withSender:(id)sender
{
    return (action == @selector(copyMessage));
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
	return table;
}

-(IBAction)btnOpenAng:(id)sender
{
    UIAlertView *alert;
    if ([stringCategory isEqualToString:@"Kabit by Kabit"])
        alert = [[UIAlertView alloc] initWithTitle:@"Open Kabit 1 -675:"
                                        message:@"\n" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
    else if([stringCategory isEqualToString:@"Sri Dasam Granth Page by Page"])
        alert = [[UIAlertView alloc] initWithTitle:@"Open Page 1 -2820:"
                                           message:@"\n" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
    else
        alert = [[UIAlertView alloc] initWithTitle:@"Open ang 1-1430:"
                                           message:@"\n" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
        
//    textAng = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 70.0, 260.0, 25.0)];
//    [textAng setBackgroundColor:[UIColor whiteColor]];
//    textAng.userInteractionEnabled=YES;
//    textAng.keyboardType=UIKeyboardTypeNumberPad;
//    textAng.delegate=self;
//    [alert addSubview:textAng];
    
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
    
    
    textAng=[[UITextField alloc]init];
    textAng = [alert textFieldAtIndex: 0];
    textAng.delegate=self;
    alert.tag=1;
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1 && buttonIndex==0)
    {
        page=[textAng.text intValue];
        if ([stringCategory isEqualToString:@"Kabit by Kabit"] && page>675)
        {
            [[[UIAlertView alloc] initWithTitle:@"Invalid Page Number" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        }
        else if([stringCategory isEqualToString:@"Sri Dasam Granth Page by Page"] && page>2820)
        {
            [[[UIAlertView alloc] initWithTitle:@"Invalid Page Number" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        }
        else if([stringCategory isEqualToString:@"SGGS Ang by Ang"] && page>1430)
        {
            [[[UIAlertView alloc] initWithTitle:@"Invalid Page Number" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        }
        else
            [self getData];
    }
    else if(alertView.tag==2 && buttonIndex==0)
    {
        //[self insertBookmark];
        [self showFolderList];
    }
    else if(alertView.tag==3 && buttonIndex==0)
    {
        UIAlertView *alertBookmark = [[UIAlertView alloc] initWithTitle:@"Are you sure to add this to your favourites?"
                                                                message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
       
       // txtAlert = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 80.0, 260.0, 30.0)];
        
        
        txtAlert = [[UITextField alloc]init];
        [txtAlert setBackgroundColor:[UIColor whiteColor]];
        txtAlert.text=sggsData;
        txtAlert.font=[UIFont fontWithName:@"GurbaniLipiBold" size:22];
        txtAlert.userInteractionEnabled=NO;
       // [alertBookmark addSubview:txtAlert];
        [alertBookmark show];
        alertBookmark.tag=2;
    }
    else if(alertView.tag==3 && buttonIndex==1)
    {
         [self.sideMenu open];
    }
    else if (alertView.tag == 100 && buttonIndex == 1)
    {
        UITextField * textFiled = [alertView textFieldAtIndex:0];
        if (textFiled.text.length == 0)
        {
            [[[UIAlertView alloc] initWithTitle:@"Invalid text" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        else
        {
            
            [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
            [self showFolderList];
        }
    }
    else if(alertView.tag==3 && buttonIndex==2)
    {
       // imageForPrint = [self screenShotScrollView:table];
        NSData *imageData = [ScrollViewToPDF pdfDataOfScrollView:table];//[self createPDFfromUIView:table]; //UIImagePNGRepresentation(imageForPrint);
        [self printItem:imageData];
    }
}


- (NSData *)createPDFfromUIView:(UIScrollView *)aView {
    // Creates a mutable data object for updating with binary data, like a byte array
    NSMutableData *pdfData = [NSMutableData data];
    
    // Points the pdf converter to the mutable data object and to the UIView to be converted
    UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil);
    UIGraphicsBeginPDFPage();
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
    [aView.layer renderInContext:pdfContext];
    
    // remove PDF rendering context
    UIGraphicsEndPDFContext();
    
    return pdfData;
}



- (UIImage *)captureView:(UIScrollView *)view {
    UIImage* image = nil;
    
    CGPoint savedContentOffset = view.contentOffset;
    CGRect savedFrame = view.frame;
    
    UIGraphicsBeginImageContextWithOptions(view.contentSize, 0, 0);
    //UIGraphicsBeginImageContext(view.contentSize);// view.contentSize.height
    view.contentOffset = CGPointZero;
    view.frame = CGRectMake(0, 0, view.contentSize.width,view.contentSize.height);
    
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    view.contentOffset = savedContentOffset;
    view.frame = savedFrame;
    
    UIGraphicsEndImageContext();
    
    // after all of this, crop image to needed size
    return image;
}

- (void) showFolderList
{
    arrayFolders = [BookMarkFolders fetchAllBookMarkFolders];
    [arrayNames removeAllObjects];
    [arraySortedFolders removeAllObjects];
    
    for (int i = (int)arrayFolders.count; i>=0; i--)
    {
        int count = 0;
        for (BookMarkFolders * folder in arrayFolders)
        {
            if (i == [folder.index intValue])
            {
                [arraySortedFolders addObject:arrayFolders[count]];
            }
            else
                count++;
        }
    }
    
    [arraySortedFolders enumerateObjectsUsingBlock:^(BookMarkFolders *obj, NSUInteger idx, BOOL *stop)
     {
         [arrayNames addObject:obj.folderName];
     }];
    
    if (arrayNames.count == 0)
    {
        [self createFolder];
    }
    else
    {
        UIView * kgView = [[UIView alloc] initWithFrame:CGRectMake(40, 100, 240, 240)];
        tableViewList = [[UITableView alloc] initWithFrame:CGRectMake(0, 0 , kgView.frame.size.width, kgView.frame.size.height - 40) style:UITableViewStylePlain];
        tableViewList.delegate = self;
        tableViewList.dataSource = self;
        tableViewList.backgroundColor = [UIColor whiteColor];
        [kgView addSubview:tableViewList];
        
        UIButton * buttonCreateFolder = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(tableViewList.frame), kgView.frame.size.width, 40)];
        [buttonCreateFolder setTitle:@"Create folder" forState:UIControlStateNormal];
        [buttonCreateFolder setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [buttonCreateFolder setBackgroundColor:[UIColor whiteColor]];
        [buttonCreateFolder addTarget:self action:@selector(createFolder) forControlEvents:UIControlEventTouchUpInside];
        [kgView addSubview:buttonCreateFolder];
        [[KGModal sharedInstance] showWithContentView:kgView andAnimated:YES];
    }
}

-(void)createFolder
{
    [[KGModal sharedInstance] hideAnimated:YES];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"No BookMark Folders" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Create", nil];
    alert.tag = 100;
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) [self facebookShare];
    else if (buttonIndex==1) [self twitterShare];
    else if (buttonIndex==2) [self emailShare];
}
-(IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender
{   
    if ([stringCategory isEqualToString:@"BookMarks"])
    {
        if (![[NSNumber numberWithInt:page] isEqualToNumber:[NSNumber numberWithInt:arrayPages[arrayPages.count-1]]])
        {
            currentIndex++;
            page = [arrayPages[currentIndex] intValue];
            scriptID = arrayScript[currentIndex];
        
        }
    }
    else if ([stringCategory isEqualToString:@"Bhai Gurdas Vaar index"] || [stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"])
    {
        if (![[NSNumber numberWithInt:page] isEqualToNumber:[NSNumber numberWithInt:arrayPages[arrayPages.count-1]]])
        {
            currentIndex++;
            page = [arrayDetails[currentIndex][@"page"] intValue];
            firstPage = [arrayDetails[currentIndex][@"firstLine"] intValue];
            lastPage = [arrayDetails[currentIndex][@"lastLine"] intValue];
            scriptID = arrayScript[currentIndex];
            
        }
    }
    else
        page++;
    
    //table.frame = CGRectMake(table.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
       // table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    } completion:^(BOOL finished) {
        table.alpha = 1.0;
    }];
    
    
    [self getData];
}
-(IBAction)swipeRight:(UISwipeGestureRecognizer *)sender
{
    if ([stringCategory isEqualToString:@"BookMarks"])
    {
        if(![[NSNumber numberWithInt:page] isEqualToNumber:[NSNumber numberWithInt:arrayPages[0]]])
        {
            currentIndex--;
            page = [arrayPages[currentIndex] intValue];
            scriptID = arrayScript[currentIndex];
        }
    }
    else if ([stringCategory isEqualToString:@"Bhai Gurdas Vaar index"] || [stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"])
    {
        if (![[NSNumber numberWithInt:page] isEqualToNumber:[NSNumber numberWithInt:arrayPages[0]]])
        {
            currentIndex--;
            page = [arrayDetails[currentIndex][@"page"] intValue];
            firstPage = [arrayDetails[currentIndex][@"firstLine"] intValue];
            lastPage = [arrayDetails[currentIndex][@"lastLine"] intValue];
            scriptID = arrayScript[currentIndex];
            
        }
    }
    else
        page--;
    
   // table.frame = CGRectMake(x- self.view.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
        //table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    } completion:^(BOOL finished) {
        table.alpha = 1.0;
    }];
    
    [self getData];
}
-(void)emailShare
{
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailCompose = [[MFMailComposeViewController alloc] init];
        mailCompose.mailComposeDelegate = self;
        NSData *data = [NSData dataWithData:UIImageJPEGRepresentation(attachmentImage, 1.0)];
        [mailCompose setSubject:@"iSearchGurbani share"];
        [mailCompose addAttachmentData:data
                              mimeType:@"jpg"
                              fileName:@"iSearchGurbani.jpg"];
        [self presentViewController:mailCompose animated:YES completion:Nil];
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"Please configure a mail account in your device and try again later" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
   
    message=@"Your email has been discarded";
	switch (result) {
            
        case MFMailComposeResultFailed:
            message=@"Your email has failed to send";
            break;
            
        case MFMailComposeResultSent :
            message=@"Your email has sent Successfully";
            break;
            
        case MFMailComposeResultCancelled :
            NSLog(@"cancelled");
            break;
            
        case MFMailComposeResultSaved :
            message=@"Your email has been saved to draft";
            break;
        default:
            break;
    }
    [[[UIAlertView alloc]initWithTitle:@"Mail Status" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
    [self dismissViewControllerAnimated:YES completion:Nil];
    
}

-(void)facebookShare
{
    HUD.labelText=@"Sharing...";
    [HUD show:YES];
//    appDelegate = (ISGAppDelegate *)[[UIApplication sharedApplication] delegate] ;
//    if (!appDelegate.session)
//    {
//        NSLog(@"enterd");
//        appDelegate.session = [[FBSession alloc]initWithPermissions:[NSArray arrayWithObjects:@"publish_stream",nil]];
//    }
//    if (![appDelegate.session isOpen])
//    {
//        NSLog(@"open");
//        [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
//         {
//             if (!error)
//             {
//                 [self fbShare];
//             }
//         }];
//    }
//    else{
        [self fbShare];
//    }

}
-(void) fbShare
{
//    [FBSession setActiveSession:appDelegate.session];
//    NSData *dataImage=UIImageJPEGRepresentation(attachmentImage,1.0);
//    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
//    [parameters setObject:@"iSearchGurbani" forKey:@"name"];
//    [parameters setObject:@"http://www.searchgurbani.com/" forKey:@"caption"];
////    [parameters setObject:dataImage forKey:@"picture"];
//    
//    [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession] parameters:parameters handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//        [HUD hide:YES];
//        if(error == nil)
//        {
//            if(result != FBWebDialogResultDialogNotCompleted && resultURL.query != nil)
//            {
//                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sucessfully shared to facebook" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
//            }
//            else
//            {
//                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sharing canceled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
//            }
//        }
//        else
//        {
//            [[[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
//        }
//    }];

    SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [mySLComposerSheet setInitialText:@"iSearchGurbaani"];
    
    [mySLComposerSheet addImage:attachmentImage];
    
    [mySLComposerSheet addURL:[NSURL URLWithString:@"http://www.searchgurbani.com/"]];
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sharing canceled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                break;
            case SLComposeViewControllerResultDone:
                [[[UIAlertView alloc]initWithTitle:@"Success" message:@"Sucessfully shared to facebook" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                break;
                
            default:
                break;
        }
    }];
    
    [self presentViewController:mySLComposerSheet animated:YES completion:^{
        [HUD hide:YES];
    }];
}

-(void)twitterShare
{
    TWTweetComposeViewController *tweetView = [[TWTweetComposeViewController alloc] init];
    
    TWTweetComposeViewControllerCompletionHandler
    completionHandler =
    ^(TWTweetComposeViewControllerResult result) {
        
        if(result == TWTweetComposeViewControllerResultDone) {
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"The Tweet was posted successfully." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        if(result == TWTweetComposeViewControllerResultCancelled) {
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You cancelled posting the Tweet." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    };
    [tweetView setCompletionHandler:completionHandler];
    
    //[tweetView setInitialText:@"Try this App: Design Studio"];
    [tweetView addImage:attachmentImage];
    //[tweetView addURL:url];
    [self presentViewController:tweetView animated:YES completion:nil];
}

-(void)insertBookmark
{
//   NSManagedObjectContext *managedObjectContext = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).managedObjectContext;
//   Favourites *favourites=[NSEntityDescription insertNewObjectForEntityForName:@"Favourites" inManagedObjectContext:managedObjectContext];
//   favourites.text=sggsData;
//   favourites.ang=[NSString stringWithFormat:@"%d",page];
//   favourites.scrpt=script;
//   NSLog(@"scrpt... = %@",script);
//   NSError *error;
//   if(![managedObjectContext save:&error])
//   {
//       NSLog(@"%@",[error localizedDescription]);
//   }
//   else
//   {
//       NSLog(@"inserted1");
//   }
    [database open];
    [database executeUpdate:@"INSERT INTO bookmark (markText,page,scrpt,tab) VALUES (?,?,?,'ISGRead');",sggsData,[NSString stringWithFormat:@"%d",page],script];
//    NSLog(@"%@",[database lastErrorMessage]);
    [database close];
    [SVProgressHUD showSuccessWithStatus:@"Bookmarked successfully"]; 
    [[NSNotificationCenter defaultCenter]postNotificationName:@"favourites" object:Nil];
}


-(void)getData
{
    if(page > 0 || [stringCategory isEqualToString:@"BookMarks"])
    {
        [database open];
        
        NSString *querry;
        if ([stringCategory isEqualToString:@"SGGS Chapter Index"])
        {
            querry=[NSString stringWithFormat:@"SELECT * FROM gurbani INDEXED BY myIndex where page='%d'AND scrpt='1'",page];
        }
        else if ([stringCategory isEqualToString:@"SGGS Ang by Ang"])
        {
            querry=[NSString stringWithFormat:@"SELECT * FROM gurbani INDEXED BY myIndex where page='%d' AND scrpt='1'",page];
        }
        else if ([stringCategory isEqualToString:@"Sri Dasam Granth Page by Page"])
        {
            querry=[NSString stringWithFormat:@"SELECT * FROM gurbani INDEXED BY myIndex where page='%d' AND scrpt='4'",page];
        }
        else if ([stringCategory isEqualToString:@"Sri Dasam Granth Index"])
        {
            querry=[NSString stringWithFormat:@"SELECT * FROM gurbani INDEXED BY myIndex where page='%d' AND scrpt='4'",page];
        }
        else if ([stringCategory isEqualToString:@"Kabit by Kabit"])
        {
            querry=[NSString stringWithFormat:@"SELECT * FROM gurbani INDEXED BY myIndex where page='%d' AND scrpt='3'",page];
        }
        else if ([stringCategory isEqualToString:@"Bhai Gurdas Vaar index"])
        {
//            querry=[NSString stringWithFormat:@"SELECT * FROM gurbani INDEXED BY myIndex where page='%d' AND scrpt='2' AND line BETWEEN %d AND %d",page,firstPage,lastPage];
            querry=[NSString stringWithFormat:@"SELECT * FROM gurbani INDEXED BY pageIndex where plineID='%d' AND scrpt='2' AND line BETWEEN %d AND %d",page,firstPage,lastPage];
        }
        else if ([stringCategory isEqualToString:@"BookMarks"])
        {
            //querry=[NSString stringWithFormat:@"SELECT * FROM gurbani INDEXED BY myIndex WHERE page='%d' AND scrpt='%@'",page,scriptID];
            querry = stringQuery;
        }
        else if ([stringCategory isEqualToString:@"Bhai Nand Lal Baani"])
        {
            querry=[NSString stringWithFormat:@"SELECT * FROM gurbani INDEXED BY myIndex where page='%d' AND scrpt='5'",page];
        }
        else if ( [stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"])
        {
            querry=[NSString stringWithFormat:@"SELECT * FROM gurbani INDEXED BY shabdIndex where page='%d' AND scrpt='1' AND line BETWEEN %d AND %d",page,firstPage,lastPage];
        }

        
        NSLog(@"%@",querry);
        queryExecuted = querry;
        FMResultSet *results = [database executeQuery:querry];
        [arrayItems removeAllObjects];
        while([results next])
        {
           
            ISGDataModel *dataModel=[[ISGDataModel alloc]initWithResult:results];
            [arrayItems addObject:dataModel];
         
        }
       // NSLog(@"data count %d",arrayItems.count);
        [database close];
        [self buildCells];
        [table reloadData];
        [table selectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        
        //buttonPrev.hidden = (page==[arrayPages[0] intValue])?TRUE:FALSE;
        if ([stringCategory isEqualToString:@"SGGS Chapter Index"] || [stringCategory isEqualToString:@"Sri Dasam Granth Index"] || [stringCategory isEqualToString:@"Bhai Nand Lal Baani"])
        {
            if (page == firstPage)  //[arrayPages[0] intValue]
            {
                buttonPrev.hidden = TRUE;
                [self.view removeGestureRecognizer:rightGesture];
            }
            else
            {
                buttonPrev.hidden = FALSE;
                [self.view addGestureRecognizer:rightGesture];
            }
            if (page==lastPage-1 || page==lastPage)      //[arrayPages[arrayPages.count - 1] intValue]
            {
                buttonNext.hidden = TRUE;
                [self.view removeGestureRecognizer:leftGesture];
            }
            else
            {
                buttonNext.hidden = FALSE;
                [self.view addGestureRecognizer:leftGesture];
            }
        }
        else if ([stringCategory isEqualToString:@"Bhai Gurdas Vaar index"])
        {
            if (page== [arrayPages[0] intValue])  //
            {
                buttonPrev.hidden = TRUE;
                [self.view removeGestureRecognizer:rightGesture];
            }
            else
            {
                buttonPrev.hidden = FALSE;
                [self.view addGestureRecognizer:rightGesture];
            }
            if (page == [arrayPages[arrayPages.count - 1] intValue])      //
            {
                buttonNext.hidden = TRUE;
                [self.view removeGestureRecognizer:leftGesture];
            }
            else
            {
                buttonNext.hidden = FALSE;
                [self.view addGestureRecognizer:leftGesture];
            }

        }
        else if ([stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"])
        {
            if (page== [arrayPages[0] intValue] && firstPage == [arrayDetails[0][@"firstLine"] intValue] && lastPage == [arrayDetails[0][@"lastLine"] intValue])  //
            {
                buttonPrev.hidden = TRUE;
                [self.view removeGestureRecognizer:rightGesture];
            }
            else
            {
                buttonPrev.hidden = FALSE;
                [self.view addGestureRecognizer:rightGesture];
            }
            if (page == [arrayPages[arrayPages.count - 1] intValue] && firstPage == [arrayDetails[arrayDetails.count-1][@"firstLine"] intValue] && lastPage == [arrayDetails[arrayDetails.count-1][@"lastLine"] intValue])      //
            {
                buttonNext.hidden = TRUE;
                [self.view removeGestureRecognizer:leftGesture];
            }
            else
            {
                buttonNext.hidden = FALSE;
                [self.view addGestureRecognizer:leftGesture];
            }
            
        }
        else if ([stringCategory isEqualToString:@"SGGS Ang by Ang"])
        {
            if (page==1)
            {
                buttonPrev.hidden = TRUE;
                [self.view removeGestureRecognizer:rightGesture];
            }
            else
            {
                buttonPrev.hidden = FALSE;
                [self.view addGestureRecognizer:rightGesture];
            }
            if (page==1430)
            {
                buttonNext.hidden = TRUE;
                [self.view removeGestureRecognizer:leftGesture];
            }
            else
            {
                buttonNext.hidden = FALSE;
                [self.view addGestureRecognizer:leftGesture];
            }

        }
        else if ([stringCategory isEqualToString:@"Sri Dasam Granth Page by Page"])
        {
            if (page==1)
            {
                buttonPrev.hidden = TRUE;
                [self.view removeGestureRecognizer:rightGesture];
            }
            else
            {
                buttonPrev.hidden = FALSE;
                [self.view addGestureRecognizer:rightGesture];
            }
            if (page==2820)
            {
                buttonNext.hidden = TRUE;
                [self.view removeGestureRecognizer:leftGesture];
            }
            else
            {
                buttonNext.hidden = FALSE;
                [self.view addGestureRecognizer:leftGesture];
            }
            
        }
        else if ([stringCategory isEqualToString:@"Kabit by Kabit"])
        {
            if (page==1)
            {
                buttonPrev.hidden = TRUE;
                [self.view removeGestureRecognizer:rightGesture];
            }
            else
            {
                buttonPrev.hidden = FALSE;
                [self.view addGestureRecognizer:rightGesture];
            }
            if (page==675)
            {
                buttonNext.hidden = TRUE;
                [self.view removeGestureRecognizer:leftGesture];
            }
            else
            {
                buttonNext.hidden = FALSE;
                [self.view addGestureRecognizer:leftGesture];
            }
            
        }
        else if ([stringCategory isEqualToString:@"BookMarks"])
        {
            buttonPrev.hidden = TRUE;
            [self.view removeGestureRecognizer:rightGesture];
            buttonNext.hidden = TRUE;
            [self.view removeGestureRecognizer:leftGesture];
        }
        
        
        
        
        //buttonNext.hidden = (page==[arrayPages[arrayPages.count - 1] intValue])?TRUE:FALSE;
        if(arrayItems.count==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"No more data" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            if(previousPage>page) buttonPrev.hidden=TRUE;
            else buttonNext.hidden=TRUE;
            page=previousPage;
            if ([stringCategory isEqualToString:@"Kabit by Kabit"])
                [labelPage setTitle:[NSString stringWithFormat:@" Kabit No: %d",page] forState:UIControlStateNormal];
                //labelPage.text=[NSString stringWithFormat:@" Kabit No: %d",page];
            else if ([stringCategory isEqualToString:@"Sri Dasam Granth Page by Page"] || [stringCategory isEqualToString:@"Sri Dasam Granth Index"] || [stringCategory isEqualToString:@"Bhai Nand Lal Baani"])
            {
                [labelPage setTitle:[NSString stringWithFormat:@" Page No: %d",page] forState:UIControlStateNormal];
            }
            else if ([stringCategory isEqualToString:@"Bhai Gurdas Vaar index"])
            {
                labelTitle.text = arrayTitles[page-1];
                
            }
            else if ([stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"])
            {
                labelTitle.text = arrayTitles[currentIndex];
            }
            else
                [labelPage setTitle:[NSString stringWithFormat:@"Ang No: %d",page] forState:UIControlStateNormal];
                //labelPage.text=[NSString stringWithFormat:@"Ang No: %d",page];
        }
        else
        {
            //[table reloadData];
            previousPage=page;
            if([strFavour isEqualToString:@"fav"])
            {
                self.title=[NSString stringWithFormat:@"Sri Guru Granth Sahib Ang: %d",page];
            }
            else
            {
                if ([stringCategory isEqualToString:@"Kabit by Kabit"])
                    [labelPage setTitle:[NSString stringWithFormat:@" Kabit No: %d",page] forState:UIControlStateNormal];
                    //labelPage.text=[NSString stringWithFormat:@" Kabit No: %d",page];
                else if ([stringCategory isEqualToString:@"Sri Dasam Granth Page by Page"] || [stringCategory isEqualToString:@"Sri Dasam Granth Index"] || [stringCategory isEqualToString:@"Bhai Nand Lal Baani"])
                {
                    [labelPage setTitle:[NSString stringWithFormat:@" Page No: %d",page] forState:UIControlStateNormal];
                }
                else if ([stringCategory isEqualToString:@"Bhai Gurdas Vaar index"])
                {
                    labelTitle.text = arrayTitles[page-1];
                }
                else if ([stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"])
                {
                    labelTitle.text = arrayTitles[currentIndex];
                }
                else
                    [labelPage setTitle:[NSString stringWithFormat:@"Ang No: %d",page] forState:UIControlStateNormal];
                    //labelPage.text=[NSString stringWithFormat:@"Ang No: %d",page];
            }
            [table scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
         }
 
     
       if(![strFavour isEqualToString:@"fav"])
       {
           if ([stringCategory isEqualToString:@"SGGS Ang by Ang"])
           {
               NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
               [userDefaults setInteger:page forKey:@"pageMarker"];
               [userDefaults synchronize];
           }
           else if ([stringCategory isEqualToString:@"Sri Dasam Granth Page by Page"])
           {
               NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
               [userDefaults setInteger:page forKey:@"pageMarker For Sri Dasam Granth Page by Page"];
               [userDefaults synchronize];
           }
           else if ([stringCategory isEqualToString:@"Kabit by Kabit"])
           {
               NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
               [userDefaults setInteger:page forKey:@"pageMarker For Kabit by Kabit"];
               [userDefaults synchronize];
           }
       }
    
    }
//    [self buildCells];
//    [table reloadData];
}

-(IBAction)btnNext:(id)sender
{
    buttonNext.hidden=TRUE;
    if ([stringCategory isEqualToString:@"BookMarks"])
    {
        if (![[NSNumber numberWithInt:page] isEqualToNumber:[NSNumber numberWithInt:arrayPages[arrayPages.count-1]]])
        {
            currentIndex++;
            page = [arrayPages[currentIndex] intValue];
            scriptID = arrayScript[currentIndex];
            
        }
    }
    else if ([stringCategory isEqualToString:@"Bhai Gurdas Vaar index"] || [stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"])
    {
        if (![[NSNumber numberWithInt:page] isEqualToNumber:[NSNumber numberWithInt:arrayPages[arrayPages.count-1]]])
        {
            currentIndex++;
            page = [arrayDetails[currentIndex][@"page"] intValue];
            firstPage = [arrayDetails[currentIndex][@"firstLine"] intValue];
            lastPage = [arrayDetails[currentIndex][@"lastLine"] intValue];
            scriptID = arrayScript[currentIndex];
            
        }
    }
    else
        page++;
    
   // table.frame = CGRectMake(table.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
        //table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    } completion:^(BOOL finished) {
        table.alpha = 1.0;
    }];
    
    
    
    [self getData];
}
-(IBAction)btnPrev:(id)sender
{
    if ([stringCategory isEqualToString:@"BookMarks"])
    {
        if(![[NSNumber numberWithInt:page] isEqualToNumber:[NSNumber numberWithInt:arrayPages[0]]])
        {
            currentIndex--;
            page = [arrayPages[currentIndex] intValue];
            scriptID = arrayScript[currentIndex];
        }
    }
    else if ([stringCategory isEqualToString:@"Bhai Gurdas Vaar index"] || [stringCategory isEqualToString:@"SGGS Shabd Index Gurmukhi"] || [stringCategory isEqualToString:@"SGGS Shabd Index Roman"] || [stringCategory isEqualToString:@"SGGS Shabd Index Alphabetical"])
    {
        if (![[NSNumber numberWithInt:page] isEqualToNumber:[NSNumber numberWithInt:arrayPages[0]]])
        {
            currentIndex--;
            page = [arrayDetails[currentIndex][@"page"] intValue];
            firstPage = [arrayDetails[currentIndex][@"firstLine"] intValue];
            lastPage = [arrayDetails[currentIndex][@"lastLine"] intValue];
            scriptID = arrayScript[currentIndex];
        }
    }
    else
        page--;
    
   // table.frame = CGRectMake(x- self.view.frame.size.width, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationCurveEaseOut animations:^{
        //table.frame = CGRectMake(x, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
    } completion:^(BOOL finished) {
        table.alpha = 1.0;
    }];
    
    [self getData];
}
-(void)refresh:(NSNotification *)notification
{
    [self setTheme];
    [self buildCells];
    [table reloadData];
}

#pragma mark tableview delegates and datasource
-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tableViewList)
    {
        return arrayNames.count;
    }
    else
        return [cells count];
}
-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    indexpath = (int)indexPath.row;
    if (tableView == tableViewList)
    {
        UITableViewCell * cellList = [[UITableViewCell alloc] initWithStyle:UITableViewStylePlain reuseIdentifier:@"myCell"];
        if (cellList == nil)
        {
            cellList = [tableViewList dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
        }
        cellList.textLabel.text = arrayNames[indexPath.row];
        cellList.imageView.image = [UIImage imageNamed:@"folders"];
        return cellList;
    }
    else
        return [cells objectAtIndex:indexPath.row];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tableViewList)
    {
        return 40;
    }
    else {
        
    ISGCell * cell = cells[indexPath.row];
    NSLog(@"cell:%f",cell.labelHeight);
    [cell layoutSubviews];
    return  cell.labelHeight + 24;;
    }
      //  [[cells objectAtIndex:indexPath.row]frame].size.height;
       // NSLog(@"%f",[[cells objectAtIndex:indexPath.row] cellHeight]);
       // return [[cells objectAtIndex:indexPath.row]frame].size.height;
    //return [[heightsCell objectAtIndex:(int)[indexPath row]] intValue];

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tableViewList)
    {
        BookMarkFolders * fol = arraySortedFolders[indexPath.row];
        NSSet * set = fol.relationshipToPages;
        NSArray * array = [NSArray arrayWithArray:set.allObjects];
        int cnt = 0;
        
        bookMarkPage.index = [NSString stringWithFormat:@"%lu",(unsigned long)array.count];
        
        for (BookMarkPages * obj in array)
        {
            if ([bookMarkPage.pageNo isEqualToString:obj.pageNo] && [bookMarkPage.scriptID isEqualToString:obj.scriptID] && [bookMarkPage.cellImage isEqualToData:obj.cellImage] && [bookMarkPage.query isEqualToString:obj.query])
            {
                cnt++;
            }
        }
        if (cnt>0)
        {
            [SVProgressHUD showErrorWithStatus:@"Already added to the list"];
        }
        else
        {
            [BookMarkPages addBookMarkPage:bookMarkPage inBookMarkFolder:fol];
            [SVProgressHUD showSuccessWithStatus:@"Added"];
        }
        //NSLog(@"the folder name = %@",fol.folderName);
        
        [[KGModal sharedInstance] hideAnimated:YES];
    }
}

- (void)buildCells {
    // clear existing data
  
    if (cells) [cells removeAllObjects]; else cells = [[NSMutableArray alloc] init];
    if (heightsCell) [heightsCell removeAllObjects]; else heightsCell = [[NSMutableArray alloc] init];
    NSMutableArray *arrayLabels;
    for (ISGDataModel *data in arrayItems) {
        if (arrayLabels) [arrayLabels removeAllObjects]; else arrayLabels = [[NSMutableArray alloc] init];
        for(int index=1;index<=arrayLanguages.count;index++)
        {
            UILabel *label = [[UILabel alloc] init];
            if (![[arrayLanguages objectAtIndex:index-1] isEqualToString:@"lareevar"])
            {
                [label setText:[data valueForKey:[arrayLanguages objectAtIndex:index-1]]];
            }
            
            label.textColor = [UIColor whiteColor];
            
            if(index==1 && [[arrayLanguages objectAtIndex:index-1] isEqualToString:@"gurmukhi"])
            {
                [label setFont:[UIFont fontWithName:fontStyle size:gurmukhiSize]];
                [label setTextColor:gurmukhiColor];
            }
            else if (index!=1 && [[arrayLanguages objectAtIndex:index-1] isEqualToString:@"gurmukhi"])
            {
                [label setFont:[UIFont fontWithName:@"Gurbanihindi" size:hindiSize]];
                [label setTextColor:hindiColor];
                
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"teeka"])
            {
                [label setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:teekaSize]];
                [label setTextColor:teekaColor];
                
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"attributes"])
            {
                [label setFont:[UIFont fontWithName:@"GurbaniLipiBold" size:attributeSize]];
                [label setTextColor:attributeColor];
                
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"english"])
            {
                label.font=[UIFont boldSystemFontOfSize:englishSize];
                [label setTextColor:englishColor];
                
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"roman"])
            {
                label.font=[UIFont boldSystemFontOfSize:romanSize];
                //label.font = [UIFont fontWithName:@"Rosemary-Roman" size:romanSize];
                [label setTextColor:romanColor];
                
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"punctuation"])
            {
                [label setFont:[UIFont fontWithName:fontStyle size:gurmukhiSize]];
                [label setTextColor:gurmukhiColor];
                
            }
            else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"translit"])
            {
                [label setFont:[UIFont boldSystemFontOfSize:romanSize]];
                //label.font = [UIFont fontWithName:@"Rosemary-Roman" size:romanSize];
                [label setTextColor:romanColor];
                
            }
            if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"lareevar"])
            {
                NSString * str = [data valueForKey:@"gurmukhi"];
                NSString * lareevar = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
                [label setFont:[UIFont fontWithName:fontStyle size:gurmukhiSize]];
                [label setTextColor:gurmukhiColor];
                label.text = lareevar;
            }
            
            //highlighting search results
//            if([stringBackgroud isEqualToString:@"LightPattern"]||[stringBackgroud isEqualToString:@"WhitePattern"])
//            {
//                if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"gurmukhi"]||[[arrayLanguages objectAtIndex:index-1] isEqualToString:@"punctuation"])
//                {
//                    if([selectedId isEqualToString:data.dataID] && [_selectedLang isEqualToString:@"gurmukhi"])
//                    {
//                        label.textColor=[UIColor colorWithHexString:@"ff0000"];
//                    }
//                }
//                else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"roman"]||[[arrayLanguages objectAtIndex:index-1] isEqualToString:@"translit"])
//                {
//                    if([selectedId isEqualToString:data.dataID] && [_selectedLang isEqualToString:@"roman"])
//                    {
//                        label.textColor=[UIColor colorWithHexString:@"ff0000"];
//                    }
//                    
//                }
//            }
//            else
//            {
//                if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"gurmukhi"]||[[arrayLanguages objectAtIndex:index-1] isEqualToString:@"punctuation"])
//                {
//                    if([selectedId isEqualToString:data.dataID] && [_selectedLang isEqualToString:@"gurmukhi"])
//                    {
//                        label.textColor=[UIColor colorWithHexString:@"ff0000"];
//                    }
//                }
//                else if([[arrayLanguages objectAtIndex:index-1] isEqualToString:@"roman"]||[[arrayLanguages objectAtIndex:index-1] isEqualToString:@"translit"])
//                {
//                    if([selectedId isEqualToString:data.dataID] && [_selectedLang isEqualToString:@"roman"])
//                    {
//                        label.textColor=[UIColor colorWithHexString:@"ff0000"];
//                    }
//                }
//            }

            
            
            
//            if (data == arrayItems[0])
//            {
//                label.textColor = [UIColor redColor];
//            }
            
            lpHandler = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressHandler:)];
            lpHandler.minimumPressDuration = 0.5;
            label.userInteractionEnabled = YES;
            [label addGestureRecognizer:lpHandler];
            label.textAlignment = NSTextAlignmentCenter;
            
            [arrayLabels addObject:label];
        }
        static NSString *cellIdentifier = @"ISGCell";
        cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[ISGCell alloc] initWithLabels:arrayLabels];
            int labelHeight = 0;
            for (UILabel *label in arrayLabels) {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    label.frame = CGRectMake(5, labelHeight+2, 685, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(685, 9999)].height);
                }
                else
                {
                    //287
                    label.frame = CGRectMake(0, labelHeight+2, 287, [self getHeightForString:label.text forFont:label.font forConstraintSize:CGSizeMake(287, 9999)].height);
                }
                
                labelHeight += label.frame.size.height;
                
            }
            NSNumber *someNumber = [NSNumber numberWithInt:labelHeight + 10];
            [heightsCell addObject:someNumber];
        }
        
        cell.backgroundColor=[UIColor clearColor];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [cell.layer setMasksToBounds:YES];
        [cells addObject:cell];
    }
    table.separatorColor=[UIColor grayColor];
   
}
- (CGSize)getHeightForString:(NSString *)string forFont:(UIFont *)font forConstraintSize:(CGSize)constrainSize {
    //    NSLog(@"string height %f",[string sizeWithFont:font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByWordWrapping].height);
    return [string sizeWithFont:font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByWordWrapping];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Animations

- (void)moveViewFromSuperViewAnimatedWithView:(UIView *)view
                               backgroundView:(UIView *)backgroundView
                                   andToValue:(CGFloat)toValue {
    [UIView animateWithDuration:kAnimationDuration animations:^(void) {
        CGRect frame = view.frame;
        frame.origin.x = toValue;
        view.frame = frame;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:kAnimationDuration animations:^(void) {
            backgroundView.alpha = 1;
            backgroundView.alpha = 0;
        } completion:^(BOOL finished) {
            //[view removeFromSuperview];
            //[backgroundView removeFromSuperview];
        }];
    }];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


#pragma mark - Printing

-(void)printItem :(NSData*)data {
    printController = [UIPrintInteractionController sharedPrintController];
    if(printController && [UIPrintInteractionController canPrintData:data]) {
        printController.delegate = self;
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [NSString stringWithFormat:@""];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        printController.printInfo = printInfo;
        printController.showsPageRange = YES;
        printController.printingItem = data;
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                //NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
            }
        };
        //[printController presentFromBarButtonItem:self.item animated:YES completionHandler:completionHandler];
        //[printController presentAnimated:YES completionHandler:completionHandler];
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            [printController presentFromRect:CGRectMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2, 0, 0) inView:self.view animated:YES completionHandler:completionHandler];
        }
        else
        {
            [printController presentAnimated:YES completionHandler:completionHandler];
        }
    }
}




@end
