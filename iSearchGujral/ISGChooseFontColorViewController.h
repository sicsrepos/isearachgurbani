//
//  ISGChooseFontColorViewController.h
//  iSearchGujral
//
//  Created by SICS on 10/12/14.
//  Copyright (c) 2014 sics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ColorViewController.h"
#import "ColorPickerClasses/RSColorPickerView.h"
#import "ColorPickerClasses/RSColorFunctions.h"

@class RSBrightnessSlider;
@class RSOpacitySlider;
@class RSSaturationSlider;
@class RSHueSlider;

@interface ISGChooseFontColorViewController : UIViewController<RSColorPickerViewDelegate>
{
    BOOL isSmallSize;
}

@property (nonatomic, strong) NSString * language;
@property (nonatomic) RSColorPickerView *colorPicker;
@property (nonatomic) RSBrightnessSlider *brightnessSlider;
@property (nonatomic) RSOpacitySlider *opacitySlider;
@property (nonatomic) RSSaturationSlider *saturationSlider;
@property (nonatomic) RSHueSlider *hueSlider;
@property (nonatomic) UIView *colorPatch;

@end
