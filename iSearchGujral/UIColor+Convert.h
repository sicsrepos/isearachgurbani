//
//  ConvertColor.h
//  VenistarFashionable
//
//  Created by Vignesh R on 27/10/11.
//  Copyright 2011 SICS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface  UIColor (BBColor)


+ (UIColor *) colorWithHexString: (NSString *) stringToConvert;
@end
