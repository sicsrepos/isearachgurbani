//
//  ISGSearchHistoryViewController.m
//  iSearchGujral
//
//  Created by SICS on 15/06/15.
//  Copyright (c) 2015 sics. All rights reserved.
//

#import "ISGSearchHistoryViewController.h"
#import "SearchHistory.h"
#import "FMDatabase.h"
#import "ISGDataModel.h"
#import "ISGAppDelegate.h"
#import "ISGSearchResultsViewController.h"
#import "ISGTabViewController.h"

@interface ISGSearchHistoryViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UITabBarDelegate>


@end

@implementation ISGSearchHistoryViewController
{
    IBOutlet UITableView * tableViewHistory;
    NSMutableArray * arraySearchHistory, *arrayFilter;
    FMDatabase *database;
    NSMutableArray * arrayItems;
    int searchFlag, itration, filterFlag;
    NSString * stringInput, *stringLanguage;
    BOOL searchMoreData;
    SearchHistory * searchHistory;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"History";
    database = ((ISGAppDelegate*)[[UIApplication sharedApplication]delegate ]).database;
    
    arrayItems = [NSMutableArray new];
    arraySearchHistory = [NSMutableArray new];
    [arraySearchHistory removeAllObjects];
    
    UIBarButtonItem * buttonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(actionButtonCancel:)];
    [buttonCancel setTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem = buttonCancel;
    
    UIBarButtonItem * buttonClear = [[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered target:self action:@selector(actionButtonClear:)];
    [buttonClear setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = buttonClear;
    
    NSArray * arr = [SearchHistory fetchAllSearchHistory];
    for (int i = arr.count - 1; i >= 0; i--)
    {
        [arraySearchHistory addObject:arr[i]];
    }
    
    searchFlag = 0;
    itration = 0;
    filterFlag = 0;
    searchMoreData = NO;
    arrayFilter = [NSMutableArray new];
    [arrayFilter removeAllObjects];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (arraySearchHistory.count == 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"There is no search history" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

-(void)actionButtonCancel:(UIButton *) sender
{
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

-(void)actionButtonClear:(UIButton *) sender
{
    [[[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Do you want to clear history" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil] show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [SearchHistory deleteAllSearchHistory];
        [arraySearchHistory removeAllObjects];
        [tableViewHistory reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arraySearchHistory.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [cell.textLabel setTextColor:[UIColor blackColor]];
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.numberOfLines=1;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    SearchHistory * searchHistory1 = (SearchHistory *)arraySearchHistory[indexPath.row];
    cell.textLabel.text=searchHistory1.searchText;
    cell.textLabel.userInteractionEnabled = YES;
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    searchHistory = (SearchHistory *)arraySearchHistory[indexPath.row];
    stringLanguage = searchHistory.searchLanguage;
    [self getData];
}

#pragma Get Chapters
-(void) getData
{
    [database open];
    FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
    [arrayItems removeAllObjects];
    while([results next])
    {
        ISGDataModel *dataModel=[[ISGDataModel alloc]initWithResult:results];
        [arrayItems addObject:dataModel];
    }
    [database close];
    
    if ([searchHistory.searchLanguage isEqualToString:@"roman"])
    {
        [self getDataRomanNew];
    }
    else
    {
        [self getDataGurmukhi];
    }
}

-(void)getDataRomanNew
{
    searchFlag=0;
    NSString *str=@"%";
    stringInput=[searchHistory.searchText lowercaseString];
    [database open];
    
    if([searchHistory.searchCategory isEqualToString:@"First Letter Anywhere"])
    {
        if ([stringInput componentsSeparatedByString:@" "].count > 1 && itration==0)
        {
            NSString *filterPattern=@"";
            NSString *querryPattern=@"";
            NSArray *arrayPattern=[stringInput componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            for (NSString *pattern in arrayPattern) {
                querryPattern=[NSString stringWithFormat:@"%@ %@%@",querryPattern,pattern,str];
                filterPattern=[NSString stringWithFormat:@"%@%c",filterPattern,[pattern characterAtIndex:0]];
            }
            //filterPattern=[NSString stringWithFormat:@" %@",txtSearch.text];
            NSArray *ary=[stringInput componentsSeparatedByString:@" "];
            int wordLen=0;
            for (NSString *item in ary) {
                if(item.length>wordLen) wordLen=item.length;
            }
            if(wordLen==1)
            {
                searchFlag=1;
            }
            else
            {
                searchFlag=2;
            }
            FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
            [arrayItems removeAllObjects];
            while([results next])
            {
                ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
                [arrayItems addObject:dataModel];
            }
            searchMoreData=YES;
            if([searchHistory.searchType isEqualToString:@"notWildcard"])[self filterResultsWith:filterPattern];
            
        }
        else  if ([stringInput componentsSeparatedByString:@"*"].count > 1 && itration==0)
        {
            if([searchHistory.searchType isEqualToString:@"Wildcard"])
            {
                stringInput=[stringInput stringByReplacingOccurrencesOfString:@"*" withString:@"% "];
                FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
                [arrayItems removeAllObjects];
                while([results next])
                {
                    ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
                    // //NSLog(@"id= %@",dataModel.dataID);
                    [arrayItems addObject:dataModel];
                }
            }
        }
        else if(itration==0)
        {
            NSString *filterPattern=searchHistory.searchText;

            FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
            [arrayItems removeAllObjects];
            while([results next])
            {
                ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
                // //NSLog(@"id= %@",dataModel.dataID);
                [arrayItems addObject:dataModel];
            }
            if([searchHistory.searchType isEqualToString:@"notWildcard"]) [self filterResultsWith:filterPattern];
        }
    }
    else if ([searchHistory.searchCategory isEqualToString:@"First Letter Beginning"])
    {
        NSString *type;
        int wordLenght=0;
        NSString *checkedString;
        unichar last = [searchHistory.searchText characterAtIndex:[searchHistory.searchText length] - 1];
        if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:last]) {
            checkedString=[searchHistory.searchText substringToIndex:searchHistory.searchText.length-1];
        } else checkedString=searchHistory.searchText;
        NSArray *words = [checkedString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (words.count > 1)
        {
            wordLenght = words.count;
        }
        else
        {
            for (NSString *item in words)
            {
                if(item.length>wordLenght) wordLenght=item.length;
            }
        }
        
        if(wordLenght==1) type=@"single"; else type=@"more";
        if ([stringInput componentsSeparatedByString:@"*"].count > 1 && itration==0)
        {
            //NSLog(@"type :%@",type);
            if([searchHistory.searchType isEqualToString:@"Wildcard"])
            {
                FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
                [arrayItems removeAllObjects];
                while([results next])
                {
                    ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
                    // //NSLog(@"id= %@",dataModel.dataID);
                    [arrayItems addObject:dataModel];
                }
            }
        }
        else if([type isEqualToString:@"single"])
        {
            NSString *formatedString=[stringInput stringByReplacingOccurrencesOfString:@" " withString:@""];
            FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
            [arrayItems removeAllObjects];
            while([results next])
            {
                ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
                // //NSLog(@"id= %@",dataModel.dataID);
                [arrayItems addObject:dataModel];
            }
            
            if([searchHistory.searchType isEqualToString:@"notWildcard"])[self filterResults:formatedString];
        }
        else
        {
            NSString *pattern=@"";
            NSString *formatedString=@"";
            NSArray *array=[checkedString componentsSeparatedByString:@" "];
            int j=0;
            for (NSString *item in array) {
                pattern=[NSString stringWithFormat:@"%@%@",pattern,[NSString stringWithFormat:@"%c",[item characterAtIndex:0]]];
                j++;
            }
            for(int i=0;i<array.count;i++)
            {
                NSString *space;
                if(i>=0 && i!=array.count-1) space=@"% ";
                else if(i==array.count-1) space=@""; else space=@" ";
                formatedString=[NSString stringWithFormat:@"%@%@%@",formatedString,[array objectAtIndex:i],space];
            }
            
            FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
            [arrayItems removeAllObjects];
            while([results next])
            {
                ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
                // //NSLog(@"id= %@",dataModel.dataID);
                [arrayItems addObject:dataModel];
            }
            
            if([searchHistory.searchType isEqualToString:@"notWildcard"]) [self filterResults:pattern];
        }
    }
    else
    {
        FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
        [arrayItems removeAllObjects];
        while([results next])
        {
            ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
            ////NSLog(@"id= %@",dataModel.dataID);
            [arrayItems addObject:dataModel];
        }
    }

    [database close];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    
    ISGSearchResultsViewController * searchResultsViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGSearchResultsViewController"];
   // ISGSearchResultsViewController *searchResultsViewController=[[ISGSearchResultsViewController alloc]initWithNibName:@"ISGSearchResultsViewController" bundle:Nil];
    searchResultsViewController.searchedString = searchHistory.searchText;
    searchResultsViewController.querry = searchHistory.searchQuery;
    searchResultsViewController.lang = searchHistory.searchLanguage;
    if(arrayFilter.count>0 || arrayItems.count>0)
    {
        if(arrayFilter.count>0)
        {
            NSComparisonResult (^sortBlock)(id, id) = ^(ISGDataModel *obj1, ISGDataModel *obj2) {
                if ([obj1.dataID intValue] > [obj2.dataID intValue]) {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                if ([obj1.dataID intValue] < [obj2.dataID intValue]) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                return (NSComparisonResult)NSOrderedSame;
            };
            NSArray *sorted = [arrayFilter sortedArrayUsingComparator:sortBlock];
            searchResultsViewController.arrayItems=sorted.mutableCopy;
            searchResultsViewController.lang=searchHistory.searchLanguage;
            [self.navigationController pushViewController:searchResultsViewController animated:YES];
        }
        else
        {
            searchResultsViewController.arrayItems=arrayItems;
            searchResultsViewController.lang=searchHistory.searchLanguage;
            [self.navigationController pushViewController:searchResultsViewController animated:YES];
        }
    }
}

-(void)getDataGurmukhi
{
    NSString *str=@"*";
    [database open];
    BOOL wildCard;
    
    if ([searchHistory.searchType isEqualToString:@"notWildcard"])
    {
        wildCard = NO;
    }
    else
    {
        wildCard = YES;
    }
    
    if([searchHistory.searchCategory isEqualToString:@"First Letter Anywhere"])
    {
        if ([stringInput componentsSeparatedByString:@" "].count > 1 && itration==0)
        {
            NSString *filterPattern=@"";
            NSString *querryPattern=@"";
            NSArray *arrayPattern=[stringInput componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            for (NSString *pattern in arrayPattern) {
                querryPattern=[NSString stringWithFormat:@"%@%@%@",querryPattern,pattern,str];
                filterPattern=[NSString stringWithFormat:@"%@%c",filterPattern,[pattern characterAtIndex:0]];
            }
            NSArray *ary=[stringInput componentsSeparatedByString:@" "];
            int wordLen=0;
            for (NSString *item in ary) {
                if(item.length>wordLen) wordLen=item.length;
            }
            if(wordLen==1)
            {
                stringInput=[stringInput stringByReplacingOccurrencesOfString:@" " withString:@""];
            }
            
            FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
            [arrayItems removeAllObjects];
            while([results next])
            {
                ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
                [arrayItems addObject:dataModel];
            }
            searchMoreData=YES;
            if(!wildCard) [self filterResultsWith:filterPattern];
        }
        else  if ([stringInput componentsSeparatedByString:@"*"].count > 1 && itration==0)
        {
            if(wildCard)
            {
                stringInput=[stringInput stringByReplacingOccurrencesOfString:@"*" withString:@"% "];
                FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
                [arrayItems removeAllObjects];
                while([results next])
                {
                    ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
                    [arrayItems addObject:dataModel];
                }
            }
        }
        else if(itration==0)
        {
            FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
            [arrayItems removeAllObjects];
            while([results next])
            {
                ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
                [arrayItems addObject:dataModel];
            }
        }
    }
    else if ([searchHistory.searchCategory isEqualToString:@"First Letter Beginning"])
    {
        
        NSString *type;
        int wordLenght=0;
        // //NSLog(@"txt len %d",txtSearch.text.length);
        NSString *checkedString;
        unichar last = [searchHistory.searchText characterAtIndex:[searchHistory.searchText length] - 1];
        if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:last]) {
            checkedString=[searchHistory.searchText substringToIndex:searchHistory.searchText.length-1];
        } else checkedString=searchHistory.searchText;
        // //NSLog(@"check len %d",checkedString.length);
        NSArray *words = [checkedString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        //NSLog(@"array words : %@",words);
        for (NSString *item in words) {
            //NSLog(@"len in loop :%d",item.length);
            if(item.length>wordLenght) wordLenght=item.length;
        }
        //NSLog(@"word len :%d",wordLenght);
        if(wordLenght==1) type=@"single"; else type=@"more";
        ////NSLog(@"type : %@",type);
        if([type isEqualToString:@"single"])
        {
            
            NSString *formatedString=[stringInput stringByReplacingOccurrencesOfString:@" " withString:@""];
            FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
            [arrayItems removeAllObjects];
            while([results next])
            {
                ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
                [arrayItems addObject:dataModel];
            }
            if(!wildCard)[self filterResults:formatedString];
        }
        else
        {
            NSString *pattern=@"";
            NSString *formatedString=@"";
            
            NSArray *array=[stringInput componentsSeparatedByString:@" "];
            int j=0;
            for (NSString *item in array) {
                pattern=[NSString stringWithFormat:@"%@%@",pattern,[NSString stringWithFormat:@"%c",[item characterAtIndex:0]]];
                j++;
            }
            for(int i=0;i<array.count;i++)
            {
                NSString *space;
                if(i>=0 && i!=array.count-1) space=@"* ";
                else if(i==array.count-1) space=@""; else space=@" ";
                formatedString=[NSString stringWithFormat:@"%@%@%@",formatedString,[array objectAtIndex:i],space];
            }
            
            FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
            [arrayItems removeAllObjects];
            while([results next])
            {
                ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
                [arrayItems addObject:dataModel];
            }
            if(!wildCard) [self filterResults:pattern];
        }
    }
    else
    {
        FMResultSet *results = [database executeQuery:searchHistory.searchQuery];
        [arrayItems removeAllObjects];
        while([results next])
        {
            ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
            [arrayItems addObject:dataModel];
        }
    }
    [database close];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    
    ISGSearchResultsViewController * searchResultsViewController  = [storyboard instantiateViewControllerWithIdentifier:@"ISGSearchResultsViewController"];
    //ISGSearchResultsViewController *searchResultsViewController=[[ISGSearchResultsViewController alloc]initWithNibName:@"ISGSearchResultsViewController" bundle:Nil];
    searchResultsViewController.searchedString = searchHistory.searchText;
    searchResultsViewController.querry = searchHistory.searchQuery;
    searchResultsViewController.lang = searchHistory.searchLanguage;
    if(arrayFilter.count>0 || arrayItems.count>0)
    {
        if(arrayFilter.count>0)
        {
            NSComparisonResult (^sortBlock)(id, id) = ^(ISGDataModel *obj1, ISGDataModel *obj2) {
                if ([obj1.dataID intValue] > [obj2.dataID intValue]) {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                if ([obj1.dataID intValue] < [obj2.dataID intValue]) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                return (NSComparisonResult)NSOrderedSame;
            };
            NSArray *sorted = [arrayFilter sortedArrayUsingComparator:sortBlock];
            searchResultsViewController.arrayItems=sorted.mutableCopy;
        }
        else
        {
            searchResultsViewController.arrayItems=arrayItems;
        }
        
    }
    searchResultsViewController.lang=searchHistory.searchLanguage;
    [self.navigationController pushViewController:searchResultsViewController animated:YES];
    
}

-(void)filterAdditionalResultsForGurmukhi:(NSString *)filter
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    int counter;
    //NSLog(@"filterAdditionalResultsForGurmukhi %@",filter);
    NSMutableArray *arrayComponent;
    for (ISGDataModel *data in arrayItems)
    {
        if(data.gurmukhi.length>0)
        {
            NSArray *arrayWords=[data.gurmukhi componentsSeparatedByString:@" "];
            //NSLog(@"array %@",arrayWords);
            arrayComponent=[[NSMutableArray alloc]init];
            counter=0;
            // //NSLog(@"words in array : %@",arrayWords);
            if(arrayWords.count>filter.length)
            {
                for(int i=0;i<filter.length;i++)
                {
                    [arrayComponent removeAllObjects];
                    NSString *arrayElement=[arrayWords objectAtIndex:i];
                    if(arrayElement.length>0)
                    {
                        [arrayComponent addObject:arrayElement];
                    }
                    else [arrayComponent addObject:@"xxxxxxxxx"];
                    //NSLog(@"word =%@",arrayComponent);
                    //NSLog(@"letter =%c",[filter characterAtIndex:i]);
                    NSPredicate *pred = [NSPredicate predicateWithFormat:@"self BEGINSWITH[c]  %@",[NSString stringWithFormat:@"%c",[filter characterAtIndex:i]]];
                    NSArray *arr = [arrayComponent filteredArrayUsingPredicate:pred];
                    if(arr.count>0)
                    {
                        counter++;
                    }
                    //NSLog(@"conter =%d",counter);
                }
                if(counter==filter.length)
                {
                    if(![arrayFilter containsObject:data])[arrayFilter addObject:data];
                }
            }
        }
    }
    //NSLog(@"arrayFilter filterAdditionalResults count = %d",arrayFilter.count);
    //NSLog(@"arrayResults filterAdditionalResults count = %d",arrayResults.count);
    if(arrayItems.count==0)//additional change in Roman
    {
        arrayItems=[[NSMutableArray alloc]initWithArray:arrayFilter];
        [self superFilter];
    }
    //NSLog(@"arrayResults count after chenage = %d",arrayResults.count);
}


-(void)filterAdditionalResults:(NSString *)filter
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    int counter;
    //NSLog(@"filterAdditionalResults %@",filter);
    NSMutableArray *arrayComponent;
    for (ISGDataModel *data in arrayItems)
    {
        if(data.roman.length>0)
        {
            NSArray *arrayWords=[data.roman componentsSeparatedByString:@" "];
            arrayComponent=[[NSMutableArray alloc]init];
            counter=0;
            // //NSLog(@"words in array : %@",arrayWords);
            if(arrayWords.count>filter.length)
            {
                for(int i=0;i<filter.length;i++)
                {
                    [arrayComponent removeAllObjects];
                    NSString *arrayElement=[arrayWords objectAtIndex:i];
                    if(arrayElement.length>0)
                    {
                        [arrayComponent addObject:arrayElement];
                    }
                    else [arrayComponent addObject:@"xxxxxxxxx"];
                    //NSLog(@"word =%@",arrayComponent);
                    //NSLog(@"letter =%c",[filter characterAtIndex:i]);
                    NSPredicate *pred = [NSPredicate predicateWithFormat:@"self BEGINSWITH[c]  %@",[NSString stringWithFormat:@"%c",[filter characterAtIndex:i]]];
                    NSArray *arr = [arrayComponent filteredArrayUsingPredicate:pred];
                    if(arr.count>0)
                    {
                        counter++;
                    }
                    //NSLog(@"conter =%d",counter);
                }
                if(counter==filter.length)
                {
                    [arrayFilter addObject:data];
                }
            }
        }
    }
    ////NSLog(@"arrayFilter filterAdditionalResults count = %d",arrayFilter.count);
    ////NSLog(@"arrayResults filterAdditionalResults count = %d",arrayResults.count);
    if(arrayItems.count==0)//additional change in Roman
    {
        arrayItems=[[NSMutableArray alloc]initWithArray:arrayFilter];
        [self superFilter];
    }
    ////NSLog(@"arrayResults count after chenage = %d",arrayResults.count);
    
    //    //NSLog(@"The Filtered Results are:");
    //    [arrayFilter enumerateObjectsUsingBlock:^(ISGDataModel * obj, NSUInteger idx, BOOL *stop) {
    //        //NSLog(@"%@",obj.roman);
    //    }];
    
}
-(void)superFilter
{
    NSString *filter=stringInput;
    //NSLog(@"%s and filfer %@",__func__,filter);
    [arrayFilter removeAllObjects];
    for (ISGDataModel *data in arrayItems)
    {
        if([stringLanguage isEqualToString:@"roman"])
        {
            if([data.walpha componentsSeparatedByString:filter].count>1){
                ////NSLog(@"walpha : %@",data.walpha);
                [arrayFilter addObject:data];
            }
        }
        else
        {
            if([data.wgamma componentsSeparatedByString:filter].count>1){
                ////NSLog(@"wgamma : %@",data.wgamma);
                [arrayFilter addObject:data];
            }
        }
    }
    ////NSLog(@"array superFilter %d",arrayFilter.count);
}

-(void)filterResults:(NSString *)filter
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    int counter;
    //NSLog(@"Filter %@",filter);
    if([filter isEqualToString:@"nana"]) return;// nana found exception. So added this
    arrayFilter=[[NSMutableArray alloc]init];
    NSMutableArray *arrayComponent;
    for (ISGDataModel *data in arrayItems)
    {
        if(data.roman.length>0)
        {
            NSArray *arrayWords=[data.roman componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            arrayComponent=[[NSMutableArray alloc]init];
            counter=0;
            for(int i=0;i<filter.length;i++)
            {
                
                [arrayComponent removeAllObjects];
                NSString *arrayElement=[arrayWords objectAtIndex:i];
                if(arrayElement.length>0)
                {
                    [arrayComponent addObject:arrayElement];
                }
                else [arrayComponent addObject:@"xxxxxxxxx"];
                //        //NSLog(@"word =%@",arrayComponent);
                //        //NSLog(@"letter =%c",[filter characterAtIndex:i]);
                NSPredicate *pred = [NSPredicate predicateWithFormat:@"self BEGINSWITH[c]  %@",[NSString stringWithFormat:@"%c",[filter characterAtIndex:i]]];
                NSArray *arr = [arrayComponent filteredArrayUsingPredicate:pred];
                if(arr.count>0)
                {
                    counter++;
                }
                // //NSLog(@"conter =%d",counter);
            }
            if(counter==filter.length)
            {
                [arrayFilter addObject:data];
            }
        }
    }
    //NSLog(@"arrayFilter filterResults count = %d",arrayFilter.count);
    //NSLog(@"arrayResults filterResults count = %d",arrayResults.count);
}
-(void)filterResultsWith:(NSString *)filter
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    //NSLog(@"filter == %@",filter);
    NSString *indexPattern;
    filterFlag=1;
    arrayFilter=[[NSMutableArray alloc]init];
    if([stringLanguage isEqualToString:@"roman"])
    {
        for (ISGDataModel *data in arrayItems)
        {
            if(searchFlag==0) indexPattern=data.walpha;
            else indexPattern=data.alpha;
            if([indexPattern componentsSeparatedByString:filter].count >1)
            {
                //NSLog(@"indexPattern roman :%@",indexPattern);
                [arrayFilter addObject:data];
            }
        }
    }
    else
    {
        for (ISGDataModel *data in arrayItems)
        {
            //NSLog(@"check :%@",data.gamma);
            indexPattern=data.gamma;
            if([indexPattern componentsSeparatedByString:filter].count >1)
            {
                [arrayFilter addObject:data];
            }
        }
    }
    if(searchMoreData && [stringLanguage isEqualToString:@"roman"])[self getDataByQuerryRoman];
}

-(void)getDataByQuerryRoman
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    NSString *str=@"% ";
    NSString *querryPattern=@"";
    NSString *newQuerry=@"";
    if([stringLanguage isEqualToString:@"roman"])stringInput=searchHistory.searchText;
    NSArray *arrayPattern=[stringInput componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    for (NSString *pattern in arrayPattern) {
        querryPattern=[NSString stringWithFormat:@"%@%@%@",querryPattern,pattern,str];
    }
    unichar last = [querryPattern characterAtIndex:[querryPattern length] - 1];
    if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:last]) {
        newQuerry=[querryPattern substringToIndex:[querryPattern length]-1];
    } else newQuerry=querryPattern;
    
    FMResultSet *results = [database executeQuery:[NSString stringWithFormat:@"%@ LIMIT 0,60", searchHistory.searchQueryWildcard]];
    [arrayItems removeAllObjects];
    while([results next])
    {
        ISGDataModel * dataModel=[[ISGDataModel alloc]initWithResult:results];
        [arrayItems addObject:dataModel];
    }
    
    NSString *pattern=@"";
    if([stringLanguage isEqualToString:@"roman"])
    {
        NSString *checkedString;
        unichar lastLetter = [stringInput characterAtIndex:[stringInput length] - 1];
        if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:lastLetter]) {
            checkedString=[stringInput substringToIndex:stringInput.length-1];
        } else checkedString=stringInput;
        
        NSArray *array=[checkedString componentsSeparatedByString:@" "];
        int j=0;
        for (NSString *item in array) {
            pattern=[NSString stringWithFormat:@"%@%@",pattern,[NSString stringWithFormat:@"%c",[item characterAtIndex:0]]];
            j++;
        }
    }
    else
    {
        pattern=[newQuerry stringByReplacingOccurrencesOfString:@"%" withString:@""];
    }
    
    [self filterAdditionalResults:pattern];
}




@end
