//
//  ISGCustomAlertViewController.m
//  iSearchGujral
//
//  Created by XLR8 on 23/10/17.
//  Copyright © 2017 sics. All rights reserved.
//

#import "ISGCustomAlertViewController.h"
#import "ISGBaniDetailViewController.h"
#import "KGModal.h"
#import "BookMarkFolders.h"
#import "ISGDataModel.h"
#import "BookMarkPages.h"
#import "HMSideMenu.h"
#import <Twitter/Twitter.h>
#import "ISGTabViewController.h"
#import "MBProgressHUD.h"
#import "ScrollViewToPDF.h"
#import "ISGBaniDetailViewController.h"
#import "ScrollViewToPDF.h"

@interface ISGCustomAlertViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UIPrintInteractionControllerDelegate,UIAlertViewDelegate,MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *bckGroundImg;
@property (strong, nonatomic) IBOutlet UIView *msgPopUp;
@property (strong, nonatomic) IBOutlet UIButton *cancel;
@property (strong, nonatomic) IBOutlet UIButton *print;
@property (strong, nonatomic) IBOutlet UIButton *share;
@property (strong, nonatomic) IBOutlet UIButton *addTofav;
@property (strong, nonatomic) IBOutlet UIImageView *imgaddTofav;
@property (strong, nonatomic) IBOutlet UIImageView *imgshare;
@property (strong, nonatomic) IBOutlet UIImageView *imgprint;
@property (strong, nonatomic) IBOutlet UIImageView *imgcancel;

@end

@implementation ISGCustomAlertViewController
@synthesize bani,arrayData,arrayIndex,strFavour,arrayBaani,key,rootClass;
@synthesize pageNo,scriptID,cellImage,query,heading,imageData,imageSMS;

NSArray * arrayFolders;
NSMutableArray * arrayNames;
UITableView * TableList;
NSMutableArray *arrayItems,*cells;
BookMarkPages * bookMarkPage;
HMSideMenu *sideMenu;
UIImage *attachmentImage;
MBProgressHUD *HUD;
UIPrintInteractionController * printController;
FMDatabase *database;
NSString *message;
NSString *title;
CGFloat *height;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.5];
    [self setSliderMenu];
    NSManagedObjectContext * managedObjectContext = ((ISGAppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    bookMarkPage = [[BookMarkPages alloc] initWithEntity:[NSEntityDescription entityForName:@"BookMarkPages" inManagedObjectContext:managedObjectContext] insertIntoManagedObjectContext:managedObjectContext];
    bookMarkPage.pageNo = pageNo;
    bookMarkPage.scriptID = scriptID;
    bookMarkPage.cellImage = cellImage;
    bookMarkPage.query = query;
    bookMarkPage.title = heading;
    TableList.separatorColor = [UIColor clearColor];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}


- (IBAction)btnActionClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];

}
- (IBAction)btnActionCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];

}
- (IBAction)btnActionAddToFav:(id)sender
{
    UIAlertView *alertBookmark = [[UIAlertView alloc] initWithTitle:@"Are you sure to add this to your favourites?"
                                                            message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
    
    UITextField *txtAlert = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 80.0, 260.0, 30.0)];
    [txtAlert setBackgroundColor:[UIColor whiteColor]];
    //  txtAlert.text=sggsData;
    txtAlert.font=[UIFont fontWithName:@"GurbaniLipiBold" size:22];
    txtAlert.userInteractionEnabled=NO;
    // [alertBookmark addSubview:txtAlert];
    [alertBookmark show];
    alertBookmark.tag=2;

}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 2 && buttonIndex == 0) {
       // [self dismissViewControllerAnimated:YES completion:nil];
        [self.bckGroundImg setHidden:true];
        self.cancel.hidden = YES;
        self.print.hidden = YES;
        self.share.hidden = YES;
        self.addTofav.hidden = YES;
        self.imgaddTofav.hidden = YES;
        self.imgshare.hidden = YES;
        self.imgprint.hidden = YES;
        self.imgcancel.hidden = YES;
        
        

        [self showFolderList];
        
    }
    else if (alertView.tag == 100 && buttonIndex == 0) {
         [self dismissViewControllerAnimated:YES completion:nil];
        
        
    }

    
    else if (alertView.tag == 100 && buttonIndex == 1)
    {
        [self dismissViewControllerAnimated:YES completion:nil];

        UITextField * textFiled = [alertView textFieldAtIndex:0];
        if (textFiled.text.length == 0)
        {
            [[[UIAlertView alloc] initWithTitle:@"Invalid text" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        
        }
        else
        {
          if (UIScreen.mainScreen.bounds.size.height == 736)
          {
            if (textFiled.text.length > 8)
            {
               [[[UIAlertView alloc] initWithTitle:@"Folder name should be less than 8 characters" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
            }
            else
            {
                [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
                [self showFolderList];
            }
          }
          else if ((UIScreen.mainScreen.bounds.size.height == 667))
          {
              if (textFiled.text.length > 7)
              {
                  [[[UIAlertView alloc] initWithTitle:@"Folder name should be less than 7 characters" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
              }
              else
              {
                  [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
                  [self showFolderList];
              }
  
          }
          else if ((UIScreen.mainScreen.bounds.size.height == 568))
          {
              if (textFiled.text.length > 6)
              {
                  [[[UIAlertView alloc] initWithTitle:@"Folder name should be less than 6 characters" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
              }
              else
              {
                  [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
                  [self showFolderList];
              }
              
          }
          else if ((UIScreen.mainScreen.bounds.size.height == 1024))
          {
              if (textFiled.text.length > 15)
              {
                  [[[UIAlertView alloc] initWithTitle:@"Folder name should be less than 15 characters" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
              }
              else
              {
                  [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
                  [self showFolderList];
              }
              
          }
          else
          {
              [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
              [self showFolderList];
          }
//            [BookMarkFolders addBookMarkFolderWithName:textFiled.text index:(int)arrayNames.count];
//            [self showFolderList];
        }
    }
    else if (alertView.tag == 100 && buttonIndex == 0)
    {
        [alertView dismissWithClickedButtonIndex:0 animated:true];
    }
    }



-(void) showFolderList
{
//    self.cancel.hidden = NO;
//    self.print.hidden = NO;
//    self.share.hidden = NO;
//    self.addTofav.hidden = NO;
//    self.imgaddTofav.hidden = NO;
//    self.imgshare.hidden = NO;
//    self.imgprint.hidden = NO;
//    self.imgcancel.hidden = NO;
//    self.bckGroundImg.hidden = NO;

    arrayFolders = [BookMarkFolders fetchAllBookMarkFolders];
    [arrayNames removeAllObjects];
    arrayNames = [[NSMutableArray alloc] init];

    for (BookMarkFolders * obj in arrayFolders) {
                  [arrayNames addObject:obj.folderName];
                 }
    if (arrayNames.count == 0)
    {
        [self createFolder];
        
    }
    else
    {
       // [self.bckGroundImg setHidden:true];
        UIView * kgView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, UIScreen.mainScreen.bounds.size.width - 20, UIScreen.mainScreen.bounds.size.height/2)];
        UIImageView * kgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 40, UIScreen.mainScreen.bounds.size.width - 20, UIScreen.mainScreen.bounds.size.height/2)];
        
        [kgImageView setImage:[UIImage imageNamed:@"alertBg"]];
        if (UIScreen.mainScreen.bounds.size.height == 1024)
        {
        TableList = [[UITableView alloc] initWithFrame:CGRectMake(kgImageView.frame.origin.x + 70, kgImageView.frame.origin.y + 50,  kgImageView.bounds.size.width - 150, 350) style:UITableViewStylePlain];
        }
        else if (UIScreen.mainScreen.bounds.size.height == 736)
        {
            TableList = [[UITableView alloc] initWithFrame:CGRectMake(kgImageView.frame.origin.x + 70, kgImageView.frame.origin.y + 50,  kgImageView.bounds.size.width - 150, 240) style:UITableViewStylePlain];
        }
        else if (UIScreen.mainScreen.bounds.size.height == 667)
        {
             TableList = [[UITableView alloc] initWithFrame:CGRectMake(kgImageView.frame.origin.x + 70, kgImageView.frame.origin.y + 50,  kgImageView.bounds.size.width - 150, 200) style:UITableViewStylePlain];
        }
        else if (UIScreen.mainScreen.bounds.size.height == 568)
        {
            TableList = [[UITableView alloc] initWithFrame:CGRectMake(kgImageView.frame.origin.x + 70, kgImageView.frame.origin.y + 50,  kgImageView.bounds.size.width - 150, 155) style:UITableViewStylePlain];
        }

        else
        {
        TableList = [[UITableView alloc] initWithFrame:CGRectMake(kgImageView.frame.origin.x + 70, kgImageView.frame.origin.y + 50,  kgImageView.bounds.size.width - 150, 200) style:UITableViewStylePlain];
        }
       
        TableList.delegate = self;
        TableList.dataSource = self;
        TableList.backgroundColor = [UIColor clearColor];
        TableList.separatorColor = [UIColor clearColor];
        
        
        
        

        [kgView addSubview:kgImageView];
        [kgView addSubview:TableList];

        //Change button Y = TableList.frame.origin.y +  TableList.bounds.size.height - 40
        
        if (UIScreen.mainScreen.bounds.size.height == 736)
        {
            UIButton * buttonCreateFolder = [[UIButton alloc] initWithFrame:CGRectMake(TableList.frame.origin.x + 20, kgView.frame.size.height - 40, TableList.bounds.size.width - 30, 40)];
            [buttonCreateFolder setTitle:@"CREATE FOLDER" forState:UIControlStateNormal];
            buttonCreateFolder.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [buttonCreateFolder setBackgroundImage:[UIImage imageNamed:@"alertCancelBtnBg"] forState:(UIControlStateNormal)];
            [buttonCreateFolder setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            //[buttonCreateFolder setBackgroundColor:[UIColor whiteColor]];
            [buttonCreateFolder addTarget:self action:@selector(createFolder) forControlEvents:UIControlEventTouchUpInside];
            [kgView addSubview:buttonCreateFolder];
            

        UIButton * buttonClose = [[UIButton alloc] initWithFrame:CGRectMake(280,50,15,17)];
        [buttonClose setBackgroundImage:[UIImage imageNamed:@"closeBtn"] forState:(UIControlStateNormal)];
        [buttonClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //[buttonCreateFolder setBackgroundColor:[UIColor whiteColor]];
        [buttonClose addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
        [kgView addSubview:buttonClose];
        [kgImageView bringSubviewToFront:buttonClose];

        }
        else if (UIScreen.mainScreen.bounds.size.height == 667)
        {
            UIButton * buttonCreateFolder = [[UIButton alloc] initWithFrame:CGRectMake(TableList.frame.origin.x + 20, kgView.frame.size.height - 40, TableList.bounds.size.width - 30, 40)];
            [buttonCreateFolder setTitle:@"CREATE FOLDER" forState:UIControlStateNormal];
            buttonCreateFolder.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [buttonCreateFolder setBackgroundImage:[UIImage imageNamed:@"alertCancelBtnBg"] forState:(UIControlStateNormal)];
            [buttonCreateFolder setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            //[buttonCreateFolder setBackgroundColor:[UIColor whiteColor]];
            [buttonCreateFolder addTarget:self action:@selector(createFolder) forControlEvents:UIControlEventTouchUpInside];
            [kgView addSubview:buttonCreateFolder];
            

            UIButton * buttonClose = [[UIButton alloc] initWithFrame:CGRectMake(250,50,15,17)];
            [buttonClose setBackgroundImage:[UIImage imageNamed:@"closeBtn"] forState:(UIControlStateNormal)];
            [buttonClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            //[buttonCreateFolder setBackgroundColor:[UIColor whiteColor]];
            [buttonClose addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
            [kgView addSubview:buttonClose];
            [kgImageView bringSubviewToFront:buttonClose];
  
        }

        else if(UIScreen.mainScreen.bounds.size.height==1024.000000)
        {
            UIButton * buttonCreateFolder = [[UIButton alloc] initWithFrame:CGRectMake(TableList.frame.origin.x + 100, kgView.frame.size.height - 75, TableList.bounds.size.width - 190, 55)];
            [buttonCreateFolder setTitle:@"CREATE FOLDER" forState:UIControlStateNormal];
            buttonCreateFolder.titleLabel.font = [UIFont systemFontOfSize:15.0];
            [buttonCreateFolder setBackgroundImage:[UIImage imageNamed:@"alertCancelBtnBg"] forState:(UIControlStateNormal)];
            [buttonCreateFolder setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            //[buttonCreateFolder setBackgroundColor:[UIColor whiteColor]];
            [buttonCreateFolder addTarget:self action:@selector(createFolder) forControlEvents:UIControlEventTouchUpInside];
            [kgView addSubview:buttonCreateFolder];
            

            UIButton * buttonClose = [[UIButton alloc] initWithFrame:CGRectMake(570,55,18,20)];
            [buttonClose setBackgroundImage:[UIImage imageNamed:@"closeBtn"] forState:(UIControlStateNormal)];
            [buttonClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            //[buttonCreateFolder setBackgroundColor:[UIColor whiteColor]];
            [buttonClose addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
            [kgView addSubview:buttonClose];
            [kgImageView bringSubviewToFront:buttonClose];

        }
        else
        {
            UIButton * buttonCreateFolder = [[UIButton alloc] initWithFrame:CGRectMake(TableList.frame.origin.x + 20, kgView.frame.size.height - 40, TableList.bounds.size.width - 30, 40)];
            [buttonCreateFolder setTitle:@"CREATE FOLDER" forState:UIControlStateNormal];
            buttonCreateFolder.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [buttonCreateFolder setBackgroundImage:[UIImage imageNamed:@"alertCancelBtnBg"] forState:(UIControlStateNormal)];
            [buttonCreateFolder setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            //[buttonCreateFolder setBackgroundColor:[UIColor whiteColor]];
            [buttonCreateFolder addTarget:self action:@selector(createFolder) forControlEvents:UIControlEventTouchUpInside];
            [kgView addSubview:buttonCreateFolder];
            

            
            UIButton * buttonClose = [[UIButton alloc] initWithFrame:CGRectMake(200,50,15,17)];
            [buttonClose setBackgroundImage:[UIImage imageNamed:@"closeBtn"] forState:(UIControlStateNormal)];
            [buttonClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            //[buttonCreateFolder setBackgroundColor:[UIColor whiteColor]];
            [buttonClose addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
            [kgView addSubview:buttonClose];
            [kgImageView bringSubviewToFront:buttonClose];

        }
        
        
        
        [[KGModal sharedInstance] showWithContentView:kgView andAnimated:YES];
    }
}

-(void)close{
    [self.bckGroundImg setHidden:false];
    self.cancel.hidden = NO;
    self.print.hidden = NO;
    self.share.hidden = NO;
    self.addTofav.hidden = NO;
    self.imgaddTofav.hidden = NO;
    self.imgshare.hidden = NO;
    self.imgprint.hidden = NO;
    self.imgcancel.hidden = NO;
    


    [[KGModal sharedInstance] hideAnimated:YES];

}

-(void)createFolder
{
    
    
    [[KGModal sharedInstance] hideAnimated:YES];
    //[self.bckGroundImg setHidden:false];
    

    if (arrayNames.count==0)
    {
        message = @"No BookMark Folders";
        title = @"Alert!";
    }
    else
    {
        message = @"Create your folder";
        title = @"ISearchGurbani";
    }
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Create", nil];
    alert.tag = 100;
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
    }



#pragma mark tableview delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == TableList)
    {
        return arrayNames.count;
    }
    else
    {
        [tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        return [cells count];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ((tableView == TableList) && (UIScreen.mainScreen.bounds.size.height==1024.000000))
    {
        return 75;
    }
    else if (tableView == TableList)
    {
        return 40;
    }
    else {
     /*   ISGCell * cell = cells[indexPath.row];
        NSLog(@"cell:%f",cell.labelHeight);
        [cell layoutSubviews];
        
        
        return  cell.labelHeight + 20;*/
        
        return [[cells objectAtIndex:indexPath.row] cellHeight];

    }
}
-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == TableList)
    {
        UITableViewCell * cellList = [[UITableViewCell alloc] initWithStyle:UITableViewStylePlain reuseIdentifier:@"myCell"];
        cellList.backgroundColor = [UIColor clearColor];
        cellList.userInteractionEnabled = true;
        cellList.selectionStyle = UITableViewCellSelectionStyleNone;
        

        if (cellList == nil)
        {
            cellList = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
        }
        if (UIScreen.mainScreen.bounds.size.height==1024.000000)
        {
            UIImageView * lblFolderImg = [[UIImageView alloc] initWithFrame:CGRectMake(cellList.bounds.origin.x + 100,cellList.bounds.origin.y + 8 ,cellList.bounds.size.width,72)];
            lblFolderImg.image = [UIImage imageNamed:@"selestfolderBg"];
            
            UILabel * lblFolderName = [[UILabel alloc] initWithFrame:CGRectMake(cellList.bounds.origin.x + 95,cellList.bounds.origin.y + 15 ,cellList.bounds.size.width - 30,cellList.bounds.size.height - 10)];
            lblFolderName.text = arrayNames[indexPath.row];
            lblFolderName.textColor = [UIColor whiteColor];
            [lblFolderName adjustsFontSizeToFitWidth];
            
            [cellList addSubview:lblFolderImg];
            [lblFolderImg addSubview:lblFolderName];
            [lblFolderImg bringSubviewToFront:lblFolderName];
  
        }
        else if (UIScreen.mainScreen.bounds.size.height == 736)
        {
            UIImageView * lblFolderImg = [[UIImageView alloc] initWithFrame:CGRectMake(cellList.bounds.origin.x + 20,cellList.bounds.origin.y + 5 ,cellList.bounds.size.width - 130,cellList.bounds.size.height)];
            lblFolderImg.image = [UIImage imageNamed:@"selestfolderBg"];
            
            UILabel * lblFolderName = [[UILabel alloc] initWithFrame:CGRectMake(cellList.bounds.origin.x + 60,cellList.bounds.origin.y + 5 ,120,cellList.bounds.size.height - 10)];
            lblFolderName.text = arrayNames[indexPath.row];
            lblFolderName.textColor = [UIColor whiteColor];
            [lblFolderName setFont:[UIFont systemFontOfSize:13]];

            
            [cellList addSubview:lblFolderImg];
            [lblFolderImg addSubview:lblFolderName];
            [lblFolderImg bringSubviewToFront:lblFolderName];

 
        }
        else if (UIScreen.mainScreen.bounds.size.height == 667)
        {
            UIImageView * lblFolderImg = [[UIImageView alloc] initWithFrame:CGRectMake(cellList.bounds.origin.x + 8,cellList.bounds.origin.y + 5 ,cellList.bounds.size.width - 130,cellList.bounds.size.height)];
            lblFolderImg.image = [UIImage imageNamed:@"selestfolderBg"];
            
            UILabel * lblFolderName = [[UILabel alloc] initWithFrame:CGRectMake(cellList.bounds.origin.x + 60,cellList.bounds.origin.y + 5 ,90/*30*/,cellList.bounds.size.height - 10)];
            lblFolderName.text = arrayNames[indexPath.row];
            lblFolderName.textColor = [UIColor whiteColor];
            [lblFolderName setFont:[UIFont systemFontOfSize:13]];

            
            [cellList addSubview:lblFolderImg];
            [lblFolderImg addSubview:lblFolderName];
            [lblFolderImg bringSubviewToFront:lblFolderName];

            
        }
        else if (UIScreen.mainScreen.bounds.size.height == 568)
        {
            UIImageView * lblFolderImg = [[UIImageView alloc] initWithFrame:CGRectMake(cellList.bounds.origin.x + 5,cellList.bounds.origin.y + 5 ,150,cellList.bounds.size.height - 5)];
            lblFolderImg.image = [UIImage imageNamed:@"selestfolderBg"];
            
            UILabel * lblFolderName = [[UILabel alloc] initWithFrame:CGRectMake(cellList.bounds.origin.x + 45,cellList.bounds.origin.y + 2 ,100,cellList.bounds.size.height - 10)];
            lblFolderName.text = arrayNames[indexPath.row];
            lblFolderName.textColor = [UIColor whiteColor];
            [lblFolderName setFont:[UIFont systemFontOfSize:12]];
            [cellList addSubview:lblFolderImg];
            [lblFolderImg addSubview:lblFolderName];
            [lblFolderImg bringSubviewToFront:lblFolderName];
            
            
        }

        else
        {
            UIImageView * lblFolderImg = [[UIImageView alloc] initWithFrame:CGRectMake(cellList.bounds.origin.x + 5,cellList.bounds.origin.y + 5 ,cellList.bounds.size.width - 100,cellList.bounds.size.height)];
            lblFolderImg.image = [UIImage imageNamed:@"selestfolderBg"];
            
            UILabel * lblFolderName = [[UILabel alloc] initWithFrame:CGRectMake(cellList.bounds.origin.x + 60,cellList.bounds.origin.y + 5 ,cellList.bounds.size.width - 30,cellList.bounds.size.height - 10)];
            lblFolderName.text = arrayNames[indexPath.row];
            lblFolderName.textColor = [UIColor whiteColor];
            
            
            [cellList addSubview:lblFolderImg];
            [lblFolderImg addSubview:lblFolderName];
            [lblFolderImg bringSubviewToFront:lblFolderName];
  
        }

        
        
      
       
        return cellList;
    }
    else
    {
        ISGDataModel *dataModel=arrayItems[indexPath.row];
        //[self setNavTitle:dataModel.script forPage:dataModel.page];
        return [cells objectAtIndex:indexPath.row];
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == TableList)
    {
        BookMarkFolders * fol = arrayFolders[indexPath.row];
        NSSet * set = fol.relationshipToPages;
        NSArray * array = [NSArray arrayWithArray:set.allObjects];
        int cnt = 0;
        bookMarkPage.index = [NSString stringWithFormat:@"%lu",(unsigned long)array.count];
        for (BookMarkPages * obj in array)
        {
            if ([bookMarkPage.pageNo isEqualToString:obj.pageNo] && [bookMarkPage.scriptID isEqualToString:obj.scriptID] && [bookMarkPage.cellImage isEqualToData:obj.cellImage] && [bookMarkPage.query isEqualToString:obj.query])
            {
                cnt++;
            }
        }
        if (cnt>0)
        {
            [SVProgressHUD showErrorWithStatus:@"Already added to the list"];
        }
        else
        {
            [BookMarkPages addBookMarkPage:bookMarkPage inBookMarkFolder:fol];
            [SVProgressHUD showSuccessWithStatus:@"Added"];
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
        [[KGModal sharedInstance] hideAnimated:YES];

    }
}


-(void)insertBookmark
{
    [database open];
   // NSLog(@"sggs data :%@",sggsData);
   // NSLog(@"bani :%@",bani);
    if([rootClass isEqualToString:@"ISGBaniViewController"])
    {
        [database executeUpdate:@"INSERT INTO bookmark (markText,scrpt,indexValue,key,tab) VALUES (?,?,?,?,'Baani2');",sggsData,bani,[NSString stringWithFormat:@"%d",arrayIndex],key];
    }
    else
        
    {
        [database executeUpdate:@"INSERT INTO bookmark (markText,scrpt,indexValue,key,tab) VALUES (?,?,?,?,'Baani1');",sggsData,bani,[NSString stringWithFormat:@"%d",arrayIndex],key];
    }
    //    NSLog(@"%@",[database lastErrorMessage]);
    [database close];
    [SVProgressHUD showSuccessWithStatus:@"Bookmarked successfully"];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"favourites" object:Nil];
}



- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath;
}

- (float)cellHeight {
    return self.view.frame.size.height+5;
}


- (IBAction)btnActionShare:(id)sender
{
    [sideMenu open];
}


- (IBAction)btnActionPrint:(id)sender
{
    NSData *imageDataToPrint = imageData;
    [self printItem:imageDataToPrint];

}

-(void)setSliderMenu
{
    int xx = 0;
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) xx=20;
    
    UIView *WhatsAppItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [WhatsAppItem setMenuActionWithBlock:^{
        [self whatsAppShare];
        [sideMenu close];
    }];
    UIImageView *WhatsAppIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [WhatsAppIcon setImage:[UIImage imageNamed:@"WPButton.png"]];
    [WhatsAppItem addSubview:WhatsAppIcon];
    

    UIView *twitterItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [twitterItem setMenuActionWithBlock:^{
        [self twitterShare];
        [sideMenu close];
    }];
    UIImageView *twitterIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [twitterIcon setImage:[UIImage imageNamed:@"tw.png"]];
    [twitterItem addSubview:twitterIcon];
    
    
    
    UIView *emailItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [emailItem setMenuActionWithBlock:^{
        [self emailShare];
        [sideMenu close];
    }];
    UIImageView *emailIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx , 40+xx)];
    [emailIcon setImage:[UIImage imageNamed:@"mail.png"]];
    [emailItem addSubview:emailIcon];
    
    UIView *facebookItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [facebookItem setMenuActionWithBlock:^{
        [self ShareFacebook];
        [sideMenu close];
    }];
    UIImageView *facebookIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 36+xx)];
    [facebookIcon setImage:[UIImage imageNamed:@"fb.png"]];
    [facebookItem addSubview:facebookIcon];
    
    UIView *browserItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [browserItem setMenuActionWithBlock:^{
        [sideMenu close];
    }];
    
    
    UIImageView *browserIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 40+xx)];
    [browserIcon setImage:[UIImage imageNamed:@"closeButton.png"]];
    [browserItem addSubview:browserIcon];
    
    UIView *messageItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 37+xx)];
    [messageItem setMenuActionWithBlock:^{
        NSLog(@"mms item");
        [self sendMms];
        [sideMenu close];
    }];
    UIImageView *messageIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40+xx, 36+xx)];
    [messageIcon setImage:[UIImage imageNamed:@"mmsButton.png"]];
    [messageItem addSubview:messageIcon];
    
    sideMenu = [[HMSideMenu alloc] initWithItems:@[twitterItem, WhatsAppItem, emailItem, facebookItem ,messageItem,browserItem]];
    [sideMenu setItemSpacing:5.0f];
    [self.view addSubview:sideMenu];
}

-(void)sendMms
{
    
   /* UIView * msgPopUp = [[UIView alloc] initWithFrame:CGRectMake(30,30,UIScreen.mainScreen.bounds.size.width - 60, UIScreen.mainScreen.bounds.size.height - 150)];
    UIImageView * msgPopUpImage = [[UIImageView alloc] initWithFrame:CGRectMake(30,30,UIScreen.mainScreen.bounds.size.width - 100, UIScreen.mainScreen.bounds.size.height - 150)];
    msgPopUpImage.image = [UIImage imageNamed:@"mms screenshot copy.png"];

    [msgPopUp addSubview:msgPopUpImage];
    UIButton * btnProceed = [[UIButton alloc] initWithFrame:CGRectMake(30,100,100,50)];
    [btnProceed setTitle:@"Proceed" forState:UIControlStateNormal];
    btnProceed.titleLabel.font = [UIFont systemFontOfSize:10.0];
    [btnProceed setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnProceed setBackgroundColor:[UIColor whiteColor]];
    [btnProceed addTarget:self action:@selector(btnProceed) forControlEvents:UIControlEventTouchUpInside];
    [msgPopUpImage addSubview:btnProceed];
    [msgPopUpImage bringSubviewToFront:btnProceed];*/

    
    

    
    [[KGModal sharedInstance]showWithContentView:_msgPopUp andAnimated:YES];
}

- (IBAction)btnActionproceed:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.persistent = YES;
    pasteboard.image = imageSMS;
    NSString *phoneToCall = @"sms:";
    NSString *phoneToCallEncoded = [phoneToCall stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:phoneToCallEncoded];
    [[UIApplication sharedApplication] openURL:url];
 
}
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}



-(void)twitterShare
{
    
        UIImage *shareImg = imageSMS;

        NSArray *objectsToShare = @[shareImg];
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo,UIActivityTypePostToFacebook,UIActivityTypeMail,UIActivityTypeMessage];
        
        activityVC.excludedActivityTypes = excludeActivities;
        activityVC.popoverPresentationController.sourceView = self.view;
        activityVC.popoverPresentationController.sourceRect = CGRectMake(0, (UIScreen.mainScreen.bounds.size.height)/2 - 10, (UIScreen.mainScreen.bounds.size.width), (UIScreen.mainScreen.bounds.size.height)/2);

        [self presentViewController:activityVC animated:YES completion:nil];
            

        [activityVC setCompletionHandler:^(NSString *act, BOOL done)
         {
             
             NSLog(@"act type %@",act);
             NSString *ServiceMsg = nil;
             if ( [act isEqualToString:UIActivityTypePostToTwitter] )  ServiceMsg = @"Post on twitter, ok!";
             
             if ( done )
             {
                 UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:ServiceMsg message:@"The Tweet was posted successfully." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
                 [Alert show];
                 // [Alert release];
             }
             else
             {
             }
         }];
 
    
    
  }



-(void)whatsAppShare
{
    
    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]])
    {
        UIImage     * iconImage = imageSMS;
        NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"/Documents/whatsAppTmp.wai"];
        
        [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
        
        _documentationInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
        _documentationInteractionController.UTI = @"net.whatsapp.image";
        _documentationInteractionController.delegate = self;
        
        [_documentationInteractionController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
        
        
        
    } else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Please install whatsapp and try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

}


- (void)ShareFacebook
{

    UIImage *shareImg = imageSMS;
    
    NSArray *objectsToShare = @[shareImg];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo,UIActivityTypePostToTwitter,UIActivityTypeMail,UIActivityTypeMessage];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    activityVC.popoverPresentationController.sourceView = self.view;
    activityVC.popoverPresentationController.sourceRect = CGRectMake(0, (UIScreen.mainScreen.bounds.size.height)/2, (UIScreen.mainScreen.bounds.size.width), (UIScreen.mainScreen.bounds.size.height)/2);
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


-(void)emailShare
{
    if([MFMailComposeViewController canSendMail])
    {  dispatch_async(dispatch_get_main_queue(), ^{

        MFMailComposeViewController *mailCompose = [[MFMailComposeViewController alloc] init];
        mailCompose.mailComposeDelegate = self;
        NSData *data = [NSData dataWithData:UIImageJPEGRepresentation(imageSMS, 1.0)];
        [mailCompose setSubject:@"iSearchGurbani share"];
        [mailCompose addAttachmentData:data
                              mimeType:@"jpg"
                              fileName:@"iSearchGurbani.jpg"];
        [self presentViewController:mailCompose animated:YES completion:Nil];
         });
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"Please configure a mail account in your device and try again later" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    NSString *message=@"Your email has been discarded";
    switch (result) {
            
        case MFMailComposeResultFailed:
            message=@"Your email has failed to send";
            break;
            
        case MFMailComposeResultSent :
            message=@"Your email has sent Successfully";
            break;
            
        case MFMailComposeResultCancelled :
            NSLog(@"cancelled");
            break;
            
        case MFMailComposeResultSaved :
            message=@"Your email has been saved to draft";
            break;
        default:
            break;
    }
    dispatch_async(dispatch_get_main_queue(), ^{

    [[[UIAlertView alloc]initWithTitle:@"Mail Status" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];        
   
        });
     [controller dismissViewControllerAnimated:YES completion:Nil];
}

-(void)printItem :(NSData*)data
{
    printController = [UIPrintInteractionController sharedPrintController];
    if(printController && [UIPrintInteractionController canPrintData:data])
    {
        printController.delegate = self;
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [NSString stringWithFormat:@""];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        printController.printInfo = printInfo;
        printController.showsPageRange = YES;
        printController.printingItem = data;
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error)
            {
                //NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
            }
        };
        //[printController presentFromBarButtonItem:self.item animated:YES completionHandler:completionHandler];
        //[printController presentAnimated:YES completionHandler:completionHandler];
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            [printController presentFromRect:CGRectMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2, 0, 0) inView:self.view animated:YES completionHandler:completionHandler];
        }
        else
        {
            [printController presentAnimated:YES completionHandler:completionHandler];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
